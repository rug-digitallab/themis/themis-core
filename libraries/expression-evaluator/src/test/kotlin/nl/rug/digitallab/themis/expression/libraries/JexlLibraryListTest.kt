package nl.rug.digitallab.themis.expression.libraries

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.expression.EmptyTestContext
import nl.rug.digitallab.themis.expression.ExpressionEngine
import nl.rug.digitallab.themis.expression.exceptions.EvaluatorException
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.math.BigDecimal
import kotlin.test.assertEquals

@QuarkusTest
class JexlLibraryListTest {
    @Inject
    private lateinit var engine: ExpressionEngine

    @Test
    fun `A list of BigDecimals can be created by the jexl library`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<List<BigDecimal>>("list(1, 2, 3)")

        assertEquals(BigDecimal(1), result[0])
        assertEquals(BigDecimal(2), result[1])
        assertEquals(BigDecimal(3), result[2])
    }

    @Test
    fun `The evaluator has access to the sum function in the jexl library for integers`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<Int>("sum(list(1, 2, 3))")

        assertEquals(6, result)
    }

    @Test
    fun `The evaluator has access to the sum function in the jexl library for doubles`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<Double>("sum(list(1.0, 2.0, 3.0))")

        assertEquals(6.0, result)
    }

    @Test
    fun `The sum function can deal with empty lists`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<Int>("sum(list())")

        assertEquals(0, result)
    }

    @Test
    fun `The evaluator has access to the min function in the jexl library for integers`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<Int>("min(list(1, 2, 3))")

        assertEquals(1, result)
    }

    @Test
    fun `The evaluator has access to the min function in the jexl library for doubles`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<Double>("min(list(1.0, 2.0, 3.0))")

        assertEquals(1.0, result)
    }

    @Test
    fun `The min function throws an error on empty lists`() {
        assertThrows<EvaluatorException> {
            engine
                .evaluator(EmptyTestContext())
                .evaluate<Int>("min(list())")
        }
    }

    @Test
    fun `The evaluator has access to the max function in the jexl library for integers`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<Int>("max(list(1, 2, 3))")

        assertEquals(3, result)
    }

    @Test
    fun `The max function throws an error on empty lists`() {
        assertThrows<EvaluatorException> {
            engine
                .evaluator(EmptyTestContext())
                .evaluate<Int>("max(list())")
        }
    }

    @Test
    fun `The evaluator has access to the max function in the jexl library for doubles`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<Double>("max(list(1.0, 2.0, 3.0))")

        assertEquals(3.0, result)
    }

    @Test
    fun `The evaluator has access to the average function in the jexl library for integers`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<Int>("average(list(1, 2, 6))")

        assertEquals(3, result)
    }

    @Test
    fun `The evaluator has access to the average function in the jexl library for doubles`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<Double>("average(list(1.0, 6.0, 2.0))")

        assertEquals(3.0, result)
    }

    @Test
    fun `The average function throws an error on empty lists`() {
        assertThrows<EvaluatorException> {
            engine
                .evaluator(EmptyTestContext())
                .evaluate<Int>("average(list())")
        }
    }

    @Test
    fun `The evaluator has access to the median function in the jexl library for integers`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<Int>("median(list(1, 3, 2))")

        assertEquals(2, result)
    }

    @Test
    fun `The evaluator has access to the median function in the jexl library for doubles`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<Double>("median(list(1.0, 3.0, 2.0))")

        assertEquals(2.0, result)
    }

    @Test
    fun `The median function can deal with even number of integers`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<Double>("median(list(1, 2, 3, 4))")

        assertEquals(2.5, result)
    }

    @Test
    fun `The median function throws an error on empty lists`() {
        assertThrows<EvaluatorException> {
            engine
                .evaluator(EmptyTestContext())
                .evaluate<Int>("median(list())")
        }
    }

    @Test
    fun `The evaluator has access to the median function in the jexl library for even number of integers`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<Double>("median(list(1, 2, 3, 4))")

        assertEquals(2.5, result)
    }

    @Test
    fun `The evaluator has access to the mode function in the jexl library for integers`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<Int>("mode(list(1, 2, 2, 3, 3, 3))")

        assertEquals(3, result)
    }

    @Test
    fun `The evaluator has access to the mode function in the jexl library for doubles`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<Double>("mode(list(1.0, 2.0, 2.0, 3.0, 3.0, 3.0))")

        assertEquals(3.0, result)
    }

    @Test
    fun `The mode function throws an error on empty lists`() {
        assertThrows<EvaluatorException> {
            engine
                .evaluator(EmptyTestContext())
                .evaluate<Int>("mode(list())")
        }
    }

    @Test
    fun `The evaluator has access to the size function in the jexl library for integers`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<Int>("size(list(1, 2, 3))")

        assertEquals(3, result)
    }

    @Test
    fun `The evaluator has access to the size function in the jexl library for doubles`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<Int>("size(list(1.0, 2.0, 3.0))")

        assertEquals(3, result)
    }

    @Test
    fun `The evaluator has access to the size function in the jexl library for empty lists`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<Int>("size(list())")

        assertEquals(0, result)
    }
}
