package nl.rug.digitallab.themis.expression

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

@QuarkusTest
class JexlEvaluatorTestInteger {
    @Inject
    private lateinit var engine: ExpressionEngine

    @Test
    fun `A numerical expression can be evaluated`() {
        assertEquals(11, engine.evaluator(EmptyTestContext()).evaluate("5 + 6"))
    }

    @Test
    fun `A numerical expression with variables can be evaluated`() {
        val result = engine
            .evaluator(
                VariableTestContext(
                    a = 5,
                    b = 6,
                )
            )
            .evaluate<Int>("a + b")

        assertEquals(11, result)
    }
}
