package nl.rug.digitallab.themis.expression.libraries

import io.quarkus.test.junit.QuarkusTest
import io.smallrye.common.constraint.Assert.assertTrue
import jakarta.inject.Inject
import nl.rug.digitallab.themis.expression.ExpressionEngine
import nl.rug.digitallab.themis.expression.contexts.Context
import nl.rug.digitallab.themis.expression.exceptions.EvaluatorException
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertEquals

@QuarkusTest
class JexlLibraryMapTest {
    @Inject
    private lateinit var engine: ExpressionEngine

    @Test
    fun `The sum function can deal with maps`() {
        val result = engine
            .evaluator(
                object : Context {
                    val map = mapOf(
                        "a" to 1,
                        "b" to 2,
                        "c" to 3,
                    )
                }
            )
            .evaluate<Int>("sum(map)")

        assertTrue(result == 6)
    }

    @Test
    fun `The sum function can deal with empty maps`() {
        val result = engine
            .evaluator(
                object : Context {
                    val map = mapOf<String, Int>()
                }
            )
            .evaluate<Int>("sum(map)")

        assertEquals(0, result)
    }

    @Test
    fun `The min function can deal with maps`() {
        val result = engine
            .evaluator(
                object : Context {
                    val map = mapOf(
                        "a" to 1,
                        "b" to 2,
                        "c" to 3,
                    )
                }
            )
            .evaluate<Int>("min(map)")

        assertEquals(1, result)
    }

    @Test
    fun `The min function can deal with empty maps`() {
        assertThrows<EvaluatorException> {
            engine
                .evaluator(
                    object : Context {
                        val map = mapOf<String, Int>()
                    }
                )
                .evaluate<Int>("min(map)")
        }
    }

    @Test
    fun `The max function can deal with maps`() {
        val result = engine
            .evaluator(
                object : Context {
                    val map = mapOf(
                        "a" to 1,
                        "b" to 2,
                        "c" to 3,
                    )
                }
            )
            .evaluate<Int>("max(map)")

        assertEquals(3, result)
    }

    @Test
    fun `The max function can deal with empty maps`() {
        assertThrows<EvaluatorException> {
            engine
                .evaluator(
                    object : Context {
                        val map = mapOf<String, Int>()
                    }
                )
                .evaluate<Int>("max(map)")
        }
    }

    @Test
    fun `The average function can deal with maps`() {
        val result = engine
            .evaluator(
                object : Context {
                    val map = mapOf(
                        "a" to 1,
                        "b" to 2,
                        "c" to 3,
                    )
                }
            )
            .evaluate<Double>("average(map)")

        assertEquals(2.0, result)
    }

    @Test
    fun `The average function can deal with empty maps`() {
        assertThrows<EvaluatorException> {
            engine
                .evaluator(
                    object : Context {
                        val map = mapOf<String, Int>()
                    }
                )
                .evaluate<Double>("average(map)")
        }
    }

    @Test
    fun `The median function can deal with empty maps`() {
        assertThrows<EvaluatorException> {
            engine
                .evaluator(
                    object : Context {
                        val map = mapOf<String, Int>()
                    }
                )
                .evaluate<Double>("median(map)")
        }
    }

    @Test
    fun `The median function can deal with maps with an odd number of elements`() {
        val result = engine
            .evaluator(
                object : Context {
                    val map = mapOf(
                        "a" to 1,
                        "b" to 2,
                        "c" to 3,
                        "d" to 4,
                        "e" to 5,
                    )
                }
            )
            .evaluate<Double>("median(map)")

        assertEquals(3.0, result)
    }

    @Test
    fun `The median function can deal with maps with an even number of elements`() {
        val result = engine
            .evaluator(
                object : Context {
                    val map = mapOf(
                        "a" to 1,
                        "b" to 2,
                        "c" to 3,
                        "d" to 4,
                    )
                }
            )
            .evaluate<Double>("median(map)")

        assertEquals(2.5, result)
    }

    @Test
    fun `The mode function can deal with maps with a single mode`() {
        val result = engine
            .evaluator(
                object : Context {
                    val map = mapOf(
                        "a" to 1,
                        "b" to 2,
                        "c" to 2,
                        "d" to 3,
                    )
                }
            )
            .evaluate<Int>("mode(map)")

        assertEquals(2, result)
    }

    @Test
    fun `The mode function can deal with empty maps`() {
        assertThrows<EvaluatorException> {
            engine
                .evaluator(
                    object : Context {
                        val map = mapOf<String, Int>()
                    }
                )
                .evaluate<Int>("mode(map)")
        }
    }
}
