package nl.rug.digitallab.themis.expression.contexts

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.expression.ExpressionEngine
import org.junit.jupiter.api.Assertions.assertEquals
import kotlin.test.Test

@QuarkusTest
class ComposableContextTest {
    @Inject
    private lateinit var engine: ExpressionEngine

    interface FirstContext : Context {
        val a: Int
    }

    interface SecondContext : Context {
        val b: Int
    }

    object composedContext : FirstContext, SecondContext {
        override val a: Int = 1
        override val b: Int = 2
    }

    @Test
    fun `Two contexts can be composed together to form a bigger context`() {
        val evaluator = engine.evaluator(composedContext)
        val result = evaluator.evaluate<Int>("a + b")
        assertEquals(3, result)
    }
}
