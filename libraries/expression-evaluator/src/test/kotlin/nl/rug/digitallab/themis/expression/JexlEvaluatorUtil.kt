package nl.rug.digitallab.themis.expression

import nl.rug.digitallab.themis.expression.contexts.Context

class EmptyTestContext: Context

data class VariableTestContext(
    val a: Int,
    val b: Int,
) : Context

data class InnerTestContext(
    val a: Int,
    val b: Int,
    val innerClass: VariableTestContext,
) : Context

data class EnumTestContext(
    val test: TestEnum,
) : Context

enum class TestEnum {
    A
}
