package nl.rug.digitallab.themis.expression.contexts

import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

@QuarkusTest
class ContextTypingTest {

    @Test
    fun `Lists retain their inner typing`() {
        val context = object : Context {
            val list = listOf(1, 2)
        }.toEvaluable()

        val list = context["list"] as List<*>

        assertTrue(list.all { it is Int })
    }

    @Test
    fun `Nested lists retain their inner typing`() {
        val context = object : Context {
            val list = listOf(listOf(1, 2), listOf(3, 4))
        }.toEvaluable()

        val list = context["list"] as List<*>

        assertTrue(list.all { innerList ->
            innerList is List<*> && (innerList).all { it is Int }
        })
    }

    @Test
    fun `Maps retain their inner typing`() {
        val context = object : Context {
            val map = mapOf("one" to 1, "two" to 2)
        }.toEvaluable()

        val map = context["map"] as Map<*, *>

        assertTrue(map.all { (key, value) -> key is String && value is Int })
    }

    @Test
    fun `Nested maps retain their inner typing`() {
        val context = object : Context {
            val map = mapOf(
                "first" to mapOf("a" to 1, "b" to 2),
                "second" to mapOf("c" to 3, "d" to 4)
            )
        }.toEvaluable()

        val map = context["map"] as Map<*, *>

        assertTrue(map.all { (key, value) ->
            key is String && value is Map<*, *> && (value).all { (innerKey, innerValue) ->
                innerKey is String && innerValue is Int
            }
        })
    }
}
