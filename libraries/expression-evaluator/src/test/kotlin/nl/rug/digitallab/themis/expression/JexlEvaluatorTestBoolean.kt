package nl.rug.digitallab.themis.expression

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.expression.exceptions.DisabledFeaturesException
import nl.rug.digitallab.themis.expression.exceptions.UndefinedVariableException
import nl.rug.digitallab.themis.expression.exceptions.ParsingException
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows

@QuarkusTest
class JexlEvaluatorTestBoolean {
    @Inject
    private lateinit var engine: ExpressionEngine

    @Test
    fun `The evaluator can evaluate a simple boolean expression`() {
        val result = engine
            .evaluator(
                VariableTestContext(
                    a = 1,
                    b = 2,
                )
            )
            .evaluate<Boolean>("a < b")
        assertTrue(result)
    }

    @Test
    fun `The evaluator can deal with Kotlin data classes`() {
        val result = engine
            .evaluator(
                InnerTestContext(
                    a = 1,
                    b = 2,
                    innerClass = VariableTestContext(
                        a = 4,
                        b = 3,
                    )
                )
            )
            .evaluate<Boolean>("innerClass.a > innerClass.b")

        assertTrue(result)
    }

    @Test
    fun `The user can use variable assignment`() {
        val evaluator = engine.evaluator(VariableTestContext(a = 1, b = 2))

        assertThrows<DisabledFeaturesException> { evaluator.evaluate<Boolean>("a = 5") }
    }

    @Test
    fun `The user cannot import any libraries`() {
        val evaluator = engine.evaluator(EmptyTestContext())

        assertThrows<DisabledFeaturesException> { evaluator.evaluate<Boolean>("#pragma jexl.import java.lang") }
    }

    @Test
    fun `The evaluator can deal with enums`() {
        val evaluator = engine.evaluator(
            EnumTestContext(
                test = TestEnum.A,
            )
        )

        assertTrue(evaluator.evaluate<Boolean>("TestEnum.A == TestEnum.A"))
        assertFalse(evaluator.evaluate<Boolean>("TestEnum.A == TestEnum.B"))
        assertTrue(evaluator.evaluate<Boolean>("TestEnum.A != TestEnum.B"))
    }

    @Test
    fun `The evaluator can deal with instantiated enums`() {
        val evaluator = engine
            .evaluator(
                EnumTestContext(
                    test = TestEnum.A,
                )
            )

        assertTrue(evaluator.evaluate<Boolean>("test == TestEnum.A"))
        assertFalse(evaluator.evaluate<Boolean>("test == TestEnum.B"))
    }

    @Test
    fun `The evaluator throws an exception on missing variables`() {
        val evaluator = engine.evaluator(EmptyTestContext())

        assertThrows<UndefinedVariableException> { evaluator.evaluate<Boolean>("a < b") }
    }

    @Test
    fun `The evaluator does not use copy-on-context`() {
        val evaluator = engine.evaluator(
            VariableTestContext(
                a = 1,
                b = 2,
            )
        )

        // a and b should not be available as context should return a copy of the evaluator while leaving the original untouched
        assertDoesNotThrow { evaluator.evaluate<Boolean>("a < b") }
    }

    @Test
    fun `The JEXL engine can throw parsing exceptions`() {
        val evaluator = engine.evaluator(EmptyTestContext())

        assertThrows<ParsingException> {
            // Nonsensical expression
            evaluator.evaluate<Boolean>("1 2")
        }
    }
}
