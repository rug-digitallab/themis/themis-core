package nl.rug.digitallab.themis.expression.libraries.util

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.themis.expression.libraries.utils.toBigDecimal
import org.junit.jupiter.api.assertThrows
import kotlin.test.Test

@QuarkusTest
class BigDecimalUtilTest {
    @Test
    fun `The converter throws an error when the number type is not supported`() {
        assertThrows<IllegalArgumentException> {
            NewNumber().toBigDecimal()
        }
    }

    class NewNumber : Number() {
        override fun toByte(): Byte {
            return 0
        }

        override fun toDouble(): Double {
            return 0.0
        }

        override fun toFloat(): Float {
            return 0f
        }

        override fun toInt(): Int {
            return 0
        }

        override fun toLong(): Long {
            return 0
        }

        override fun toShort(): Short {
            return 0
        }

    }
}
