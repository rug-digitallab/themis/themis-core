package nl.rug.digitallab.themis.expression.contexts

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.expression.*
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

@QuarkusTest
class ContextTest {
    @Inject
    private lateinit var engine: ExpressionEngine

    @Test
    fun `Maps are supported in the context`() {
        val evaluator = engine.evaluator(
            object : Context {
                val map = mapOf(
                    "a" to 1,
                    "b" to 2,
                )
            }
        )

        assertTrue(evaluator.evaluate<Boolean>("map['a'] == 1"))
    }

    @Test
    fun `Lists are supported in the context`() {
        val evaluator = engine.evaluator(
            object : Context {
                val list = listOf(1, 2)
            }
        )

        assertTrue(evaluator.evaluate<Boolean>("list[0] == 1"))
    }

    @Test
    fun `Objects are supported in the context`() {
        val evaluator = engine.evaluator(
            object : Context {
                val objectParam = object {
                    val number = 1
                }
            }
        )

        assertTrue(evaluator.evaluate<Boolean>("objectParam.number == 1"))
    }

    @Test
    fun `Enums are supported in the context`() {
        val evaluator = engine.evaluator(
            object : Context {
                val test = TestEnum.A
            }
        )

        assertTrue(evaluator.evaluate<Boolean>("test == TestEnum.A"))
    }
}
