package nl.rug.digitallab.themis.expression.libraries

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.expression.EmptyTestContext
import nl.rug.digitallab.themis.expression.ExpressionEngine
import nl.rug.digitallab.themis.expression.VariableTestContext
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

@QuarkusTest
class JexlLibraryTest {
    @Inject
    private lateinit var engine: ExpressionEngine

    @Test
    fun `The evaluator has access to the abs function in the jexl library for integers`() {
        val result = engine
            .evaluator(
                VariableTestContext(
                    a = -1,
                    b = 2,
                )
            )
            .evaluate<Int>("abs(a)")

        Assertions.assertEquals(1, result)
    }

    @Test
    fun `The evaluator has access to the abs function in the jexl library for doubles`() {
        val result = engine
            .evaluator(EmptyTestContext())
            .evaluate<Double>("abs(-1.0)")

        Assertions.assertEquals(1.0, result)
    }
}
