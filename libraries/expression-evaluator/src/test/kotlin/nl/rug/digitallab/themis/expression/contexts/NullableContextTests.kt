package nl.rug.digitallab.themis.expression.contexts

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.expression.ExpressionEngine
import nl.rug.digitallab.themis.expression.TestEnum
import nl.rug.digitallab.themis.expression.exceptions.NullableContextException
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

@QuarkusTest
class NullableContextTests {
    @Inject
    private lateinit var engine: ExpressionEngine

    @Test
    fun `The context can deal with nested enums`() {
        // Do the above but with an object
        val evaluator = engine.evaluator(
            object : Context {
                val test = object : Context {
                    val test = TestEnum.A
                }
            }
        )

        assertTrue(evaluator.evaluate<Boolean>("test.test == TestEnum.A"))
    }

    @Test
    fun `The context throws an exception on null values in maps`() {
        assertThrows<NullableContextException> {
            engine.evaluator(
                object : Context {
                    val map = mapOf(
                        "a" to 1,
                        "b" to null,
                    )
                }
            )
        }
    }

    @Test
    fun `The context throws an exception on null values in objects`() {
        assertThrows<NullableContextException> {
            engine.evaluator(
                object : Context {
                    val a = null
                }
            )
        }
    }
}
