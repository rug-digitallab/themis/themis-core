package nl.rug.digitallab.themis.expression.exceptions

/**
 * An exception that is thrown when the expression cannot be parsed.
 *
 * @param expression The expression that cannot be parsed.
 * @param cause The root exception thrown by the internal engine.
 */
class ParsingException(
    expression: String,
    cause: Throwable,
) : EvaluatorException(
    "Failed to parse expression \"$expression\"",
    cause,
)
