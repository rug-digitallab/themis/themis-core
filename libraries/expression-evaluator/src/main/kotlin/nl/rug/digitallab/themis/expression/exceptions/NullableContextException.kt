package nl.rug.digitallab.themis.expression.exceptions

/**
 * An exception that is thrown when a nullable property is used in a context.
 *
 * @param propertyName The name of the property that is nullable.
 */
class NullableContextException(
    propertyName: String
): EvaluatorException(
    "Property '$propertyName' is nullable, but nullable properties are not allowed in contexts."
)
