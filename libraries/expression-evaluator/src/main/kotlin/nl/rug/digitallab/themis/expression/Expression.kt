package nl.rug.digitallab.themis.expression

import nl.rug.digitallab.themis.expression.contexts.Context

/**
 * An engine that can create expression evaluators.
 * CDI injectable.
 */
interface ExpressionEngine {
    /**
     * Creates an evaluator with the provided context.
     *
     * @param context The context to use for evaluation.
     *
     * @return The evaluator with the provided context.
     */
    fun evaluator(context: Context): ExpressionEvaluator
}

/**
 * An evaluator that can evaluate expressions.
 */
interface ExpressionEvaluator {
    /**
     * Evaluates an expression and returns the result.
     *
     * @param expression The expression to evaluate.
     *
     * @return The result of the evaluation.
     */
    fun <T> evaluate(expression: String): T
}
