package nl.rug.digitallab.themis.expression.contexts

import nl.rug.digitallab.themis.expression.contexts.helpers.EnumCollector
import nl.rug.digitallab.themis.expression.exceptions.NullableContextException
import kotlin.reflect.full.memberProperties

/**
 * The base of all contexts that can be used in the evaluator.
 * Every context defines a unique use case for the evaluator.
 *
 * IMPORTANT: All properties of a context must be non-nullable.
 *
 * Implementations of this interface are responsible for the public JEXL api of those expressions to the user.
 */
interface Context {
    /**
     * We implement a custom [toEvaluable] function for [Context] objects.
     * This is to guarantee type-safety of the context.
     * While the default [Any.toEvaluable] function would have worked, it returns [Any], which we cannot safely typecast to a Map<String, Any>.
     *
     * @receiver The context to convert to an evaluable object.
     *
     * @return The evaluable object.
     */
    fun toEvaluable(): Map<String, Any> {
        val collector = EnumCollector()

        // Recursively convert the context to an evaluable map
        val properties = with(collector) {
            this@Context.objectToEvaluable()
        }

        // Merge the discovered properties with the enum constants
        val enumConstants = collector.extractEnumConstants()
        val context = properties + enumConstants

        check(context.size == properties.size + enumConstants.size) {
            "The context contains duplicate keys. Please ensure that your properties do not clash with the enum constants."
        }

        return properties + collector.extractEnumConstants()
    }

    /**
     * Classes in Kotlin are not directly evaluable by JEXL, so we need to convert them to a Map.
     * This function recursively converts the object to something that is evaluable by JEXL.
     *
     * @receiver The object to convert to an evaluable object.
     * @return The evaluable object.
     */
    context(EnumCollector)
    private fun Any.toEvaluable(): Any = when (this) {
        is String, is Number, is Boolean -> this
        is Enum<*> -> {
            registerEnum(this.javaClass)
            "${this.javaClass.simpleName}.${this.name}"
        }
        is Map<*, *> -> this.mapValues { (key, value) ->
            value?.toEvaluable() ?: throw NullableContextException(key.toString())
        }
        is List<*> -> this.map { it?.toEvaluable() }
        else -> this.objectToEvaluable()
    }

    /**
     * Converts a custom object to an evaluable map.
     *
     * @receiver The object to convert to an evaluable map.
     *
     * @return The evaluable map.
     */
    context(EnumCollector)
    private fun Any.objectToEvaluable(): Map<String, Any> {
        this::class.memberProperties
            .filter { it.returnType.isMarkedNullable }
            .forEach { throw NullableContextException(it.name) }

        return this::class.memberProperties.associate { property ->
            property.name to (property.call(this)?.toEvaluable() ?: throw NullableContextException(property.name))
        }
    }
}
