package nl.rug.digitallab.themis.expression

import jakarta.enterprise.context.ApplicationScoped
import nl.rug.digitallab.themis.expression.contexts.Context
import nl.rug.digitallab.themis.expression.exceptions.DisabledFeaturesException
import nl.rug.digitallab.themis.expression.exceptions.EvaluatorException
import nl.rug.digitallab.themis.expression.exceptions.UndefinedVariableException
import nl.rug.digitallab.themis.expression.exceptions.ParsingException
import nl.rug.digitallab.themis.expression.libraries.JexlLibrary
import org.apache.commons.jexl3.*
import org.apache.commons.jexl3.JexlEngine
import org.apache.commons.jexl3.introspection.JexlPermissions

/**
 * An implementation of the [ExpressionEngine] that uses the JEXL engine for evaluation.
 */
@ApplicationScoped
class JexlEngine: ExpressionEngine {
    private val jexlFeatures = JexlFeatures.createNone()
        // Lexical scoping
        .lexical(true)          // Lexical scope, prevents redefining local variables
        .lexicalShade(true)     // Local variables shade globals, prevents confusing a global variable with a local one
        // Language features
        .methodCall(true)       // Method calls (obj.method()). Namespace methods are independent of this flag.

    private val jexlEngine = JexlBuilder()
        .features(jexlFeatures)
        .permissions(
            // RESTRICTED provides a set of packages that are allowed to be added to the JEXL engine.
            // If a package is added to the context/namespaces, but is not in this list, the package will not be accessible.
            // This is mainly to protect the host from malicious code execution from packages like System, Runtime, ProcessBuilder, etc.
            // Note that this fails silently, if a function is added that is not permitted, and you try to use the function,
            // it will simply throw an error that the function is not found. It will not explicitly state that the package is not allowed.
            // For a list of all packages that are allowed by default, see
            // https://commons.apache.org/proper/commons-jexl/apidocs/org/apache/commons/jexl3/introspection/JexlPermissions.html#RESTRICTED
            JexlPermissions.RESTRICTED.compose(
                // This allows any class in the nl.rug.digitallab.themis.expression.libraries package to be added to the context.
                "nl.rug.digitallab.themis.expression.libraries.*",
            )
        )
        .namespaces(mapOf(
            null to JexlLibrary,
        ))
        .create()

    /**
     * Creates an evaluator with the provided context.
     *
     * @param context The context to use for evaluation.
     *
     * @return The evaluator with the provided context.
     */
    override fun evaluator(context: Context): ExpressionEvaluator = JexlEvaluator(jexlEngine, context.toEvaluable())
}

/**
 * An implementation of the [ExpressionEvaluator] that uses the JEXL engine.
 *
 * @property jexlEngine The JEXL engine to use for evaluation.
 * @property context The context which provides the expression access to variables.
 */
class JexlEvaluator(
    private val jexlEngine: JexlEngine,
    private val context: Map<String, Any>,
) : ExpressionEvaluator {
    /**
     * Maps JEXL exceptions to assertion library exceptions
     *
     * @param expression The expression that was evaluated.
     * @param block The block of code to execute.
     *
     * @return The result of the block.
     */
    private fun <T> mapExceptions(
        expression: String,
        block: () -> T,
    ): T {
        return try {
            block()
        } catch (e: JexlException) {
            throw when (e) {
                is JexlException.Feature -> DisabledFeaturesException(expression, e)
                is JexlException.Variable -> UndefinedVariableException(expression, e, e.variable)
                is JexlException.Parsing -> ParsingException(expression, e)
                else -> EvaluatorException("An exception occurred while evaluating the expression:\n$expression", e)
            }
        }
    }

    /**
     * Evaluates an expression and returns the result.
     *
     * @param expression The expression to evaluate.
     *
     * @return The result of the expression.
     */
    override fun <T> evaluate(expression: String): T {
        val jexlContext = MapContext(context)

        return mapExceptions(expression) {
            val result = jexlEngine.createExpression(expression).evaluate(jexlContext)
            @Suppress("UNCHECKED_CAST")
            return@mapExceptions (result as T)
        }
    }
}
