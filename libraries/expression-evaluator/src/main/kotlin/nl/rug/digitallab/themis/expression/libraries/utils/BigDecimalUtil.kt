package nl.rug.digitallab.themis.expression.libraries.utils

import java.math.BigDecimal

/**
 * Convert a [Number] to a [BigDecimal].
 *
 * @return The [BigDecimal].
 */
fun Number.toBigDecimal(): BigDecimal = when (this) {
    is Int -> BigDecimal(this)
    is Long -> BigDecimal(this)
    is Float -> this.toBigDecimal()
    is Double -> BigDecimal.valueOf(this)
    is BigDecimal -> this
    else -> throw IllegalArgumentException("Unsupported number type: ${this::class}")
}

/**
 * Convert a list of [Number]s to a list of [BigDecimal]s.
 *
 * @return The list of [BigDecimal]s.
 */
fun Collection<Number>.toBigDecimal(): List<BigDecimal> = this.map { it.toBigDecimal() }
