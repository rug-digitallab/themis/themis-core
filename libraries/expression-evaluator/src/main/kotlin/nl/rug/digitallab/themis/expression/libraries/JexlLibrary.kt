package nl.rug.digitallab.themis.expression.libraries

import nl.rug.digitallab.themis.expression.libraries.utils.toBigDecimal
import java.math.BigDecimal

/**
 * This library is directly inserted into the root namespace of the JEXL engine.
 */
object JexlLibrary {
    /**
     * Create a list of [BigDecimal]s from a vararg of [Number]s.
     *
     * @param elements The elements to create the list from.
     *
     * @return The list of [BigDecimal]s.
     */
    fun list(vararg elements: Number): List<BigDecimal> {
        return elements.map { BigDecimal(it.toString()) }
    }

    /**
     * Calculate the absolute value of a [Number].
     *
     * @param value The [Number] to calculate the absolute value of.
     *
     * @return The absolute value as a [BigDecimal].
     */
    fun abs(value: Number): BigDecimal {
        return value.toBigDecimal().abs()
    }

    /**
     * Sum up a list of [Number]s.
     *
     * @param list The list of [Number]s to sum up.
     *
     * @return The sum of the list as a [BigDecimal].
     */
    fun sum(list: List<Number>): BigDecimal {
        return list.toBigDecimal().sumOf { it }
    }

    /**
     * Sum up a map of [String]s to [Number]s.
     *
     * @param map The map of [String]s to [Number]s to sum up.
     *
     * @return The sum of the map as a [BigDecimal].
     */
    fun sum(map: Map<String, Number>): BigDecimal {
        return sum(map.values.toBigDecimal())
    }

    /**
     * Find the minimum value in a list of [Number]s.
     *
     * @param list The list of [Number]s to find the minimum value in.
     *
     * @return The minimum value in the list as a [BigDecimal].
     */
    fun min(list: List<Number>): BigDecimal {
        require(list.isNotEmpty()) { "Cannot find the minimum of an empty list." }

        return list.toBigDecimal().min()
    }

    /**
     * Find the minimum value in a map of [String]s to [Number]s.
     *
     * @param map The map of [String]s to [Number]s to find the minimum value in.
     *
     * @return The minimum value in the map as a [BigDecimal].
     */
    fun min(map: Map<String, Number>): BigDecimal {
        require(map.isNotEmpty()) { "Cannot find the minimum of an empty map." }

        return min(map.values.toList())
    }


    /**
     * Find the maximum value in a list of [Number]s.
     *
     * @param list The list of [Number]s to find the maximum value in.
     *
     * @return The maximum value in the list as a [BigDecimal].
     */
    fun max(list: List<Number>): BigDecimal {
        require(list.isNotEmpty()) { "Cannot find the maximum of an empty list." }

        return list.toBigDecimal().max()
    }

    /**
     * Find the maximum value in a map of [String]s to [Number]s.
     *
     * @param map The map of [String]s to [Number]s to find the maximum value in.
     *
     * @return The maximum value in the map as a [BigDecimal].
     */
    fun max(map: Map<String, Number>): BigDecimal {
        require(map.isNotEmpty()) { "Cannot find the maximum of an empty map." }

        return max(map.values.toList())
    }


    /**
     * Calculate the average of a list of [Number]s.
     *
     * @param list The list of [Number]s to calculate the average of.
     *
     * @return The average of the list as a [BigDecimal].
     */
    fun average(list: List<Number>): BigDecimal {
        require(list.isNotEmpty()) { "Cannot calculate the average of an empty list." }

        return sum(list).divide(list.size.toBigDecimal())
    }

    /**
     * Calculate the average of a map of [String]s to [Number]s.
     *
     * @param map The map of [String]s to [Number]s to calculate the average of.
     *
     * @return The average of the map as a [BigDecimal].
     */
    fun average(map: Map<String, Number>): BigDecimal {
        require(map.isNotEmpty()) { "Cannot calculate the average of an empty map." }

        return average(map.values.toList())
    }

    /**
     * Calculate the median of a list of [Number]s.
     *
     * @param list The list of [Number]s to calculate the median of.
     *
     * @return The median of the list as a [BigDecimal].
     */
    fun median(list: List<Number>): BigDecimal {
        require(list.isNotEmpty()) { "Cannot calculate the median of an empty list." }

        val sorted = list.toBigDecimal().sorted()
        val middle = sorted.size / 2
        return if (sorted.size % 2 == 0) {
            (sorted[middle - 1] + sorted[middle]).divide(2.toBigDecimal())
        } else {
            sorted[middle]
        }
    }

    /**
     * Calculate the median of a map of [String]s to [Number]s.
     *
     * @param map The map of [String]s to [Number]s to calculate the median of.
     *
     * @return The median of the map as a [BigDecimal].
     */
    fun median(map: Map<String, Number>): BigDecimal {
        require(map.isNotEmpty()) { "Cannot calculate the median of an empty map." }

        return median(map.values.toList())
    }

    /**
     * Calculate the mode of a list of [Number]s.
     *
     * @param list The list of [Number]s to calculate the mode of.
     *
     * @return The mode of the list as a [BigDecimal].
     */
    fun mode(list: List<Number>): BigDecimal {
        require(list.isNotEmpty()) { "Cannot calculate the mode of an empty list." }

        val counts = list.groupingBy { it }.eachCount()
        val maxCount = counts.values.max()
        return counts.filterValues { it == maxCount }.keys.first().toBigDecimal()
    }

    /**
     * Calculate the mode of a map of [String]s to [Number]s.
     *
     * @param map The map of [String]s to [Number]s to calculate the mode of.
     *
     * @return The mode of the map as a [BigDecimal].
     */
    fun mode(map: Map<String, Number>): BigDecimal {
        require(map.isNotEmpty()) { "Cannot calculate the mode of an empty map." }

        return mode(map.values.toList())
    }
}
