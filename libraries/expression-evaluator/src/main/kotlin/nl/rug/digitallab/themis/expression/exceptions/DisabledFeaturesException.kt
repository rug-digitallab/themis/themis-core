package nl.rug.digitallab.themis.expression.exceptions

/**
 * An exception that is thrown when a feature is used in the expression that is disabled.
 *
 * @param expression The expression that contains the disabled features.
 * @param cause The root exception thrown by the internal engine.
 */
class DisabledFeaturesException(expression: String, cause: Throwable) : EvaluatorException(
    "Disabled features detected in expression:\n$expression",
    cause,
)
