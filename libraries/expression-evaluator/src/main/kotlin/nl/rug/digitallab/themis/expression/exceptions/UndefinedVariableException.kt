package nl.rug.digitallab.themis.expression.exceptions

/**
 * An exception that is thrown when a variable is missing from the context but is addressed in an expression.
 *
 * @param variableName The name of the missing variable.
 * @param expression The expression that references the missing variable.
 * @param cause The root exception thrown by the internal engine.
 */
class UndefinedVariableException(
    expression: String,
    cause: Throwable,
    variableName: String,
) : EvaluatorException(
    "Undefined variable '$variableName' in expression \"$expression\"",
    cause,
)
