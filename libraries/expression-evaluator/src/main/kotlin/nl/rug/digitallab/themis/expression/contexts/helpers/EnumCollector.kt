package nl.rug.digitallab.themis.expression.contexts.helpers

/**
 * A helper class for collecting enum constants.
 * The goal of this class is to collect all enums (including nested ones) that appear in the context while traversing the context.
 * The constant values of the enums are then extracted and made available to the JEXL expressions.
 */
class EnumCollector {
    private val enums: MutableSet<Class<Enum<*>>> = mutableSetOf()

    /**
     * Register an enum to make its constant values available to the JEXL expressions.
     *
     * @param enum The enum to register.
     */
    fun registerEnum(enum: Class<Enum<*>>) {
        assert(enum.isEnum)

        enums.add(enum)
    }

    /**
     * Extract the constant values of all registered enums.
     *
     * @return A map containing the enum constant values.
     */
    fun extractEnumConstants(): Map<String, String> {
        return enums.flatMap {
            it.enumConstants.map { constant -> "${it.simpleName}.$constant" }
        }.associateWith { it }
    }
}
