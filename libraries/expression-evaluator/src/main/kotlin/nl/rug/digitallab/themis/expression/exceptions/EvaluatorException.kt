package nl.rug.digitallab.themis.expression.exceptions

/**
 * Base exception for all exceptions thrown by the evaluator.
 *
 * @param message Explanation of the exception including debugging information.
 * @param cause The cause of the exception.
 */
open class EvaluatorException(
    message: String,
    cause: Throwable? = null,
) : Exception(message, cause)
