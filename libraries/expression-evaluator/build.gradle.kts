plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.library")
}

dependencies {
    val jexlVersion: String by project

    implementation("org.apache.commons:commons-jexl3:$jexlVersion")

    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}
