package nl.rug.digitallab.themis.common.data

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.themis.common.exceptions.StageNotFoundException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows

@QuarkusTest
class RuntimeProfileTest {
    data class DummyJob(override val name: String): RuntimeProfile.Job

    @Test
    fun `A profile filled with unique Stages is valid`() {
        assertDoesNotThrow { RuntimeProfile(
                stages = listOf(
                    RuntimeProfile.Stage(
                        name = "stage1",
                        jobs = emptyList()
                    ),
                    RuntimeProfile.Stage(
                        name = "stage2",
                        jobs = emptyList()
                    ),
                    RuntimeProfile.Stage(
                        name = "stage3",
                        jobs = emptyList()
                    ),
                )
            )
        }
    }

    @Test
    fun `A profile filled with unique Jobs is valid`() {
        assertDoesNotThrow { RuntimeProfile(
                stages = listOf(
                    RuntimeProfile.Stage(
                        name = "stage1",
                        jobs = listOf(
                            DummyJob("job1"),
                            DummyJob("job2"),
                            DummyJob("job3"),
                        )
                    ),
                    RuntimeProfile.Stage(
                        name = "stage2",
                        jobs = listOf(
                            DummyJob("job1"),
                            DummyJob("job2"),
                            DummyJob("job3"),
                        )
                    ),
                )
            )
        }
    }

    @Test
    fun `A profile with duplicate Stage names is invalid`() {
        assertThrows<IllegalArgumentException> { RuntimeProfile(
                stages = listOf(
                    RuntimeProfile.Stage(
                        name = "stage1",
                        jobs = emptyList()
                    ),
                    RuntimeProfile.Stage(
                        name = "stage1",
                        jobs = emptyList()
                    ),
                )
            )
        }
    }

    @Test
    fun `A profile with duplicate Job names in a Stage is invalid`() {
        assertThrows<IllegalArgumentException> { RuntimeProfile(
                stages = listOf(
                    RuntimeProfile.Stage(
                        name = "stage1",
                        jobs = listOf(
                            DummyJob("job1"),
                            DummyJob("job1"),
                        )
                    ),
                )
            )
        }
    }

    @Test
    fun `A profile can be zipped with another profile using a merging functions`() {
        val profile1 = RuntimeProfile(
            stages = listOf(
                RuntimeProfile.Stage(
                    name = "stage1",
                    jobs = listOf(
                        DummyJob("job1"),
                        DummyJob("job2"),
                    )
                ),
                RuntimeProfile.Stage(
                    name = "stage2",
                    jobs = listOf(
                        DummyJob("job1"),
                        DummyJob("job2"),
                    )
                ),
            )
        )

        val profile2 = RuntimeProfile(
            stages = listOf(
                RuntimeProfile.Stage(
                    name = "stage1",
                    jobs = listOf(
                        DummyJob("job1"),
                        DummyJob("job2"),
                    )
                ),
                RuntimeProfile.Stage(
                    name = "stage2",
                    jobs = listOf(
                        DummyJob("job1"),
                        DummyJob("job2"),
                    )
                ),
            )
        )

        val zippedProfile = profile1.zipWith(profile2) { job1, job2 -> DummyJob("${job1.name} ${job2.name}") }

        assertEquals(2, zippedProfile.stages.size)
        assertEquals(2, zippedProfile.stages[0].jobs.size)
        assertEquals(2, zippedProfile.stages[1].jobs.size)
        assertEquals("job1 job1", zippedProfile.stages[0].jobs[0].name)
        assertEquals("job2 job2", zippedProfile.stages[0].jobs[1].name)
        assertEquals("job1 job1", zippedProfile.stages[1].jobs[0].name)
        assertEquals("job2 job2", zippedProfile.stages[1].jobs[1].name)
    }

    @Test
    fun `A profile can be zipped with another profile using a merging functions with different Job types`() {
        data class DummyJob2(override val name: String): RuntimeProfile.Job

        val profile1 = RuntimeProfile(
            stages = listOf(
                RuntimeProfile.Stage(
                    name = "stage1",
                    jobs = listOf(
                        DummyJob("job1"),
                        DummyJob("job2"),
                    )
                ),
                RuntimeProfile.Stage(
                    name = "stage2",
                    jobs = listOf(
                        DummyJob("job1"),
                        DummyJob("job2"),
                    )
                ),
            )
        )

        val profile2 = RuntimeProfile(
            stages = listOf(
                RuntimeProfile.Stage(
                    name = "stage1",
                    jobs = listOf(
                        DummyJob2("job1"),
                        DummyJob2("job2"),
                    )
                ),
                RuntimeProfile.Stage(
                    name = "stage2",
                    jobs = listOf(
                        DummyJob2("job1"),
                        DummyJob2("job2"),
                    )
                ),
            )
        )

        val zippedProfile = profile1.zipWith(profile2) { job1, job2 -> DummyJob("${job1.name} ${job2.name}") }

        assertEquals(2, zippedProfile.stages.size)
        assertEquals(2, zippedProfile.stages[0].jobs.size)
        assertEquals(2, zippedProfile.stages[1].jobs.size)
        assertEquals("job1 job1", zippedProfile.stages[0].jobs[0].name)
        assertEquals("job2 job2", zippedProfile.stages[0].jobs[1].name)
        assertEquals("job1 job1", zippedProfile.stages[1].jobs[0].name)
        assertEquals("job2 job2", zippedProfile.stages[1].jobs[1].name)
    }

    @Test
    fun `A profile can be operated on by associateBy`() {
        val profile = RuntimeProfile(
            stages = listOf(
                RuntimeProfile.Stage(
                    name = "stage1",
                    jobs = listOf(
                        DummyJob("job1"),
                        DummyJob("job2"),
                    )
                ),
                RuntimeProfile.Stage(
                    name = "stage2",
                    jobs = listOf(
                        DummyJob("job1"),
                        DummyJob("job2"),
                    )
                ),
            )
        )

        val associated = profile.associateBy { it.name }

        assertEquals(2, associated.size)
        assertEquals(2, associated["stage1"]?.size)
        assertEquals(2, associated["stage2"]?.size)
        assertEquals("job1", associated["stage1"]?.get("job1"))
        assertEquals("job2", associated["stage1"]?.get("job2"))
        assertEquals("job1", associated["stage2"]?.get("job1"))
        assertEquals("job2", associated["stage2"]?.get("job2"))
    }

    @Test
    fun `An exception is thrown when two incompatible profiles are zipped`() {
        val profile1 = RuntimeProfile(
            stages = listOf(
                RuntimeProfile.Stage(
                    name = "stage1",
                    jobs = listOf(
                        DummyJob("job1"),
                        DummyJob("job2"),
                    )
                ),
                RuntimeProfile.Stage(
                    name = "stage2",
                    jobs = listOf(
                        DummyJob("job1"),
                        DummyJob("job2"),
                    )
                ),
            )
        )

        val profile2 = RuntimeProfile(
            stages = listOf(
                RuntimeProfile.Stage(
                    name = "stage1",
                    jobs = listOf(
                        DummyJob("job1"),
                        DummyJob("job2"),
                    )
                ),
                RuntimeProfile.Stage(
                    name = "stage3",
                    jobs = listOf(
                        DummyJob("job1"),
                        DummyJob("job2"),
                    )
                ),
            )
        )

        assertThrows<StageNotFoundException> {
            profile1.zipWith(profile2) { job1, job2 -> DummyJob("${job1.name} ${job2.name}") }
        }
    }
}
