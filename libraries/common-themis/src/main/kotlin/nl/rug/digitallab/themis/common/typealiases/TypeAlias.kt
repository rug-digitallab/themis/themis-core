package nl.rug.digitallab.themis.common.typealiases

import java.util.UUID

typealias CourseSeriesId = UUID
typealias CourseInstanceId = UUID
typealias AssignmentId = UUID
typealias ProfileId = UUID
typealias ConfigurationId = UUID
typealias GroupId = UUID
typealias SubmissionId = UUID
typealias RunId = UUID
typealias JobJudgementId = UUID
typealias ActionJudgementId = UUID
typealias ActionResourceId = UUID
typealias JudgementId = UUID
