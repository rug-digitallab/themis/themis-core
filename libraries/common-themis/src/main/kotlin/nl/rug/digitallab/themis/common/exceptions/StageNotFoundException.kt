package nl.rug.digitallab.themis.common.exceptions

/**
 * Exception thrown when a stage is not found within a profile.
 *
 * @param message The message of the exception.
 */
class StageNotFoundException(
    message: String,
    cause: Throwable,
) : NoSuchElementException(message, cause)
