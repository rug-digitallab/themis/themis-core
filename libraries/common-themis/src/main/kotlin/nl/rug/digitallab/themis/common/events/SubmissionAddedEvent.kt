package nl.rug.digitallab.themis.common.events

import nl.rug.digitallab.common.kotlin.helpers.noarg.NoArgConstructor
import nl.rug.digitallab.themis.common.typealiases.*

/**
 * The [SubmissionAddedEvent] is the event that is emitted after a submission has been submitted and processed.
 *
 * @property courseInstanceId The ID of the course instance the submission is for.
 * @property groupId The ID of the group that submitted the submission.
 * @property assignmentId The ID of the assignment the submission is for.
 * @property submissionId The ID of the submission.
 * @property configId The ID of the config that should be used to execute the submission.
 */
@NoArgConstructor
data class SubmissionAddedEvent(
    val courseInstanceId: CourseInstanceId,
    val groupId: GroupId,
    val assignmentId: AssignmentId,
    val submissionId: SubmissionId,
    val configId: ConfigurationId,
)

