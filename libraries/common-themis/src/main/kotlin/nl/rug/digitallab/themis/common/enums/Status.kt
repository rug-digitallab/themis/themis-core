package nl.rug.digitallab.themis.common.enums

/**
 * The available execution statuses that an action can have finished with.
 *
 * @property code The unique code of the status to map it to an integer.
 */
enum class Status(val code: Int) {
    SUCCEEDED(0),
    CRASHED(10),

    EXCEEDED_WALL_TIME(20),
    EXCEEDED_MEMORY(30),
    EXCEEDED_DISK(40),
    EXCEEDED_STREAM_OUTPUT(50),
    EXCEEDED_OUTPUT_FILE(60),
    EXCEEDED_PROCESSES(70),

    PRODUCED_ORPHANS(80),
    PRODUCED_ZOMBIES(90),
    PRODUCED_ZOMBIES_AND_ORPHANS(100),
}
