package nl.rug.digitallab.themis.common.constants

// This file contains the names of the message queues that are used within Themis.

/**
 * Queue between the runtime orchestrator and the judgement service.
 * The runtime orchestrator sends the state of the submission to the judgement service.
 * The judgement service then labels and judges the submission.
 */
const val JUDGEMENTS_QUEUE = "update-judgement"

/**
 * Queue used by the assignment service to notify services that a submission has been added.
 * Used by for example the runtime orchestrator to start the judgement process.
 */
const val SUBMISSION_QUEUE = "themis-submissions-added"
