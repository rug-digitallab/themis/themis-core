package nl.rug.digitallab.themis.common.data

import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.themis.common.enums.Status
import java.net.URI
import kotlin.time.Duration

/**
 * Represents a themis-core version of the response produced by an action.
 * It closely resembles the response defined in the protobuf spec, but is more suitable for within themis-core.
 *
 * Main changes:
 * - Cut off protobuf dependencies.
 * - Store the resources in minio rather than directly in the field.
 *
 * @property result The result of the action.
 * @property usage The usage of resources by the action during its execution.
 */
data class ActionResponse(
    val result: Result,
    val usage: Usage,
) {
    /**
     * The result as produced by the action's execution
     *
     * @property exitCode The exit code of the action.
     * @property status The status of the action.
     * @property resources The resources that were produced as outputs by the action.
     */
    data class Result(
        val exitCode: Int,
        val status: Status,
        val resources: List<ThemisResource>,
    ) {
        /**
         * The representation of 1 resource outputted by an action.
         *
         * @property uri The unique identifier of the resource.
         * @property isPresent Whether the resource is present.
         * @property minioPath The path of the resource in MinIO. Defines as `bucket:object`.
         */
        data class ThemisResource(
            val uri: URI,
            val isPresent: Boolean,
            val minioPath: String?,
        )
    }

    /**
     * The usage of resources by the action during its execution.
     *
     * @property wallTime The amount of time the action was running.
     * @property memory The amount of memory used by the action.
     * @property disk The amount of disk space used by the action.
     * @property output The amount of output produced by the action.
     * @property createdProcesses The amount of processes created by the action.
     * @property maxProcesses The maximum amount of processes at any time during the execution.
     * @property orphanedProcesses The amount of orphaned processes.
     * @property zombieProcesses The amount of zombie processes.
     * @property diskTime The amount of time the action spent on disk operations.
     * @property ioTime The amount of time the action spent on IO operations.
     */
    data class Usage(
        val wallTime: Duration?,
        val memory: ByteSize?,
        val disk: ByteSize?,
        val output: ByteSize?,
        val createdProcesses: Int?,
        val maxProcesses: Int?,
        val orphanedProcesses: Int?,
        val zombieProcesses: Int?,
        val diskTime: Int?,
        val ioTime: Int?,
    )
}
