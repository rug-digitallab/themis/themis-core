package nl.rug.digitallab.themis.common.events

import nl.rug.digitallab.common.kotlin.helpers.noarg.NoArgConstructor
import nl.rug.digitallab.themis.common.data.ActionResponse
import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.common.typealiases.ConfigurationId
import nl.rug.digitallab.themis.common.typealiases.SubmissionId

/**
 * Represents a submission's current state.
 *
 * This event is sent from the runtime orchestrator to the judgement service.
 * The judgement service will then use this to recalculate data for the frontend like passing testcases.
 *
 * @property submissionId The ID of the submission.
 * @property configurationId The ID of the assignment configuration that is used to execute and judge the submission.
 * @property profile The profile of the submission. Contains the responses of the actions.
 */
@NoArgConstructor
data class JudgementEvent(
    val submissionId: SubmissionId,
    val configurationId: ConfigurationId,
    val profile: RuntimeProfile<Job>,
) {
    /**
     * Represents a [Job] within a submission.
     *
     * @param name The name of the [Job].
     * @property actions The actions that are executed in this [Job].
     */
    data class Job(
        override val name: String,
        val actions: List<ActionState>,
    ): RuntimeProfile.Job

    /**
     * An action that may have been executed.
     * If the action has been executed, the response will be non-null.
     *
     * @property name The name of the action.
     * @property response The response of the action.
     */
    class ActionState(
        val name: String,
        val response: ActionResponse?,
    )
}
