package nl.rug.digitallab.themis.common.enums

/**
 * Tracks the status of an ActionNode.
 * PENDING: The action has not been executed yet. It is waiting for all its dependencies to have finished.
 * RUNNING: The action is currently being executed.
 * SUCCEEDED: The action succeeded, post condition is expected.
 * FAILED: The action has failed, post condition is not expected.
 */
enum class ActionStatus {
    PENDING,
    RUNNING,
    SUCCEEDED,
    FAILED,
}
