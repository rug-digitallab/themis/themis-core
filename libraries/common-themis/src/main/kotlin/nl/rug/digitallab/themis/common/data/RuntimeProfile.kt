package nl.rug.digitallab.themis.common.data

import nl.rug.digitallab.common.kotlin.helpers.noarg.NoArgConstructor
import nl.rug.digitallab.themis.common.exceptions.StageNotFoundException

/**
 * A [RuntimeProfile] is used to represent data in a profile structure.
 * This data structure is purely used to represent concepts such as [Stage]s and [Job]s.
 * It is agnostic to the actual data contained within the [Job]s, which can be defined by the user.
 *
 * @property stages The [Stage]s that the submission has.
 *
 * @param T The type of the [Job]s that the [RuntimeProfile] has.
 */
@NoArgConstructor
data class RuntimeProfile<T: RuntimeProfile.Job>(
    val stages: List<Stage<T>>,
) {
    init {
        require(stages.map { it.name }.toSet().size == stages.size) {
            "Stage names must be unique. Duplicate names found: ${stages.map { it.name }}"
        }
    }

    /**
     * A [Stage] represents a group of [Job]s that are related to each other.
     * [Job]s within a [Stage] are executed in parallel.
     *
     * @property name The name of the [Stage]. Must be unique within the [RuntimeProfile].
     * @property jobs The [Job]s that the stage has.
     *
     * @param T The type of the [Job]s that the [Stage] has.
     */
    @NoArgConstructor
    data class Stage<T: Job>(
        val name: String,
        val jobs: List<T>,
    ) {
        /*
         * Check that every stage has a unique name.
         */
        init {
            require(jobs.map { it.name }.toSet().size == jobs.size) {
                "Job names must be unique within a stage. Duplicate names found: ${jobs.map { it.name }}"
            }
        }
    }

    /**
     * A job represents a sequence of actions that are executed in order.
     *
     * @property name The name of the job. Must be unique within the stage.
     */
    interface Job {
        val name: String
    }

    /**
     * Combines the jobs from the current profile (`left`) with those from the provided
     * profile (`right`) using the [combine] function, matching them by their stage
     * and job name. This requires the right profile to contain at least all stages and
     * jobs that the left profile has as well, but any additional stages or jobs in the
     * right profile will be ignored.
     *
     * @param profile The `right` profile to combine with the current (`left`) profile
     * @param combine Combination function to merge the matched jobs
     *
     * @throws [NoSuchElementException] In case the `right` profile does not contain one
     *      of the stages or jobs of the `left` profile
     *
     * @return [RuntimeProfile] with the new jobs
     */
    fun <R : Job, V : Job> zipWith(profile: RuntimeProfile<R>, combine: (T, R) -> V): RuntimeProfile<V> {
        return RuntimeProfile(
            stages = stages
                .associateWith { stage ->
                    try {
                        profile.stages.first { it.name == stage.name }
                    } catch (e: NoSuchElementException) {
                        throw StageNotFoundException("Stage '${stage.name}' not found in the provided profile", e)
                    }
                }
                .map { (leftStage, rightStage) ->
                    Stage(
                        name = leftStage.name,
                        jobs = leftStage.jobs
                            .associateWith { job -> rightStage.jobs.first { it.name == job.name } }
                            .map { (leftJob, rightJob) -> combine(leftJob, rightJob) }
                    )
                }
        )
    }

    /**
     * Associates the jobs in the profile by the result of the [keySelector] function.
     * The result is a map of stage names to a map of job names to the result of the [keySelector] function.
     *
     * @param keySelector Function that returns the key to associate the jobs by
     *
     * @return A map of stage names to a map of job names to the result of the [keySelector] function
     */
    fun <V> associateBy(keySelector: (T) -> V): Map<String, Map<String, V>> {
        return stages.associateBy({ it.name }, { stage -> stage.jobs.associateBy({ it.name }, keySelector) })
    }
}
