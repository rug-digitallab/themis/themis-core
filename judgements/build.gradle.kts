plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.project")
}

dependencies {
    val commonQuarkusVersion: String by project
    val jacksonVersion: String by project
    val kotlinMockitoVersion: String by project

    // Message Queue
    implementation("io.quarkus:quarkus-messaging-kafka")
    // Themis' common data structures
    implementation(project(":libraries:common-themis"))
    // Themis' assertions library
    implementation(project(":libraries:expression-evaluator"))

    // Hibernate ORM Panache
    implementation("io.quarkus:quarkus-hibernate-validator")
    implementation("io.quarkus:quarkus-jdbc-mariadb")

    // REST
    implementation("io.quarkus:quarkus-rest-client-jackson")

    // Flyway DB Migrations
    implementation("io.quarkus:quarkus-flyway")
    implementation("io.quarkus:quarkus-jdbc-mariadb") // Flyway requires a JDBC driver instead of a reactive driver
    implementation("org.flywaydb:flyway-mysql")

    // Observability
    implementation("io.quarkus:quarkus-observability-devservices-lgtm")

    // Jackson modules
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:$jacksonVersion")

    implementation("nl.rug.digitallab.common.quarkus:exception-mapper-hibernate:$commonQuarkusVersion")
    implementation("nl.rug.digitallab.common.quarkus:hibernate-orm:$commonQuarkusVersion")

    testImplementation("io.quarkus:quarkus-junit5-mockito")
    testImplementation("org.mockito.kotlin:mockito-kotlin:$kotlinMockitoVersion")
}
