package nl.rug.digitallab.themis.judgements.persistence.entities

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.themis.common.enums.Status
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

@QuarkusTest
class ActionJudgementTest {
    @Test
    fun `A status can be properly converted to its code`() {
        val converter = ActionJudgement.SubmissionStatusConverter()

        val result = converter.convertToEntityAttribute(Status.SUCCEEDED.code)

        assertEquals(Status.SUCCEEDED, result)
    }

    @Test
    fun `A status code can be properly converted to its status`() {
        val converter = ActionJudgement.SubmissionStatusConverter()

        val result = converter.convertToDatabaseColumn(Status.SUCCEEDED)

        assertEquals(Status.SUCCEEDED.code, result)
    }

    @Test
    fun `An exception is thrown when a status code is not linked to a valid status`() {
        val converter = ActionJudgement.SubmissionStatusConverter()

        assertThrows<IllegalArgumentException> {
            converter.convertToEntityAttribute(-10)
        }
    }
}
