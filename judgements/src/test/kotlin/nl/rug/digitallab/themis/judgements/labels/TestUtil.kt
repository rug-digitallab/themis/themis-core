package nl.rug.digitallab.themis.judgements.labels

import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.digitallab.themis.common.data.ActionResponse
import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.common.enums.Status
import nl.rug.digitallab.themis.judgements.labels.config.LabelConfiguration
import kotlin.time.Duration.Companion.seconds

object TestUtil {
    val succeededResponse = ActionResponse(
        result = ActionResponse.Result(
            exitCode = 1,
            resources = emptyList(),
            status = Status.SUCCEEDED,
        ),
        usage = ActionResponse.Usage(
            wallTime = 1.seconds,
            memory = 1024.B,
            disk = 1024.B,
            output = 1024.B,
            createdProcesses = 1,
            maxProcesses = 1,
            orphanedProcesses = 0,
            zombieProcesses = 0,
            diskTime = 1000,
            ioTime = 1000,
        ),
    )

    val failedResponse = ActionResponse(
        result = ActionResponse.Result(
            exitCode = 0,
            resources = emptyList(),
            status = Status.CRASHED,
        ),
        usage = ActionResponse.Usage(
            wallTime = 1.seconds,
            memory = 1024.B,
            disk = 1024.B,
            output = 1024.B,
            createdProcesses = 1,
            maxProcesses = 1,
            orphanedProcesses = 0,
            zombieProcesses = 0,
            diskTime = 1000,
            ioTime = 1000,
        ),
    )

    val simpleLabelConfig = LabelConfiguration(
        profile = RuntimeProfile(
            stages = listOf(
                RuntimeProfile.Stage(
                    name = "stage",
                    jobs = listOf(
                        LabelConfiguration.Job(
                            name = "job",
                            labelAssertions = mapOf(
                                "correct" to "actions['execute'].exitCode == 1",
                                "incorrect" to "actions['execute'].exitCode == 0",
                            ),
                        )
                    )
                )
            )
        )
    )
}
