package nl.rug.digitallab.themis.judgements.labels

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.common.events.JudgementEvent
import nl.rug.digitallab.themis.common.typealiases.ConfigurationId
import nl.rug.digitallab.themis.common.typealiases.SubmissionId
import nl.rug.digitallab.themis.judgements.labels.config.LabelConfiguration
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

@QuarkusTest
class JudgementLabelerTest {
    @Inject
    private lateinit var judgementLabeler: JudgementLabeler

    @Test
    fun `A job which has not fully finished yet does not get evaluated`() {
        val submission = JudgementEvent(
            submissionId = SubmissionId.randomUUID(),
            configurationId = ConfigurationId.randomUUID(),
            profile = RuntimeProfile(
                stages = listOf(
                    RuntimeProfile.Stage(
                        name = "stage",
                        jobs = listOf(
                            JudgementEvent.Job(
                                name = "job",
                                actions = listOf(
                                    JudgementEvent.ActionState(
                                        name = "execute",
                                        response = TestUtil.succeededResponse,
                                    ),
                                    JudgementEvent.ActionState(
                                        name = "diff",
                                        // Response is still null so the action has not finished yet
                                        response = null,
                                    ),
                                ),
                            ),
                        )
                    )
                )
            ),
        )

        val labeledSubmission = judgementLabeler.labelSubmission(
            submission,
            TestUtil.simpleLabelConfig,
        )

        assertTrue(labeledSubmission.profile.stages[0].jobs[0].labels.isEmpty())
    }

    @Test
    fun `A job with 1 action can be properly labeled by the submission labeler`() {
        val submission = JudgementEvent(
            submissionId = SubmissionId.randomUUID(),
            configurationId = ConfigurationId.randomUUID(),
            profile = RuntimeProfile(
                stages = listOf(
                    RuntimeProfile.Stage(
                        name = "stage",
                        jobs = listOf(
                            JudgementEvent.Job(
                                name = "job",
                                actions = listOf(
                                    JudgementEvent.ActionState(
                                        name = "execute",
                                        response = TestUtil.succeededResponse,
                                    ),
                                ),
                            )
                        )
                    )
                )
            ),
        )

        val labeledSubmission = judgementLabeler.labelSubmission(
            submission,
            TestUtil.simpleLabelConfig,
        )

        assertEquals(listOf("correct"), labeledSubmission.profile.stages[0].jobs[0].labels)
    }

    @Test
    fun `A job with 2 actions can be properly labeled by the submission labeler`() {
        val submission = JudgementEvent(
            submissionId = SubmissionId.randomUUID(),
            configurationId = ConfigurationId.randomUUID(),
            profile = RuntimeProfile(
                stages = listOf(
                    RuntimeProfile.Stage(
                        name = "stage",
                        jobs = listOf(
                            JudgementEvent.Job(
                                name = "job",
                                actions = listOf(
                                    JudgementEvent.ActionState(
                                        name = "execute",
                                        response = TestUtil.succeededResponse,
                                    ),
                                    JudgementEvent.ActionState(
                                        name = "diff",
                                        response = TestUtil.failedResponse,
                                    ),
                                ),
                            )
                        )
                    )
                )
            ),
        )

        val labelConfig = LabelConfiguration(
            profile = RuntimeProfile(
                stages = listOf(
                    RuntimeProfile.Stage(
                        name = "stage",
                        jobs = listOf(
                            LabelConfiguration.Job(
                                name = "job",
                                labelAssertions = mapOf(
                                    "first" to "actions['execute'].exitCode == 1 && actions['diff'].exitCode == 1",
                                    "second" to "actions['execute'].exitCode == 0 && actions['diff'].exitCode == 1",
                                    "third" to "actions['execute'].exitCode == 1 && actions['diff'].exitCode == 0",
                                    "fourth" to "actions['execute'].exitCode == 0 && actions['diff'].exitCode == 0",
                                )
                            )
                        )
                    )
                )
            )
        )

        val labeledSubmission = judgementLabeler.labelSubmission(
            submission,
            labelConfig,
        )

        assertEquals(listOf("third"), labeledSubmission.profile.stages[0].jobs[0].labels)
    }
}
