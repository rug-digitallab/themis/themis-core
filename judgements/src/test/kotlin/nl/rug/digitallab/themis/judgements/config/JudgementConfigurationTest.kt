package nl.rug.digitallab.themis.judgements.config

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.judgements.grades.config.GradeConfiguration
import nl.rug.digitallab.themis.judgements.labels.config.LabelConfiguration
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

@QuarkusTest
class JudgementConfigurationTest {
    private val config = JudgementConfiguration(
        profile = RuntimeProfile(
            stages = listOf(
                RuntimeProfile.Stage(
                    name = "stage",
                    jobs = listOf(
                        JudgementConfiguration.Job(
                            name = "job",
                            labelPoints = mapOf(
                                "correct" to 2,
                                "incorrect" to 0,
                            ),
                            labelAssertions = mapOf(
                                "correct" to "actions['execute'].exitCode == 0",
                                "incorrect" to "actions['execute'].exitCode != 0",
                            )
                        )
                    )
                )
            )
        ),
        pointCombinationExpression = "sum(points['stage'])",
    )

    @Test
    fun `A judgement configuration can be converted to a label configuration`() {
        val labelConfig = config.toLabelConfiguration()

        assertEquals(
            RuntimeProfile(
                stages = listOf(
                    RuntimeProfile.Stage(
                        name = "stage",
                        jobs = listOf(
                            LabelConfiguration.Job(
                                name = "job",
                                labelAssertions = mapOf(
                                    "correct" to "actions['execute'].exitCode == 0",
                                    "incorrect" to "actions['execute'].exitCode != 0",
                                )
                            )
                        )
                    )
                )
            ),
            labelConfig.profile
        )
    }

    @Test
    fun `A judgement configuration can be converted to a grade configuration`() {
        val gradeConfig = config.toGradeConfiguration()

        assertEquals(
            GradeConfiguration(
                pointMappings = RuntimeProfile(
                    stages = listOf(
                        RuntimeProfile.Stage(
                            name = "stage",
                            jobs = listOf(
                                GradeConfiguration.Job(
                                    name = "job",
                                    labelPoints = mapOf(
                                        "correct" to 2,
                                        "incorrect" to 0,
                                    )
                                )
                            )
                        )
                    )
                ),
                pointCombinationExpression = "sum(points['stage'])",
            ),
            gradeConfig
        )
    }
}
