package nl.rug.digitallab.themis.judgements

import io.quarkus.test.InjectMock
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.digitallab.themis.common.data.ActionResponse
import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.common.enums.Status
import nl.rug.digitallab.themis.common.events.JudgementEvent
import nl.rug.digitallab.themis.common.typealiases.ConfigurationId
import nl.rug.digitallab.themis.common.typealiases.SubmissionId
import nl.rug.digitallab.themis.expression.libraries.utils.toBigDecimal
import nl.rug.digitallab.themis.judgements.config.JudgementConfiguration
import nl.rug.digitallab.themis.judgements.persistence.repositories.JudgementRepository
import nl.rug.digitallab.themis.judgements.rest.AssignmentsRestClient
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.whenever
import kotlin.time.Duration.Companion.seconds

@QuarkusTest
class JudgementEventConsumerTest {
    @Inject
    private lateinit var consumer: JudgementEventConsumer

    @InjectMock
    @RestClient
    private lateinit var restClient: AssignmentsRestClient

    @Inject
    lateinit var repository: JudgementRepository

    @BeforeEach
    fun setup() {
        // Mock the assignments service to return a dummy configuration.
        whenever(restClient.getConfig(any())).thenReturn(
            JudgementConfiguration(
                pointCombinationExpression = "sum(points['compile'])",
                profile = RuntimeProfile(
                    stages = listOf(
                        RuntimeProfile.Stage(
                            name = "compile",
                            jobs = listOf(
                                JudgementConfiguration.Job(
                                    name = "job",
                                    labelAssertions = mapOf(
                                        "correct" to "actions['execute'].exitCode == 0",
                                    ),
                                    labelPoints = mapOf(
                                        "correct" to 5,
                                    ),
                                )
                            )
                        ),
                    )
                ),
            )
        )
    }

    @Test
    @TestTransaction
    fun `The consumer can receive a judgement event`() {
        val submissionId = SubmissionId.randomUUID()

        consumer.processGraphStateChangedEvent(
            JudgementEvent(
                submissionId = submissionId,
                configurationId = ConfigurationId.randomUUID(),
                profile = RuntimeProfile(
                    stages = listOf(
                        RuntimeProfile.Stage(
                            name = "compile",
                            jobs = listOf(
                                JudgementEvent.Job(
                                    name = "job",
                                    actions = listOf(
                                        JudgementEvent.ActionState(
                                            name = "execute",
                                            response = ActionResponse(
                                                result = ActionResponse.Result(
                                                    exitCode = 0,
                                                    status = Status.SUCCEEDED,
                                                    resources = emptyList(),
                                                ),
                                                usage = ActionResponse.Usage(
                                                    wallTime = 0.seconds,
                                                    memory = 0.B,
                                                    disk = 0.B,
                                                    output = 0.B,
                                                    createdProcesses = 0,
                                                    maxProcesses = 0,
                                                    orphanedProcesses = 0,
                                                    zombieProcesses = 0,
                                                    diskTime = 0,
                                                    ioTime = 0,
                                                ),
                                            )
                                        ),
                                    ),
                                ),
                            )
                        )
                    )
                )
            )
        )

        assertEquals(5.toBigDecimal(), repository.findByNaturalId(submissionId, 1)!!.obtainedPoints)
    }
}
