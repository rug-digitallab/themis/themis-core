package nl.rug.digitallab.themis.judgements.persistence.repositories

import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.common.typealiases.ConfigurationId
import nl.rug.digitallab.themis.common.typealiases.SubmissionId
import nl.rug.digitallab.themis.judgements.persistence.PersistenceTestUtil
import nl.rug.digitallab.themis.judgements.persistence.PersistenceTestUtil.uuid
import nl.rug.digitallab.themis.judgements.persistence.doCompare
import nl.rug.digitallab.themis.judgements.persistence.entities.JobJudgement
import nl.rug.digitallab.themis.judgements.persistence.entities.Judgement
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.time.Instant

@QuarkusTest
class JudgementRepositoryTest {
    @Inject
    lateinit var repository: JudgementRepository

    @Test
    @TestTransaction
    fun `A basic judgement can be uploaded to the database`() {
        val basicJudgement = PersistenceTestUtil.generateBasicJudgement()

        repository.create(basicJudgement)

        val foundJudgement = repository.findByNaturalId(
            submissionId = basicJudgement.submissionId,
            judgementRevision = basicJudgement.judgementRevision,
        )

        basicJudgement.doCompare(foundJudgement!!)
    }

    /**
     * Hibernate by default makes a string a varchar(255), which is not enough for the labels array.
     * To support a properly scalable list of labels, we needed to make manual changes. So we need to verify that this works.
     * This test checks if the database can deal with a long labels array.
     */
    @Test
    @TestTransaction
    fun `The database can deal with a long labels array`() {
        val basicJudgement = Judgement(
            configurationId = ConfigurationId.randomUUID(),
            submissionId = SubmissionId.randomUUID(),
            judgementTime = Instant.now(),
            obtainedPoints = 15.toBigDecimal(),
        )

        val firstJobJudgement = JobJudgement(
            judgement = basicJudgement,
            obtainedPoints = 5.toBigDecimal(),
            name = "job1",
            labels = List(1000) { "LABEL_$it" },
        )

        basicJudgement.jobs.add(firstJobJudgement)

        repository.create(basicJudgement)

        val foundJudgement = repository.findByNaturalId(
            submissionId = basicJudgement.submissionId,
            judgementRevision = basicJudgement.judgementRevision,
        )

        basicJudgement.doCompare(foundJudgement!!)
    }

    @Test
    @TestTransaction
    fun `The judgement revision is properly calculated by the database trigger`() {
        val targetUUID = SubmissionId.randomUUID()

        repository.create(generateJudgement(targetUUID))
        assertTrue{ repository.exists(targetUUID, 1) }
        assertFalse { repository.exists(targetUUID, 2) }

        repository.create(generateJudgement(targetUUID))
        assertTrue { repository.exists(targetUUID, 2) }
        assertFalse { repository.exists(targetUUID, 3) }

        repository.create(generateJudgement(targetUUID))
        assertTrue { repository.exists(targetUUID, 3) }
        assertFalse { repository.exists(targetUUID, 4) }

        repository.create(generateJudgement(targetUUID))
        assertTrue { repository.exists(targetUUID, 4) }
        assertFalse { repository.exists(targetUUID, 5) }

        repository.create(generateJudgement(targetUUID))
        assertTrue { repository.exists(targetUUID, 5) }
    }

    @Test
    @TestTransaction
    fun `Retrieving the highest judgement revision is not impacted by unrelated judgements`() {
        val targetUUID = SubmissionId.randomUUID()

        repository.create(generateJudgement(targetUUID))
        repository.create(generateJudgement(targetUUID))
        repository.create(generateJudgement(SubmissionId.randomUUID()))
        repository.create(generateJudgement(SubmissionId.randomUUID()))
        repository.create(generateJudgement(SubmissionId.randomUUID()))

        assertTrue { repository.exists(targetUUID, 1) }
        assertTrue { repository.exists(targetUUID, 2) }
        assertFalse { repository.exists(targetUUID, 3) }
    }

    /**
     * Checks for the existence of a judgement with the given ID.
     * Simplifies the testcases in this class significantly.
     */
    private fun JudgementRepository.exists(submissionId: SubmissionId, judgementRevision: Int): Boolean {
        return findByNaturalId(submissionId, judgementRevision) != null
    }

    private fun generateJudgement(submissionId: SubmissionId): Judgement {
        return Judgement(
            configurationId = uuid,
            submissionId = submissionId,
            judgementTime = Instant.now(),
            obtainedPoints = 15.toBigDecimal(),
        )
    }
}
