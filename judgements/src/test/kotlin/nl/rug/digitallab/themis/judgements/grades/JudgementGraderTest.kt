package nl.rug.digitallab.themis.judgements.grades

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.common.typealiases.SubmissionId
import nl.rug.digitallab.themis.judgements.grades.config.GradeConfiguration
import nl.rug.digitallab.themis.judgements.labels.data.LabeledJudgement
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.math.BigDecimal

@QuarkusTest
class JudgementGraderTest {
    @Inject
    lateinit var judgementGrader: JudgementGrader

    @Test
    fun `A judgement with no labels should be worth 0 points`() {
        val labeledJudgement = LabeledJudgement(
            submissionId = SubmissionId.randomUUID(),
            profile = RuntimeProfile(
                stages = listOf(
                    RuntimeProfile.Stage(
                        name = "stage-1",
                        jobs = listOf(
                            LabeledJudgement.Job(
                                name = "job-1",
                                completed = true,
                                labels = emptyList(),
                            )
                        )
                    )
                )
            )
        )

        val gradeConfiguration = GradeConfiguration(
            pointMappings = RuntimeProfile(
                stages = listOf(RuntimeProfile.Stage(
                    name = "stage-1",
                    jobs = listOf(
                        GradeConfiguration.Job(
                            name = "job-1",
                            labelPoints = mapOf(
                                "succeeded" to 2,
                                "memory-leak" to 1,
                                "failed" to 0,
                            )
                        )
                    )
                ))
            ),
            pointCombinationExpression = "sum(points['stage-1'])",
        )

        val points = judgementGrader.gradeJudgement(labeledJudgement, gradeConfiguration)

        assertEquals(BigDecimal(0), points)
    }

    @Test
    fun `A judgement with a single job should be worth the points of that job`() {
        val labeledJudgement = LabeledJudgement(
            submissionId = SubmissionId.randomUUID(),
            profile = RuntimeProfile(
                stages = listOf(
                    RuntimeProfile.Stage(
                        name = "stage-1",
                        jobs = listOf(
                            LabeledJudgement.Job(
                                name = "job-1",
                                completed = true,
                                labels = listOf("succeeded"),
                            )
                        )
                    )
                )
            )
        )

        val gradeConfiguration = GradeConfiguration(
            pointMappings = RuntimeProfile(
                stages = listOf(RuntimeProfile.Stage(
                    name = "stage-1",
                    jobs = listOf(
                        GradeConfiguration.Job(
                            name = "job-1",
                            labelPoints = mapOf(
                                "succeeded" to 2,
                                "memory-leak" to 1,
                                "failed" to 0,
                            )
                        )
                    )
                ))
            ),
            pointCombinationExpression = "sum(points['stage-1'])",
        )

        val points = judgementGrader.gradeJudgement(labeledJudgement, gradeConfiguration)

        assertEquals(BigDecimal(2), points)
    }

    @Test
    fun `A judgement with multiple jobs should be worth the sum of the points of those jobs`() {
        val labeledJudgement = LabeledJudgement(
            submissionId = SubmissionId.randomUUID(),
            profile = RuntimeProfile(
                stages = listOf(
                    RuntimeProfile.Stage(
                        name = "stage-1",
                        jobs = listOf(
                            LabeledJudgement.Job(
                                name = "job-1",
                                completed = true,
                                labels = listOf("succeeded"),
                            ),
                            LabeledJudgement.Job(
                                name = "job-2",
                                completed = true,
                                labels = listOf("memory-leak"),
                            ),
                            LabeledJudgement.Job(
                                name = "job-3",
                                completed = true,
                                labels = listOf("failed"),
                            ),
                        )
                    )
                )
            )
        )

        val gradeConfiguration = GradeConfiguration(
            pointMappings = RuntimeProfile(
                stages = listOf(RuntimeProfile.Stage(
                    name = "stage-1",
                    jobs = listOf(
                        GradeConfiguration.Job(
                            name = "job-1",
                            labelPoints = mapOf(
                                "succeeded" to 2,
                                "memory-leak" to 1,
                                "failed" to 0,
                            )
                        ),
                        GradeConfiguration.Job(
                            name = "job-2",
                            labelPoints = mapOf(
                                "succeeded" to 2,
                                "memory-leak" to 1,
                                "failed" to 0,
                            )
                        ),
                        GradeConfiguration.Job(
                            name = "job-3",
                            labelPoints = mapOf(
                                "succeeded" to 2,
                                "memory-leak" to 1,
                                "failed" to 0,
                            )
                        ),
                    )
                ))
            ),
            pointCombinationExpression = "sum(points['stage-1'])",
        )

        val points = judgementGrader.gradeJudgement(labeledJudgement, gradeConfiguration)

        assertEquals(BigDecimal(3), points)
    }

    @Test
    fun `An exception is thrown when an expression is syntactically valid, but produces no result`() {
        val labeledJudgement = LabeledJudgement(
            submissionId = SubmissionId.randomUUID(),
            profile = RuntimeProfile(
                stages = listOf(
                    RuntimeProfile.Stage(
                        name = "stage-1",
                        jobs = listOf(
                            LabeledJudgement.Job(
                                name = "job-1",
                                completed = true,
                                labels = listOf("succeeded"),
                            )
                        )
                    )
                )
            )
        )

        val gradeConfiguration = GradeConfiguration(
            pointMappings = RuntimeProfile(
                stages = listOf(RuntimeProfile.Stage(
                    name = "stage-1",
                    jobs = listOf(
                        GradeConfiguration.Job(
                            name = "job-1",
                            labelPoints = mapOf(
                                "succeeded" to 2,
                                "memory-leak" to 1,
                                "failed" to 0,
                            )
                        )
                    )
                ))
            ),
            pointCombinationExpression = "",
        )

        assertThrows<IllegalStateException> {
            judgementGrader.gradeJudgement(labeledJudgement, gradeConfiguration)
        }
    }
}
