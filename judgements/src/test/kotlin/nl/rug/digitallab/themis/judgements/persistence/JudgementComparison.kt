package nl.rug.digitallab.themis.judgements.persistence

import nl.rug.digitallab.themis.judgements.persistence.entities.Judgement
import org.junit.jupiter.api.Assertions.assertEquals

fun Judgement.doCompare(other: Judgement) {
    assertEquals(this.obtainedPoints.stripTrailingZeros(), other.obtainedPoints.stripTrailingZeros())
    assertEquals(this.submissionId, other.submissionId)
    assertEquals(this.judgementRevision, other.judgementRevision)

    assertEquals(this.jobs.size, other.jobs.size)
    this.jobs.forEach { thisJob ->
        val otherJob = this.jobs.find { it.name == thisJob.name }!!
        assertEquals(thisJob, otherJob)

        assertEquals(thisJob.name, otherJob.name)
        assertEquals(thisJob.obtainedPoints.stripTrailingZeros(), otherJob.obtainedPoints.stripTrailingZeros())
        assertEquals(thisJob.labels, otherJob.labels)

        assertEquals(thisJob.judgement, otherJob.judgement)

        assertEquals(thisJob.responses.size, otherJob.responses.size)
        thisJob.responses.forEach { thisResponse ->
            val otherResponse = otherJob.responses.find { it.name == thisResponse.name }!!

            assertEquals(thisResponse, otherResponse)

            assertEquals(thisResponse.status, otherResponse.status)
            assertEquals(thisResponse.exitCode, otherResponse.exitCode)
            assertEquals(thisResponse.name, otherResponse.name)

            assertEquals(thisResponse.metrics.wallTimeMs, otherResponse.metrics.wallTimeMs)
            assertEquals(thisResponse.metrics.memory, otherResponse.metrics.memory)
            assertEquals(thisResponse.metrics.disk, otherResponse.metrics.disk)
            assertEquals(thisResponse.metrics.output, otherResponse.metrics.output)
            assertEquals(thisResponse.metrics.createdProcesses, otherResponse.metrics.createdProcesses)
            assertEquals(thisResponse.metrics.maxProcesses, otherResponse.metrics.maxProcesses)
            assertEquals(thisResponse.metrics.orphanedProcesses, otherResponse.metrics.orphanedProcesses)
            assertEquals(thisResponse.metrics.zombieProcesses, otherResponse.metrics.zombieProcesses)
            assertEquals(thisResponse.metrics.diskTimeMs, otherResponse.metrics.diskTimeMs)
            assertEquals(thisResponse.metrics.ioTimeMs, otherResponse.metrics.ioTimeMs)

            assertEquals(thisResponse.jobJudgement, otherResponse.jobJudgement)

            assertEquals(thisResponse.resources.size, otherResponse.resources.size)
            thisResponse.resources.forEach { thisResource ->
                val otherResource = otherResponse.resources.find { it.path == thisResource.path }!!

                assertEquals(thisResource, otherResource)

                assertEquals(thisResource.path, otherResource.path)
                assertEquals(thisResource.minioPath, otherResource.minioPath)
                assertEquals(thisResource.actionJudgement, otherResource.actionJudgement)
                assertEquals(thisResource.id, otherResource.id)
            }
        }
    }
}
