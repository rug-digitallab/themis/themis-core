package nl.rug.digitallab.themis.judgements.labels

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.common.events.JudgementEvent
import nl.rug.digitallab.themis.common.typealiases.SubmissionId
import nl.rug.digitallab.themis.judgements.labels.config.LabelConfiguration
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

@QuarkusTest
class JudgementErrorTest {
    @Inject
    private lateinit var judgementLabeler: JudgementLabeler

    private val submission = JudgementEvent(
        submissionId = SubmissionId.randomUUID(),
        configurationId = SubmissionId.randomUUID(),
        profile = RuntimeProfile(
            stages = listOf(
                RuntimeProfile.Stage(
                    name = "stage",
                    jobs = listOf(
                        JudgementEvent.Job(
                            name = "job",
                            actions = listOf(
                                JudgementEvent.ActionState(
                                    name = "execute",
                                    response = TestUtil.succeededResponse,
                                ),
                            ),
                        )
                    )
                )
            )
        ),
    )

    @Test
    fun `An error label is properly attached when a judgement fails because of unparsable assertions`() {
        val labelConfig = LabelConfiguration(
            profile = RuntimeProfile(
                stages = listOf(
                    RuntimeProfile.Stage(
                        name = "stage",
                        jobs = listOf(
                            LabelConfiguration.Job(
                                name = "job",
                                labelAssertions = mapOf(
                                    "correct" to "actions['execute'].exitCode ==",
                                ),
                            )
                        )
                    ),
                )
            )
        )

        val labeledSubmission = judgementLabeler.labelSubmission(submission, labelConfig)

        assertEquals(
            listOf("JudgementFailure - Failed to parse expression \"actions['execute'].exitCode ==\""),
            labeledSubmission.profile.stages[0].jobs.first().labels,
        )
    }

    @Test
    fun `An error label is properly attached when a judgement fails because of undefined variables`() {
        val labelConfig = LabelConfiguration(
            profile = RuntimeProfile(
                stages = listOf(
                    RuntimeProfile.Stage(
                        name = "stage",
                        jobs = listOf(
                            LabelConfiguration.Job(
                                name = "job",
                                labelAssertions = mapOf(
                                    "correct" to "abcde == 1",
                                ),
                            )
                        )
                    ),
                )
            )
        )

        val labeledSubmission = judgementLabeler.labelSubmission(submission, labelConfig)

        assertEquals(
            listOf("JudgementFailure - Undefined variable 'abcde' in expression \"abcde == 1\""),
            labeledSubmission.profile.stages[0].jobs.first().labels,
        )
    }
}
