package nl.rug.digitallab.themis.judgements.persistence.entities

import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

@QuarkusTest
class LabeledJobJudgementTest {
    @Test
    fun `The job judgement converter converts a json array to a list of strings`() {
        val converter = JobJudgement.LabelsConverter()

        val result = converter.convertToEntityAttribute("[\"1\", \"2\", \"3\"]")

        assertEquals(listOf("1", "2", "3"), result)
    }

    @Test
    fun `The job judgement converter converts a list of strings to a json array`() {
        val converter = JobJudgement.LabelsConverter()

        val result = converter.convertToDatabaseColumn(listOf("1", "2", "3"))

        assertEquals("[\"1\",\"2\",\"3\"]", result)
    }
}
