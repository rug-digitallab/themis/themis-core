package nl.rug.digitallab.themis.judgements.persistence

import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.digitallab.themis.common.enums.Status
import nl.rug.digitallab.themis.judgements.persistence.entities.*
import java.time.Instant
import java.util.UUID

object PersistenceTestUtil {
    val uuid = UUID.randomUUID()

    fun generateBasicJudgement(): Judgement {
        val basicJudgement = Judgement(
            configurationId = uuid,
            submissionId = uuid,
            judgementTime = Instant.now(),
            obtainedPoints = 15.toBigDecimal(),
        )

        val firstJobJudgement = JobJudgement(
            judgement = basicJudgement,
            obtainedPoints = 5.toBigDecimal(),
            name = "job1",
            labels = listOf("FAILED", "COMPILE_ERROR"),
        )

        val firstAction = ActionJudgement(
            jobJudgement = firstJobJudgement,
            name = "action1",
            status = Status.CRASHED,
            exitCode = 1,
            metrics = ActionMetrics(
                wallTimeMs = 1000,
                memory = 1024.B,
                disk = 2048.B,
                output = 0.B,
                createdProcesses = 1,
                maxProcesses = 1,
                diskTimeMs = null,
                ioTimeMs = null,
                zombieProcesses = null,
                orphanedProcesses = null,
            ),
        )

        firstJobJudgement.responses.add(firstAction)

        val firstResource = ActionResource(
            actionJudgement = firstAction,
            path = "path/to/resource",
            minioPath = "minio://uri/to/resource",
        )

        firstAction.resources.add(firstResource)

        basicJudgement.jobs.add(firstJobJudgement)

        return basicJudgement
    }
}
