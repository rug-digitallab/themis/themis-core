
create table action_judgement (
                                  id binary(16) not null,
                                  exit_code integer not null check ((exit_code<=255) and (exit_code>=0)),
                                  created_processes integer,
                                  disk bigint,
                                  disk_time_ms integer,
                                  io_time_ms integer,
                                  max_processes integer,
                                  memory bigint,
                                  orphaned_processes integer,
                                  output bigint,
                                  wall_time_ms integer,
                                  zombie_processes integer,
                                  name varchar(255) not null,
                                  status integer not null,
                                  job_judgement_id binary(16) not null,
                                  primary key (id)
) engine=InnoDB;

create table action_resource (
                                 id binary(16) not null,
                                 minio_path varchar(4096) not null,
                                 path varchar(4096) not null,
                                 action_judgement_id binary(16) not null,
                                 primary key (id)
) engine=InnoDB;

create table job_judgement (
                               id binary(16) not null,
                               labels JSON not null,
                               name varchar(255) not null,
                               obtained_points decimal(22,12) not null,
                               judgement_id binary(16) not null,
                               primary key (id)
) engine=InnoDB;

create table judgement (
                           id binary(16) not null,
                           configuration_id binary(16) not null,
                           judgement_revision integer not null,
                           judgement_time datetime(6) not null,
                           obtained_points decimal(22,12) not null,
                           submission_id binary(16) not null,
                           primary key (id)
) engine=InnoDB;

alter table if exists action_judgement
    add constraint UK9e5rl6p9acb5r1s2icf6ywdmc unique (job_judgement_id, name);

alter table if exists job_judgement
    add constraint UKrhh369w07o4nv8efndyb2rmnx unique (judgement_id, name);

alter table if exists judgement
    add constraint UK5361i75bg87isn4wyct5jersf unique (judgement_revision, submission_id);

alter table if exists action_judgement
    add constraint FKayphfnp5ra1j25l9oihikn2ac
    foreign key (job_judgement_id)
    references job_judgement (id);

alter table if exists action_resource
    add constraint FK61ory40ipov9wdbip6ohvs0wb
    foreign key (action_judgement_id)
    references action_judgement (id);

alter table if exists job_judgement
    add constraint FKployc3sltitjj7a07nnvinbh3
    foreign key (judgement_id)
    references judgement (id);
