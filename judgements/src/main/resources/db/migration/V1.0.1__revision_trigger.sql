-- This table stores the last judgement_revision of every submission
-- Without this table, the database performance would drop dramatically as the number of judgements increases
-- Testing has shown that this easily scales to 10 million judgements without any performance issues
-- Without this table, performance became unreasonably slow at around 50k judgements
CREATE TABLE judgement_latest_revision (
                                 submission_id binary(16) NOT NULL,
                                 latest_revision INTEGER,
                                 PRIMARY KEY (submission_id)
) engine=InnoDB;

-- Automatically calculates the judgement_revision partial key so that the application does not have to worry about it.
-- Both simpler from the application perspective, less risk of race conditions, and saves a database roundtrip,
-- as the application would otherwise have to query the database to find the highest judgement_revision for a given submission_id.
CREATE TRIGGER judgement_calculate_revision
    BEFORE INSERT ON judgement
    FOR EACH ROW
BEGIN
    DECLARE latest_rev INT;

    -- First try to select and lock the existing row
    SELECT latest_revision INTO latest_rev
    FROM judgement_latest_revision
    WHERE submission_id = NEW.submission_id
    FOR UPDATE; -- Lock the row during the transaction. This will prevent any race conditions when generating the judgement_revision.

    IF latest_rev IS NULL THEN
        -- No row exists yet, insert with initial value
        INSERT INTO judgement_latest_revision (submission_id, latest_revision)
        VALUES (NEW.submission_id, 1);
        SET latest_rev = 1;
    ELSE
        -- Row exists and is locked, update it
        UPDATE judgement_latest_revision
        SET latest_revision = latest_revision + 1
        WHERE submission_id = NEW.submission_id;
        SET latest_rev = latest_rev + 1;
    END IF;

    -- Assign the value to NEW.judgement_revision
    SET NEW.judgement_revision = latest_rev;
END;
