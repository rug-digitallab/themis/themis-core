package nl.rug.digitallab.themis.judgements.labels

import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.common.events.JudgementEvent
import nl.rug.digitallab.themis.expression.ExpressionEngine
import nl.rug.digitallab.themis.expression.exceptions.EvaluatorException
import nl.rug.digitallab.themis.judgements.labels.config.LabelConfiguration
import nl.rug.digitallab.themis.judgements.labels.data.LabeledJudgement
import nl.rug.digitallab.themis.judgements.labels.data.toContext
import org.jboss.logging.Logger

/**
 * Labels judgements based on the results of the jobs.
 *
 * The labeler receives a configuration, which defines the conditions for applying a label to a job.
 * It then iterates over every Job in every Stage of the submission and applies the labels based on the configuration.
 * The conditions for applying a label are evaluated using the JEXL [ExpressionEngine].
 * The user configures the requirements for a label to be applied.
 */
@ApplicationScoped
class JudgementLabeler {
    @Inject
    private lateinit var engine: ExpressionEngine

    @Inject
    private lateinit var logger: Logger

    /**
     * Labels the submission based on the results of the jobs.
     *
     * @param submission The submission to label.
     * @param labelConfig The configuration that defines when a label is applied to a job.
     */
    @WithSpan("LABEL submission")
    fun labelSubmission(
        submission: JudgementEvent,
        labelConfig: LabelConfiguration,
    ): LabeledJudgement {
        return LabeledJudgement(
            submissionId = submission.submissionId,
            // Both the submission.profile and the labelConfig.profile contain a profile of jobs.
            // This function will iterate over all jobs in both profiles, and label each job based on the results and label configuration.
            profile = submission.profile.zipWith(labelConfig.profile, ::labelJob)
        )
    }

    /**
     * Labels a [JudgementEvent.Job]. The configuration defines a set of expressions together with labels.
     * If a label is evaluated as true, the associated label is applied to the [JudgementEvent.Job].
     *
     * @param job The [JudgementEvent.Job] to label.
     * @param labelConfig The configuration that defines when a label is applied to a job for this [RuntimeProfile.Job].
     */
    private fun labelJob(
        job: JudgementEvent.Job,
        labelConfig: LabelConfiguration.Job,
    ): LabeledJudgement.Job {
        // Only label jobs that have received responses from all actions.
        if (job.actions.any { it.response == null })
            return LabeledJudgement.Job(
                name = job.name,
                completed = false,
                labels = emptyList(),
            )

        val assertionContext = job.toContext()

        val labels = mutableListOf<String>()
        labelConfig.labelAssertions.forEach { (label, expression) ->
            try {
                if (engine.evaluator(assertionContext).evaluate(expression)) {
                    labels.add(label)
                }
            } catch (e: EvaluatorException) {
                // If an exception occurs during judging, add the exception as a label to the job for future tracking.
                logger.error("Failed to evaluate expression \"$expression\" while labeling a job", e)
                labels.add("JudgementFailure - ${e.message}")
            }
        }

        return LabeledJudgement.Job(
            name = job.name,
            completed = true,
            labels = labels,
        )
    }
}
