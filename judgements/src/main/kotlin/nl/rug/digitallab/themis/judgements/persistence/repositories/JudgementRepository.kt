package nl.rug.digitallab.themis.judgements.persistence.repositories

import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.persistence.EntityManager
import nl.rug.digitallab.common.quarkus.hibernate.orm.panache.repositories.BaseRepository
import nl.rug.digitallab.themis.common.typealiases.JudgementId
import nl.rug.digitallab.themis.common.typealiases.SubmissionId
import nl.rug.digitallab.themis.judgements.persistence.entities.Judgement
import org.hibernate.Session
import kotlin.jvm.optionals.getOrNull

/**
 * The repository abstraction for interacting with judgements in the database.
 *
 * This repository should be used to store and retrieve judgements from the database.
 * It can also be used to perform queries on the judgements in the database.
 */
@ApplicationScoped
class JudgementRepository : BaseRepository<Judgement, JudgementId>() {
    @Inject
    private lateinit var entityManager: EntityManager

    /**
     * Find a judgement by its natural ID.
     *
     * @param submissionId The ID of the submission this judgement is for.
     * @param judgementRevision The revision of the judgement.
     *
     * @return The [Judgement] entity.
     */
    fun findByNaturalId(submissionId: SubmissionId, judgementRevision: Int): Judgement? {
        return entityManager.unwrap(Session::class.java)
            .byNaturalId(Judgement::class.java)
            .using("submissionId", submissionId)
            .using("judgementRevision", judgementRevision)
            .loadOptional().getOrNull()
    }
}
