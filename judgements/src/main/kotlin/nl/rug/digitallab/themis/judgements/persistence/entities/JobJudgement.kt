package nl.rug.digitallab.themis.judgements.persistence.entities

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import jakarta.persistence.*
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import nl.rug.digitallab.themis.common.typealiases.JobJudgementId
import org.hibernate.annotations.NaturalId
import java.math.BigDecimal

/**
 * Represents a judgement of a single job of a submission.
 * It contains the labels attached to the job, the responses of the actions in the job, and the obtained points.
 *
 * Uniquely identified by the judgement it is part of, combined with the stage it represents from the assignment.
 *
 * @property judgement The judgement this [JobJudgement] is part of.
 * @property name The name of the job. Must be unique within the judgement and the same as the name of the job in the profile.
 * @property responses The responses of the actions in the job.
 * @property labels The labels attached to the job.
 * @property obtainedPoints The points obtained in this job.
 */
@Entity
class JobJudgement (
    @field:NaturalId
    @field:ManyToOne(fetch = FetchType.LAZY)
    @field:NotNull
    val judgement: Judgement,

    @field:NaturalId
    @field:NotBlank
    val name: String,

    @field:OneToMany(mappedBy = "jobJudgement", cascade = [CascadeType.ALL], orphanRemoval = true, fetch = FetchType.EAGER)
    val responses: MutableSet<ActionJudgement> = mutableSetOf(),

    // JSON array of labels
    @field:NotNull
    @field:Column(columnDefinition = "JSON")
    @field:Convert(converter = LabelsConverter::class)
    val labels: List<String>,

    @field:NotNull
    @field:Column(precision=22, scale=12)
    val obtainedPoints: BigDecimal,
) {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    lateinit var id: JobJudgementId

    /**
     * To reduce the amount of entities in the database, we convert the list of labels to a JSON array before storing it.
     */
    @Converter(autoApply = true)
    class LabelsConverter: AttributeConverter<List<String>, String> {
        private val objectMapper = ObjectMapper()

        /**
         * Converts a list of labels to a JSON array.
         *
         * @param attribute The list of labels to convert.
         *
         * @return The JSON array representation of the list of labels.
         */
        override fun convertToDatabaseColumn(attribute: List<String>): String {
            return objectMapper.writeValueAsString(attribute)
        }

        /**
         * Converts a JSON array to a list of labels.
         *
         * @param dbData The JSON array to convert.
         *
         * @return The list of labels represented by the JSON array.
         */
        override fun convertToEntityAttribute(dbData: String): List<String> {
            return objectMapper.readValue<List<String>>(dbData)
        }
    }
}
