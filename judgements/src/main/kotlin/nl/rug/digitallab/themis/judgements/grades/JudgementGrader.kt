package nl.rug.digitallab.themis.judgements.grades

import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.themis.expression.ExpressionEngine
import nl.rug.digitallab.themis.judgements.grades.config.GradeConfiguration
import nl.rug.digitallab.themis.judgements.grades.data.GradeJudgementContext
import nl.rug.digitallab.themis.judgements.grades.data.GradedJudgement
import nl.rug.digitallab.themis.judgements.labels.data.LabeledJudgement
import java.math.BigDecimal

/**
 * The [JudgementGrader] is responsible for grading a submission and assigning a certain amount of points to it.
 */
@ApplicationScoped
class JudgementGrader {
    @Inject
    private lateinit var engine: ExpressionEngine

    /**
     * Grade a whole judgement based on the grading configuration, produced results and labels.
     *
     * @param labeledJudgement The judgement that needs to be graded. Contains both execution results and labels.
     * @param gradeConfiguration The configuration that defines how to grade the judgement.
     *
     * @return The amount of points that the judgement is worth.
     */
    @WithSpan("GRADE judgement")
    fun gradeJudgement(labeledJudgement: LabeledJudgement, gradeConfiguration: GradeConfiguration): BigDecimal {
        // Both the labeledJudgement and the gradeConfiguration contain a profile of jobs.
        // This function will iterate over all jobs in both profiles, and grade each job based on the labels and grading configuration.
        val gradedJudgement = labeledJudgement.profile.zipWith(gradeConfiguration.pointMappings, ::gradeJob)
        val pointsMap = gradedJudgement.associateBy { it.points }

        val expressionContext = object : GradeJudgementContext {
            override val points = pointsMap
        }

        val result: BigDecimal? = engine.evaluator(expressionContext)
            .evaluate(gradeConfiguration.pointCombinationExpression)

        checkNotNull(result) {
            "Failed to generate a grade using the provided expression.\n" +
            "Expression: ${gradeConfiguration.pointCombinationExpression}\n" +
            "Provided points: ${expressionContext.points}"
        }

        return result
    }

    /**
     * Grade a job based on the grading configuration, produced results and labels.
     *
     * @param labeledJob The job that needs to be graded. Contains both execution results and labels.
     * @param jobGrading The configuration that defines how to grade this specific job based on the labels.
     */
    private fun gradeJob(labeledJob: LabeledJudgement.Job, jobGrading: GradeConfiguration.Job): GradedJudgement.Job {
        return GradedJudgement.Job(
            name = labeledJob.name,
            points = labeledJob.labels.sumOf { jobGrading.labelPoints[it] ?: 0 }.toBigDecimal()
        )
    }

}
