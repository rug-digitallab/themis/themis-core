package nl.rug.digitallab.themis.judgements.persistence.entities

import jakarta.persistence.*
import jakarta.validation.constraints.DecimalMin
import jakarta.validation.constraints.NotNull
import nl.rug.digitallab.themis.common.typealiases.ConfigurationId
import nl.rug.digitallab.themis.common.typealiases.JudgementId
import nl.rug.digitallab.themis.common.typealiases.SubmissionId
import org.hibernate.annotations.NaturalId
import java.math.BigDecimal
import java.time.Instant

/**
 * Represents a judgement of a submission.
 * It contains the jobs that were judged, the points obtained, and the time of judgement.
 *
 * Uniquely identified by the submission it is part of, combined with an incrementing integer that represents the revision of the judgement.
 * For example, if a submission gets rejudged, the revision will be incremented by 1 (so judgement 2 of the submission)
 *
 * @property submissionId The ID of the submission this judgement is part of.
 * @property judgementRevision The revision of the judgement. Starts at 1, increments by 1 for every new judgement made for the submission.
 * @property configurationId The ID of the assignment configuration that was used to judge the submission.
 * @property jobs The results of judging the jobs of the submission.
 * @property obtainedPoints The points obtained in this judgement.
 * @property judgementTime The time the judgement was made.
 */
@Entity
class Judgement(
    @field:NaturalId
    @field:NotNull
    val submissionId: SubmissionId,

    // Reference to the assignment configuration that was used to judge the submission
    @field:NotNull
    val configurationId: ConfigurationId,

    @field:OneToMany(mappedBy = "judgement", cascade = [CascadeType.ALL], orphanRemoval = true, fetch = FetchType.EAGER)
    val jobs: MutableSet<JobJudgement> = mutableSetOf(),

    @field:NotNull
    @field:Column(precision=22, scale=12)
    val obtainedPoints: BigDecimal,

    @field:NotNull
    val judgementTime: Instant,
) {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    lateinit var id: JudgementId

    // The judgement revision is automatically calculated by a database trigger, the 1 is just a placeholder
    // See V1.0.1__revision_trigger.sql in the flyway migrations for more information.
    @NaturalId
    @DecimalMin("1")
    @NotNull
    val judgementRevision: Int = 1
}
