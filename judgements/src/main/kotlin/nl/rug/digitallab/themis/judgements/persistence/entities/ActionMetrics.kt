package nl.rug.digitallab.themis.judgements.persistence.entities

import jakarta.persistence.Convert
import jakarta.persistence.Embeddable
import jakarta.validation.constraints.DecimalMin
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.common.kotlin.quantities.integrations.jakarta.ByteSizeConverter

/**
 * Represents the metrics of the execution of an action.
 *
 * @property wallTimeMs How long the action took to execute, in milliseconds.
 * @property memory The amount of memory used by the action.
 * @property disk The amount of disk space used by the action.
 * @property output The amount of output produced by the action.
 * @property createdProcesses The number of processes created in total by the action.
 * @property maxProcesses The maximum number of processes created by the action.
 * @property orphanedProcesses The number of orphaned processes created by the action.
 * @property zombieProcesses The number of zombie processes created by the action.
 * @property diskTimeMs The time the action spent waiting for disk I/O, in milliseconds.
 * @property ioTimeMs The time the action spent performing I/O operations, in milliseconds.
 */
@Embeddable
class ActionMetrics(
    @field:DecimalMin("0")
    val wallTimeMs: Int?,

    @field:DecimalMin("0")
    @field:Convert(converter = ByteSizeConverter::class)
    val memory: ByteSize?,

    @field:DecimalMin("0")
    @field:Convert(converter = ByteSizeConverter::class)
    val disk: ByteSize?,

    @field:DecimalMin("0")
    @field:Convert(converter = ByteSizeConverter::class)
    val output: ByteSize?,

    @field:DecimalMin("0")
    val createdProcesses: Int?,

    @field:DecimalMin("0")
    val maxProcesses: Int?,

    @field:DecimalMin("0")
    val orphanedProcesses: Int?,

    @field:DecimalMin("0")
    val zombieProcesses: Int?,

    @field:DecimalMin("0")
    val diskTimeMs: Int?,

    @field:DecimalMin("0")
    val ioTimeMs: Int?,
)
