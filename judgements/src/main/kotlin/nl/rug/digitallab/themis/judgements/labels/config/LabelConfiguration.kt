package nl.rug.digitallab.themis.judgements.labels.config

import nl.rug.digitallab.themis.common.data.RuntimeProfile

/**
 * Represents the assertions that define which labels are applied to a job of a submission.
 *
 * @property profile The profile that contains the jobs that can be labeled.
 */
data class LabelConfiguration(
    val profile: RuntimeProfile<Job>,
) {
    /**
     * Contains the assertions that define which labels to apply to this job.
     *
     * @param name The name of the job.
     * @property labelAssertions The assertions that define when a label is applied to this job.
     * The key is the label, while the value is the associated expression
     */
    data class Job(
        override val name: String,
        var labelAssertions: Map<String, String>,
    ): RuntimeProfile.Job
}
