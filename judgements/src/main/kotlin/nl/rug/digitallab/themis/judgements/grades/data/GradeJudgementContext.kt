package nl.rug.digitallab.themis.judgements.grades.data

import nl.rug.digitallab.themis.expression.contexts.Context
import java.math.BigDecimal

/**
 * The JEXL expression context for determining the amount of points of a judgement.
 * This is based on the points assigned to individual jobs.
 *
 * @property points The points that are assigned to the jobs in the judgement.
 * The first key is the stage name, the second key is the job name.
 */
interface GradeJudgementContext : Context {
    val points: Map<String, Map<String, BigDecimal>>
}
