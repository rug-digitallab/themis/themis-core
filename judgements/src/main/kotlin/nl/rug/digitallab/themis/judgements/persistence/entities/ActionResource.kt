package nl.rug.digitallab.themis.judgements.persistence.entities

import jakarta.persistence.*
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import nl.rug.digitallab.themis.common.typealiases.ActionResourceId

/**
 * Represents a resource that was used in the execution of an action.
 * Examples of resources are files, streams, logs, etc.
 *
 * Uniquely identified by the path of the resource together with the action judgement it is part of.
 *
 * @property path The path of the resource in the action's execution environment. Unique within the action.
 * @property actionJudgement The action judgement this resource is part of.
 * @property minioPath The path of the resource in the MinIO storage.
 */
@Entity
class ActionResource(
    @field:Column(length = 4096)
    @field:NotBlank
    val path: String,

    @field:ManyToOne(fetch = FetchType.LAZY)
    @field:NotNull
    val actionJudgement: ActionJudgement,

    @field:Column(length = 4096)
    @field:NotBlank
    val minioPath: String,
) {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    lateinit var id: ActionResourceId
}
