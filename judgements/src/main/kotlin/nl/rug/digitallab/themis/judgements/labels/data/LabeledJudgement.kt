package nl.rug.digitallab.themis.judgements.labels.data

import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.common.typealiases.SubmissionId

/**
 * Represents a judgement which has jobs that can be labeled.
 *
 * @property submissionId The ID of the submission.
 * @property profile The profile that represents the content of the judgement.
 */
data class LabeledJudgement(
    val submissionId: SubmissionId,
    val profile: RuntimeProfile<Job>,
) {
    /**
     * Contains the labels that are attached to this job.
     *
     * @param name The name of the job.
     * @param completed Whether the job has fully completed running all its actions.
     * @property labels The labels that are applied to the job.
     */
    data class Job(
        override val name: String,
        val completed: Boolean,
        var labels: List<String> = emptyList(),
    ): RuntimeProfile.Job
}
