package nl.rug.digitallab.themis.judgements.common

import nl.rug.digitallab.themis.common.enums.Status

/**
 * The context of a single action within a stage.
 * This is used by both the label engine and the grading engine.
 *
 * @property exitCode The exit code of the action.
 * @property status The status of the action.
 */
data class ActionContext(
    val exitCode: Int,
    val status: Status,
)
