package nl.rug.digitallab.themis.judgements.labels.data

import nl.rug.digitallab.themis.common.events.JudgementEvent
import nl.rug.digitallab.themis.expression.contexts.Context
import nl.rug.digitallab.themis.judgements.common.ActionContext
import nl.rug.digitallab.themis.judgements.labels.data.LabeledJudgement.Job

/**
 * The context used by the expression engine to evaluate which labels should be applied to a job.
 *
 * @property actions Map of actions in this job, indexed by action name.
 */
interface LabelJobContext : Context {
    val actions: Map<String, ActionContext>
}

/**
 * Converts a [Job] to a [LabelJobContext].
 *
 * @receiver The [Job] to convert.
 *
 * @return The [LabelJobContext] that can be used to label a [Job] using the expression engine.
 */
fun JudgementEvent.Job.toContext(): LabelJobContext {
    val jobActions = actions.associate { action ->
        require(action.response != null) {
            "An action that is set for judgement must have a response."
        }

        action.name to ActionContext(
            exitCode = action.response!!.result.exitCode,
            status = action.response!!.result.status,
        )
    }

    return object : LabelJobContext {
        override val actions: Map<String, ActionContext> = jobActions
    }
}
