package nl.rug.digitallab.themis.judgements.grades.data

import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.common.typealiases.SubmissionId
import java.math.BigDecimal

/**
 * Represents a judgement with the number of points attached to each job.
 *
 * @property submissionId The ID of the submission.
 * @property profile The profile that represents the content of the judgement, including the points assigned to each job.
 */
data class GradedJudgement(
    val submissionId: SubmissionId,
    val profile: RuntimeProfile<Job>,
) {
    /**
     * Represents a job with the number of points attached to it.
     *
     * @param name The name of the job.
     * @param points The number of points awarded to this job
     */
    data class Job(
        override val name: String,
        val points: BigDecimal,
    ): RuntimeProfile.Job
}
