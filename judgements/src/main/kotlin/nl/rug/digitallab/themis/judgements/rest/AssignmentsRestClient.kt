package nl.rug.digitallab.themis.judgements.rest

import jakarta.ws.rs.GET
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import nl.rug.digitallab.themis.common.typealiases.ConfigurationId
import nl.rug.digitallab.themis.judgements.config.JudgementConfiguration
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient

/**
 * A client to interact with the assignments service.
 */
@Path("/api/v1/themis/")
@RegisterRestClient(configKey="assignments-api")
interface AssignmentsRestClient {
    /**
     * Get a specific assignment configuration by its synthetic ID.
     *
     * @param configId The id of the assignment configuration.
     *
     * @return The full configuration together with its metadata.
     */
    @GET
    @Path("configurations/{configId}")
    fun getConfig(@PathParam("configId") configId: ConfigurationId): JudgementConfiguration
}
