package nl.rug.digitallab.themis.judgements.config

import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.judgements.grades.config.GradeConfiguration
import nl.rug.digitallab.themis.judgements.labels.config.LabelConfiguration

/**
 * The full configuration for judging a submission.
 * This configuration is a subset of the full profile configuration used in Themis.
 *
 * This configuration contains the data used for both labelling and grading of submissions.
 *
 * @property profile The profile that holds judgement configuration data for individual jobs.
 * @property pointCombinationExpression The JEXL expression that describes how to combine the points of individual jobs into a single grade.
 */
data class JudgementConfiguration(
    val profile: RuntimeProfile<Job>,
    val pointCombinationExpression: String,
) {
    /**
     * Holds all the configuration data for judging a single job within a submission.
     *
     * @param name The name of the job.
     * @property labelAssertions A map of assertions to labels, where a label is applied to a job if the assertion is satisfied.
     * @property labelPoints A map of labels to points, where a job receives a certain amount of points if it has a certain label.
     */
    data class Job(
        override val name: String,
        var labelAssertions: Map<String, String>,
        val labelPoints: Map<String, Int>,
    ): RuntimeProfile.Job

    /**
     * Convert this configuration to a [LabelConfiguration] that only contains the labelling data.
     * The resulting configuration is used to label judgements.
     *
     * @return The configuration that only contains the labelling data.
     */
    fun toLabelConfiguration(): LabelConfiguration {
        return LabelConfiguration(
            profile = RuntimeProfile(
                stages = profile.stages.map { stage ->
                    RuntimeProfile.Stage(
                        name = stage.name,
                        jobs = stage.jobs.map { job ->
                            LabelConfiguration.Job(
                                name = job.name,
                                labelAssertions = job.labelAssertions
                            )
                        }
                    )
                }
            )
        )
    }

    /**
     * Convert this configuration to a [GradeConfiguration] that only contains the grading data.
     * The resulting configuration is used to grade labeled judgements.
     *
     * @return The configuration that only contains the grading data.
     */
    fun toGradeConfiguration(): GradeConfiguration {
        return GradeConfiguration(
            pointMappings = RuntimeProfile(
                stages = profile.stages.map { stage ->
                    RuntimeProfile.Stage(
                        name = stage.name,
                        jobs = stage.jobs.map { job ->
                            GradeConfiguration.Job(
                                name = job.name,
                                labelPoints = job.labelPoints
                            )
                        }
                    )
                }
            ),
            pointCombinationExpression = pointCombinationExpression
        )
    }
}
