package nl.rug.digitallab.themis.judgements

import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.themis.common.constants.JUDGEMENTS_QUEUE
import nl.rug.digitallab.themis.common.events.JudgementEvent
import nl.rug.digitallab.themis.judgements.grades.JudgementGrader
import nl.rug.digitallab.themis.judgements.labels.JudgementLabeler
import nl.rug.digitallab.themis.judgements.persistence.entities.Judgement
import nl.rug.digitallab.themis.judgements.persistence.repositories.JudgementRepository
import nl.rug.digitallab.themis.judgements.rest.AssignmentsRestClient
import org.eclipse.microprofile.reactive.messaging.Incoming
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.jboss.logging.Logger
import java.time.Instant

/**
 * The main entry point for the judgement service.
 *
 * The judgement service listens for updates from the runtime orchestrator over the message queue.
 *
 * A judgement of a submission has the following steps:
 * 1. Attach labels to the jobs in the submission.
 * 2. Attach grades to the jobs in the submission and the submission itself.
 */
@ApplicationScoped
class JudgementEventConsumer {
    @Inject
    private lateinit var judgementLabeler: JudgementLabeler

    @Inject
    private lateinit var judgementGrader: JudgementGrader

    @Inject
    private lateinit var judgementRepository: JudgementRepository

    @RestClient
    private lateinit var assignmentsClient: AssignmentsRestClient

    @Inject
    private lateinit var logger: Logger

    /**
     * Processes the [JudgementEvent] that is emitted by the runtime orchestrator.
     *
     * @param event The event that is emitted by the runtime orchestrator.
     */
    @Incoming(JUDGEMENTS_QUEUE)
    fun processGraphStateChangedEvent(event: JudgementEvent) {
        logger.info("Received event: $event")

        // Step 1: Retrieve the judgement configuration from the assignments service
        val config = assignmentsClient.getConfig(event.configurationId)

        // Step 2: Label the submission
        val labeledJudgement = judgementLabeler.labelSubmission(
            event,
            config.toLabelConfiguration(),
        )

        // Step 3: Grade the submission
        val grade = judgementGrader.gradeJudgement(
            labeledJudgement,
            config.toGradeConfiguration(),
        )

        // Step 4: Create the Judgement entity
        val judgement = Judgement(
            judgementTime = Instant.now(),
            obtainedPoints = grade,
            submissionId = event.submissionId,
            configurationId = event.configurationId,
        )

        // Step 5: Store the result asynchronously
        judgementRepository.create(judgement)
    }
}
