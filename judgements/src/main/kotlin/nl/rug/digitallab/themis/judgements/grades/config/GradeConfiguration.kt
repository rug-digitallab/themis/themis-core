package nl.rug.digitallab.themis.judgements.grades.config

import nl.rug.digitallab.themis.common.data.RuntimeProfile

/**
 * The configuration for a judgement that defines how to grade a judgement.
 *
 * @property pointMappings The mapping of labels to points to use for assigning points to jobs. There is a separate mapping for every job.
 * @property pointCombinationExpression The expression that defines how to combine the points of the jobs into a judgement grade.
 */
data class GradeConfiguration(
    val pointMappings: RuntimeProfile<Job>,
    val pointCombinationExpression: String,
) {
    /**
     * Contains the mapping of labels to points for a job.
     *
     * @param name The name of the job.
     * @property labelPoints The mapping of labels to points for this job.
     */
    data class Job(
        override val name: String,
        val labelPoints: Map<String, Int>,
    ): RuntimeProfile.Job
}
