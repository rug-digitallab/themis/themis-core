package nl.rug.digitallab.themis.judgements.persistence.entities

import jakarta.persistence.*
import jakarta.validation.constraints.Max
import jakarta.validation.constraints.Min
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import nl.rug.digitallab.themis.common.enums.Status
import nl.rug.digitallab.themis.common.typealiases.ActionJudgementId
import org.hibernate.annotations.NaturalId

/**
 * Represents the judgement of a single action in a stage.
 *
 * Uniquely identified by the stage it is part of and the action within the stage that it represents.
 *
 * @property jobJudgement The [JobJudgement] this [ActionJudgement] is part of.
 * @property name The name of the action. Must be unique within the job and the same as the name of the action in the profile.
 * @property exitCode The exit code of the action.
 * @property status The status of the action.
 * @property resources The resources produced by the action (e.g. files, logs, streams).
 * @property metrics The metrics of the action (e.g. wall time, memory usage).
 */
@Entity
class ActionJudgement(
    @field:NaturalId
    @field:ManyToOne(fetch = FetchType.LAZY)
    @field:NotNull
    val jobJudgement: JobJudgement,

    @field:NaturalId
    @field:NotBlank
    val name: String,

    @field:Min(0)
    @field:Max(255)
    @field:NotNull
    val exitCode: Int,

    @field:NotNull
    val status: Status,

    @field:OneToMany(mappedBy = "actionJudgement", cascade = [CascadeType.ALL], orphanRemoval = true, fetch = FetchType.EAGER)
    val resources: MutableSet<ActionResource> = mutableSetOf(),

    @field:Embedded
    val metrics: ActionMetrics
) {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    lateinit var id: ActionJudgementId

    /**
     * Converts a [Status] to and from an integer. This is necessary because JPA does not know how to serialize
     * and deserialize [Status] enum values with a custom field. By default, JPA uses the ordinal value of the
     * enum, which is not what we want.
     */
    @Converter(autoApply = true)
    class SubmissionStatusConverter: AttributeConverter<Status, Int> {
        override fun convertToDatabaseColumn(attribute: Status): Int = attribute.code
        override fun convertToEntityAttribute(dbData: Int): Status = Status.entries.find { it.code == dbData }
            ?: throw IllegalArgumentException("Attempted to convert an action status with an unknown code: $dbData")
    }
}
