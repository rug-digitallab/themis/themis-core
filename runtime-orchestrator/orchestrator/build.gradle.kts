plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.library")
}

dependencies {
    val communicationSpecVersion: String by project
    val kotlinMockitoVersion: String by project
    val quarkusMinioVersion: String by project

    // Communication Spec
    implementation("nl.rug.digitallab.themis.judgement:communication-spec:$communicationSpecVersion")
    // MinIO Object Store
    implementation("io.quarkiverse.minio:quarkus-minio:$quarkusMinioVersion")
    // Spans
    implementation("io.quarkus:quarkus-opentelemetry")

    // Relevant submodules from themis-core
    implementation(project(":libraries:common-themis"))
    // Themis' assertions library
    implementation(project(":libraries:expression-evaluator"))

    testImplementation("org.mockito.kotlin:mockito-kotlin:$kotlinMockitoVersion")
    testImplementation("io.quarkus:quarkus-junit5-mockito")
}
