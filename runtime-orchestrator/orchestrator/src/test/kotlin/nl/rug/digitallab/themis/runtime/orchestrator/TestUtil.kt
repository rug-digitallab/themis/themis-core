package nl.rug.digitallab.themis.runtime.orchestrator

import nl.rug.digitallab.themis.common.data.ActionResponse
import nl.rug.digitallab.themis.runtime.orchestrator.graph.nodes.ActionNode
import nl.rug.protospec.judgement.v1.RunActionRequest

object TestUtil {
    fun generateSimpleNode(id: String): ActionNode {
        // We don't want the response to be shared between various instances.
        // This object ensures that each node instance has its own response.
        val responseContainer = object {
            var response: ActionResponse? = null
        }

        return ActionNode(
            id = id,
            action = RunActionRequest.getDefaultInstance(),
            runtime = "container-runtime",
            responseDelegate = responseContainer::response,
        )
    }
}
