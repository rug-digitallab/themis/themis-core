package nl.rug.digitallab.themis.runtime.orchestrator.orchestrator

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.themis.runtime.orchestrator.TestUtil.generateSimpleNode
import nl.rug.digitallab.themis.runtime.orchestrator.graph.RuntimeGraph
import nl.rug.digitallab.themis.runtime.orchestrator.runtime.ActionRunner
import nl.rug.protospec.judgement.v1.RunActionResponse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.kotlin.*

@QuarkusTest
class OrchestratorParallelTest {
    @Inject
    private lateinit var executor: Orchestrator

    @InjectMock
    private lateinit var mockedClient: ActionRunner

    /**
     * Mock the gRPC client such that we do not need to send actual gRPC requests to a runtime.
     */
    @BeforeEach
    fun setupMockedClient(): Unit = runBlocking {
        whenever(mockedClient.runAction(any(), any())).then {
            Thread.sleep(500)
            return@then RunActionResponse.getDefaultInstance()
        }
    }

    @Test
    fun `A parallel pipeline is executed in parallel and not in series`(): Unit = runBlocking {
        val startTime = System.currentTimeMillis()
        with(OrchestrationContext(mock())) {
            executor.executeGraph(generateParallelPipeline(), mapOf())
        }
        val endTime = System.currentTimeMillis()

        verify(mockedClient, times(10)).runAction(any(), any())

        assertTrue(endTime - startTime < 3000)
    }

    private fun generateParallelPipeline(): RuntimeGraph {
        val runtimeGraph = RuntimeGraph()

        val firstAction = generateSimpleNode("first_action")
        val secondAction = generateSimpleNode("second_action")
        val thirdAction = generateSimpleNode("third_action")
        val fourthAction = generateSimpleNode("fourth_action")
        val fifthAction = generateSimpleNode("fifth_action")
        val sixthAction = generateSimpleNode("sixth_action")
        val seventhAction = generateSimpleNode("seventh_action")
        val eighthAction = generateSimpleNode("eighth_action")
        val ninthAction = generateSimpleNode("ninth_action")
        val tenthAction = generateSimpleNode("tenth_action")

        RuntimeGraph.addDependency(runtimeGraph.rootNode, firstAction)
        RuntimeGraph.addDependency(runtimeGraph.rootNode, secondAction)
        RuntimeGraph.addDependency(runtimeGraph.rootNode, thirdAction)
        RuntimeGraph.addDependency(runtimeGraph.rootNode, fourthAction)
        RuntimeGraph.addDependency(runtimeGraph.rootNode, fifthAction)
        RuntimeGraph.addDependency(runtimeGraph.rootNode, sixthAction)
        RuntimeGraph.addDependency(runtimeGraph.rootNode, seventhAction)
        RuntimeGraph.addDependency(runtimeGraph.rootNode, eighthAction)
        RuntimeGraph.addDependency(runtimeGraph.rootNode, ninthAction)
        RuntimeGraph.addDependency(runtimeGraph.rootNode, tenthAction)

        return runtimeGraph
    }
}
