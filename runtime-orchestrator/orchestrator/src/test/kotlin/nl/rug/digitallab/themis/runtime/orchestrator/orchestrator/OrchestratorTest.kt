package nl.rug.digitallab.themis.runtime.orchestrator.orchestrator

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.themis.runtime.orchestrator.TestUtil.generateSimpleNode
import nl.rug.digitallab.themis.runtime.orchestrator.graph.RuntimeGraph
import nl.rug.digitallab.themis.runtime.orchestrator.graph.nodes.EpsilonNode
import nl.rug.digitallab.themis.runtime.orchestrator.runtime.ActionRunner
import nl.rug.protospec.judgement.v1.RunActionResponse
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.mockito.kotlin.*

/**
 * Tests for the [Orchestrator].
 */
@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class OrchestratorTest {
    @Inject
    private lateinit var executor: Orchestrator

    @InjectMock
    private lateinit var mockedClient: ActionRunner

    /**
     * Mock the gRPC client such that we do not need to send actual gRPC requests to a runtime.
     */
    @BeforeEach
    fun setupMockedClient(): Unit = runBlocking {
        whenever(mockedClient.runAction(any(), any())).thenReturn(RunActionResponse.getDefaultInstance())
    }

    @Test
    fun `Executing a simple linear pipeline of 3 ActionNodes, all 3 nodes should be executed`(): Unit = runBlocking {
        with(OrchestrationContext(mock())) {
            executor.executeGraph(generateLinearPipeline(), mapOf())
        }

        verify(mockedClient, times(3)).runAction(any(), any())
    }

    @Test
    fun `Executing a simple parallel pipeline of 4 ActionNodes, all 4 nodes should be executed`(): Unit = runBlocking {
        with(OrchestrationContext(mock())) {
            executor.executeGraph(generateParallelPipeline(), mapOf())
        }

        verify(mockedClient, times(4)).runAction(any(), any())
    }

    @Test
    fun `BlankNodes can connect ActionNodes together`(): Unit = runBlocking {
        with(OrchestrationContext(mock())) {
            executor.executeGraph(generatePipelineWithBlank(), mapOf())
        }

        verify(mockedClient, times(6)).runAction(any(), any())
    }

    /**
     * Generates a simple linear pipeline with three actions.
     */
    private fun generateLinearPipeline(): RuntimeGraph {
        val runtimeGraph = RuntimeGraph()

        val firstAction = generateSimpleNode("first_action")
        val secondAction = generateSimpleNode("second_action")
        val thirdAction = generateSimpleNode("third_action")

        // Link the nodes together
        RuntimeGraph.addDependency(runtimeGraph.rootNode, firstAction)
        RuntimeGraph.addDependency(firstAction, secondAction)
        RuntimeGraph.addDependency(secondAction, thirdAction)

        return runtimeGraph
    }

    /**
     * Generates a parallel pipeline with five nodes
     * The first nodes is the root node.
     * The second, third and fourth nodes are executed in parallel as the first, second and third action.
     * The fourth action is executed after the first, second and third action have completed.
     */
    private fun generateParallelPipeline(): RuntimeGraph {
        val runtimeGraph = RuntimeGraph()

        val firstAction = generateSimpleNode("first_action")
        val secondAction = generateSimpleNode("second_action")
        val thirdAction = generateSimpleNode("third_action")
        val fourthAction = generateSimpleNode("fourth_action")

        RuntimeGraph.addDependency(runtimeGraph.rootNode, firstAction)
        RuntimeGraph.addDependency(runtimeGraph.rootNode, secondAction)
        RuntimeGraph.addDependency(runtimeGraph.rootNode, thirdAction)

        RuntimeGraph.addDependency(firstAction, fourthAction)
        RuntimeGraph.addDependency(secondAction, fourthAction)
        RuntimeGraph.addDependency(thirdAction, fourthAction)

        return runtimeGraph
    }

    private fun generatePipelineWithBlank(): RuntimeGraph {
        val runtimeGraph = RuntimeGraph()

        val firstAction = generateSimpleNode("first_action")
        val secondAction = generateSimpleNode("second_action")
        val thirdAction = generateSimpleNode("third_action")

        val fourthAction = generateSimpleNode("fourth_action")
        val fifthAction = generateSimpleNode("fifth_action")
        val sixthAction = generateSimpleNode("sixth_action")

        val epsilonNode = EpsilonNode("blank_node")

        RuntimeGraph.addDependency(runtimeGraph.rootNode, firstAction)
        RuntimeGraph.addDependency(runtimeGraph.rootNode, secondAction)
        RuntimeGraph.addDependency(runtimeGraph.rootNode, thirdAction)

        RuntimeGraph.addDependency(firstAction, epsilonNode)
        RuntimeGraph.addDependency(secondAction, epsilonNode)
        RuntimeGraph.addDependency(thirdAction, epsilonNode)

        RuntimeGraph.addDependency(epsilonNode, fourthAction)
        RuntimeGraph.addDependency(epsilonNode, fifthAction)
        RuntimeGraph.addDependency(epsilonNode, sixthAction)

        return runtimeGraph
    }
}
