package nl.rug.digitallab.themis.runtime.orchestrator.orchestrator

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.themis.common.data.ActionResponse
import nl.rug.digitallab.themis.common.enums.ActionStatus
import nl.rug.digitallab.themis.runtime.orchestrator.graph.nodes.ActionNode
import nl.rug.digitallab.themis.runtime.orchestrator.persistence.repositories.ResourceObjectRepository
import nl.rug.digitallab.themis.runtime.orchestrator.runtime.ActionRunner
import nl.rug.protospec.judgement.v1.*
import nl.rug.protospec.judgement.v1.extensions.uri
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.net.URI

@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class AssertionsTest {
    @Inject
    private lateinit var orchestrator: Orchestrator

    @InjectMock
    private lateinit var mockedClient: ActionRunner

    @InjectMock
    private lateinit var objectRepository: ResourceObjectRepository

    private var response: ActionResponse? = null

    @BeforeEach
    fun setup(): Unit = runBlocking {
        whenever(objectRepository.uploadResource(any(), any(), any())).thenReturn(mock())

        // We only want to mock the uploadResource method to avoid uploading files to MinIO. So enable the real method for the rest.
        whenever(objectRepository.uploadActionResources(any(), any(), any())).thenCallRealMethod()

        whenever(mockedClient.runAction(any(), any())).thenReturn(
            runActionResponse {
                result = executionResult {
                    outputs.add(
                        output {
                            resource = resource {
                                uri = URI("file:test.txt")
                            }
                            isPresent = false
                        }
                    )
                }
            }
        )
    }

    @Test
    fun `A failing assertion should lead to the node being marked as FAILED`(): Unit = runBlocking {
        val node = ActionNode(
            id = "test",
            action = RunActionRequest.getDefaultInstance(),
            runtime = "container-runtime",
            assertion = "exitCode == 1",
            responseDelegate = ::response,
        )

        with(OrchestrationContext(mock())) {
            orchestrator.tryExecuteNode(node)
        }

        assertEquals(ActionStatus.FAILED, node.state)
    }

    @Test
    fun `A successful assertion should lead to the node being marked as SUCCEEDED`(): Unit = runBlocking {
        val node = ActionNode(
            id = "test",
            action = RunActionRequest.getDefaultInstance(),
            runtime = "container-runtime",
            assertion = "exitCode == 0",
            responseDelegate = ::response,
        )

        with(OrchestrationContext(mock())) {
            orchestrator.tryExecuteNode(node)
        }

        assertEquals(ActionStatus.SUCCEEDED, node.state)
    }

    @Test
    fun `The orchestrator can evaluate expressions with statuses`(): Unit = runBlocking {
        val node = ActionNode(
            id = "test",
            action = RunActionRequest.getDefaultInstance(),
            runtime = "container-runtime",
            assertion = "status == Status.SUCCEEDED",
            responseDelegate = ::response,
        )

        with(OrchestrationContext(mock())) {
            orchestrator.tryExecuteNode(node)
        }

        assertEquals(ActionStatus.SUCCEEDED, node.state)
    }

    @Test
    fun `The orchestrator can evaluate resources`(): Unit = runBlocking {
        val correctNode = ActionNode(
            id = "test",
            action = RunActionRequest.getDefaultInstance(),
            runtime = "container-runtime",
            assertion = "resources['file:test.txt'].isPresent == false",
            responseDelegate = ::response,
        )

        val incorrectNode = ActionNode(
            id = "test",
            action = RunActionRequest.getDefaultInstance(),
            runtime = "container-runtime",
            assertion = "resources['file:test.txt'].isPresent == true",
            responseDelegate = ::response,
        )

        with(OrchestrationContext(mock())) {
            orchestrator.tryExecuteNode(correctNode)
            orchestrator.tryExecuteNode(incorrectNode)
        }

        assertEquals(ActionStatus.SUCCEEDED, correctNode.state)
        assertEquals(ActionStatus.FAILED, incorrectNode.state)
    }
}
