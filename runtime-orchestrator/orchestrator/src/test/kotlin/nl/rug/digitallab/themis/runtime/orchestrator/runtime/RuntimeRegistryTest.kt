package nl.rug.digitallab.themis.runtime.orchestrator.runtime

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows

@QuarkusTest
class RuntimeRegistryTest {
    @Inject
    private lateinit var registry: RuntimeRegistry

    @Test
    fun `Retrieving a valid runtime from the registry does not throw an error`() {
        assertDoesNotThrow {
            registry.getClient("container-runtime")
        }
    }

    @Test
    fun `Retrieving an invalid runtime from the registry throws an error`() {
        assertThrows<NoSuchElementException> {
            registry.getClient("invalid-runtime")
        }
    }
}
