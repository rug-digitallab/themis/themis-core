package nl.rug.digitallab.themis.runtime.orchestrator.orchestrator

import com.google.protobuf.ByteString
import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.themis.common.data.ActionResponse
import nl.rug.digitallab.themis.runtime.orchestrator.graph.GraphResourceReference
import nl.rug.digitallab.themis.runtime.orchestrator.graph.RuntimeGraph
import nl.rug.digitallab.themis.runtime.orchestrator.graph.nodes.ActionNode
import nl.rug.digitallab.themis.runtime.orchestrator.persistence.repositories.ResourceObjectRepository
import nl.rug.digitallab.themis.runtime.orchestrator.runtime.ActionRunner
import nl.rug.protospec.judgement.v1.*
import nl.rug.protospec.judgement.v1.extensions.uri
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.mockito.kotlin.*
import java.net.URI

@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class InputOutputTest {
    @Inject
    private lateinit var executor: Orchestrator

    @InjectMock
    private lateinit var objectRepository: ResourceObjectRepository

    @InjectMock
    private lateinit var mockedClient: ActionRunner

    private var response: ActionResponse? = null

    @BeforeEach
    fun setupMockedClient() {
        whenever(objectRepository.uploadResource(any(), any(), any())).thenReturn(mock())

        // We only want to mock the uploadResource method to avoid uploading files to MinIO. So enable the real method for the rest.
        whenever(objectRepository.uploadActionResources(any(), any(), any())).thenCallRealMethod()
    }

    @Test
    fun `The orchestrator can access global variables requested by nodes`(): Unit = runBlocking {
        whenever(mockedClient.runAction(any(), any())).then {
            val request = it.getArgument<RunActionRequest>(1)

            Assertions.assertEquals("test1", request.inputsList.find { input ->
                input.uri == URI("file:input1")
            }?.contents?.toStringUtf8())
            Assertions.assertEquals("test2", request.inputsList.find { input ->
                input.uri == URI("file:input2")
            }?.contents?.toStringUtf8())

            return@then RunActionResponse.getDefaultInstance()
        }

        // Set up the global files
        val globalFiles = HashMap<GraphResourceReference.Root, Resource>()
        globalFiles[GraphResourceReference.Root(URI("file:input1"))] = resource {
            uri = URI("file:input1")
            contents = ByteString.copyFrom("test1".toByteArray())
        }
        globalFiles[GraphResourceReference.Root(URI("file:input2"))] = resource {
            uri = URI("file:input2")
            contents = ByteString.copyFrom("test2".toByteArray())
        }

        with(OrchestrationContext(mock())) {
            executor.executeGraph(generateGlobalVariablesTestGraph(), globalFiles)
        }
        verify(mockedClient, times(2)).runAction(any(), any())
    }

    @Test
    fun `The orchestrator can pass around local variables between nodes`(): Unit = runBlocking {
        whenever(mockedClient.runAction(any(), any())).then {
            val request = it.getArgument<RunActionRequest>(1)

            // Only check the second action for whether it received the correct files
            if(request.commandList[0] == "id2") {
                Assertions.assertEquals("test1", request.inputsList.find { input ->
                    input.uri == URI("file:file1")
                }?.contents?.toStringUtf8())
                Assertions.assertEquals("test2", request.inputsList.find { input ->
                    input.uri == URI("file:file2")
                }?.contents?.toStringUtf8())
            }

            return@then runActionResponse {
                result = executionResult {
                    outputs += output {
                        resource = resource {
                            uri = URI("file:file1")
                            contents = ByteString.copyFrom("test1".toByteArray())
                        }
                    }
                    outputs += output {
                        resource = resource {
                            uri = URI("file:file2")
                            contents = ByteString.copyFrom("test2".toByteArray())
                        }
                    }
                }
            }
        }

        with(OrchestrationContext(mock())) {
            executor.executeGraph(generateLocalVariablesTestGraph(), emptyMap())
        }
        verify(mockedClient, times(2)).runAction(any(), any())
    }

    @Test
    fun `Throws an error if a requested file from another node does not exist`(): Unit = runBlocking {
        whenever(mockedClient.runAction(any(), any())).thenReturn(RunActionResponse.getDefaultInstance())

        assertThrows(IllegalStateException::class.java) {
            runBlocking {
                with(OrchestrationContext(mock())) {
                    executor.executeGraph(generateGraphWithBadNodeInputs(), emptyMap())
                }
            }
        }
    }

    @Test
    fun `Throws an error if a root file does not exist`(): Unit = runBlocking {
        whenever(mockedClient.runAction(any(), any())).thenReturn(RunActionResponse.getDefaultInstance())

        assertThrows(IllegalStateException::class.java) {
            runBlocking {
                with(OrchestrationContext(mock())) {
                    executor.executeGraph(generateGraphWithBadRootInputs(), emptyMap())
                }
            }
        }
    }

    private fun generateGraphWithBadNodeInputs(): RuntimeGraph {
        val runtimeGraph = RuntimeGraph()

        val firstAction = ActionNode(
            id = "first_action",
            // Abuse the command field to identify the action that calls the mocked client
            action = runActionRequest { command += "id1" },
            runtime = "container-runtime",
            responseDelegate = ::response,
        )

        val secondAction = ActionNode(
            id = "second_action",
            // Abuse the command field to identify the action that calls the mocked client
            action = runActionRequest { command += "id2" },
            runtime = "container-runtime",
            inputs = setOf(
                GraphResourceReference.Node("first_action", URI("file:file1")),
            ),
            responseDelegate = ::response,
        )

        RuntimeGraph.addDependency(runtimeGraph.rootNode, firstAction)
        RuntimeGraph.addDependency(firstAction, secondAction)

        return runtimeGraph
    }

    private fun generateGraphWithBadRootInputs(): RuntimeGraph {
        val runtimeGraph = RuntimeGraph()

        val firstAction = ActionNode(
            id = "first_action",
            // Abuse the command field to identify the action that calls the mocked client
            action = runActionRequest { command += "id1" },
            runtime = "container-runtime",
            responseDelegate = ::response,
            inputs = setOf(
                GraphResourceReference.Root(URI("file:file1")),
            ),
        )

        RuntimeGraph.addDependency(runtimeGraph.rootNode, firstAction)

        return runtimeGraph
    }

    private fun generateLocalVariablesTestGraph(): RuntimeGraph {
        val runtimeGraph = RuntimeGraph()

        val firstAction = ActionNode(
            id = "first_action",
            // Abuse the command field to identify the action that calls the mocked client
            action = runActionRequest { command += "id1" },
            runtime = "container-runtime",
            outputs = setOf(
                "file1",
                "file2",
            ),
            responseDelegate = ::response,
        )

        val secondAction = ActionNode(
            id = "second_action",
            // Abuse the command field to identify the action that calls the mocked client
            action = runActionRequest { command += "id2" },
            runtime = "container-runtime",
            inputs = setOf(
                GraphResourceReference.Node("first_action", URI("file:file1")),
                GraphResourceReference.Node("first_action", URI("file:file2")),
            ),
            responseDelegate = ::response,
        )

        RuntimeGraph.addDependency(runtimeGraph.rootNode, firstAction)
        RuntimeGraph.addDependency(firstAction, secondAction)

        return runtimeGraph
    }

    /**
     * Generates a simple parallel pipeline with two actions.
     * It the graph expects two global files to be present.
     */
    private fun generateGlobalVariablesTestGraph(): RuntimeGraph {
        val runtimeGraph = RuntimeGraph()

        val firstAction = ActionNode(
            id = "first_action",
            action = RunActionRequest.getDefaultInstance(),
            runtime = "container-runtime",
            inputs = setOf(
                GraphResourceReference.Root(URI("file:input1")),
                GraphResourceReference.Root(URI("file:input2")),
            ),
            responseDelegate = ::response,
        )

        val secondAction = ActionNode(
            id = "second_action",
            action = RunActionRequest.getDefaultInstance(),
            runtime = "container-runtime",
            inputs = setOf(
                GraphResourceReference.Root(URI("file:input1")),
                GraphResourceReference.Root(URI("file:input2")),
            ),
            responseDelegate = ::response,
        )

        RuntimeGraph.addDependency(runtimeGraph.rootNode, firstAction)
        RuntimeGraph.addDependency(firstAction, secondAction)

        return runtimeGraph
    }
}
