package nl.rug.digitallab.themis.runtime.orchestrator.runtime

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import io.smallrye.mutiny.uni
import jakarta.inject.Inject
import kotlinx.coroutines.runBlocking
import nl.rug.protospec.judgement.v1.ActionService
import nl.rug.protospec.judgement.v1.RunActionResponse
import nl.rug.protospec.judgement.v1.executionLimits
import nl.rug.protospec.judgement.v1.runActionRequest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.mockito.kotlin.any
import org.mockito.kotlin.whenever

@QuarkusTest
class ActionRunnerTest {
    @Inject
    private lateinit var client: ActionRunner

    @InjectMock
    @io.quarkus.grpc.GrpcClient("container-runtime")
    private lateinit var mockedRunner: ActionService

    @Test
    fun `The gRPC client can handle a basic request`() {
        whenever(mockedRunner.runAction(any())).thenReturn (
            uni {
                RunActionResponse.getDefaultInstance()
            }
        )

        runBlocking {
            assertDoesNotThrow { client.runAction(
                runtime = "container-runtime",
                request = runActionRequest {
                    limits = executionLimits {
                        wallTimeMs = 1
                    }
                },
            ) }
        }
    }
}
