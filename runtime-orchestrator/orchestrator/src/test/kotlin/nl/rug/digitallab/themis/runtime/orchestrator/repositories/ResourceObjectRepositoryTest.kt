package nl.rug.digitallab.themis.runtime.orchestrator.repositories

import com.google.protobuf.kotlin.toByteStringUtf8
import io.minio.GetObjectArgs
import io.minio.MakeBucketArgs
import io.minio.MinioClient
import io.minio.errors.ErrorResponseException
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.common.typealiases.*
import nl.rug.digitallab.themis.runtime.orchestrator.persistence.namespaces.RunNamespace
import nl.rug.digitallab.themis.runtime.orchestrator.persistence.repositories.ResourceObjectRepository
import nl.rug.protospec.judgement.v1.extensions.uri
import nl.rug.protospec.judgement.v1.resource
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.net.URI
import java.util.*

@QuarkusTest
class ResourceObjectRepositoryTest {
    @Inject
    private lateinit var resourceRepository: ResourceObjectRepository

    @Inject
    private lateinit var minioClient: MinioClient

    private val courseInstanceId: UUID = CourseInstanceId.randomUUID()

    val resource = resource {
        uri = URI("file:/temp/resource/file.txt")
        contents = "Hello, World!".toByteStringUtf8()
    }

    @BeforeEach
    fun `Set up MinIO client`() {
        val makeBucketArgs = MakeBucketArgs.Builder()
            .bucket(courseInstanceId.toString())
            .objectLock(true) // Enforces Write Once Read Many (WORM) on the bucket
            .build()

        minioClient.makeBucket(makeBucketArgs)
    }

    @Test
    fun `A resource should be uploaded to the correct bucket and path`() {
        val runNamespace = RunNamespace(
            courseInstanceId,
            groupId = GroupId.randomUUID(),
            assignmentId = AssignmentId.randomUUID(),
            runId = RunId.randomUUID(),
            submissionId = SubmissionId.randomUUID(),
        )

        val objectWriteResponse = resourceRepository.uploadResource(resource, runNamespace, "compile")

        assertEquals(runNamespace.bucketName, objectWriteResponse.bucket())
        // Double slash because the first slash is the action-path delimiter, and the second is because the path starts with /
        // We cannot really avoid this, because resources can be "/a/b/c.txt", but also just "stdin"
        assertEquals("${runNamespace.runPath}/compile/file//temp/resource/file.txt", objectWriteResponse.`object`())
    }

    @Test
    fun `An uploaded resource should retain its content`() {
        val runNamespace = RunNamespace(
            courseInstanceId = courseInstanceId,
            groupId = GroupId.randomUUID(),
            assignmentId = AssignmentId.randomUUID(),
            runId = RunId.randomUUID(),
            submissionId = SubmissionId.randomUUID(),
        )

        resourceRepository.uploadResource(resource, runNamespace, "test")

        val getArgs = GetObjectArgs.builder()
            .bucket(courseInstanceId.toString())
            .`object`("${runNamespace.runPath}/test/file//temp/resource/file.txt")
            .build()

        minioClient.getObject(getArgs).use { stream ->
            val content = stream.readBytes()

            assertEquals("Hello, World!", String(content))
        }
    }

    @Test
    fun `An IOException is thrown when an upload fails`() {
        val runNamespace = RunNamespace(
            // Random course instance, so no bucket was created for it, which will cause the upload to fail
            courseInstanceId = CourseInstanceId.randomUUID(),
            groupId = GroupId.randomUUID(),
            assignmentId = AssignmentId.randomUUID(),
            runId = RunId.randomUUID(),
            submissionId = SubmissionId.randomUUID(),
        )

        val resource = resource {
            uri = URI("file:/temp/resource/file.txt")
            contents = "Hello, World!".toByteStringUtf8()
        }

        assertThrows<ErrorResponseException> {
            resourceRepository.uploadResource(resource, runNamespace, "compile")
        }
    }
}
