package nl.rug.digitallab.themis.runtime.orchestrator.orchestrator

import com.google.protobuf.ByteString
import io.minio.GetObjectArgs
import io.minio.MakeBucketArgs
import io.minio.MinioClient
import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.themis.common.typealiases.*
import nl.rug.digitallab.themis.runtime.orchestrator.TestUtil.generateSimpleNode
import nl.rug.digitallab.themis.runtime.orchestrator.graph.RuntimeGraph
import nl.rug.digitallab.themis.runtime.orchestrator.persistence.namespaces.RunNamespace
import nl.rug.digitallab.themis.runtime.orchestrator.runtime.ActionRunner
import nl.rug.protospec.judgement.v1.executionResult
import nl.rug.protospec.judgement.v1.extensions.uri
import nl.rug.protospec.judgement.v1.output
import nl.rug.protospec.judgement.v1.resource
import nl.rug.protospec.judgement.v1.runActionResponse
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.whenever
import java.net.URI
import java.util.*

@QuarkusTest
class ResourcesTest {
    @InjectMock
    private lateinit var mockedClient: ActionRunner

    @Inject
    private lateinit var orchestrator: Orchestrator

    @Inject
    private lateinit var minioClient: MinioClient

    private val courseInstanceId: UUID = CourseInstanceId.randomUUID()

    @BeforeEach
    fun `Set up MinIO client`() {
        val makeBucketArgs = MakeBucketArgs.Builder()
            .bucket(courseInstanceId.toString())
            .objectLock(true) // Enforces Write Once Read Many (WORM) on the bucket
            .build()

        minioClient.makeBucket(makeBucketArgs)
    }

    @BeforeEach
    fun setupMockedClient(): Unit = runBlocking {
        whenever(mockedClient.runAction(any(), any())).thenReturn(
            runActionResponse {
                result = executionResult {
                    outputs.add(
                        output {
                            isPresent = true
                            exceededSize = false
                            resource = resource {
                                uri = URI("file:/tmp/output.txt")
                                contents = ByteString.copyFromUtf8("This is the contents of a txt file.")
                            }
                        }
                    )
                }
            }
        )
    }

    @Test
    fun `A resource that was received from a runtime gets properly saved in MinIO by the orchestrator`(): Unit = runBlocking {
        val graph = RuntimeGraph()
        val node = generateSimpleNode("compile")
        RuntimeGraph.addDependency(graph.rootNode, node)

        val runNamespace = RunNamespace(
            courseInstanceId = courseInstanceId,
            groupId = GroupId.randomUUID(),
            assignmentId = AssignmentId.randomUUID(),
            runId = RunId.randomUUID(),
            submissionId = SubmissionId.randomUUID(),
        )

        with(OrchestrationContext(namespace = runNamespace)) {
            orchestrator.executeGraph(graph, emptyMap())
        }

        val getArgs = GetObjectArgs.builder()
            // We could have easily gotten the bucket and object from the namespace.
            // However, getting this from the response also ensures that the MinIO uri is properly saved to the response object.
            .bucket(node.response!!.result.resources[0].minioPath!!.split(":")[0])
            .`object`(node.response!!.result.resources[0].minioPath!!.split(":")[1])
            .build()

        val getResponse = minioClient.getObject(getArgs)
        getResponse.use { stream ->
            val content = stream.readBytes()

            assertEquals("This is the contents of a txt file.", String(content))
        }

        assertEquals(runNamespace.bucketName, getResponse.bucket())
        assertEquals("${runNamespace.runPath}/compile/file//tmp/output.txt", getResponse.`object`())
    }
}
