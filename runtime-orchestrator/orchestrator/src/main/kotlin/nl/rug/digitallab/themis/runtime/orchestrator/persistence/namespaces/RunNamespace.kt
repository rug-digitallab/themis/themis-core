package nl.rug.digitallab.themis.runtime.orchestrator.persistence.namespaces

import nl.rug.digitallab.themis.common.typealiases.*

/**
 * Represents the namespace of a pipeline which was run within MinIO:
 * /{courseInstanceId}/{groupId}/{assignmentId}/run/{submissionId}/{runId}
 *
 * @param courseInstanceId The id of the course instance the submission is for.
 * @param groupId The id of the group that the submission is for.
 * @param assignmentId The id of the assignment the submission is for.
 * @param submissionId The id of the submission.
 * @param runId The id of the pipeline that was run by the runtime orchestrator.
 */
open class RunNamespace(
    courseInstanceId: CourseInstanceId,
    groupId: GroupId,
    assignmentId: AssignmentId,
    submissionId: SubmissionId,
    runId: RunId,
) {
    val bucketName: String = courseInstanceId.toString()
    val runPath = "$groupId/$assignmentId/$submissionId/$runId"
}
