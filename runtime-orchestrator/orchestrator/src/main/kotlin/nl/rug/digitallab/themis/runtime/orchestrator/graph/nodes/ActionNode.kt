package nl.rug.digitallab.themis.runtime.orchestrator.graph.nodes

import nl.rug.digitallab.themis.common.data.ActionResponse
import nl.rug.digitallab.themis.runtime.orchestrator.graph.GraphResourceReference
import nl.rug.protospec.judgement.v1.RunActionRequest
import kotlin.reflect.KMutableProperty0

/**
 * Implements an action node in the judgement graph.
 *
 * An action node represents an action that should be executed on one of the runtime environments
 * (like for example the companion container)
 * This action will be sent over a gRPC connection to the runtime environment.
 *
 * The node includes the following information:
 * - All details about the action to execute.
 * - Expected inputs of the action (which are retrieved either globally or from the previous node this node depends on).
 * - Outputs of the action (which are available to any node that depends on this node)
 * - Parent nodes this node depends on.
 * - Child nodes that depend on this node.
 *
 * In order to provide the full flexibility of an acyclic graph, we need both forward and backward references.
 * Forward references are used for traversing the graph.
 * Backward references are used for checking if all parent nodes have completed before executing the action.
 *
 * @param id The identifier of the action. Mainly used for logging purposes. Should be human-readable and unique.
 * @property runtime The runtime environment to execute the action on.
 * @property action The action to execute.
 * @property response The response from the runtime environment after the action has been executed.
 *
 * @property inputs The resources required by the action. The key is a reference to the file within an arbitrary node within the graph.
 * @property outputs The resources produced by the action. The key is a reference to the file from the action object.
 *
 * @property assertion The structured assertion that should be checked after the action has been executed to determine if the child nodes should be executed.
 * This assertion should follow JEXL syntax (https://commons.apache.org/proper/commons-jexl/reference/syntax.html).
 * If null, the child nodes will always be executed.
 *
 * @param responseDelegate A delegate to hook into the response of the action. You can use this to automatically update other classes when the response is updated by the orchestrator in the graph.
 * Any property that is passed as this delegate will be updated by the orchestrator when the response is updated.
 */
class ActionNode (
    id: String,
    val action: RunActionRequest,
    val runtime: String,

    val inputs: Set<GraphResourceReference> = emptySet(),
    val outputs: Set<String> = emptySet(),

    val assertion: String? = null,

    responseDelegate: ResponseDelegate
): Node(id) {
    var response: ActionResponse? by responseDelegate
}

typealias ResponseDelegate = KMutableProperty0<ActionResponse?>
