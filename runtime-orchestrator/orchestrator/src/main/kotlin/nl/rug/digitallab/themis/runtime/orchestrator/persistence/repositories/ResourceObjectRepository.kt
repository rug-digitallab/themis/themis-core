package nl.rug.digitallab.themis.runtime.orchestrator.persistence.repositories

import io.minio.MinioClient
import io.minio.ObjectWriteResponse
import io.minio.PutObjectArgs
import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.themis.runtime.orchestrator.persistence.namespaces.RunNamespace
import nl.rug.protospec.judgement.v1.Output
import nl.rug.protospec.judgement.v1.Resource
import nl.rug.protospec.judgement.v1.extensions.uri
import org.jboss.logging.Logger
import java.net.URI

/**
 * Repository for interacting with resources in MinIO.
 */
@ApplicationScoped
class ResourceObjectRepository {
    @Inject
    private lateinit var minioClient: MinioClient

    @Inject
    private lateinit var log: Logger

    /**
     * Uploads the resources produced by an action to MinIO.
     *
     * @param outputs The outputs produced by the action. They contain the file content in-memory.
     * @param namespace The namespace of the run within MinIO.
     * @param actionName The name of the action that produced the resources.
     *
     * @return A map of MinIO paths to the resources. The key is the resource path (Unique ID of the resource), and the value is the MinIO path.
     */
    fun uploadActionResources(
        outputs: List<Output>,
        namespace: RunNamespace,
        actionName: String,
    ): Map<URI, String> {
        return outputs.map {output ->
            val uploadResponse = uploadResource(output.resource, namespace, actionName)
            return@map output.resource.uri to "${uploadResponse.bucket()}:${uploadResponse.`object`()}"
        }.toMap()
    }

    /**
     * Uploads a single resource to MinIO.
     *
     * @param resource The resource to upload.
     * @param namespace The namespace of the run within MinIO that the resource falls under.
     * @param actionName The name of the action that produced the resource.
     *
     * @return The response from MinIO.
     */
    @WithSpan("UPLOAD a resource to MinIO")
    fun uploadResource(
        resource: Resource,
        namespace: RunNamespace,
        actionName: String,
    ): ObjectWriteResponse {
        val uploadPath = "${namespace.runPath}/${actionName}/${resource.uri.scheme}/${resource.uri.schemeSpecificPart}"

        log.debug("Starting upload resource '$uploadPath' to MinIO bucket '${namespace.bucketName}'")

        val uploadArgs = PutObjectArgs.Builder()
            .bucket(namespace.bucketName)
            .`object`(uploadPath)
            .stream(resource.contents.newInput(), resource.contents.size().toLong(), -1)
            .build()

        // Upload the submission to MinIO
        val response = minioClient.putObject(uploadArgs)

        log.info("Resource '$uploadPath' uploaded to MinIO bucket '${namespace.bucketName}'")

        return response
    }
}
