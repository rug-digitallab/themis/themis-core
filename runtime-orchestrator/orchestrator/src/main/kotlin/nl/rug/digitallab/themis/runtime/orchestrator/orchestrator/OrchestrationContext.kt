package nl.rug.digitallab.themis.runtime.orchestrator.orchestrator

import nl.rug.digitallab.themis.runtime.orchestrator.graph.GraphResourceReference
import nl.rug.digitallab.themis.runtime.orchestrator.persistence.namespaces.RunNamespace
import nl.rug.protospec.judgement.v1.Resource


/**
 * This class holds the context of an orchestration. Any data located here is freely available to orchestration steps without parameter passing.
 *
 * @property resources The resources that are passed between nodes. Identified by the node that produced the file and the URI of the file.
 *                     This is used to pass the resources between nodes, to implement inputs and outputs of nodes.
 *
 * @property namespace The namespace within which the resources produced by the actions are stored. Should be unique per pipeline.
 *                     Used by MinIO to store the resources.
 */
data class OrchestrationContext(
    val namespace: RunNamespace,
) {
    val resources: MutableMap<GraphResourceReference, Resource> = mutableMapOf()
}
