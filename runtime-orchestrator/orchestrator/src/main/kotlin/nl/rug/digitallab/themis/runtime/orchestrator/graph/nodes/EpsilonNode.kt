package nl.rug.digitallab.themis.runtime.orchestrator.graph.nodes

/**
 * An [EpsilonNode] is a [Node] that has no action associated with it.
 * It is used to represent a connecting node, connecting a group of parents to a group of children.
 * The idea is that this can reduce the amount of edges in the graph, even if the node does not do anything.
 *
 * @property id The unique identifier of the node.
 */
open class EpsilonNode(
    id: String,
): Node(
    id = id,
)
