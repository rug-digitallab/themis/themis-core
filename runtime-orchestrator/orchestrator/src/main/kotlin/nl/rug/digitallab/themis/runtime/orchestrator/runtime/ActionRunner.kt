package nl.rug.digitallab.themis.runtime.orchestrator.runtime

import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.protospec.judgement.v1.RunActionRequest
import nl.rug.protospec.judgement.v1.RunActionResponse
import nl.rug.protospec.judgement.v1.extensions.wallTime
import kotlin.time.toJavaDuration

/**
 * Runs an action on a runtime.
 */
@ApplicationScoped
class ActionRunner {
    @Inject
    lateinit var runtimeRegistry: RuntimeRegistry

    /**
     * Runs an action on a runtime.
     *
     * @param runtime The runtime to run the action on.
     * @param request The request to run against the runtime.
     *
     * @return The response from the runtime.
     */
    suspend fun runAction(runtime: String, request: RunActionRequest): RunActionResponse {
        val client = runtimeRegistry.getClient(runtime)
        return client.runAction(request)
            .await()
            // The atMost is a pure safety measure in case the runtime hangs indefinitely.
            // This reduces the chance of indefinite resource usage. The runtime itself should have its own limits.
            // This should only be triggered if something went really wrong, therefore the time is set to 100 times the wall time.
            .atMost((request.limits.wallTime * 100).toJavaDuration())
    }
}
