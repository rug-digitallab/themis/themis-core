package nl.rug.digitallab.themis.runtime.orchestrator.orchestrator

import nl.rug.digitallab.themis.common.enums.Status
import nl.rug.digitallab.themis.expression.contexts.Context

/**
 * A context for evaluations of shouldContinue in the runtime orchestrator.
 *
 * @property exitCode The exit code of the action.
 * @property status The status of the action.
 * @property resources The files that were produced by the action.
 */
interface ShouldContinueContext : Context {
    val exitCode: Int
    val status: Status
    val resources: Map<String, ActionResources>
}

/**
 * Representation of a file retrieved from the response. Used in shouldContinue expressions.
 * Within shouldContinue, it is only relevant whether a file is present or not.
 * The contents of the file are therefore not made available to the expressions.
 *
 * @param isPresent Whether the file is present.
 */
data class ActionResources(
    val isPresent: Boolean,
)
