package nl.rug.digitallab.themis.runtime.orchestrator.orchestrator

import jakarta.enterprise.context.ApplicationScoped
import nl.rug.digitallab.themis.common.data.ActionResponse
import nl.rug.digitallab.themis.common.enums.Status
import nl.rug.protospec.judgement.v1.*
import nl.rug.protospec.judgement.v1.extensions.*
import java.net.URI

/**
 * Converter for conversion of [ActionResponse] with other types.
 */
@ApplicationScoped
class ActionResponseConverter {
    /**
     * Converts protobuf's RunActionResponse to common-themis's ActionResponse.
     *
     * @param response The RunActionResponse to convert.
     * @param minioPaths The URIs of the resources produced by the action.
     *                   The key is the resource URI (Unique ID of the resource), and the value is the MinIO path.
     *
     * @return The converted ActionResponse.
     */
    fun toActionResponse(response: RunActionResponse, minioPaths: Map<URI, String>): ActionResponse {
        return ActionResponse(
            result = ActionResponse.Result(
                exitCode = response.result.exitCode,
                status = response.result.status.toActionStatus(),
                resources = response.result.outputsList.map {
                    ActionResponse.Result.ThemisResource(
                        uri = it.resource.uri,
                        isPresent = it.isPresent,
                        minioPath = minioPaths[it.resource.uri]
                            ?: error("MinIO path not found for resource ${it.resource.uri}"),
                    )
                },
            ),
            usage = ActionResponse.Usage(
                wallTime = response.metrics.takeIf { it.hasWallTimeMs() }?.wallTime,
                memory = response.metrics.takeIf { it.hasMemoryBytes() }?.memory,
                disk = response.metrics.takeIf { it.hasDiskBytes() }?.disk,
                output = response.metrics.takeIf { it.hasOutputBytes() }?.output,
                createdProcesses = response.metrics.takeIf { it.hasCreatedProcesses() }?.createdProcesses,
                maxProcesses = response.metrics.takeIf { it.hasMaxProcesses() }?.maxProcesses,
                diskTime = response.metrics.takeIf { it.hasDiskTime() }?.diskTime,
                ioTime = response.metrics.takeIf { it.hasIoTime() }?.ioTime,
                orphanedProcesses = response.metrics.takeIf { it.hasOrphanedProcesses() }?.orphanedProcesses,
                zombieProcesses = response.metrics.takeIf { it.hasZombieProcesses() }?.zombieProcesses
            ),
        )
    }

    /**
     * Convert's common-themis's ActionResponse to the context usable in a ShouldContinue expression.
     *
     * @param response The [ActionResponse] to convert.
     *
     * @return The ShouldContinueContext equivalent of the ActionResponse.
     */
    fun toShouldContinueContext(response: ActionResponse): ShouldContinueContext {
        return object : ShouldContinueContext {
            override val exitCode = response.result.exitCode
            override val status = response.result.status
            override val resources = response.result.resources.associate {
                it.uri.toString() to ActionResources(
                    it.isPresent
                )
            }
        }
    }

    /**
     * Converts the protobuf [ExecutionStatus] to an [Status] for use in evaluation.
     *
     * @receiver The [ExecutionStatus] to convert.
     *
     * @return The [Status] equivalent of the [ExecutionStatus].
     */
    fun ExecutionStatus.toActionStatus() = Status.valueOf(name)
}
