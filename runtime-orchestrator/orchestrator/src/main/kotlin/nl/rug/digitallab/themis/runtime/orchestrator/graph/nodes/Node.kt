package nl.rug.digitallab.themis.runtime.orchestrator.graph.nodes

import nl.rug.digitallab.themis.common.enums.ActionStatus
import java.lang.ref.WeakReference

/**
 * This class is the base class for all elements in the judgement graph.
 * This includes:
 * - RootNode
 * - EpsilonNode
 * - ActionNode (non-root node)
 *
 * @property id The unique identifier of the node.
 * @property childNodes The nodes that are called after this node.
 * @property parentNodes The nodes that are called prior to this node.
 *
 * @property state The state of the node. This is used to track the execution status within the graph
 */
abstract class Node(
    val id: String,
    val childNodes: MutableSet<Node> = mutableSetOf(),
    val parentNodes: MutableSet<WeakReference<Node>> = mutableSetOf(),
) {
    var state = ActionStatus.PENDING
}
