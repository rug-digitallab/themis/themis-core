package nl.rug.digitallab.themis.runtime.orchestrator.graph.nodes

import nl.rug.digitallab.themis.common.enums.ActionStatus

/**
 * The root node of an action graph is essentially a no-op node. It will not run an action, it simply calls its neighbours.
 * The identifier of the root node is always "root".
 */
class RootNode: EpsilonNode(
    id = "root",
) {
    init {
        state = ActionStatus.SUCCEEDED
    }
}
