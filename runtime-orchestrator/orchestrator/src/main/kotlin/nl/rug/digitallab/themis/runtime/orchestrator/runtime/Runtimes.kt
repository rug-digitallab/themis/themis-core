package nl.rug.digitallab.themis.runtime.orchestrator.runtime

/**
 * The IDs of the available runtimes the orchestrator can send requests to.
 */
object RuntimeId {
    const val CONTAINER = "container-runtime"
    const val EXECUTABLE = "executable-runtime"
}

/**
 * Enum container the available runtimes.
 */
enum class Runtime(val id: String) {
    CONTAINER(RuntimeId.CONTAINER),
    EXECUTABLE(RuntimeId.EXECUTABLE),
}
