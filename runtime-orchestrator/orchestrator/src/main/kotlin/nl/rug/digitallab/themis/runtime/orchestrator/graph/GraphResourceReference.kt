package nl.rug.digitallab.themis.runtime.orchestrator.graph

import java.net.URI

/**
 * A reference of a resource within a graph.
 *
 * @see RuntimeGraph for an explanation as to what a graph is within this context.
 */
sealed class GraphResourceReference {
    /**
     * A reference of a resource located in a node within an orchestration graph.
     *
     * @property nodeId The ID of the node that the resource is located in.
     * @property uri The URI of the resource within the action.
     */
    data class Node(
        val nodeId: String,
        val uri: URI,
    ): GraphResourceReference()

    /**
     * A reference to a resource that was provided prior to the start of graph execution.
     * These are for example the files that are part of the submission or files inserted by teachers.
     *
     * They are globally available to all nodes in the graph from the start of the graph execution.
     *
     * @property uri The URI of the resource.
     */
    data class Root(
        val uri: URI,
    ): GraphResourceReference()
}
