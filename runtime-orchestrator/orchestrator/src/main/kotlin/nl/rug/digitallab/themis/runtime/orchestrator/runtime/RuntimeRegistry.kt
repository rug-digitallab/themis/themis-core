package nl.rug.digitallab.themis.runtime.orchestrator.runtime

import io.quarkus.grpc.GrpcClient
import jakarta.annotation.PostConstruct
import jakarta.enterprise.context.ApplicationScoped
import nl.rug.protospec.judgement.v1.ActionService

/**
 * Handles selecting the right runtime to communicate with and running actions on it.
 */
@ApplicationScoped
class RuntimeRegistry {
    @GrpcClient(RuntimeId.CONTAINER)
    private lateinit var containerRuntime: ActionService

    @GrpcClient(RuntimeId.EXECUTABLE)
    private lateinit var executableRuntime: ActionService

    private val clients = mutableMapOf<Runtime, ActionService>()

    /**
     * Initializes the registry with the available runtimes.
     *
     * Must be called after the CDI container is initialized to prevent lateinit property access exceptions.
     */
    @PostConstruct
    fun init() {
        clients[Runtime.CONTAINER] = containerRuntime
        clients[Runtime.EXECUTABLE] = executableRuntime
    }

    /**
     * Gets the associated gRPC client for the given runtime ID.
     *
     * @param runtimeId The ID of the runtime to get the client for.
     *
     * @return The gRPC client for the given runtime ID.
     */
    fun getClient(runtimeId: String): ActionService =
        clients[Runtime.entries.first { it.id == runtimeId }]
            ?: throw IllegalArgumentException("No gRPC client registered for $runtimeId")
}

