package nl.rug.digitallab.themis.runtime.orchestrator.orchestrator

import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import nl.rug.digitallab.themis.common.enums.ActionStatus
import nl.rug.digitallab.themis.expression.ExpressionEngine
import nl.rug.digitallab.themis.runtime.orchestrator.graph.GraphResourceReference
import nl.rug.digitallab.themis.runtime.orchestrator.graph.RuntimeGraph
import nl.rug.digitallab.themis.runtime.orchestrator.graph.nodes.ActionNode
import nl.rug.digitallab.themis.runtime.orchestrator.graph.nodes.EpsilonNode
import nl.rug.digitallab.themis.runtime.orchestrator.graph.nodes.Node
import nl.rug.digitallab.themis.runtime.orchestrator.persistence.repositories.ResourceObjectRepository
import nl.rug.digitallab.themis.runtime.orchestrator.runtime.ActionRunner
import nl.rug.protospec.judgement.v1.Resource
import nl.rug.protospec.judgement.v1.RunActionResponse
import nl.rug.protospec.judgement.v1.copy
import nl.rug.protospec.judgement.v1.extensions.uri

/**
 * This class implements an orchestration strategy for executing an action graph.
 *
 * The graph is represented as a directed acyclic graph, where each node is an action to be executed.
 * The execution strategy simply involves executing each node in the graph in a topological order.
 * Most of the logic relates to dealing with dependencies between nodes.
 *
 * @property client The gRPC client used to communicate with the runtime environment.
 * @property engine The expression engine used to evaluate assertions for shouldContinue.
 * @property responseConverter The converter used to convert between the Protobuf response and the internal ActionResponse.
 * @property resourceRepository The repository used to upload resources produced by runtime actions to MinIO for long-term storage.
 * @property engine The expression engine used to evaluate assertions for shouldContinue.
 * @property responseConverter The converter used to convert between the Protobuf response and the internal ActionResponse.
 * @property resourceRepository The repository used to upload resources produced by runtime actions to MinIO for long-term storage.
 */
@ApplicationScoped
class Orchestrator {
    @Inject
    private lateinit var client: ActionRunner

    @Inject
    private lateinit var engine: ExpressionEngine

    @Inject
    private lateinit var responseConverter: ActionResponseConverter

    @Inject
    private lateinit var resourceRepository: ResourceObjectRepository

    /**
     * Handles all edits that need to be made to the action before it is executed.
     * This current includes:
     * - Inserting relevant resources into the action.
     *
     * @param node The node to prepare the action for.
     *
     * @return The prepared action.
     */
    context(OrchestrationContext)
    private fun prepareAction(node: ActionNode) = node.action.copy {
        inputs += node.inputs.map { resourceReference ->
            resources[resourceReference] ?: throw IllegalStateException(
                when(resourceReference) {
                    is GraphResourceReference.Node -> {
                        "Resource ${resourceReference.uri} was not produced by the previous node ${resourceReference.nodeId}"
                    }
                    is GraphResourceReference.Root -> {
                        "Resource ${resourceReference.uri} was not provided as a root input to the graph"
                    }
                }
            )
        }
    }

    /**
     * Handles all operations that need to be done after the action has been executed.
     * This currently includes:
     * - Inserting relevant resources into the global file storage.
     *
     * @param nodeId The id of the node that was executed.
     *
     * @param response The response from the runtime environment.
     */
    context(OrchestrationContext)
    private fun completeAction(nodeId: String, response: RunActionResponse) {
        // Insert produced resources specified in the response into the files map
        resources.putAll(response.result.outputsList.map {
            GraphResourceReference.Node(
                nodeId = nodeId,
                uri = it.resource.uri
            ) to it.resource
        })
    }

    /**
     * Executes the action of a node.
     * Also takes care of inserting inputs and writing the outputs into the global variable storage.
     *
     * @param node The node to execute.
     *
     * @return The response from the runtime environment.
     */
    context(OrchestrationContext)
    private suspend fun executeAction(node: ActionNode): RunActionResponse {
        // Perform all actions that need to be done before the action is executed
        val preparedAction = prepareAction(node)

        // Execute the action
        val response = client.runAction(
            runtime = node.runtime,
            request = preparedAction,
        )

        // Perform all actions that need to be done after the action is executed
        completeAction(node.id, response)

        return response
    }

    /**
     * Calls the children nodes of the given node to execute.
     *
     * @param node The node whose children should be executed.
     */
    context(OrchestrationContext)
    private suspend fun executeChildren(node: Node) = coroutineScope {
        for(child in node.childNodes) {
            launch(Dispatchers.IO) {
                tryExecuteNode(child)
            }
        }
    }

    /**
     * All the logic required for visiting an action node.
     * This includes:
     * - Executing the action
     * - Uploading the resources produced by the action to MinIO
     * - Evaluating the assertion to check whether the child nodes should be executed
     * - Executing the child nodes
     * - Setting the state of the node
     *
     * @param node The node to execute.
     *
     * @return The response from the runtime environment.
     */
    context(OrchestrationContext)
    private suspend fun processActionNode(node: ActionNode) {
        // Make one of the runtimes execute the action
        val runtimeResponse = executeAction(node)

        // Upload the resources produced by the action to MinIO
        val minioPaths = resourceRepository.uploadActionResources(
            outputs = runtimeResponse.result.outputsList,
            namespace = namespace,
            actionName = node.id,
        )

        // Convert the Protobuf response to the internal datatype to lose the Protobuf dependency
        node.response = responseConverter.toActionResponse(
            response = runtimeResponse,
            minioPaths = minioPaths,
        )

        if(node.assertion != null) {
            // There is an assertion, so we need to evaluate it
            if (!engine
                    .evaluator(responseConverter.toShouldContinueContext(node.response!!))
                    .evaluate<Boolean>(node.assertion!!)
            ) {
                node.state = ActionStatus.FAILED
                return
            }
        }

        node.state = ActionStatus.SUCCEEDED

        executeChildren(node)
    }

    /**
     * Executes the action.
     * This version is used on non-root nodes, which have a caller.
     *
     * @param node The node to execute.
     *
     * @return Whether the node was executed.
     */
    context(OrchestrationContext)
    @WithSpan("EXECUTE node")
    suspend fun tryExecuteNode(node: Node): Boolean {
        // Do not go through with execution request if all dependencies are not finished
        // This is not an error state, all dependencies will try to execute this node once they are finished
        // And this check ensures that the node is only executed once all dependencies are finished
        if(!RuntimeGraph.readyToExecute(node))
            return false

        node.state = ActionStatus.RUNNING

        // Execute the node differently depending on the type of node
        when(node) {
            // Action nodes contain actual requests to send to the runtime environment
            is ActionNode -> processActionNode(node)
            // Blank nodes just need to execute their children
            is EpsilonNode ->  {
                node.state = ActionStatus.SUCCEEDED
                executeChildren(node)
            }
            else -> error("Unknown node type: ${node::class.simpleName}")
        }

        return true
    }

    /**
     * Main entrypoint for executing an action graph.
     * This function sets up the context of the execution, and then starts the orchestration of the graph.
     * Makes use of Kotlin context receivers.
     *
     * @param runtimeGraph The graph to execute.
     * @param files The global files to pass to the graph. These are files that are not produced by any node, but are known prior to the execution of the graph.
     */
    context(OrchestrationContext)
    @WithSpan("EXECUTE graph")
    suspend fun executeGraph(runtimeGraph: RuntimeGraph, files: Map<GraphResourceReference.Root, Resource>) {
        // Insert the root files into the context to make them available to the nodes
        resources.putAll(files)

        // Execute all children of the root, the root is always a no-op.
        executeChildren(runtimeGraph.rootNode)
    }
}
