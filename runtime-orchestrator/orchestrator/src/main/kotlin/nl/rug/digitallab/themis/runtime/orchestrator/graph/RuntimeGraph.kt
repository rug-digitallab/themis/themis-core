package nl.rug.digitallab.themis.runtime.orchestrator.graph

import nl.rug.digitallab.themis.common.enums.ActionStatus
import nl.rug.digitallab.themis.runtime.orchestrator.graph.nodes.Node
import nl.rug.digitallab.themis.runtime.orchestrator.graph.nodes.RootNode
import java.lang.ref.WeakReference

/**
 * This class represents the graph of actions that should be executed.
 * It contains the root node of the graph to allow for traversal of the graph.
 * It also contains helper functions for various graph operations.
 *
 * @property rootNode The root node of the graph.
 */
class RuntimeGraph(
    val rootNode: RootNode = RootNode(),
) {
    companion object {
        /**
         * Adds a dependency to the node.
         * This makes this node depend on the given node.
         *
         * @param parent The node that [child] depends on.
         * @param child The node that depends on [parent].
         */
        fun addDependency(parent: Node, child: Node) {
            child.parentNodes.add(WeakReference(parent))
            parent.childNodes.add(child)
        }

        /**
         * This function checks whether a node is ready to be executed.
         * A node is ready to be executed if all of its dependencies have finished executing and the node itself has not been executed yet.
         *
         * @param node The node to execute.
         *
         * @return Whether the current node is ready to be executed.
         */
        fun readyToExecute(node: Node): Boolean {
            return node.parentNodes.all { it.get()?.state == ActionStatus.SUCCEEDED }
                    && node.state == ActionStatus.PENDING
        }
    }
}
