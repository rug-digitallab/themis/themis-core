plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.project")
}

dependencies {
    val communicationSpecVersion: String by project
    val kotlinMockitoVersion: String by project
    val wiremockVersion: String by project

    // Communication Spec
    implementation("nl.rug.digitallab.themis.judgement:communication-spec:$communicationSpecVersion")
    // REST
    implementation("io.quarkus:quarkus-rest-client-jackson")
    // Message Queue Reactive
    implementation("io.quarkus:quarkus-messaging-kafka")
    // Observability
    implementation("io.quarkus:quarkus-observability-devservices-lgtm")

    // Library containing the definitions of all events sent through the message queue within Themis
    implementation(project(":libraries:common-themis"))
    // Library providing the core implementation logic of orchestrating the graph of actions
    // This module is responsible for actually running the pipeline,
    // while the service module is responsible handling message queues, REST, etc
    implementation(project(":runtime-orchestrator:orchestrator"))

    testImplementation("io.quarkus:quarkus-junit5-mockito")
    testImplementation("io.quarkus:quarkus-test-kafka-companion")
    testImplementation("io.quarkus:quarkus-test-vertx")
    testImplementation("org.mockito.kotlin:mockito-kotlin:$kotlinMockitoVersion")
    testImplementation("io.quarkiverse.wiremock:quarkus-wiremock:$wiremockVersion")
    testImplementation("io.quarkiverse.wiremock:quarkus-wiremock-test:$wiremockVersion")
}
