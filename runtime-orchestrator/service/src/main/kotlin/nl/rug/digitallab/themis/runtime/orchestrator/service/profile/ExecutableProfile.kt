package nl.rug.digitallab.themis.runtime.orchestrator.service.profile

import nl.rug.digitallab.themis.common.data.ActionResponse
import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.protospec.judgement.v1.RunActionRequest

/**
 * Defines an action that can be stored within a [RuntimeProfile].
 * This action, on top of the data that an action already stores, also stores the request to be sent to the runtime.
 * A [RuntimeProfile] with [ExecutableAction]s can be used to execute the actions in the profile.
 *
 * @property name The name of the action.
 * @property response The response that the action should produce.
 * @property request The request that should be sent to the runtime.
 * @property inputs The resources that the action depends on and wants imported into it. Uniquely identified by the path of the resource and the action to retrieve it from.
 * @property outputs The resources that the action produces and wants exported from it. Uniquely identified by the path of the resource, the action is implicitly the current action.
 */
class ExecutableAction(
    val name: String,
    val request: RunActionRequest,
    val runtime: String,
    val inputs: Set<ProfileResourceReference> = emptySet(),
    val outputs: Set<String> = emptySet(),
) {
    var response: ActionResponse? = null
}

/**
 * Defines a job that can be stored within a [RuntimeProfile].
 * This job, on top of the data that a job already stores, also stores the actions that should be executed in the job.
 * A [RuntimeProfile] with [ExecutableJob]s can be used to execute the stages in the profile.
 *
 * @param name The name of the job.
 * @property actions The actions that should be executed in the job.
 */
data class ExecutableJob(
    override val name: String,
    val actions: List<ExecutableAction>,
): RuntimeProfile.Job
