package nl.rug.digitallab.themis.runtime.orchestrator.service.resources

import com.google.protobuf.ByteString
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.themis.runtime.orchestrator.graph.GraphResourceReference
import nl.rug.protospec.judgement.v1.Resource
import nl.rug.protospec.judgement.v1.extensions.uri
import nl.rug.protospec.judgement.v1.resource
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream
import java.io.ByteArrayOutputStream
import org.jboss.logging.Logger
import java.net.URI

/**
 * The [ResourceExtractor] is can extract resources from a zip file.
 * Resources are used by the orchestrator to pass files, streams etc between different nodes in the execution graph.
 */
@ApplicationScoped
class ResourceExtractor {
    @Inject
    private lateinit var log: Logger

    /**
     * Extract resources from a zip file.
     *
     * @param zipStream The input stream of the zip file.
     *
     * @return A map of [GraphResourceReference.Root] to [Resource] containing the extracted resources.
     */
    fun extractResourcesFromZip(zipStream: ZipArchiveInputStream): Map<GraphResourceReference.Root, Resource> {
        val resources = mutableMapOf<GraphResourceReference.Root, Resource>()

        var entry = zipStream.nextEntry

        while (entry != null) {
            log.info("Extracting resource ${entry.name}")
            val resourceURI = URI("file:${entry.name}")
            val content = ByteArrayOutputStream().apply {
                zipStream.copyTo(this)
            }.toByteArray()

            resources[GraphResourceReference.Root(resourceURI)] = resource {
                this.uri = uri
                this.contents = ByteString.copyFrom(content)
            }

            entry = zipStream.nextEntry
        }

        return resources
    }
}
