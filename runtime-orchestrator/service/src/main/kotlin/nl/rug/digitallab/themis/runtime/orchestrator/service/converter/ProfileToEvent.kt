package nl.rug.digitallab.themis.runtime.orchestrator.service.converter

import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.common.events.JudgementEvent
import nl.rug.digitallab.themis.common.typealiases.ConfigurationId
import nl.rug.digitallab.themis.common.typealiases.SubmissionId
import nl.rug.digitallab.themis.runtime.orchestrator.service.profile.ExecutableJob

/**
 * Converts a [RuntimeProfile] to a [JudgementEvent].
 * This function will be used to send updates to the judgement service.
 * It will be both used at the end of execution to send the final state of the submission,
 * and during execution to provide continuous updates of the current state.
 *
 * The [RuntimeProfile] gets automatically updated during execution of the graph,
 * as the responses of the graph are directly linked to the profile.
 * As such, even though the [RuntimeProfile] is an input of the execution process,
 * it can be used during and after the execution to determine the state of the submission.
 *
 * @receiver The [RuntimeProfile] to convert.
 *
 * @param submissionId The ID of the submission to attach to the [JudgementEvent].
 * @param configurationId The ID of the assignment configuration to attach to the [JudgementEvent].
 *
 * @return The [JudgementEvent] that represents the [RuntimeProfile].
 */
fun RuntimeProfile<ExecutableJob>.toJudgementEvent(
    submissionId: SubmissionId,
    configurationId: ConfigurationId,
): JudgementEvent {
    val eventProfile: RuntimeProfile<JudgementEvent.Job> = RuntimeProfile(
        stages = this.stages.map { stage ->
            RuntimeProfile.Stage(
                name = stage.name,
                jobs = stage.jobs.map { job ->
                    JudgementEvent.Job(
                        name = job.name,
                        actions = job.actions.map { action ->
                            JudgementEvent.ActionState(
                                name = action.name,
                                response = action.response,
                            )
                        }
                    )
                }
            )
        }
    )

    return JudgementEvent(
        submissionId = submissionId,
        configurationId = configurationId,
        profile = eventProfile,
    )
}
