package nl.rug.digitallab.themis.runtime.orchestrator.service.converter

import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.runtime.orchestrator.graph.GraphResourceReference
import nl.rug.digitallab.themis.runtime.orchestrator.service.profile.ExecutableJob
import nl.rug.digitallab.themis.runtime.orchestrator.graph.RuntimeGraph
import nl.rug.digitallab.themis.runtime.orchestrator.graph.nodes.ActionNode
import nl.rug.digitallab.themis.runtime.orchestrator.graph.nodes.EpsilonNode
import nl.rug.digitallab.themis.runtime.orchestrator.graph.nodes.Node
import nl.rug.digitallab.themis.runtime.orchestrator.service.profile.ProfileResourceReference

/**
 * Converts a [RuntimeProfile] to a [RuntimeGraph].
 * It does this by creating a graph of a specific structure.
 * The graph is split into sections, which implement the stages of the submission profile.
 * Each section is a set of parallel jobs, which each are a sequential list of actions.
 * The stages are connected through a blank node, of which the ends of each job action list are connected to.
 * The start of each job action list of the next stage is connected to the blank node.
 *
 * @receiver The [RuntimeProfile] to convert.
 *
 * @return The [RuntimeGraph] that represents the [RuntimeProfile].
 */
fun RuntimeProfile<ExecutableJob>.toRuntimeGraph(): RuntimeGraph {
    val graph = RuntimeGraph()

    // Hosts the node that connects two stages together.
    // At the start, it hosts the root node, which the first stage is connected to.
    // Later, it will host a blank node that is connected to the previous stage and the next stage.
    var previousInterstageNode: Node = graph.rootNode

    this.stages.forEachIndexed { stageIndex, stage ->
        val nextInterstageNode = EpsilonNode("Interstage-$stageIndex")

        // Construct the stage in the graph.
        stage.jobs.forEach { job ->
            // Reference the node that the next action in the job needs to be connected to.
            // Initially it is the inter-stage node.
            var previousNode: Node = previousInterstageNode

            job.actions.forEach { action ->
                val actionNode = ActionNode(
                    // Uniquely identified by the stage name, the job name, and the action name.
                    id = generateNodeId(stage.name, job.name, action.name),
                    action = action.request,
                    runtime = action.runtime,
                    responseDelegate = action::response,
                    inputs = action.inputs.map {
                        // Convert the profile resource references to graph resource references.
                        return@map when(it) {
                            is ProfileResourceReference.Root -> GraphResourceReference.Root(
                                uri = it.uri,
                            )
                            is ProfileResourceReference.Action -> GraphResourceReference.Node(
                                nodeId = generateNodeId(it.stage, it.job, it.action),
                                uri = it.uri,
                            )
                        }
                    }.toSet(),
                    outputs = action.outputs,
                )

                // Attach the action node to the previous node.
                RuntimeGraph.addDependency(previousNode, actionNode)
                // Set the previous node to the current action node, such that the next action is connected to this action.
                previousNode = actionNode
            }

            // The last action in the stage is connected to the inter-stage node.
            RuntimeGraph.addDependency(previousNode, nextInterstageNode)
        }

        // Cycle the inter-stage node to the next inter-stage node.
        previousInterstageNode = nextInterstageNode
    }

    return graph
}

/**
 * Generates the ID of a node based on the ID of an action within a profile.
 *
 * @param stage The name of the stage that the action is in within the profile.
 * @param job The name of the job that the action is in within the profile.
 * @param action The name of the action within the profile.
 *
 * @return The generated node ID as a string.
 */
fun generateNodeId(stage: String, job: String, action: String) = "$stage-$job-$action"
