package nl.rug.digitallab.themis.runtime.orchestrator.service.profile

import java.net.URI

/**
 * A reference of a resource within a profile.
 *
 * @see nl.rug.digitallab.themis.common.data.RuntimeProfile for an explanation about what a profile is within this context.
 */
sealed class ProfileResourceReference {
    /**
     * A reference of a resource located in another action within a profile.
     * Mainly used to define inputs of actions. The inputs need to not just define the path, but also the specific action that the resource is located in.
     *
     * @property stage The stage that the job is located in.
     * @property job The job that the action is located in.
     * @property action The action that the resource is located in.
     * @property uri The URI of the resource within the action.
     */
    data class Action(
        val stage: String,
        val job: String,
        val action: String,
        val uri: URI,
    ): ProfileResourceReference()

    /**
     * A reference to a resource that was provided prior to the start of graph execution.
     * These are for example the files that are part of the submission or files inserted by teachers.
     *
     * They are globally available to all actions in the graph from the start of the graph execution.
     *
     * @property uri The URI of the resource.
     */
    data class Root(
        val uri: URI,
    ): ProfileResourceReference()
}
