package nl.rug.digitallab.themis.runtime.orchestrator.service.config

import nl.rug.digitallab.common.kotlin.quantities.GB
import nl.rug.digitallab.common.kotlin.quantities.MB
import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.runtime.orchestrator.service.profile.ExecutableAction
import nl.rug.digitallab.themis.runtime.orchestrator.service.profile.ExecutableJob
import nl.rug.protospec.judgement.v1.executionLimits
import nl.rug.protospec.judgement.v1.extensions.*
import nl.rug.protospec.judgement.v1.runActionRequest
import kotlin.time.Duration.Companion.seconds

// TODO: Implement a real profile retrieved from the assignments service
val mockedProfile = RuntimeProfile(
    stages = listOf(
        RuntimeProfile.Stage(
            name = "compile",
            jobs = listOf(
                ExecutableJob(
                    name = "compile",
                    actions = listOf(
                        ExecutableAction(
                            name = "compile",
                            request = runActionRequest {
                                environment = "gcc:14.2.0"
                                command += "gcc"
                                command += "*.c"
                                limits = executionLimits {
                                    wallTime = 1.seconds
                                    cpuCores = 1.0f
                                    memory = 1.GB
                                    disk = 1.GB
                                    output = 1.MB
                                    actionProcesses = 50
                                    userProcesses = 10
                                }
                            },
                            runtime = "container-runtime",
                        ),
                    )
                )
            )
        )
    )
)
