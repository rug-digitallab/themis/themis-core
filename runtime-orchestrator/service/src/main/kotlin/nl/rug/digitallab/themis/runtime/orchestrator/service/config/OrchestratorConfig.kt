package nl.rug.digitallab.themis.runtime.orchestrator.service.config

import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.runtime.orchestrator.service.profile.ExecutableJob

/**
 * Defines the runtime orchestrator-specific sections of the internal Themis configuration.
 * This configuration must be a subset of the global internal Themis configuration.
 * It should be possible to convert from global to orchestrator-specific configuration through dropping fields.
 *
 * @param profile The profile configuration for the orchestrator. This contains configuration on a per-job basis.
 */
data class OrchestratorConfig(
    val profile: RuntimeProfile<ExecutableJob>
)
