package nl.rug.digitallab.themis.runtime.orchestrator.service.rest

import jakarta.ws.rs.GET
import nl.rug.digitallab.themis.common.typealiases.SubmissionId
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient
import java.io.InputStream

/**
 * A client to interact with the assignments service.
 */
@Path("/api/v1/themis/")
@RegisterRestClient(configKey="assignments-api")
interface AssignmentsRestClient {
    /**
     * Download the submission files for a given submission ID.
     *
     * @param submissionId The ID of the submission to download.
     *
     * @return An [InputStream] containing the submission files as a zip file.
     */
    @GET
    @Path("submissions/{submissionId}/download")
    fun downloadSubmission(@PathParam("submissionId") submissionId: SubmissionId): InputStream
}
