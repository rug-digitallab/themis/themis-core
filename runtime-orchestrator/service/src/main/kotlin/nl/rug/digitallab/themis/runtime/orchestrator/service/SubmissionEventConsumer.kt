package nl.rug.digitallab.themis.runtime.orchestrator.service

import io.opentelemetry.instrumentation.annotations.WithSpan
import io.smallrye.mutiny.Uni
import io.smallrye.reactive.messaging.MutinyEmitter
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.themis.common.constants.JUDGEMENTS_QUEUE
import nl.rug.digitallab.themis.common.constants.SUBMISSION_QUEUE
import nl.rug.digitallab.themis.common.events.JudgementEvent
import nl.rug.digitallab.themis.common.events.SubmissionAddedEvent
import nl.rug.digitallab.themis.common.typealiases.RunId
import nl.rug.digitallab.themis.runtime.orchestrator.orchestrator.OrchestrationContext
import nl.rug.digitallab.themis.runtime.orchestrator.orchestrator.Orchestrator
import nl.rug.digitallab.themis.runtime.orchestrator.persistence.namespaces.RunNamespace
import nl.rug.digitallab.themis.runtime.orchestrator.service.config.OrchestratorConfig
import nl.rug.digitallab.themis.runtime.orchestrator.service.config.mockedProfile
import nl.rug.digitallab.themis.runtime.orchestrator.service.converter.toJudgementEvent
import nl.rug.digitallab.themis.runtime.orchestrator.service.converter.toRuntimeGraph
import nl.rug.digitallab.themis.runtime.orchestrator.service.resources.ResourceExtractor
import nl.rug.digitallab.themis.runtime.orchestrator.service.rest.AssignmentsRestClient
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream
import org.eclipse.microprofile.reactive.messaging.Channel
import org.eclipse.microprofile.reactive.messaging.Incoming
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.jboss.logging.Logger

/**
 * The [SubmissionEventConsumer] is the main entrypoint of the runtime orchestrator.
 *
 * It listens for newly created submission events from the assignments service on a Kafka topic.
 * As a response, it will orchestrate the execution of the submission and pass the results to the judgement service.
 */
@ApplicationScoped
class SubmissionEventConsumer {
    @Inject
    private lateinit var log: Logger

    @Inject
    private lateinit var resourceExtractor: ResourceExtractor

    @Inject
    private lateinit var orchestrator: Orchestrator

    // The REST client to send REST requests to the assignments service
    @RestClient
    private lateinit var assignmentsClient: AssignmentsRestClient

    // The message queue emitter to send events to the judgement service
    @Channel(JUDGEMENTS_QUEUE)
    private lateinit var judgementEmitter: MutinyEmitter<JudgementEvent>

    /**
     * Event handler for submission events on the Kafka topic.
     *
     * This method will download the submission files from the assignments service,
     * and make those submission files globally accessible to the graph that is to be executed.
     * It will then call the orchestrator to orchestrate the execution of the submission.
     *
     * @param event The submission event received from the Kafka topic.
     *
     * @return A [Uni] that will emit the resources extracted from the submission.
     */
    @Incoming(SUBMISSION_QUEUE)
    @WithSpan("PROCESS Submission")
    fun handleSubmission(event: SubmissionAddedEvent) {
        log.info("Received new submission event: $event")

        log.info("Downloading submission ${event.submissionId}")
        val zipStream = assignmentsClient.downloadSubmission(event.submissionId)

        log.info("Extracting resources from submission")
        val resources = resourceExtractor.extractResourcesFromZip(ZipArchiveInputStream(zipStream))

        log.info("Starting graph execution of submission ${event.submissionId}")
        val config = OrchestratorConfig(
            profile = mockedProfile,
        )

        val context = OrchestrationContext(namespace = RunNamespace(
            courseInstanceId = event.courseInstanceId,
            groupId = event.groupId,
            assignmentId = event.assignmentId,
            submissionId = event.submissionId,
            // Generate an ID that corresponds to this specific graph orchestration
            runId = RunId.randomUUID(),
        ))

        with(context) {
            runBlocking {
                orchestrator.executeGraph(config.profile.toRuntimeGraph(), resources)
            }
        }

        judgementEmitter.sendAndAwait(mockedProfile.toJudgementEvent(
            submissionId = event.submissionId,
            configurationId = event.configId,
        ))
    }
}
