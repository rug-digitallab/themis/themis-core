package nl.rug.digitallab.themis.runtime.orchestrator.service.util

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream

object TestUtil {
    fun generateByteArray(): ByteArray {
        val byteArrayOutputStream = ByteArrayOutputStream()
        ZipArchiveOutputStream(byteArrayOutputStream).use { zipOut ->
            zipOut.putArchiveEntry(ZipArchiveEntry("sample.txt"))
            zipOut.write("Sample content".toByteArray())
            zipOut.closeArchiveEntry()
            zipOut.putArchiveEntry(ZipArchiveEntry("sample2.txt"))
            zipOut.write("Sample content 2".toByteArray())
            zipOut.closeArchiveEntry()
        }

        return byteArrayOutputStream.toByteArray()
    }

    fun generateZipStream(): ZipArchiveInputStream {
        return ZipArchiveInputStream(ByteArrayInputStream(generateByteArray()))
    }
}
