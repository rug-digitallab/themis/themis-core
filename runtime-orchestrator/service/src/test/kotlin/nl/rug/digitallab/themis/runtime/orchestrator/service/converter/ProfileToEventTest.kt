package nl.rug.digitallab.themis.runtime.orchestrator.service.converter

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.common.kotlin.quantities.GB
import nl.rug.digitallab.themis.common.data.ActionResponse
import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.common.enums.Status
import nl.rug.digitallab.themis.common.typealiases.ConfigurationId
import nl.rug.digitallab.themis.common.typealiases.SubmissionId
import nl.rug.digitallab.themis.runtime.orchestrator.service.profile.ExecutableAction
import nl.rug.digitallab.themis.runtime.orchestrator.service.profile.ExecutableJob
import nl.rug.protospec.judgement.v1.runActionRequest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import kotlin.time.Duration.Companion.seconds

@QuarkusTest
class ProfileToEventTest {
    @Test
    fun `An executed profile can be converted to a judgement event`() {
        val profile: RuntimeProfile<ExecutableJob> = RuntimeProfile(
            stages = listOf(
                RuntimeProfile.Stage(
                    name = "compile",
                    jobs = listOf(
                        ExecutableJob(
                            name = "compile",
                            actions = listOf(
                                ExecutableAction(
                                    name = "compile",
                                    runtime = "container-runtime",
                                    request = runActionRequest {},
                                ),
                            )
                        )
                    )
                )
            )
        )

        val response = ActionResponse(
            result = ActionResponse.Result(
                exitCode = 0,
                status = Status.SUCCEEDED,
                resources = emptyList()
            ),
            usage = ActionResponse.Usage(
                wallTime = 1.seconds,
                memory = 1.GB,
                disk = 1.GB,
                output = 1.GB,
                createdProcesses = 1,
                maxProcesses = 1,
                orphanedProcesses = 1,
                zombieProcesses = 1,
                diskTime = 1,
                ioTime = 1,
            ),
        )

        profile.stages.first().jobs.first().actions.first().response = response

        val submissionId = SubmissionId.randomUUID()
        val configurationId = ConfigurationId.randomUUID()
        val event = profile.toJudgementEvent(
            submissionId = submissionId,
            configurationId = configurationId,
        )

        assertEquals(
            profile.stages.first().jobs.first().actions.first().response,
            event.profile.stages.first().jobs.first().actions.first().response,
        )

        assertEquals(submissionId, event.submissionId)
        assertEquals(configurationId, event.configurationId)
    }
}
