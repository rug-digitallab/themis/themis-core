package nl.rug.digitallab.themis.runtime.orchestrator.service

import com.github.tomakehurst.wiremock.client.WireMock
import io.quarkiverse.wiremock.devservice.ConnectWireMock
import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.themis.common.events.SubmissionAddedEvent
import nl.rug.digitallab.themis.common.typealiases.*
import nl.rug.digitallab.themis.runtime.orchestrator.runtime.ActionRunner
import nl.rug.digitallab.themis.runtime.orchestrator.service.util.TestUtil
import nl.rug.protospec.judgement.v1.RunActionResponse
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@QuarkusTest
@ConnectWireMock
class SubmissionEventConsumerTest {
    @Inject
    private lateinit var submissionEventConsumer: SubmissionEventConsumer

    // Injected automatically by WireMock Quarkus extension because of the @ConnectWireMock annotation
    private lateinit var wiremock: WireMock

    @InjectMock
    private lateinit var runtimeRegistry: ActionRunner

    private val event = SubmissionAddedEvent(
        submissionId = SubmissionId.randomUUID(),
        assignmentId = AssignmentId.randomUUID(),
        groupId = GroupId.randomUUID(),
        configId = ConfigurationId.randomUUID(),
        courseInstanceId = CourseInstanceId.randomUUID(),
    )

    @BeforeEach
    fun setup() {
        // Set up a Mock HTTP server to make requests against, so we don't have to mock the HTTP client.
        // Previously we had the HTTP client break due to upstream bugs, which wasn't caught by a mocked HTTP client.
        // Testing this way mocks less code, since the HTTP client is not mocked, the server is. So we would catch broken HTTP clients.
        wiremock.register(WireMock.get(WireMock.urlMatching("/api/v1/themis/submissions/.*/download"))
            .willReturn(WireMock.aResponse().withStatus(200).withBody(TestUtil.generateByteArray())))

        // Configure the mocked gRPC client
        runBlocking {
            whenever(runtimeRegistry.runAction(any(), any())).thenReturn(
                RunActionResponse.getDefaultInstance()
            )
        }
    }

    @Test
    fun `The orchestrator can orchestrate run a valid submission request`() {
        submissionEventConsumer.handleSubmission(event)

        runBlocking {
            verify(runtimeRegistry, times(1)).runAction(any(), any())
        }
    }
}
