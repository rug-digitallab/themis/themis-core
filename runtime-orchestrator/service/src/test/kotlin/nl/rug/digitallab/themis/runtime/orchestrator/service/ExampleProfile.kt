package nl.rug.digitallab.themis.runtime.orchestrator.service

import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.runtime.orchestrator.service.profile.ExecutableAction
import nl.rug.digitallab.themis.runtime.orchestrator.service.profile.ExecutableJob
import nl.rug.digitallab.themis.runtime.orchestrator.service.profile.ProfileResourceReference
import nl.rug.protospec.judgement.v1.RunActionRequest
import java.net.URI

fun generateTestcase(jobName: String): ExecutableJob {
    return ExecutableJob(
        name = jobName,
        actions = listOf(
            ExecutableAction(
                name = "prepare",
                request = RunActionRequest.getDefaultInstance(),
                runtime = "container-runtime",
            ),
            ExecutableAction(
                name = "execute",
                request = RunActionRequest.getDefaultInstance(),
                outputs = setOf("stream:stdout", "stream:stderr"),
                runtime = "container-runtime",
            ),
            ExecutableAction(
                name = "diff",
                request = RunActionRequest.getDefaultInstance(),
                runtime = "container-runtime",
                inputs = setOf(
                    ProfileResourceReference.Action(
                        stage = "testcases",
                        job = jobName,
                        action = "execute",
                        uri = URI("stream:stdout"),
                    ),
                    ProfileResourceReference.Action(
                        stage = "testcases",
                        job = jobName,
                        action = "execute",
                        uri = URI("stream:stderr"),
                    ),
                )
            ),
        )
    )
}

val exampleProfile = RuntimeProfile(
    stages = listOf(
        RuntimeProfile.Stage(
            name = "compile",
            jobs = listOf(
                ExecutableJob(
                    name = "compile",
                    actions = listOf(
                        ExecutableAction(
                            name = "GenerateSource",
                            request = RunActionRequest.getDefaultInstance(),
                            runtime = "container-runtime",
                            outputs = setOf("/tmp/source/generated.c", "/submission/squares.c")
                        ),
                        ExecutableAction(
                            name = "CompileSource",
                            request = RunActionRequest.getDefaultInstance(),
                            runtime = "container-runtime",
                            inputs = setOf(
                                ProfileResourceReference.Action(
                                    stage = "compile",
                                    job = "compile",
                                    action = "GenerateSource",
                                    uri = URI("file:/tmp/source/generated.c")
                                ),
                                ProfileResourceReference.Action(
                                    stage = "compile",
                                    job = "compile",
                                    action = "GenerateSource",
                                    uri = URI("file:/submission/squares.c")
                                ),
                                ProfileResourceReference.Root(URI("file:/tmp/source/generated.c")),
                            )
                        ),
                    )
                )
            )
        ),
        RuntimeProfile.Stage(
            name = "testcases",
            jobs = listOf(
                generateTestcase("1"),
                generateTestcase("2"),
                generateTestcase("3"),
                generateTestcase("4"),
                generateTestcase("5"),
                generateTestcase("6"),
                generateTestcase("7"),
                generateTestcase("8"),
                generateTestcase("9"),
                generateTestcase("10"),
            )
        ),
        RuntimeProfile.Stage(
            name = "cleanup",
            jobs = listOf(
                ExecutableJob(
                    name = "cleanup",
                    actions = listOf(
                        ExecutableAction(
                            name = "Cleanup",
                            request = RunActionRequest.getDefaultInstance(),
                            runtime = "container-runtime",
                        ),
                    )
                )
            )
        ),
    )
)
