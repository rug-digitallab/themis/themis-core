package nl.rug.digitallab.themis.runtime.orchestrator.service.converter

import com.google.protobuf.ByteString
import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.runtime.orchestrator.orchestrator.OrchestrationContext
import nl.rug.digitallab.themis.runtime.orchestrator.orchestrator.Orchestrator
import nl.rug.digitallab.themis.runtime.orchestrator.persistence.repositories.ResourceObjectRepository
import nl.rug.digitallab.themis.runtime.orchestrator.runtime.ActionRunner
import nl.rug.digitallab.themis.runtime.orchestrator.service.profile.ExecutableAction
import nl.rug.digitallab.themis.runtime.orchestrator.service.profile.ExecutableJob
import nl.rug.digitallab.themis.runtime.orchestrator.service.profile.ProfileResourceReference
import nl.rug.protospec.judgement.v1.*
import nl.rug.protospec.judgement.v1.extensions.uri
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import org.mockito.kotlin.any
import org.mockito.kotlin.whenever
import java.net.URI

@QuarkusTest
class ProfileTest {
    @Inject
    private lateinit var orchestrator: Orchestrator

    @InjectMock
    private lateinit var mockedClient: ActionRunner

    @InjectMock
    private lateinit var objectRepository: ResourceObjectRepository

    private var compileActionHasRun = false

    @BeforeEach
    fun setupMockedClient(): Unit = runBlocking {
        whenever(objectRepository.uploadActionResources(any(), any(), any())).thenReturn(
            mapOf(
                URI("file:/tmp/source/generated.c") to "",
                URI("file:/submission/squares.c") to "",
            )
        )

        whenever(mockedClient.runAction(any(), any())).then {
            val request = it.getArgument<RunActionRequest>(1)

            if(request.commandList.contains("compileSource")) {
                val source = request.inputsList.find { input ->
                    input.uri == URI("file:/tmp/source/generated.c")
                }
                assertEquals("This is the contents of a generated source file.", source?.contents?.toStringUtf8())

                val squares = request.inputsList.find { input ->
                    input.uri == URI("file:/submission/squares.c")
                }
                assertEquals("This is the contents of a squares source file.", squares?.contents?.toStringUtf8())

                compileActionHasRun = true
            }

            return@then runActionResponse {
                result = executionResult {
                    outputs += output {
                        resource = resource {
                            uri = URI("file:/tmp/source/generated.c")
                            contents = ByteString.copyFromUtf8("This is the contents of a generated source file.")
                        }
                        isPresent = true
                        exceededSize = false
                    }
                    outputs += output {
                        resource = resource {
                            uri = URI("file:/submission/squares.c")
                            contents = ByteString.copyFromUtf8("This is the contents of a squares source file.")
                        }
                        isPresent = true
                        exceededSize = false
                    }
                }

            }
        }
    }

    @Test
    fun `The resources referencing are properly handled in a linear profile`() {
        val profile = RuntimeProfile(
            stages = listOf(
                RuntimeProfile.Stage(
                    name = "compile",
                    jobs = listOf(
                        ExecutableJob(
                            name = "compile",
                            actions = listOf(
                                ExecutableAction(
                                    name = "GenerateSource",
                                    request = runActionRequest {
                                        command += "generateSource"
                                    },
                                    runtime = "container-runtime",
                                    outputs = setOf("/tmp/source/generated.c", "/submission/squares.c")
                                ),
                                ExecutableAction(
                                    name = "CompileSource",
                                    request = runActionRequest {
                                        command += "compileSource"
                                    },
                                    runtime = "container-runtime",
                                    inputs = setOf(
                                        ProfileResourceReference.Action(
                                            stage = "compile",
                                            job = "compile",
                                            action = "GenerateSource",
                                            uri = URI("file:/tmp/source/generated.c"),
                                        ),
                                        ProfileResourceReference.Action(
                                            stage = "compile",
                                            job = "compile",
                                            action = "GenerateSource",
                                            uri = URI("file:/submission/squares.c"),
                                        ),
                                    )
                                ),
                            )
                        )
                    )
                )
            )
        )

        val graph = profile.toRuntimeGraph()

        runBlocking {
            with(OrchestrationContext(mock())) {
                orchestrator.executeGraph(graph, emptyMap())
            }
        }

        assertEquals(true, compileActionHasRun)
    }
}
