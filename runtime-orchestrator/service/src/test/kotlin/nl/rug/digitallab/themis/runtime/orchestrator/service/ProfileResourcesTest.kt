package nl.rug.digitallab.themis.runtime.orchestrator.service

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.themis.runtime.orchestrator.service.converter.toRuntimeGraph
import nl.rug.digitallab.themis.runtime.orchestrator.graph.GraphResourceReference
import nl.rug.digitallab.themis.runtime.orchestrator.graph.nodes.ActionNode
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.net.URI

@QuarkusTest
class ProfileResourcesTest {
    @Test
    fun `The resource dependencies of a profile are correctly translated to a graph`() {
        val graph = exampleProfile.toRuntimeGraph()

        val generateAction = graph.rootNode.childNodes.first()
        val compileAction = generateAction.childNodes.first()

        check(generateAction is ActionNode)
        check(compileAction is ActionNode)

        assertEquals(setOf("/tmp/source/generated.c", "/submission/squares.c"), generateAction.outputs)
        assertEquals(
            setOf(
                GraphResourceReference.Node("compile-compile-GenerateSource", URI("file:/tmp/source/generated.c")),
                GraphResourceReference.Node("compile-compile-GenerateSource", URI("file:/submission/squares.c")),
                GraphResourceReference.Root(URI("file:/tmp/source/generated.c")),
            ),
            compileAction.inputs,
        )
    }
}
