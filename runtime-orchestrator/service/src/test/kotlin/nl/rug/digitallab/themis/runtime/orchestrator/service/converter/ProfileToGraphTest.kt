package nl.rug.digitallab.themis.runtime.orchestrator.service.converter

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.themis.runtime.orchestrator.service.exampleProfile
import nl.rug.digitallab.themis.runtime.orchestrator.graph.nodes.Node
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

@QuarkusTest
class ProfileToGraphTest {
    @Test
    fun `A correct graph is generated from a submission profile`() {
        val graph = exampleProfile.toRuntimeGraph()

        // Root
        var currentNode: Node = graph.rootNode
        assertEquals(1, currentNode.childNodes.size)

        // compile/GenerateSource
        currentNode = currentNode.childNodes.first()
        assertEquals(1, currentNode.childNodes.size)

        // compile/CompileSource
        currentNode = currentNode.childNodes.first()
        assertEquals(1, currentNode.childNodes.size)

        // Interstage-1
        currentNode = currentNode.childNodes.first()
        assertEquals(10, currentNode.childNodes.size)

        // Store the diff node so we can go to its children later
        var diffNode: Node? = null

        // testcases/prepare
        for (node in currentNode.childNodes) {
            assertEquals(1, node.childNodes.size)

            // testcases/execute
            val execute = node.childNodes.first()
            assertEquals(1, execute.childNodes.size)

            // testcases/diff
            diffNode = execute.childNodes.first()
            assertEquals(1, diffNode.childNodes.size)
        }

        // Interstage-2
        currentNode = diffNode!!.childNodes.first()
        assertEquals(1, currentNode.childNodes.size)

        // cleanup/Cleanup
        currentNode = currentNode.childNodes.first()
        assertEquals(1, currentNode.childNodes.size)

        // Interstage-3
        currentNode = currentNode.childNodes.first()
        assertEquals(0, currentNode.childNodes.size)
    }
}
