package nl.rug.digitallab.themis.runtime.orchestrator.service

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.themis.common.data.ActionResponse
import nl.rug.digitallab.themis.common.enums.Status
import nl.rug.digitallab.themis.runtime.orchestrator.service.converter.toRuntimeGraph
import nl.rug.digitallab.themis.runtime.orchestrator.graph.nodes.ActionNode
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import kotlin.time.Duration.Companion.seconds

@QuarkusTest
class ProfileLinkedResponseTest {
    @Test
    fun `When a profile is converted to a graph, the responses from the graph are linked to the actions in the profile`() {
        val profile = exampleProfile
        val graph = exampleProfile.toRuntimeGraph()

        val targetNode: ActionNode = graph.rootNode.childNodes.first() as ActionNode
        val targetAction = profile.stages.first().jobs.first().actions.first()

        assertNull(targetAction.response)
        assertNull(targetNode.response)

        targetNode.response = ActionResponse(
            result = ActionResponse.Result(
                exitCode = 1,
                resources = emptyList(),
                status = Status.CRASHED,
            ),
            usage = ActionResponse.Usage(
                wallTime = 1.seconds,
                memory = ByteSize(1024),
                disk = ByteSize(1024),
                output = ByteSize(1024),
                createdProcesses = 1,
                maxProcesses = 1,
                orphanedProcesses = 0,
                zombieProcesses = 0,
                diskTime = 1000,
                ioTime = 1000,
            ),
        )

        assertNotNull(targetNode.response)
        assertNotNull(targetAction.response)
    }
}
