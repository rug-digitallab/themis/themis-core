package nl.rug.digitallab.themis.runtime.orchestrator.service.resources

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.runtime.orchestrator.graph.GraphResourceReference
import nl.rug.digitallab.themis.runtime.orchestrator.service.util.TestUtil
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.net.URI

@QuarkusTest
class ResourceExtractorTest {
    @Inject
    private lateinit var resourceExtractor: ResourceExtractor

    @Test
    fun `A resource can be correctly extracted from a zip file`() {
        val zipStream = TestUtil.generateZipStream()
        val resources = resourceExtractor.extractResourcesFromZip(zipStream)

        assertEquals(2, resources.size)
        assertTrue(resources.containsKey(GraphResourceReference.Root(URI("file:sample.txt"))))
        assertEquals("Sample content",
            resources[GraphResourceReference.Root(URI("file:sample.txt"))]?.contents?.toStringUtf8()
        )
        assertTrue(resources.containsKey(GraphResourceReference.Root(URI("file:sample2.txt"))))
        assertEquals("Sample content 2",
            resources[GraphResourceReference.Root(URI("file:sample2.txt"))]?.contents?.toStringUtf8()
        )
    }
}
