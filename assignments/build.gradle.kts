plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.project")
}

dependencies {
    val commonQuarkusVersion: String by project
    val commonsCompressVersion: String by project
    val kotlinMockitoVersion: String by project
    val quarkusMinioVersion: String by project
    val sprinklerUtilsVersion: String by project

    // Digital Lab
    implementation("nl.rug.digitallab.common.quarkus:archiver:$commonQuarkusVersion")
    // Flyway DB Migrations
    implementation("io.quarkus:quarkus-flyway")
    implementation("io.quarkus:quarkus-jdbc-mariadb")
    implementation("org.flywaydb:flyway-mysql")
    // Panache/Hibernate
    implementation("io.quarkus:quarkus-hibernate-validator")
    implementation("nl.rug.digitallab.common.quarkus:hibernate-orm:$commonQuarkusVersion")
    implementation("nl.rug.digitallab.common.quarkus:exception-mapper-hibernate:$commonQuarkusVersion")
    // REST
    implementation("io.quarkus:quarkus-rest")
    implementation("io.quarkus:quarkus-rest-jackson")
    implementation("io.quarkus:quarkus-smallrye-openapi")
    // Message Queue Reactive
    implementation("io.quarkus:quarkus-messaging-kafka")
    // MinIO Object Store
    implementation("io.quarkiverse.minio:quarkus-minio:$quarkusMinioVersion")
    implementation("org.apache.commons:commons-compress:$commonsCompressVersion")
    // Observability
    implementation("io.quarkus:quarkus-observability-devservices-lgtm")
    // Library containing the definitions of all events sent through the message queue within Themis
    implementation(project(":libraries:common-themis"))
    // closeableScope
    implementation("com.black-kamelia.sprinkler:utils:$sprinklerUtilsVersion")

    testImplementation("io.quarkus:quarkus-test-kafka-companion")
    testImplementation("io.quarkus:quarkus-test-vertx")
    testImplementation("io.quarkus:quarkus-junit5-mockito")
    testImplementation("org.mockito.kotlin:mockito-kotlin:$kotlinMockitoVersion")
    testImplementation("io.rest-assured:kotlin-extensions")
    testImplementation("io.rest-assured:rest-assured")
    testImplementation("io.smallrye.reactive:smallrye-reactive-messaging-in-memory")
    testImplementation("nl.rug.digitallab.common.quarkus:test-rest:$commonQuarkusVersion")
}
