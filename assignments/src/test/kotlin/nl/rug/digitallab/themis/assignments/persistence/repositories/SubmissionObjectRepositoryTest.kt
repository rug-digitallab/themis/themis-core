package nl.rug.digitallab.themis.assignments.persistence.repositories

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.EphemeralFile
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.common.quarkus.test.rest.MockFileUpload
import nl.rug.digitallab.themis.assignments.exceptions.submission.SubmissionAlreadyExistsException
import nl.rug.digitallab.themis.assignments.exceptions.submission.SubmissionNotFoundException
import nl.rug.digitallab.themis.assignments.persistence.namespaces.SubmissionNamespace
import nl.rug.digitallab.themis.assignments.processing.handlers.ProcessSubmissionHandler
import nl.rug.digitallab.themis.assignments.util.SubmissionsTestUtils
import nl.rug.digitallab.themis.assignments.util.TestUtil.generateSubmissionContext
import nl.rug.digitallab.themis.common.typealiases.*
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.zip.ZipInputStream
import kotlin.io.path.readBytes

@QuarkusTest
class SubmissionObjectRepositoryTest {
    @Inject
    lateinit var submissionObjectRepository: SubmissionObjectRepository

    @Inject
    lateinit var processSubmissionHandlerHandler: ProcessSubmissionHandler

    private lateinit var submissionNamespace: SubmissionNamespace

    @BeforeEach
    fun `Set submissionNamespace`() {
        submissionNamespace = SubmissionNamespace(
            CourseInstanceId.randomUUID(),
            GroupId.randomUUID(),
            AssignmentId.randomUUID(),
            SubmissionId.randomUUID(),
        )
    }

    @Test
    fun `Uploading a submission to MinIO should return the correct ObjectWriteResponse`() {
        val uploadedFile = getResource("submissions/valid/individual/valid_submission.c").asPath()
        val fileUploads = listOf(MockFileUpload(uploadedFile))

        prepareZipFile(fileUploads).use { zipFile ->
            val objectWriteResponse = submissionObjectRepository.uploadSubmission(zipFile.path, submissionNamespace, ProfileId.randomUUID())

            assertEquals(submissionNamespace.bucketName, objectWriteResponse.bucket())
            assertEquals(submissionNamespace.objectName, objectWriteResponse.`object`())
        }
    }

    @Test
    fun `Uploading the same submission twice should result in a SubmissionAlreadyExistsException`() {
        val uploadedFile = getResource("submissions/valid/individual/valid_submission.c").asPath()
        val fileUploads = listOf(MockFileUpload(uploadedFile))

        prepareZipFile(fileUploads).use { zipFile ->
            // This should not throw an exception
            submissionObjectRepository.uploadSubmission(zipFile.path, submissionNamespace, ProfileId.randomUUID())

            // This should throw an exception
            assertThrows<SubmissionAlreadyExistsException> {
                submissionObjectRepository.uploadSubmission(zipFile.path, submissionNamespace, ProfileId.randomUUID())
            }
        }
    }

    @Test
    fun `Downloading a submission should return the correct file`() {
        val fileUploads = listOf(MockFileUpload(getResource("submissions/valid/individual/valid_submission.c").asPath()))

        prepareZipFile(fileUploads).use { zipFile ->
            submissionObjectRepository.uploadSubmission(zipFile.path, submissionNamespace, ProfileId.randomUUID())
        }

        val downloadInputStream = submissionObjectRepository.downloadSubmission(submissionNamespace)

        // Unzip downloaded file stream, collect all zip entries
        val downloadedFiles = mutableListOf<ByteArray>()
        ZipInputStream(downloadInputStream).use { zipInputStream ->
            while (zipInputStream.nextEntry != null) {
                downloadedFiles.add(zipInputStream.readBytes())
            }
        }
        val uploadedFiles = fileUploads.map { it.uploadedFile().readBytes() }

        assertEquals(fileUploads.size, downloadedFiles.size)
        uploadedFiles.forEachIndexed { index, uploadedFile ->
            assertEquals(uploadedFile.size, downloadedFiles[index].size)
            assertArrayEquals(uploadedFile, downloadedFiles[index])
        }
    }

    @Test
    fun `Batch downloading multiple submissions should return the correct files`() {
        val fileUploads = SubmissionsTestUtils.validFileSubmissions().map { MockFileUpload(it) }

        val submissionNamespaces = fileUploads.map { fileUpload ->
            val submissionNamespace = SubmissionNamespace(
                CourseInstanceId.randomUUID(),
                GroupId.randomUUID(),
                AssignmentId.randomUUID(),
                SubmissionId.randomUUID(),
            )

            // Upload the files as individual zip files instead of in one zip file
            prepareZipFile(listOf(fileUpload)).use { zipFile ->
                submissionObjectRepository.uploadSubmission(zipFile.path, submissionNamespace, ProfileId.randomUUID())
            }

            return@map submissionNamespace
        }

        val downloadInputStream = submissionObjectRepository.downloadBatchSubmissions(submissionNamespaces)

        // Unzip downloaded file stream, collect all zip entries
        val downloadedFiles = mutableListOf<ByteArray>()
        ZipInputStream(downloadInputStream).use { zipInputStream ->
            while (zipInputStream.nextEntry != null) {
                downloadedFiles.add(zipInputStream.readBytes())
            }
        }
        val uploadedFiles = fileUploads.map { it.uploadedFile().readBytes() }

        assertEquals(fileUploads.size, downloadedFiles.size)
        uploadedFiles.forEachIndexed { index, uploadedFile ->
            assertEquals(uploadedFile.size, downloadedFiles[index].size)
            assertArrayEquals(uploadedFile, downloadedFiles[index])
        }
    }

    @Test
    fun `Downloading a non existing submission should result in a SubmissionNotFoundException`() {
        assertThrows(SubmissionNotFoundException::class.java) {
            submissionObjectRepository.downloadSubmission(submissionNamespace)
        }
    }

    @Test
    fun `Batch downloading a non existing submission should result in a SubmissionNotFoundException`() {
        assertThrows(SubmissionNotFoundException::class.java) {
            submissionObjectRepository.downloadBatchSubmissions(listOf(submissionNamespace))
        }
    }

    @Test
    fun `Getting a submission that does not exist should result in a SubmissionNotFoundException`() {
        assertThrows(SubmissionNotFoundException::class.java) {
            submissionObjectRepository.getSubmission(submissionNamespace)
        }
    }

    private fun prepareZipFile(fileUploads: List<MockFileUpload>): EphemeralFile {
        val context = generateSubmissionContext(fileUploads)

        // The zip file is prepared by the ProcessSubmissionHandler
        processSubmissionHandlerHandler.handle(context)
        return context.zipFile
    }
}
