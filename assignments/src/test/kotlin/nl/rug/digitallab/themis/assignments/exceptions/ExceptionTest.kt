package nl.rug.digitallab.themis.assignments.exceptions

import io.minio.UploadObjectArgs
import io.minio.errors.ErrorResponseException
import io.minio.messages.ErrorResponse
import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.common.kotlin.helpers.resource.asFile
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.themis.assignments.exceptions.minio.NoResponseException
import nl.rug.digitallab.themis.assignments.exceptions.minio.TagNotFoundException
import nl.rug.digitallab.themis.assignments.exceptions.submission.SubmissionAlreadyExistsException
import nl.rug.digitallab.themis.assignments.exceptions.submission.SubmissionNotFoundException
import nl.rug.digitallab.themis.assignments.persistence.namespaces.SubmissionNamespace
import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import nl.rug.digitallab.themis.common.typealiases.CourseInstanceId
import nl.rug.digitallab.themis.common.typealiases.GroupId
import nl.rug.digitallab.themis.common.typealiases.SubmissionId
import okhttp3.Protocol
import okhttp3.Request
import okhttp3.Response
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

@QuarkusTest
class ExceptionTest {
    @Test
    fun `NoResponseException should have the correct exception message`() {
        val tags = mapOf(
            "submissionId" to SubmissionId.randomUUID(),
            "courseInstanceId" to CourseInstanceId.randomUUID(),
            "assignmentId" to AssignmentId.randomUUID(),
            "groupId" to GroupId.randomUUID(),
            "profile" to "test-profile",
        ).mapValues { it.value.toString() }

        val file = getResource("file/illegal_file_names.txt").asFile()

        val bucketName = "test-bucket"
        val objectName = "test-object"
        val uploadArgs = UploadObjectArgs.builder()
            .filename(file.path)
            .contentType("text/plain")
            .bucket(bucketName)
            .`object`(objectName)
            .tags(tags)
            .build()

        val exception = NoResponseException(uploadArgs)

        val expectedMessage = "No response from minio for $bucketName/$objectName"
        assertEquals(expectedMessage, exception.message, "The exception message should be formatted correctly")
    }

    @Test
    fun `SubmissionAlreadyExistsException should have the correct exception message`() {
        val objectName = "objectName"
        val exception = SubmissionAlreadyExistsException(objectName)

        val expectedMessage = "Submission '$objectName' already exists"
        assertEquals(expectedMessage, exception.message, "The exception message should be formatted correctly")
    }

    @Test
    fun `TagNotFoundException should have the correct exception message`() {
        val missingTags = setOf("tag1", "tag2", "tag3")
        val exception = TagNotFoundException(missingTags)

        val expectedMessage = "The following tags were not found: $missingTags"
        assertEquals(expectedMessage, exception.message, "The exception message should be formatted correctly")
    }

    @Test
    fun `Requesting a non-existing submission should throw SubmissionNotFoundException`() {
        val request = Request.Builder()
            .url("https://example.com")
            .build()

        val response = Response.Builder()
            .request(request)
            .protocol(Protocol.HTTP_1_1)
            .code(404)
            .message("Not Found")
            .build()

        val errorResponseException = ErrorResponseException(ErrorResponse(), response, "")
        val submissionNamespace = SubmissionNamespace(CourseInstanceId.randomUUID(), GroupId.randomUUID(), AssignmentId.randomUUID(), SubmissionId.randomUUID())
        val exception = SubmissionNotFoundException(errorResponseException, submissionNamespace)

        assertEquals("Submission '${submissionNamespace.objectName}' not found: ${errorResponseException.message}", exception.message)
    }
}
