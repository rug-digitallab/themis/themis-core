package nl.rug.digitallab.themis.assignments.persistence.namespaces

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import nl.rug.digitallab.themis.common.typealiases.CourseInstanceId
import nl.rug.digitallab.themis.common.typealiases.GroupId
import nl.rug.digitallab.themis.common.typealiases.SubmissionId
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

@QuarkusTest
class NamespaceTest {
    @Test
    fun `Test whether the prefix name of the assignment namespace is set properly`() {
        val assignmentNamespace = AssignmentNamespace(
            courseInstanceId = CourseInstanceId.randomUUID(),
            groupId = GroupId.randomUUID(),
            assignmentId = AssignmentId.randomUUID(),
        )

        assertEquals(
            "${assignmentNamespace.groupId}/${assignmentNamespace.assignmentId}",
            assignmentNamespace.prefixName
        )
    }

    @Test
    fun `Test whether the object name of the submission namespace is set properly`() {
        val submissionNamespace = SubmissionNamespace(
            courseInstanceId = CourseInstanceId.randomUUID(),
            groupId = GroupId.randomUUID(),
            assignmentId = AssignmentId.randomUUID(),
            submissionId = SubmissionId.randomUUID(),
        )

        assertEquals(
            "${submissionNamespace.prefixName}/${submissionNamespace.submissionId}/submission.zip",
            submissionNamespace.objectName
        )
    }

    @Test
    fun `Test whether the full name of the submission namespace is set properly`() {
        val submissionNamespace = SubmissionNamespace(
            courseInstanceId = CourseInstanceId.randomUUID(),
            groupId = GroupId.randomUUID(),
            assignmentId = AssignmentId.randomUUID(),
            submissionId = SubmissionId.randomUUID(),
        )

        assertEquals(
            "${submissionNamespace.bucketName}/${submissionNamespace.objectName}",
            submissionNamespace.fullName
        )
    }

    @Test
    fun `Test whether the full name of the assignment namespace is set properly`() {
        val assignmentNamespace = AssignmentNamespace(
            courseInstanceId = CourseInstanceId.randomUUID(),
            groupId = GroupId.randomUUID(),
            assignmentId = AssignmentId.randomUUID(),
        )

        assertEquals("${assignmentNamespace.bucketName}/${assignmentNamespace.prefixName}", assignmentNamespace.fullName)
    }
}
