package nl.rug.digitallab.themis.assignments.managers

import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.assignments.exceptions.config.ConfigurationNotFoundException
import nl.rug.digitallab.themis.assignments.util.TestUtil
import nl.rug.digitallab.themis.common.typealiases.ConfigurationId
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Test

@QuarkusTest
class ConfigurationManagerTest {
    @Inject
    private lateinit var assignmentsManager: AssignmentsManager

    @Inject
    private lateinit var configurationManager: ConfigurationManager

    @Test
    @TestTransaction
    fun `Getting an assignment config for a config ID should return the correct config`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val assignment = assignmentsManager.addAssignment(addAssignmentRequest)

        val config = assignment.activeProfileConfigs.first()
        val retrievedConfig = configurationManager.getConfig(config.id)

        assertEquals(config.config, retrievedConfig.config)
        assertEquals(config.id, retrievedConfig.id)
        assertEquals(config.revision, retrievedConfig.revision)
    }

    @Test
    @TestTransaction
    fun `Getting an assignment config for a non-existing config ID should throw an exception`() {
        val nonExistingConfigId = ConfigurationId.randomUUID()
        assertThrows<ConfigurationNotFoundException> {
            configurationManager.getConfig(nonExistingConfigId)
        }
    }
}
