package nl.rug.digitallab.themis.assignments

import io.quarkus.test.junit.QuarkusTest
import io.restassured.module.kotlin.extensions.Extract
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import jakarta.ws.rs.core.UriBuilder
import nl.rug.digitallab.themis.assignments.AssignmentsResourceTest.Companion.addAssignment
import nl.rug.digitallab.themis.assignments.AssignmentsResourceTest.Companion.getAssignmentByIdEndpoint
import nl.rug.digitallab.themis.assignments.dtos.AssignmentResponse
import nl.rug.digitallab.themis.assignments.dtos.config.ConfigurationResponse
import nl.rug.digitallab.themis.assignments.util.TestUtil
import nl.rug.digitallab.themis.common.typealiases.ConfigurationId
import nl.rug.digitallab.themis.common.typealiases.CourseInstanceId
import org.jboss.resteasy.reactive.RestResponse.StatusCode
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import kotlin.reflect.jvm.javaMethod

@QuarkusTest
class ConfigurationResourceTest {
    @Test
    fun `A configuration should be retrievable by its synthetic ID`() {
        val courseInstanceId = CourseInstanceId.randomUUID()
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest(courseInstanceId)

        val assignmentId = addAssignment(addAssignmentRequest)

        val assignment =
            When {
                get(getAssignmentByIdEndpoint(assignmentId))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(AssignmentResponse::class.java)
            }

        val config = assignment.activeProfileConfigs.first()

        val retrievedConfig =
            When {
                get(getConfigSyntheticIdEndpoint(config.id))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(ConfigurationResponse::class.java)
            }

        assertEquals(config.id, retrievedConfig.id)
        assertEquals(config.revision, retrievedConfig.revision)
        assertEquals(config.config, retrievedConfig.config)
    }

    @Test
    fun `A configuration should not be retrievable by a non-existing synthetic ID`() {
        val nonExistingConfigId = ConfigurationId.randomUUID()

        When {
            get(getConfigSyntheticIdEndpoint(nonExistingConfigId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    companion object {
        fun getConfigSyntheticIdEndpoint(configId: ConfigurationId): String {
            return UriBuilder
                .fromResource(ConfigurationResource::class.java)
                .path(ConfigurationResource::getConfig.javaMethod)
                .build(configId)
                .toString()
        }
    }
}
