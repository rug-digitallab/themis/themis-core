package nl.rug.digitallab.themis.assignments.processing.handlers

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.assignments.persistence.repositories.SubmissionObjectRepository
import nl.rug.digitallab.themis.assignments.util.SubmissionContextGenerator
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows

@QuarkusTest
class UploadSubmissionHandlerTest {
    @Inject
    lateinit var uploadSubmissionHandler: UploadSubmissionHandler

    @Inject
    lateinit var objectRepository: SubmissionObjectRepository

    @Inject
    lateinit var contextGenerator: SubmissionContextGenerator

    @Test
    fun `Uploading a submission should upload the submission to the object repository`() {
        val context = contextGenerator.generateContextWithSubmission()
        uploadSubmissionHandler.handle(context)

        context.use {
            val namespace = it.submissionNamespace
            assertDoesNotThrow { objectRepository.downloadSubmission(namespace) }
            assertEquals(context.assignment.courseInstanceId, namespace.courseInstanceId)
            assertEquals(context.submission.id, namespace.submissionId)
            assertEquals(context.assignment.id, namespace.assignmentId)
            assertEquals(context.addSubmissionRequest.groupId, namespace.groupId)
        }
    }

    @Test
    fun `Uploading a submission should fail when the submission is not present in the context`() {
        val context = contextGenerator.generateContext()

        context.use {
            assertThrows<UninitializedPropertyAccessException> { uploadSubmissionHandler.handle(context) }
        }
    }
}
