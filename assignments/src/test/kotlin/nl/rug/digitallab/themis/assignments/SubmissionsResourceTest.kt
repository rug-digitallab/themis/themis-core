package nl.rug.digitallab.themis.assignments

import com.kamelia.sprinkler.util.closeableScope
import io.quarkus.test.junit.QuarkusTest
import io.restassured.http.ContentType
import io.restassured.module.kotlin.extensions.*
import jakarta.inject.Inject
import jakarta.ws.rs.core.UriBuilder
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.EphemeralDirectory
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.EphemeralFile
import nl.rug.digitallab.common.kotlin.helpers.resource.asFile
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.common.quarkus.archiver.TarArchiver
import nl.rug.digitallab.common.quarkus.archiver.ZipArchiver
import nl.rug.digitallab.themis.assignments.util.SubmissionsTestUtils
import nl.rug.digitallab.themis.assignments.util.TestUtil
import nl.rug.digitallab.themis.common.typealiases.*
import org.hamcrest.CoreMatchers.containsString
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.notNullValue
import org.jboss.resteasy.reactive.RestResponse.StatusCode
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import java.io.File
import kotlin.io.path.writeBytes
import kotlin.reflect.jvm.javaMethod

@QuarkusTest
class SubmissionsResourceTest {
    @Inject
    lateinit var tarArchiver: TarArchiver

    @Inject
    lateinit var zipArchiver: ZipArchiver

    @Test
    fun `Adding a submission should return the submission ID`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val assignmentId = AssignmentsResourceTest.addAssignment(addAssignmentRequest)

        addSubmission(assignmentId, addAssignmentRequest.profileConfigs.first().profileName)
    }

    @TestFactory
    fun `Adding an invalid submission should return a 400`(): List<DynamicTest> {
        return SubmissionsTestUtils.allInvalidSubmissions().map {
            DynamicTest.dynamicTest("Adding invalid submission '${it.name}' should always fail") {
                Given {
                    multiPart(it)
                } When {
                    post(addSubmissionEndpoint())
                } Then {
                    contentType(ContentType.JSON)
                    statusCode(StatusCode.BAD_REQUEST)
                    body("errorMessage", notNullValue())
                }
            }
        }
    }

    @TestFactory
    fun `Adding a submission containing invalid characters should result in HTTP 400 with corresponding error`(): List<DynamicTest> {
        return SubmissionsTestUtils.invalidIllegalCharactersSubmissions().map {
            DynamicTest.dynamicTest("Adding submission '${it.name}' containing invalid characters should result in HTTP 400 with corresponding error") {
                Given {
                    multiPart(it)
                } When {
                    post(addSubmissionEndpoint())
                } Then {
                    contentType(ContentType.JSON)
                    statusCode(StatusCode.BAD_REQUEST)
                    body("errorMessage", containsString("contains illegal characters"))
                }
            }
        }
    }

    @TestFactory
    fun `Adding a submission with an invalid file name length should result in HTTP 400 with corresponding error`(): List<DynamicTest> {
        return SubmissionsTestUtils.invalidFileNameLengthSubmissions().map {
            DynamicTest.dynamicTest("Adding submission '${it.name}' with an invalid file name length should result in HTTP 400 with corresponding error") {
                Given {
                    multiPart(it)
                } When {
                    post(addSubmissionEndpoint())
                } Then {
                    contentType(ContentType.JSON)
                    statusCode(StatusCode.BAD_REQUEST)
                    body("errorMessage", containsString("File name is too long"))
                }
            }
        }
    }

    @TestFactory
    fun `Adding a submission with duplicate files should result in HTTP 400 with corresponding error`(): List<DynamicTest> {
        return SubmissionsTestUtils.allValidSubmissions().map {
            DynamicTest.dynamicTest("Adding submission with duplicate files '${it.name}' should result in HTTP 400 with corresponding error") {
                Given {
                    multiPart(it)
                    multiPart(it) // Upload the same file twice
                } When {
                    post(addSubmissionEndpoint())
                } Then {
                    contentType(ContentType.JSON)
                    statusCode(StatusCode.BAD_REQUEST)
                    body("errorMessage", containsString("A duplicate file was uploaded"))
                }
            }
        }
    }


    @TestFactory
    fun `Adding a submission without files should result in HTTP 400 with corresponding error`(): List<DynamicTest> {
        return (SubmissionsTestUtils.invalidNoFilesSubmissions() + null).map {
            DynamicTest.dynamicTest("Adding submission '${it?.name ?: "null"}' without files should result in HTTP 400 with corresponding error") {
                Given {
                    if(it != null) {
                        multiPart(it)
                    }
                    contentType(ContentType.MULTIPART)
                } When {
                    post(addSubmissionEndpoint())
                } Then {
                    contentType(ContentType.JSON)
                    statusCode(StatusCode.BAD_REQUEST)
                    body("errorMessage", containsString("No files were uploaded"))
                }
            }
        }

    }

    @Test
    fun `Adding a submission with an invalid content type should result in HTTP 415 with corresponding error`() {
        Given {
            body("submissions/valid/individual/valid_submission.c")
            contentType(ContentType.JSON)
        } When {
            post(addSubmissionEndpoint())
        } Then {
            contentType(ContentType.JSON)
            statusCode(StatusCode.UNSUPPORTED_MEDIA_TYPE)
            body("errorMessage", containsString("The content-type header value did not match"))
        }
    }

    @Test
    fun `Listing submissions with specific filters should only return submissions that match that filter`() {
        /**
         * Validate a request to the list submissions endpoint. This function will check if the response has the correct
         * status code, content type and size.
         *
         * @param endpoint The endpoint to send the request to.
         * @param expectedSize The expected size of the response.
         */
        fun validateRequest(endpoint: String, expectedSize: Int) {
            When {
                get(endpoint)
            } Then {
                statusCode(StatusCode.OK)
                contentType(ContentType.JSON)
                body("size()", equalTo(expectedSize))
            }
        }

        val addAssignmentRequestA = TestUtil.generateAddAssignmentRequest()
        val addAssignmentRequestB = addAssignmentRequestA.copy()

        val courseInstanceId = addAssignmentRequestA.courseInstanceId
        val assignmentIds = listOf(
            AssignmentsResourceTest.addAssignment(addAssignmentRequestA),
            AssignmentsResourceTest.addAssignment(addAssignmentRequestB),
        )
        val groupId = GroupId.randomUUID()
        val profileName = addAssignmentRequestA.profileConfigs.first().profileName

        repeat(3) {
            assignmentIds.forEach {
                addSubmission(it, profileName)
            }

            assignmentIds.forEach {
                addSubmission(it, profileName, groupId)
            }
        }

        validateRequest(listSubmissionsEndpoint(courseInstanceId = courseInstanceId), 12)
        validateRequest(listSubmissionsEndpoint(assignmentId = assignmentIds.first()), 6)
        validateRequest(listSubmissionsEndpoint(groupId = groupId), 6)
        validateRequest(listSubmissionsEndpoint(assignmentId = assignmentIds.first(), groupId = groupId), 3)
        validateRequest(listSubmissionsEndpoint(courseInstanceId = courseInstanceId, groupId = groupId), 6)
        validateRequest(listSubmissionsEndpoint(courseInstanceId = courseInstanceId, assignmentId = assignmentIds.first(), groupId = groupId), 3)
    }

    @Test
    fun `Listing submissions when there are none should result in an empty list`() {
        When {
            get(listSubmissionsEndpoint(CourseInstanceId.randomUUID(), GroupId.randomUUID(), AssignmentId.randomUUID()))
        } Then {
            contentType(ContentType.JSON)
            statusCode(StatusCode.OK)
            body("size()", equalTo(0))
        }
    }

    @TestFactory
    fun `Getting a specific submission by ID should return the correct submission`(): List<DynamicTest> {
        return SubmissionsTestUtils.allValidSubmissions().map {
            DynamicTest.dynamicTest("Posting valid submission '${it.name}' should be retrievable") {
                val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
                val courseInstanceId = addAssignmentRequest.courseInstanceId
                val profileName = addAssignmentRequest.profileConfigs.first().profileName
                val assignmentId = AssignmentsResourceTest.addAssignment(addAssignmentRequest)
                val submissionId = addSubmission(assignmentId, profileName)

                When {
                    get(getSubmissionEndpoint(submissionId))
                } Then {
                    statusCode(StatusCode.OK)
                    contentType(ContentType.JSON)
                    body("id", equalTo(submissionId.toString()))
                    body("courseInstanceId", equalTo(courseInstanceId.toString()))
                    body("assignmentId", equalTo(assignmentId.toString()))
                }
            }
        }
    }

    @Test
    fun `Getting a non-existent submission by ID should return a 404 error with corresponding error message`() {
        val submissionId = SubmissionId.randomUUID()

        When {
            get(getSubmissionEndpoint(submissionId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
            contentType(ContentType.JSON)
            body("errorMessage", containsString("Submission '$submissionId' not found"))
        }
    }

    @TestFactory
    fun `Downloading a specific submission should return the submission files`(): List<DynamicTest> {
        return SubmissionsTestUtils.allValidSubmissions().map {
            DynamicTest.dynamicTest("Downloading valid submission '${it.name}' should return the submission files") {
                val submissionId = addSubmission(files = listOf(it))

                val response =
                    When {
                        get(downloadSubmissionEndpoint(submissionId))
                    } Then {
                        contentType("application/zip")
                        statusCode(StatusCode.OK)
                    } Extract {
                        response().asByteArray()
                    }

                closeableScope {
                    val tempDownloadedDir = using(EphemeralDirectory(prefix = "submission-downloaded-")).path
                    val zipFile = using(EphemeralFile(prefix = "submission", suffix = ".zip")).path

                    // Write response to temp zip file
                    zipFile.writeBytes(response)

                    zipArchiver.unarchive(zipFile, tempDownloadedDir)

                    SubmissionsTestUtils.assertUploadedFileIsEqualToDownloadedFile(tarArchiver, it, tempDownloadedDir)
                }
            }
        }
    }

    @Test
    fun `Downloading a non-existent submission should return a 404 error with corresponding error message`() {
        val submissionId = SubmissionId.randomUUID()

        When {
            get(downloadSubmissionEndpoint(submissionId))
        } Then {
            contentType(ContentType.JSON)
            statusCode(StatusCode.NOT_FOUND)
            body("errorMessage", containsString("Submission '$submissionId' not found"))
        }
    }

    @Test
    fun `Downloading a batch of valid submissions by ID should result in an archive which contains all the files`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val assignmentId = AssignmentsResourceTest.addAssignment(addAssignmentRequest)
        val profileName = addAssignmentRequest.profileConfigs.first().profileName
        val groupId = GroupId.randomUUID()

        val submissionIds = (1..3).map { addSubmission(assignmentId, profileName, groupId) }

        val response =
            When {
                get(downloadBatchSubmissionsEndpoint(submissionIds))
            } Then {
                contentType("application/zip")
                statusCode(StatusCode.OK)
            } Extract {
                response().asByteArray()
            }

        closeableScope {
            val tempDownloadedDir = using(EphemeralDirectory(prefix = "submission-downloaded-")).path
            val zipFile = using(EphemeralFile(prefix = "submission", suffix = ".zip")).path

            // Write response to temp zip file
            zipFile.writeBytes(response)

            zipArchiver.unarchive(zipFile, tempDownloadedDir)

            val submissionFile = listOf(
                getResource("submissions/valid/individual/valid_submission.c").asFile()
            )

            for(submissionId in submissionIds) {
                val submissionPath = tempDownloadedDir
                    .resolve(groupId.toString())
                    .resolve(assignmentId.toString())
                    .resolve(submissionId.toString())
                SubmissionsTestUtils.assertUploadedFileIsEqualToDownloadedFile(tarArchiver, submissionFile[0], submissionPath)
            }
        }
    }

    @Test
    fun `Downloading a batch of non-existent submissions should return a 404 error with corresponding error message`() {
        val submissionIds = (1..3).map { SubmissionId.randomUUID() }

        When {
            get(downloadBatchSubmissionsEndpoint(submissionIds))
        } Then {
            contentType(ContentType.JSON)
            statusCode(StatusCode.NOT_FOUND)
            body("errorMessage", containsString("Submissions '${submissionIds.joinToString()}' not found"))
        }
    }

    /*
     * Helper functions for the resource tests
     */

    companion object {
        private val defaultSubmission = getResource("submissions/valid/individual/valid_submission.c").asFile()

        fun addSubmission(
            assignmentId: AssignmentId,
            profileName: String,
            groupId: GroupId = GroupId.randomUUID(),
            files: List<File> = listOf(defaultSubmission),
        ): SubmissionId {
            return Given {
                files.forEach {
                    multiPart(it)
                }
                contentType(ContentType.MULTIPART)
            } When {
                post(addSubmissionEndpoint(assignmentId, groupId, profileName))
            } Then {
                statusCode(StatusCode.ACCEPTED)
            } Extract {
                body().`as`(SubmissionId::class.java)
            }
        }

        fun addSubmission(
            groupId: GroupId = GroupId.randomUUID(),
            files: List<File> = listOf(defaultSubmission),
        ): SubmissionId {
            val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
            val assignmentId = AssignmentsResourceTest.addAssignment(addAssignmentRequest)
            val profileName = addAssignmentRequest.profileConfigs.first().profileName

            return addSubmission(assignmentId, profileName, groupId, files)
        }

        fun addSubmissionEndpoint(): String {
            val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
            val assignmentId = AssignmentsResourceTest.addAssignment(addAssignmentRequest)
            val profileName = addAssignmentRequest.profileConfigs.first().profileName

            return addSubmissionEndpoint(assignmentId, GroupId.randomUUID(), profileName)
        }

        fun addSubmissionEndpoint(assignmentId: AssignmentId, groupId: GroupId, profileName: String): String {
            return UriBuilder
                .fromResource(AssignmentsResource::class.java)
                .path(AssignmentsResource::addSubmission.javaMethod)
                .queryParam("groupId", groupId)
                .queryParam("profileName", profileName)
                .build(assignmentId)
                .toString()
        }

        fun listSubmissionsEndpoint(
            courseInstanceId: CourseInstanceId? = null,
            assignmentId: AssignmentId? = null,
            groupId: GroupId? = null,
        ): String {
             val uriBuilder = UriBuilder
                .fromResource(SubmissionsResource::class.java)
                .path(SubmissionsResource::listSubmissions.javaMethod)

            if(courseInstanceId != null) uriBuilder.queryParam("courseInstanceId", courseInstanceId)
            if(assignmentId != null) uriBuilder.queryParam("assignmentId", assignmentId)
            if(groupId != null) uriBuilder.queryParam("groupId", groupId)

            return uriBuilder
                .build()
                .toString()
        }

        fun getSubmissionEndpoint(submissionId: SubmissionId): String {
            return UriBuilder
                .fromResource(SubmissionsResource::class.java)
                .path(SubmissionsResource::getSubmission.javaMethod)
                .build(submissionId)
                .toString()
        }

        fun downloadSubmissionEndpoint(submissionId: SubmissionId): String {
            return UriBuilder
                .fromResource(SubmissionsResource::class.java)
                .path(SubmissionsResource::downloadSubmission.javaMethod)
                .build(submissionId)
                .toString()
        }

        fun downloadBatchSubmissionsEndpoint(submissionIds: List<SubmissionId>): String {
            val uriBuilder = UriBuilder
                .fromResource(SubmissionsResource::class.java)
                .path(SubmissionsResource::downloadBatchSubmissions.javaMethod)

            submissionIds.forEach {
                uriBuilder.queryParam("submissionIds", it)
            }

            return uriBuilder
                .build()
                .toString()
        }
    }
}
