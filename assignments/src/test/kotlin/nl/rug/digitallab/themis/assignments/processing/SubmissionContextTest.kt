package nl.rug.digitallab.themis.assignments.processing

import nl.rug.digitallab.themis.assignments.util.TestUtil.generateAddSubmissionRequest
import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import kotlin.io.path.exists
import kotlin.io.path.notExists

class SubmissionContextTest {
    @Test
    fun `When close() is invoked on a SubmissionContext, all temporary resources should be cleaned up`() {
        val submissionContext = SubmissionContext(AssignmentId.randomUUID(), generateAddSubmissionRequest("profile"))

        submissionContext.use {
            assertTrue(submissionContext.zipFile.path.exists())
            assertTrue(submissionContext.processingDirectory.path.exists())
        }

        assertTrue(submissionContext.zipFile.path.notExists())
        assertTrue(submissionContext.processingDirectory.path.notExists())
    }
}
