package nl.rug.digitallab.themis.assignments.persistence.repositories

import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.assignments.exceptions.submission.SubmissionNotFoundException
import nl.rug.digitallab.themis.assignments.persistence.entities.*
import nl.rug.digitallab.themis.assignments.persistence.entities.Submission.SubmissionStatus
import nl.rug.digitallab.themis.assignments.util.TestUtil
import nl.rug.digitallab.themis.common.typealiases.CourseInstanceId
import nl.rug.digitallab.themis.common.typealiases.SubmissionId
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

@QuarkusTest
class SubmissionMetaRepositoryTest {
    @Inject
    lateinit var submissionMetaRepository: SubmissionMetaRepository

    @Inject
    lateinit var assignmentRepository: AssignmentRepository

    @Test
    @TestTransaction
    fun `Finding a submission by ID should return the correct submission`() {
        val assignment = TestUtil.generateRandomAssignment()
        assignmentRepository.create(assignment)

        val submission = TestUtil.generateRandomSubmission(assignment)
        submissionMetaRepository.create(submission)

        val foundSubmission = submissionMetaRepository.findByIdOrThrow(submission.id)
        submission.doCompare(foundSubmission)
    }

    @Test
    @TestTransaction
    fun `Finding a submission by ID should throw an exception if the submission does not exist`() {
        val submissionId = SubmissionId.randomUUID()

        assertThrows<SubmissionNotFoundException> {
            submissionMetaRepository.findByIdOrThrow(submissionId)
        }
    }

    @Test
    @TestTransaction
    fun `Finding all submissions with a given filter should return the correct submissions`() {
        // Generate 5 random assignments
        val courseInstanceId = CourseInstanceId.randomUUID()
        val assignment = TestUtil.generateRandomAssignment(courseInstanceId)
        assignmentRepository.create(assignment)

        val submissions = (1..5).map { TestUtil.generateRandomSubmission(assignment) }
        submissions.forEach { submissionMetaRepository.create(it) }

        val foundSubmissions = submissionMetaRepository.find(courseInstanceId = courseInstanceId)
        assertEquals(submissions.size, foundSubmissions.size)

        submissions.forEach { submission ->
            val foundSubmission = foundSubmissions.find { it.id == submission.id }
            submission.doCompare(foundSubmission!!)
        }
    }

    @Test
    @TestTransaction
    fun `Finding multiple submissions by their IDs should return the correct submissions`() {
        // Generate 5 random assignments
        val courseInstanceId = CourseInstanceId.randomUUID()
        val assignment = TestUtil.generateRandomAssignment(courseInstanceId)
        assignmentRepository.create(assignment)

        val submissions = (1..5).map { TestUtil.generateRandomSubmission(assignment) }
        submissions.forEach { submissionMetaRepository.create(it) }

        val foundSubmissions = submissionMetaRepository.find(submissions.map { it.id })
        assertEquals(submissions.size, foundSubmissions.size)

        submissions.forEach { submission ->
            val foundSubmission = foundSubmissions.find { it.id == submission.id }
            submission.doCompare(foundSubmission!!)
        }
    }

    @Test
    @TestTransaction
    fun `Updating the submission status of a submission should return the correct submission`() {
        val assignment = TestUtil.generateRandomAssignment()
        assignmentRepository.create(assignment)

        val submission = TestUtil.generateRandomSubmission(assignment)
        submissionMetaRepository.create(submission)
        submissionMetaRepository.updateStatus(submission, SubmissionStatus.QUEUED)

        assertEquals(SubmissionStatus.QUEUED, submission.status)
    }
}

fun Submission.doCompare(other: Submission) {
    assertEquals(this.id, other.id)
    assertEquals(this.created, other.created)
    assertEquals(this.updated, other.updated)
    this.assignment.doCompare(other.assignment)
    assertEquals(this.groupId, other.groupId)
    this.config.doCompare(other.config)
    assertEquals(this.status, other.status)
}

fun Configuration.doCompare(other: Configuration) {
    assertEquals(this.id, other.id)
    assertEquals(this.config, other.config)
    this.profile.doCompare(other.profile)
}

fun Profile.doCompare(other: Profile) {
    assertEquals(this.id, other.id)
    assertEquals(this.name, other.name)
    assertEquals(this.created, other.created)
    assertEquals(this.updated, other.updated)
    this.assignment.doCompare(other.assignment)
}
