package nl.rug.digitallab.themis.assignments.persistence.repositories

import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.assignments.exceptions.config.ConfigurationNotFoundException
import nl.rug.digitallab.themis.assignments.util.TestUtil
import nl.rug.digitallab.themis.common.typealiases.ConfigurationId
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

@QuarkusTest
class ConfigurationRepositoryTest {
    @Inject
    lateinit var assignmentRepository: AssignmentRepository

    @Inject
    lateinit var configurationRepository: ConfigurationRepository

    @Test
    @TestTransaction
    fun `A config can be retrieved by its natural IDs`() {
        val assignment = TestUtil.generateRandomAssignment()

        assignmentRepository.create(assignment)

        val config = configurationRepository.findByNaturalId(
            assignment.profiles.first(),
            assignment.profiles.first().configurations.first().revision,
        )

        assertEquals(
            assignment.profiles.first().configurations.first(),
            config,
        )
    }

    @Test
    @TestTransaction
    fun `Finding a config by ID should return the correct config`() {
        val assignment = TestUtil.generateRandomAssignment()

        assignmentRepository.create(assignment)
        val foundConfig = configurationRepository.findByIdOrThrow(assignment.profiles.first().configurations.first().id)

        assignment.profiles.first().configurations.first().doCompare(foundConfig)
    }

    @Test
    @TestTransaction
    fun `Finding a config by ID should throw an exception if the config does not exist`() {
        val configId = ConfigurationId.randomUUID()
        assertThrows<ConfigurationNotFoundException> {
            configurationRepository.findByIdOrThrow(configId)
        }
    }
}
