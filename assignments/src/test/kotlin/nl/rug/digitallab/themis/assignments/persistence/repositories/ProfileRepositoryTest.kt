package nl.rug.digitallab.themis.assignments.persistence.repositories

import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.assignments.util.TestUtil
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

@QuarkusTest
class ProfileRepositoryTest {
    @Inject
    lateinit var assignmentRepository: AssignmentRepository

    @Inject
    lateinit var profileRepository: ProfileRepository

    @Test
    @TestTransaction
    fun `A profile can be retrieved by its natural IDs`() {
        val assignment = TestUtil.generateRandomAssignment()

        assignmentRepository.create(assignment)

        val profile = profileRepository.findByNaturalId(
            assignment,
            assignment.profiles.first().name,
        )

        assertEquals(
            assignment.profiles.first(),
            profile,
        )
    }
}
