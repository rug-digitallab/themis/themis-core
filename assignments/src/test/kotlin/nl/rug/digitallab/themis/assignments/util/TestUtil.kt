package nl.rug.digitallab.themis.assignments.util

import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.common.quarkus.test.rest.MockFileUpload
import nl.rug.digitallab.themis.assignments.dtos.AddProfileRequest
import nl.rug.digitallab.themis.assignments.dtos.AddAssignmentRequest
import nl.rug.digitallab.themis.assignments.dtos.AddSubmissionRequest
import nl.rug.digitallab.themis.assignments.dtos.config.ParsedConfiguration
import nl.rug.digitallab.themis.assignments.persistence.entities.*
import nl.rug.digitallab.themis.assignments.processing.SubmissionContext
import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import nl.rug.digitallab.themis.common.typealiases.CourseInstanceId
import nl.rug.digitallab.themis.common.typealiases.GroupId
import java.time.Instant
import java.util.*

object TestUtil {
    fun generateRandomAssignment(courseInstanceId: CourseInstanceId = CourseInstanceId.randomUUID()): Assignment {
        val assignment = Assignment(
            name = "Test assignment",
            description = "Test description",
            courseInstanceId = courseInstanceId,
            softDeadline = Instant.EPOCH,
            hardDeadline = Instant.EPOCH.plusSeconds(60),
            visibilitySpecification = VisibilitySpecification.ALWAYS_VISIBLE,
        )

        assignment.profiles.add(generateRandomProfile(assignment))
        assignment.profiles.add(generateRandomProfile(assignment))

        return assignment
    }

    fun generateRandomProfile(assignment: Assignment): Profile {
        val profile = Profile(
            name = UUID.randomUUID().toString(),
            assignment = assignment,
        )

        profile.configurations.add(generateRandomConfiguration(profile))
        profile.configurations.add(generateRandomConfiguration(profile))

        return profile
    }

    fun generateRandomConfiguration(profile: Profile): Configuration {
        return Configuration(
            profile = profile,
            config = "{}",
        )
    }

    fun generateRandomSubmission(assignment: Assignment, groupId: GroupId = GroupId.randomUUID()): Submission {
        return Submission(
            assignment = assignment,
            groupId = groupId,
            config = assignment.profiles.first().currentConfiguration,
        )
    }

    fun generateAddAssignmentRequest(courseInstanceId: CourseInstanceId = CourseInstanceId.randomUUID()): AddAssignmentRequest {
        val profiles = listOf(
            generateAddProfileRequest(),
            generateAddProfileRequest(),
        )

        return AddAssignmentRequest(
            name = "Test assignment",
            description = "Test description",
            courseInstanceId = courseInstanceId,
            softDeadline = Instant.EPOCH,
            hardDeadline = Instant.EPOCH.plusSeconds(60),
            visibilitySpecification = VisibilitySpecification.ALWAYS_VISIBLE,
            profileConfigs = profiles,
        )
    }

    fun generateAddProfileRequest(): AddProfileRequest {
        return AddProfileRequest(
            profileName = UUID.randomUUID().toString(),
            config = ParsedConfiguration(
                pointCombinationExpression = "5 + 5",
                profile = RuntimeProfile(emptyList()),
            ),
        )
    }

    fun generateAddSubmissionRequest(profileName: String): AddSubmissionRequest {
        return AddSubmissionRequest(
            groupId = GroupId.randomUUID(),
            profileName = profileName,
            fileUploads = listOf(MockFileUpload(getResource("submissions/valid/individual/valid_submission.c").asPath())),
        )
    }

    fun generateSubmissionContext(fileUploads: List<MockFileUpload>): SubmissionContext {
        val addSubmissionRequest = AddSubmissionRequest(
            groupId = GroupId.randomUUID(),
            profileName = "profile",
            fileUploads = fileUploads,
        )

        val context = SubmissionContext(
            assignmentId = AssignmentId.randomUUID(),
            addSubmissionRequest = addSubmissionRequest,
        )

        // The zip file is prepared by the ProcessSubmissionHandler
        return context
    }
}
