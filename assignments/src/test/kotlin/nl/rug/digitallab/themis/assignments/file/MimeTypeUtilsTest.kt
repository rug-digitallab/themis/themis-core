package nl.rug.digitallab.themis.assignments.file

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import kotlin.io.path.Path

class MimeTypeUtilsTest {
    @TestFactory
    fun `getMimeType should return the expected MIME type of a file`(): List<DynamicTest> =
        mapOf(
            "some/dir/file.c" to "text/plain",
            "some/dir/file.tar" to "application/x-tar",
        ).map { (path, expected) ->
            DynamicTest.dynamicTest("getMimeType should return the MIME type of file `$path`") {
                val file = Path(path)
                val actual = file.getMimeType().toString()
                assertEquals(expected, actual)
            }
        }

    @Test
    fun `getMimeType should return application octet-stream for unknown MIME types`() {
        val actual = Path("some/dir/unknown_file").getMimeType().toString()
        assertEquals("application/octet-stream", actual)
    }
}
