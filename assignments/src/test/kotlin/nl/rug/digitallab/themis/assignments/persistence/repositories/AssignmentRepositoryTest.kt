package nl.rug.digitallab.themis.assignments.persistence.repositories

import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.assignments.exceptions.assignment.AssignmentNotFoundException
import nl.rug.digitallab.themis.assignments.persistence.entities.Assignment
import nl.rug.digitallab.themis.assignments.util.TestUtil
import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import nl.rug.digitallab.themis.common.typealiases.CourseInstanceId
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

@QuarkusTest
class AssignmentRepositoryTest {
    @Inject
    lateinit var assignmentRepository: AssignmentRepository

    @Test
    @TestTransaction
    fun `Finding an assignment by ID should return the correct assignment`() {
        val assignment = TestUtil.generateRandomAssignment()

        assignmentRepository.create(assignment)
        val foundAssignment = assignmentRepository.findByIdOrThrow(assignment.id)

        assignment.doCompare(foundAssignment)
    }

    @Test
    @TestTransaction
    fun `Finding an assignment by ID should throw an exception if the assignment does not exist`() {
        val assignmentId = AssignmentId.randomUUID()
        assertThrows<AssignmentNotFoundException> {
            assignmentRepository.findByIdOrThrow(assignmentId)
        }
    }

    @Test
    @TestTransaction
    fun `Finding all assignments with a given filter should return the correct assignments`() {
        // Generate 5 random assignments
        val courseInstanceId = CourseInstanceId.randomUUID()
        val assignments = (1..5).map { TestUtil.generateRandomAssignment(courseInstanceId) }

        assignments.forEach { assignmentRepository.create(it) }
        val foundAssignments = assignmentRepository.find(courseInstanceId = courseInstanceId)

        assertEquals(assignments.size, foundAssignments.size)
        assignments.forEach { assignment ->
            val foundAssignment = foundAssignments.find { it.id == assignment.id }
            assignment.doCompare(foundAssignment!!)
        }
    }

    @Test
    @TestTransaction
    fun `Persisting an assignment with an invalid deadline should throw an exception`() {
        val assignment = TestUtil.generateRandomAssignment()
        assignment.softDeadline = assignment.hardDeadline?.plusSeconds(1)

        assertThrows<IllegalStateException> {
            assignmentRepository.create(assignment)
        }
    }

    @Test
    @TestTransaction
    fun `Generated revisions are unique within a profile`() {
        val assignment = TestUtil.generateRandomAssignment()
        val thirdConfig = TestUtil.generateRandomConfiguration(assignment.profiles.first())
        val fourthConfig = TestUtil.generateRandomConfiguration(assignment.profiles.first())
        assignment.profiles.first().configurations.add(thirdConfig)
        assignment.profiles.first().configurations.add(fourthConfig)

        assignmentRepository.create(assignment)

        val retrievedAssignment = assignmentRepository.findByIdOrThrow(assignment.id)

        // All revisions should be unique within a profile
        val firstProfile = retrievedAssignment.profiles.first()
        assertEquals(firstProfile.configurations.size, firstProfile.configurations.map { it.revision }.distinct().size)
        val secondProfile = retrievedAssignment.profiles.last()
        assertEquals(secondProfile.configurations.size, secondProfile.configurations.map { it.revision }.distinct().size)
    }

    @Test
    @TestTransaction
    fun `Revisions are generated sequentially`() {
        val assignment = TestUtil.generateRandomAssignment()
        val thirdConfig = TestUtil.generateRandomConfiguration(assignment.profiles.first())
        val fourthConfig = TestUtil.generateRandomConfiguration(assignment.profiles.first())
        assignment.profiles.first().configurations.add(thirdConfig)
        assignment.profiles.first().configurations.add(fourthConfig)

        assignmentRepository.create(assignment)

        val retrievedAssignment = assignmentRepository.findByIdOrThrow(assignment.id)

        // Revisions should be generated sequentially
        val firstProfile = retrievedAssignment.profiles.first()
        assertEquals(firstProfile.configurations.map { it.revision }, (1..firstProfile.configurations.size).toList())
        val secondProfile = retrievedAssignment.profiles.last()
        assertEquals(secondProfile.configurations.map { it.revision }, (1..secondProfile.configurations.size).toList())
    }
}

fun Assignment.doCompare(other: Assignment) {
    assertEquals(this.id, other.id)
    assertEquals(this.created, other.created)
    assertEquals(this.updated, other.updated)
    assertEquals(this.name, other.name)
    assertEquals(this.description, other.description)
    assertEquals(this.courseInstanceId, other.courseInstanceId)
    assertEquals(this.softDeadline, other.softDeadline)
    assertEquals(this.hardDeadline, other.hardDeadline)
    assertEquals(this.visibilitySpecification.visibilityType, other.visibilitySpecification.visibilityType)
}
