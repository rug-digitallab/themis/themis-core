package nl.rug.digitallab.themis.assignments.managers

import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import io.smallrye.reactive.messaging.memory.InMemoryConnector
import jakarta.inject.Inject
import nl.rug.digitallab.themis.assignments.dtos.AddSubmissionRequest
import nl.rug.digitallab.themis.assignments.dtos.ListSubmissionsRequest
import nl.rug.digitallab.themis.assignments.dtos.SubmissionResponse
import nl.rug.digitallab.themis.assignments.persistence.entities.Submission.SubmissionStatus
import nl.rug.digitallab.themis.assignments.util.TestUtil
import nl.rug.digitallab.themis.common.constants.SUBMISSION_QUEUE
import nl.rug.digitallab.themis.common.events.SubmissionAddedEvent
import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import org.awaitility.Awaitility.await
import org.eclipse.microprofile.reactive.messaging.spi.Connector
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.zip.ZipInputStream
import kotlin.io.path.readBytes


@QuarkusTest
class SubmissionsManagerTest {
    @Inject
    lateinit var submissionsManager: SubmissionsManager

    @Inject
    lateinit var assignmentsManager: AssignmentsManager

    @Inject
    @Connector("smallrye-in-memory")
    lateinit var connector: InMemoryConnector

    @BeforeEach
    fun `Empty the queue`() {
        val consumer = connector.sink<SubmissionAddedEvent>(SUBMISSION_QUEUE)
        consumer.clear()
    }

    @Test
    @TestTransaction
    fun `Adding a submission should return the correct submission`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val addSubmissionRequest = TestUtil.generateAddSubmissionRequest(addAssignmentRequest.profileConfigs.first().profileName)

        val assignment = assignmentsManager.addAssignment(addAssignmentRequest)
        val submission = submissionsManager.addSubmission(assignment.id, addSubmissionRequest)

        submission.doCompare(addSubmissionRequest, assignment.id)
    }

    @Test
    @TestTransaction
    fun `Adding a submission should emit an event`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val addSubmissionRequest = TestUtil.generateAddSubmissionRequest(addAssignmentRequest.profileConfigs.first().profileName)

        val assignment = assignmentsManager.addAssignment(addAssignmentRequest)
        val submission = submissionsManager.addSubmission(assignment.id, addSubmissionRequest)

        val inMemorySink = connector.sink<SubmissionAddedEvent>(SUBMISSION_QUEUE)

        await().until {
            inMemorySink.received().isNotEmpty()
        }

        val receivedEvent = inMemorySink.received().first().payload
        inMemorySink.clear()

        assertEquals(submission.id, receivedEvent.submissionId)
        assertEquals(assignment.id, receivedEvent.assignmentId)
        assertEquals(addSubmissionRequest.groupId, receivedEvent.groupId)
    }

    @Test
    @TestTransaction
    fun `Getting a specific submission by ID should return the correct submission`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val addSubmissionRequest = TestUtil.generateAddSubmissionRequest(addAssignmentRequest.profileConfigs.first().profileName)

        val assignment = assignmentsManager.addAssignment(addAssignmentRequest)
        val submission = submissionsManager.addSubmission(assignment.id, addSubmissionRequest)
        val retrievedSubmission = submissionsManager.getSubmission(submission.id)

        retrievedSubmission.doCompare(addSubmissionRequest, assignment.id)
    }

    @Test
    @TestTransaction
    fun `Listing all submissions for a given course, assignment, and group should return the correct number of submissions`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val addSubmissionRequests = (1..3).map { TestUtil.generateAddSubmissionRequest(addAssignmentRequest.profileConfigs.first().profileName) }

        val assignment = assignmentsManager.addAssignment(addAssignmentRequest)
        val submissionIds = addSubmissionRequests.map { submissionsManager.addSubmission(assignment.id, it).id }

        val submissions = submissionsManager.listSubmissions(
            ListSubmissionsRequest(
                courseInstanceId = addAssignmentRequest.courseInstanceId,
                assignmentId = assignment.id,
            )
        )

        assertEquals(submissionIds.size, submissions.size)
        assertArrayEquals(submissionIds.sorted().toTypedArray(), submissions.map { it.id }.sorted().toTypedArray())
    }

    @Test
    @TestTransaction
    fun `Downloading a submission should result in the correct file`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val addSubmissionRequest = TestUtil.generateAddSubmissionRequest(addAssignmentRequest.profileConfigs.first().profileName)

        val assignment = assignmentsManager.addAssignment(addAssignmentRequest)
        val submission = submissionsManager.addSubmission(assignment.id, addSubmissionRequest)
        val downloadInputStream = submissionsManager.downloadSubmission(submission.id)

        // Unzip downloaded file stream, collect all zip entries
        val downloadedFiles = mutableListOf<ByteArray>()
        ZipInputStream(downloadInputStream).use { zipInputStream ->
            while (zipInputStream.nextEntry != null) {
                downloadedFiles.add(zipInputStream.readBytes())
            }
        }
        val uploadedFiles = addSubmissionRequest.fileUploads.map { it.uploadedFile().readBytes() }

        assertEquals(addSubmissionRequest.fileUploads.size, downloadedFiles.size)
        uploadedFiles.forEachIndexed { index, uploadedFile ->
            assertEquals(uploadedFile.size, downloadedFiles[index].size)
            assertArrayEquals(uploadedFile, downloadedFiles[index])
        }
    }

    @Test
    @TestTransaction
    fun `Batch downloading multiple submissions should return the correct files`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val addSubmissionRequests = (1..3).map { TestUtil.generateAddSubmissionRequest(addAssignmentRequest.profileConfigs.first().profileName) }

        val assignment = assignmentsManager.addAssignment(addAssignmentRequest)
        val submissionIds = addSubmissionRequests.map { submissionsManager.addSubmission(assignment.id, it).id }
        val downloadInputStream = submissionsManager.downloadBatchSubmission(submissionIds)

        // Unzip downloaded file stream, collect all zip entries
        val downloadedFiles = mutableListOf<ByteArray>()
        ZipInputStream(downloadInputStream).use { zipInputStream ->
            while (zipInputStream.nextEntry != null) {
                downloadedFiles.add(zipInputStream.readBytes())
            }
        }

        val uploadedFiles = addSubmissionRequests.flatMap { request ->
            request.fileUploads.map { it.filePath().readBytes() }
        }

        assertEquals(uploadedFiles.size, downloadedFiles.size)
        uploadedFiles.forEachIndexed { index, uploadedFile ->
            assertEquals(uploadedFile.size, downloadedFiles[index].size)
            assertArrayEquals(uploadedFile, downloadedFiles[index])
        }
    }
}

fun SubmissionResponse.doCompare(addSubmissionRequest: AddSubmissionRequest, id: AssignmentId? = null) {
    if (id != null) {
        assertEquals(id, this.assignmentId)
    }

    assertEquals(addSubmissionRequest.groupId, this.groupId)
    assertEquals(SubmissionStatus.QUEUED, this.status)
}
