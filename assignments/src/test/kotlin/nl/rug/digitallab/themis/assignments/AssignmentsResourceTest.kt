package nl.rug.digitallab.themis.assignments

import io.quarkus.test.junit.QuarkusTest
import io.restassured.http.ContentType
import io.restassured.module.kotlin.extensions.Extract
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import jakarta.ws.rs.core.UriBuilder
import nl.rug.digitallab.themis.assignments.dtos.AddAssignmentRequest
import nl.rug.digitallab.themis.assignments.dtos.AssignmentResponse
import nl.rug.digitallab.themis.assignments.dtos.config.ConfigurationResponse
import nl.rug.digitallab.themis.assignments.dtos.config.JobConfig
import nl.rug.digitallab.themis.assignments.dtos.config.ParsedConfiguration
import nl.rug.digitallab.themis.assignments.managers.doCompare
import nl.rug.digitallab.themis.assignments.persistence.entities.VisibilitySpecification
import nl.rug.digitallab.themis.assignments.util.TestUtil
import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import nl.rug.digitallab.themis.common.typealiases.CourseInstanceId
import org.jboss.resteasy.reactive.RestResponse.StatusCode
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.time.Instant
import kotlin.reflect.jvm.javaMethod

@QuarkusTest
class AssignmentsResourceTest {
    @Test
    fun `Adding a new assignment should return the assignment ID`() {
        val courseInstanceId = CourseInstanceId.randomUUID()
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest(courseInstanceId)

        addAssignment(addAssignmentRequest)
    }

    @Test
    fun `Adding a new assignment with a hard deadline before the soft deadline should fail`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest().copy(
            softDeadline = Instant.now(),
            hardDeadline = Instant.now().minusSeconds(60),
        )

        Given {
            contentType(ContentType.JSON)
        } When {
            body(addAssignmentRequest)
            post(addAssignmentEndpoint())
        } Then {
            // TODO: Change to bad request once common-quarkus supports nested IllegalStateException
            statusCode(StatusCode.INTERNAL_SERVER_ERROR)
        }
    }

    @Test
    fun `Adding a new assignment with a blank name should fail`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest().copy(
            name = "",
        )

        Given {
            contentType(ContentType.JSON)
        } When {
            body(addAssignmentRequest)
            post(addAssignmentEndpoint())
        } Then {
            statusCode(StatusCode.BAD_REQUEST)
        }
    }

    @Test
    fun `Listing all assignments for a given course instance should return the expected assignments`() {
        val courseInstanceId = CourseInstanceId.randomUUID()
        val assignmentIds = (1..5).map {
            addAssignment(TestUtil.generateAddAssignmentRequest(courseInstanceId))
        }

        val assignmentsList =
            When {
                get(listAssignmentsEndpoint(courseInstanceId))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(Array<AssignmentResponse>::class.java)
            }

        assertTrue(assignmentIds.containsAll(assignmentsList.map { it.id }))
    }

    @Test
    fun `Listing all assignments for a given visibility time should return only the visible assignments at that time`() {
        val courseInstanceId = CourseInstanceId.randomUUID()

        val currentInstant = Instant.now()

        // Always visible assignment
        val visibleAssignment = addAssignment(TestUtil.generateAddAssignmentRequest(courseInstanceId))

        // Timed assignment that should be visible
        val visibleTimedAssignment = addAssignment(
            TestUtil.generateAddAssignmentRequest(courseInstanceId).copy(visibilitySpecification = VisibilitySpecification.timed(currentInstant.minusSeconds(10), currentInstant.plusSeconds(10))),
        )

        val visibleAssignmentIds = listOf(visibleAssignment, visibleTimedAssignment)

        // Timed course instance that should not be visible, was in the past
        addAssignment(
            TestUtil.generateAddAssignmentRequest(courseInstanceId).copy(visibilitySpecification = VisibilitySpecification.timed(currentInstant.minusSeconds(20), currentInstant.minusSeconds(10))),
        )

        // Timed course instance that should not be visible, will be in the future
        addAssignment(
            TestUtil.generateAddAssignmentRequest(courseInstanceId).copy(visibilitySpecification = VisibilitySpecification.timed(currentInstant.plusSeconds(10), currentInstant.plusSeconds(20))),
        )

        // Never visible course instance
        addAssignment(
            TestUtil.generateAddAssignmentRequest(courseInstanceId).copy(visibilitySpecification = VisibilitySpecification.NEVER_VISIBLE),
        )

        val assignmentsList =
            When {
                // TODO: something goes wrong with encoding when using the URI Builder, maybe double encoding through RestAssured?
                get(listAssignmentsEndpoint(courseInstanceId) + "&visibleAtTime=$currentInstant" )
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(Array<AssignmentResponse>::class.java)
            }

        assertEquals(visibleAssignmentIds.size, assignmentsList.size)
        assertTrue(assignmentsList.map { it.id }.containsAll(visibleAssignmentIds))
    }

    @Test
    fun `Getting a specific assignment by ID should return the correct assignment`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val assignmentId = addAssignment(addAssignmentRequest)

        val assignment =
            When {
                get(getAssignmentByIdEndpoint(assignmentId))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(AssignmentResponse::class.java)
            }

        assignment.doCompare(addAssignmentRequest, assignmentId)
    }

    @Test
    fun `Getting a non-existent assignment by ID should return a 404 error`() {
        val nonExistentAssignmentId = AssignmentId.randomUUID()

        When {
            get(getAssignmentByIdEndpoint(nonExistentAssignmentId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    @Test
    fun `Getting an assignment config should return the correct assignment configs`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val assignmentId = addAssignment(addAssignmentRequest)
        val profileName = addAssignmentRequest.profileConfigs.first().profileName
        val profileConfig = addAssignmentRequest.profileConfigs.first().config

        val configuration =
            When {
                get(getConfigurationsEndpoint(assignmentId, profileName))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(Array<ConfigurationResponse>::class.java)
            }

        assertEquals(1, configuration.size)
        assertEquals(profileConfig, configuration.first().config)
    }

    @Test
    fun `Getting an assignment config for a non-existent profile should return a 404 error`() {
        val courseInstanceId = CourseInstanceId.randomUUID()
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest(courseInstanceId)
        val assignmentId = addAssignment(addAssignmentRequest)
        val nonExistentProfileName = "non-existent-profile"

        When {
            get(getConfigurationsEndpoint(assignmentId, nonExistentProfileName))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    @Test
    fun `Updating an assignment should return the updated assignment`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val assignmentId = addAssignment(addAssignmentRequest)

        val updatedAddAssignmentRequest = addAssignmentRequest.copy(
            name = "Updated name",
            description = "Updated description",
        )

        val updatedAssignment =
            Given {
                contentType(ContentType.JSON)
            } When {
                body(updatedAddAssignmentRequest)
                put(updateAssignmentEndpoint(assignmentId))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(AssignmentResponse::class.java)
            }

        updatedAssignment.doCompare(updatedAddAssignmentRequest, assignmentId)
    }

    @Test
    fun `Updating a non-existent assignment should return a 404 error`() {
        val nonExistentAssignmentId = AssignmentId.randomUUID()
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()

        Given {
            contentType(ContentType.JSON)
        } When {
            body(addAssignmentRequest)
            put(updateAssignmentEndpoint(nonExistentAssignmentId))
        } Then {
            statusCode(StatusCode.NOT_FOUND)
        }
    }

    @Test
    fun `Updating an assignment config of an existing profile should result in a new revision`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val assignmentId = addAssignment(addAssignmentRequest)
        val profileName = addAssignmentRequest.profileConfigs.first().profileName
        val oldConfig = addAssignmentRequest.profileConfigs.first().config

        val updatedConfiguration =
            Given {
                contentType(ContentType.JSON)
            } When {
                body(basicConfig)
                put(updateConfigurationEndpoint(assignmentId, profileName))
            } Then {
                statusCode(StatusCode.CREATED)
            } Extract {
                body().`as`(ConfigurationResponse::class.java)
            }

        assertEquals(basicConfig, updatedConfiguration.config)
        assertEquals(2, updatedConfiguration.revision)

        val configurations =
            When {
                get(getConfigurationsEndpoint(assignmentId, profileName))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(Array<ConfigurationResponse>::class.java)
            }

        assertEquals(2, configurations.size)
        assertTrue(configurations.toList().map { it.config }.containsAll(listOf(oldConfig, basicConfig)))
    }

    @Test
    fun `Updating an assignment config a non-existent profile should result in a new assignment profile and config`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val assignmentId = addAssignment(addAssignmentRequest)
        val profileName = "non-existent-profile"

        val updatedConfiguration =
            Given {
                contentType(ContentType.JSON)
            } When {
                body(basicConfig)
                put(updateConfigurationEndpoint(assignmentId, profileName))
            } Then {
                statusCode(StatusCode.CREATED)
            } Extract {
                body().`as`(ConfigurationResponse::class.java)
            }

        assertEquals(basicConfig, updatedConfiguration.config)
        assertEquals(1, updatedConfiguration.revision)
    }

    @Test
    fun `Deleting an assignment should return a 204`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val assignmentId = addAssignment(addAssignmentRequest)

        When {
            delete(deleteAssignmentEndpoint(assignmentId))
        } Then {
            statusCode(StatusCode.NO_CONTENT)
        }
    }

    @Test
    fun `A config should be able to be retrieved by its natural ID`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val assignmentId = addAssignment(addAssignmentRequest)

        val activeProfileConfig = addAssignmentRequest.profileConfigs.first()
        val revision = 1

        val config =
            When {
                get(getConfigNaturalIdEndpoint(assignmentId, activeProfileConfig.profileName, revision))
            } Then {
                statusCode(StatusCode.OK)
            } Extract {
                body().`as`(ConfigurationResponse::class.java)
            }

        assertEquals(activeProfileConfig.config, config.config)
        assertEquals(revision, config.revision)
    }

    /*
     * Helper functions for the resource tests
     */
    companion object {
        fun addAssignment(addAssignmentRequest: AddAssignmentRequest): AssignmentId {
            return Given {
                contentType(ContentType.JSON)
            } When {
                body(addAssignmentRequest)
                post(addAssignmentEndpoint())
            } Then {
                statusCode(StatusCode.CREATED)
            } Extract {
                body().`as`(AssignmentId::class.java)
            }
        }

        fun addAssignmentEndpoint(): String {
            return UriBuilder
                .fromResource(AssignmentsResource::class.java)
                .path(AssignmentsResource::addAssignment.javaMethod)
                .build()
                .toString()
        }

        fun listAssignmentsEndpoint(courseInstanceId: CourseInstanceId): String {
            return UriBuilder
                .fromResource(AssignmentsResource::class.java)
                .path(AssignmentsResource::listAssignments.javaMethod)
                .queryParam("courseInstanceId", courseInstanceId)
                .build()
                .toString()
        }

        fun getAssignmentByIdEndpoint(assignmentId: AssignmentId): String {
            return UriBuilder
                .fromResource(AssignmentsResource::class.java)
                .path(AssignmentsResource::getAssignment.javaMethod)
                .build(assignmentId)
                .toString()
        }

        fun getConfigurationsEndpoint(assignmentId: AssignmentId, profileName: String): String {
            return UriBuilder
                .fromResource(AssignmentsResource::class.java)
                .path(AssignmentsResource::getConfigurations.javaMethod)
                .queryParam("profileName", profileName)
                .build(assignmentId)
                .toString()
        }

        fun getConfigNaturalIdEndpoint(assignmentId: AssignmentId, profileName: String, revision: Int): String {
            return UriBuilder
                .fromResource(AssignmentsResource::class.java)
                .path(AssignmentsResource::getConfig.javaMethod)
                .build(assignmentId, profileName, revision)
                .toString()
        }

        fun updateAssignmentEndpoint(assignmentId: AssignmentId): String {
            return UriBuilder
                .fromResource(AssignmentsResource::class.java)
                .path(AssignmentsResource::updateAssignment.javaMethod)
                .build(assignmentId)
                .toString()
        }

        fun updateConfigurationEndpoint(assignmentId: AssignmentId, profileName: String): String {
            return UriBuilder
                .fromResource(AssignmentsResource::class.java)
                .path(AssignmentsResource::updateConfiguration.javaMethod)
                .queryParam("profileName", profileName)
                .build(assignmentId)
                .toString()
        }

        fun deleteAssignmentEndpoint(assignmentId: AssignmentId): String {
            return UriBuilder
                .fromResource(AssignmentsResource::class.java)
                .path(AssignmentsResource::deleteAssignment.javaMethod)
                .build(assignmentId)
                .toString()
        }
    }

    private val basicConfig = ParsedConfiguration(
        pointCombinationExpression = "10 + 10",
        profile = RuntimeProfile(
            stages = listOf(
                RuntimeProfile.Stage(
                    name = "stage",
                    jobs = listOf(
                        JobConfig(
                            name = "job",
                            labelPoints = mapOf(
                                "succeeded" to 2,
                                "memory-leak" to 1,
                                "failed" to 0,
                            ),
                            labelAssertions = mapOf(
                                "first" to "actions['execute'].exitCode == 1 && actions['diff'].exitCode == 1",
                            ),
                        )
                    )
                )
            )
        )
    )
}
