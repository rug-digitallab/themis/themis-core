package nl.rug.digitallab.themis.assignments.util

import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.transaction.Transactional
import nl.rug.digitallab.themis.assignments.persistence.repositories.AssignmentRepository
import nl.rug.digitallab.themis.assignments.persistence.repositories.SubmissionMetaRepository
import nl.rug.digitallab.themis.assignments.processing.SubmissionContext
import nl.rug.digitallab.themis.assignments.util.TestUtil.generateAddSubmissionRequest
import nl.rug.digitallab.themis.assignments.util.TestUtil.generateRandomAssignment
import nl.rug.digitallab.themis.assignments.util.TestUtil.generateRandomSubmission
import nl.rug.digitallab.themis.common.typealiases.AssignmentId

@ApplicationScoped
class SubmissionContextGenerator {
    @Inject
    private lateinit var assignmentRepository: AssignmentRepository

    @Inject
    private lateinit var submissionRepository: SubmissionMetaRepository

    fun generateContext(): SubmissionContext {
        return SubmissionContext(AssignmentId.randomUUID(), generateAddSubmissionRequest("test"))
    }

    @Transactional
    fun generateContextWithAssignment(): SubmissionContext {
        val assignment = generateRandomAssignment()
        assignmentRepository.create(assignment)
        assignmentRepository.flush()
        val profile = assignment.profiles.first()

        println("Assignment: ${assignment.id}")

        val context = SubmissionContext(assignment.id, generateAddSubmissionRequest(profile.name))
        context.assignment = assignment

        return context
    }

    @Transactional
    fun generateContextWithSubmission(): SubmissionContext {
        val context = generateContextWithAssignment()
        val submission = generateRandomSubmission(context.assignment, context.addSubmissionRequest.groupId)
        submissionRepository.create(submission)
        submissionRepository.flush()

        context.submission = submission
        return context
    }

    @Transactional
    fun generateContextWithInvalidAssignmentProfile(): SubmissionContext {
        val context = generateContextWithAssignment()
        context.addSubmissionRequest.profileName = "invalid"
        return context
    }

    @Transactional
    fun generateContextWithNoFiles(): SubmissionContext {
        val context = generateContextWithAssignment()
        context.addSubmissionRequest.fileUploads = emptyList()
        return context
    }
}
