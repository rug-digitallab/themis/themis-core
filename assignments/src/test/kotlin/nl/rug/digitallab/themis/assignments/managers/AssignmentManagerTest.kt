package nl.rug.digitallab.themis.assignments.managers

import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.assignments.exceptions.assignment.AssignmentNotFoundException
import nl.rug.digitallab.themis.assignments.exceptions.assignment.ProfileNotFoundException
import nl.rug.digitallab.themis.assignments.dtos.AddAssignmentRequest
import nl.rug.digitallab.themis.assignments.dtos.AssignmentResponse
import nl.rug.digitallab.themis.assignments.dtos.config.ParsedConfiguration
import nl.rug.digitallab.themis.assignments.util.TestUtil
import nl.rug.digitallab.themis.common.data.RuntimeProfile
import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import nl.rug.digitallab.themis.common.typealiases.CourseInstanceId
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

@QuarkusTest
class AssignmentManagerTest {
    @Inject
    lateinit var assignmentsManager: AssignmentsManager

    @Test
    @TestTransaction
    fun `Adding an assignment should return the correct assignment`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()

        val assignment = assignmentsManager.addAssignment(addAssignmentRequest)

        assertEquals(addAssignmentRequest.name, assignment.name)
    }

    @Test
    @TestTransaction
    fun `Listing assignments should return the correct assignments`() {
        // Generate 5 random assignments
        val courseInstanceId = CourseInstanceId.randomUUID()
        val addAssignmentRequests = (1..5).map { TestUtil.generateAddAssignmentRequest(courseInstanceId) }

        addAssignmentRequests.forEach {
            assignmentsManager.addAssignment(it)
        }

        val assignments = assignmentsManager.listAssignments(courseInstanceId)

        assertEquals(addAssignmentRequests.size, assignments.size)
        addAssignmentRequests.forEach { addAssignmentRequest ->
            val assignment = assignments.find { it.name == addAssignmentRequest.name }
            assignment!!.doCompare(addAssignmentRequest)
        }
    }

    @Test
    @TestTransaction
    fun `Getting an assignment by ID should return the correct assignment`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()

        val assignment = assignmentsManager.addAssignment(addAssignmentRequest)
        val retrievedAssignment = assignmentsManager.getAssignment(assignment.id)

        retrievedAssignment.doCompare(addAssignmentRequest, assignment.id)
    }

    @Test
    @TestTransaction
    fun `Getting an assignment config for an assignment and profile should return the correct config`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val profileName = addAssignmentRequest.profileConfigs.first().profileName

        val assignment = assignmentsManager.addAssignment(addAssignmentRequest)
        val configs = assignmentsManager.getConfigurations(assignment.id, profileName)

        assertEquals(1, configs.size)
        assertEquals(addAssignmentRequest.profileConfigs.first().config, configs.first().config)
    }

    @Test
    @TestTransaction
    fun `Getting an assignment config for an existing assignment but non-existing profile should throw an exception`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val profileName = "Non-existing profile"

        val assignment = assignmentsManager.addAssignment(addAssignmentRequest)
        assertThrows<ProfileNotFoundException> {
            assignmentsManager.getConfigurations(assignment.id, profileName)
        }
    }

    @Test
    @TestTransaction
    fun `Updating a course instance should return the correct course instance`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val updatedAddAssignmentRequest = addAssignmentRequest.copy(
            name = "Updated name",
            description = "Updated description",
        )

        val assignment = assignmentsManager.addAssignment(addAssignmentRequest)
        val updatedAssignment = assignmentsManager.updateAssignment(assignment.id, updatedAddAssignmentRequest)

        updatedAssignment.doCompare(updatedAddAssignmentRequest)
    }

    @Test
    @TestTransaction
    fun `Updating an assignment config should add the new config to the existing assignment profile`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val profileName = addAssignmentRequest.profileConfigs.first().profileName
        val updatedConfigString = ParsedConfiguration(
            pointCombinationExpression = "1 + 1",
            profile = RuntimeProfile(emptyList())
        )

        val assignment = assignmentsManager.addAssignment(addAssignmentRequest)

        val configuration = assignmentsManager.updateConfigurations(assignment.id, profileName, updatedConfigString)
        assertEquals(updatedConfigString, configuration.config)

        val configurations = assignmentsManager.getConfigurations(assignment.id, profileName)
        assertEquals(configurations.size, 2)
        assertEquals(updatedConfigString, configurations.find { it.revision == 2 }!!.config)
    }

    @Test
    @TestTransaction
    fun `Updating an assignment of a non-existing profile should create a new profile`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val profileName = "New profile"
        val updatedConfigString = ParsedConfiguration(
            pointCombinationExpression = "1 + 1",
            profile = RuntimeProfile(emptyList())
        )

        val assignment = assignmentsManager.addAssignment(addAssignmentRequest)
        assignmentsManager.updateConfigurations(assignment.id, profileName, updatedConfigString)

        val configurations = assignmentsManager.getConfigurations(assignment.id, profileName)
        assertEquals(configurations.size, 1)
        assertEquals(updatedConfigString, configurations.first().config)
    }

    @Test
    @TestTransaction
    fun `Deleting an assignment should remove the assignment`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()

        val response = assignmentsManager.addAssignment(addAssignmentRequest)
        assignmentsManager.deleteAssignment(response.id)

        assertThrows<AssignmentNotFoundException> {
            assignmentsManager.getAssignment(response.id)
        }
    }

    @Test
    @TestTransaction
    fun `A config can be retrieved through its natural ID`() {
        val addAssignmentRequest = TestUtil.generateAddAssignmentRequest()
        val assignment = assignmentsManager.addAssignment(addAssignmentRequest)

        val activeProfileConfig = assignment.activeProfileConfigs.first()
        val profileName = activeProfileConfig.profileName
        val revision = activeProfileConfig.revision

        val retrievedConfig = assignmentsManager.getConfig(assignment.id, profileName, revision)
        assertEquals(activeProfileConfig.revision, retrievedConfig.revision)
        assertEquals(activeProfileConfig.config, retrievedConfig.config)
    }
}

fun AssignmentResponse.doCompare(request: AddAssignmentRequest, id: AssignmentId? = null) {
    if (id != null) {
        assertEquals(id, this.id)
    }
    assertEquals(request.name, this.name)
    assertEquals(request.description, this.description)
    assertEquals(request.courseInstanceId, this.courseInstanceId)
    assertEquals(request.softDeadline, this.softDeadline)
    assertEquals(request.hardDeadline, this.hardDeadline)
    assertEquals(request.visibilitySpecification.visibilityType.toString(), this.visibilitySpecification.visibilityType)
}
