package nl.rug.digitallab.themis.assignments.util

import nl.rug.digitallab.common.kotlin.helpers.ephemeral.withEphemeralDirectory
import nl.rug.digitallab.common.kotlin.helpers.resource.asFile
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.common.quarkus.archiver.TarArchiver
import org.junit.jupiter.api.Assertions.assertEquals
import java.io.File
import java.net.URL
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.*

/**
 * Helper functions for submission tests.
 */
object SubmissionsTestUtils {
    @JvmStatic
    fun validFileSubmissions(): List<File> {
        return loadFilesFromURL(getResource("submissions/valid/individual"))
    }

    @JvmStatic
    fun validArchiveSubmissions(): List<File> {
        return loadFilesFromURL(getResource("submissions/valid/archived"))
    }

    @JvmStatic
    fun allValidSubmissions(): List<File> {
        return validFileSubmissions() + validArchiveSubmissions()
    }

    @JvmStatic
    fun invalidNoFilesSubmissions(): List<File> {
        return loadFilesFromURL(getResource("submissions/invalid/nofiles/archived"))
    }

    @JvmStatic
    fun invalidIllegalCharactersSubmissions(): List<File> {
        return loadFilesFromURL(getResource("submissions/invalid/illegal/individual")) +
                loadFilesFromURL(getResource("submissions/invalid/illegal/archived"))
    }

    @JvmStatic
    fun invalidFileNameLengthSubmissions(): List<File> {
        return loadFilesFromURL(getResource("submissions/invalid/length/individual")) +
                loadFilesFromURL(getResource("submissions/invalid/length/archived"))
    }

    @JvmStatic
    fun allInvalidSubmissions(): List<File> {
        return invalidNoFilesSubmissions() + invalidIllegalCharactersSubmissions() + invalidFileNameLengthSubmissions()
    }

    @JvmStatic
    fun readValidFileNames(): List<String> {
        return readLinesFromFile("file/valid_file_names.txt")
    }

    @JvmStatic
    fun readIllegalFileNames(): List<String> {
        return readLinesFromFile("file/illegal_file_names.txt")
    }

    /**
     * Helper function to assert that uploaded files are equal to downloaded files. If the uploaded file is a tar file,
     * the contents of the tar file will be compared to the contents of the downloaded files. Otherwise, the contents of
     * the uploaded file will be compared to the contents of the downloaded file.
     *
     * @param uploadedFile The file that was uploaded.
     * @param downloadedFileDir The directory containing all downloaded files.
     */
    @OptIn(ExperimentalPathApi::class)
    fun assertUploadedFileIsEqualToDownloadedFile(tarArchiver: TarArchiver, uploadedFile: File, downloadedFileDir: Path) {
        if(uploadedFile.name.endsWith(".tar")) {
            // Untar the uploaded file and compare the contents of the uploaded file and the downloaded files
            withEphemeralDirectory(prefix = "submission-uploaded-") { tempUploadedDir ->
                tarArchiver.unarchive(uploadedFile.toPath(), tempUploadedDir)

                val downloadedFiles = downloadedFileDir.walk().filter { !it.isDirectory() }.toList().sortedBy { it.fileName }
                val uploadedFiles = tempUploadedDir.walk().filter { !it.isDirectory() }.toList().sortedBy { it.fileName }

                val downloadedFileSizes = downloadedFiles.map { it.fileSize() }
                val uploadedFileSizes = uploadedFiles.map { it.fileSize() }

                val downloadedContents = downloadedFiles.map { Files.readString(it) }
                val uploadedContents = uploadedFiles.map { Files.readString(it) }

                assertEquals(uploadedFiles.size, downloadedFiles.size)
                assertEquals(uploadedFileSizes, downloadedFileSizes)
                assertEquals(uploadedContents, downloadedContents)
            }
        } else {
            // Compare the contents of the uploaded file and the downloaded file
            val uploadedPath = uploadedFile.toPath()
            val downloadedPath = downloadedFileDir.resolve(uploadedPath.fileName)

            assertEquals(uploadedPath.fileSize(), downloadedPath.fileSize())
            assertEquals(Files.readString(uploadedPath), Files.readString(downloadedPath))
        }
    }

    /**
     * Helper function to read lines from a file in the resources directory.
     *
     * @param fileName The name of the file to read.
     *
     * @return The lines of the file.
     */
    private fun readLinesFromFile(fileName: String): List<String> {
        return getResource(fileName).asPath().readLines()
    }

    /**
     * Helper function to load all files from a URL. This function will ignore .gitkeep files in empty directories.
     * Directories are not included in the returned list.
     *
     * @param url The URL to load files from.
     *
     * @return A list of [File]s.
     */
    private fun loadFilesFromURL(url: URL): List<File> {
        return url
            .asFile()
            .walkTopDown()
            .filter { it.isFile }
            .toList()
    }
}
