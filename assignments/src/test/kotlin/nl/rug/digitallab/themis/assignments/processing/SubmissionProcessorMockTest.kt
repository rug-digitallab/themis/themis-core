package nl.rug.digitallab.themis.assignments.processing

import io.quarkus.test.InjectMock
import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.assignments.exceptions.submission.SubmissionProcessingException
import nl.rug.digitallab.themis.assignments.persistence.entities.Submission
import nl.rug.digitallab.themis.assignments.persistence.repositories.SubmissionMetaRepository
import nl.rug.digitallab.themis.assignments.processing.handlers.*
import nl.rug.digitallab.themis.assignments.util.SubmissionContextGenerator
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.kotlin.any
import org.mockito.kotlin.whenever

@QuarkusTest
class SubmissionProcessorMockTest {
    @Inject
    lateinit var submissionProcessor: SubmissionProcessor

    @Inject
    lateinit var contextGenerator: SubmissionContextGenerator

    @Inject
    lateinit var submissionRepository: SubmissionMetaRepository

    @InjectMock
    lateinit var processSubmissionHandler: ProcessSubmissionHandler

    @Test
    @TestTransaction
    fun `The SubmissionProcessor should mark submissions as failed if an exception is thrown after persisting`() {
        whenever(processSubmissionHandler.handle(any())).thenThrow(RuntimeException("Mocked exception"))

        val context = contextGenerator.generateContextWithSubmission()

        context.use {
            assertThrows<SubmissionProcessingException> {
                try {
                    submissionProcessor.execute(it)
                } catch (e: SubmissionProcessingException) {
                    val submission = submissionRepository.findById(e.context.submission.id)
                    assertEquals(Submission.SubmissionStatus.FAILED, submission!!.status)
                    throw e // rethrow the exception to propagate the failure to assertThrows
                }
            }
        }
    }
}
