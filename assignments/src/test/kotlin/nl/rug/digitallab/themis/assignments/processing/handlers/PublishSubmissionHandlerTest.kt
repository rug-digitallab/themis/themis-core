package nl.rug.digitallab.themis.assignments.processing.handlers

import io.quarkus.test.junit.QuarkusTest
import io.smallrye.reactive.messaging.memory.InMemoryConnector
import jakarta.inject.Inject
import nl.rug.digitallab.themis.assignments.util.SubmissionContextGenerator
import nl.rug.digitallab.themis.common.constants.SUBMISSION_QUEUE
import nl.rug.digitallab.themis.common.events.SubmissionAddedEvent
import org.awaitility.Awaitility.await
import org.eclipse.microprofile.reactive.messaging.spi.Connector
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

@QuarkusTest
class PublishSubmissionHandlerTest {
    @Inject
    lateinit var publishSubmissionHandler: PublishSubmissionHandler

    @Inject
    lateinit var contextGenerator: SubmissionContextGenerator

    @Inject
    @Connector("smallrye-in-memory")
    lateinit var connector: InMemoryConnector

    @BeforeEach
    fun `Empty the queue`() {
        val consumer = connector.sink<SubmissionAddedEvent>(SUBMISSION_QUEUE)
        consumer.clear()
    }

    @Test
    fun `The PublishSubmissionHandler should publish the submission to the message queue`() {
        val inMemorySink = connector.sink<SubmissionAddedEvent>(SUBMISSION_QUEUE)

        val context = contextGenerator.generateContextWithSubmission()

        context.use {
            publishSubmissionHandler.handle(context)

            await().until {
                inMemorySink.received().isNotEmpty()
            }

            val receivedEvent = inMemorySink.received().first().payload
            assertEquals(it.submission.id, receivedEvent.submissionId)
            assertEquals(it.assignment.id, receivedEvent.assignmentId)
        }
    }

    @Test
    fun `The PublishSubmissionHandler should fail when the submission is not present in the context`() {
        val context = contextGenerator.generateContext()

        context.use {
            assertThrows<UninitializedPropertyAccessException> { publishSubmissionHandler.handle(context) }
        }
    }
}
