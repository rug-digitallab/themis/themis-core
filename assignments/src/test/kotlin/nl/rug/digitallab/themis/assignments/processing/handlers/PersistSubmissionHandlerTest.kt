package nl.rug.digitallab.themis.assignments.processing.handlers

import io.quarkus.test.TestTransaction
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.assignments.persistence.entities.Submission
import nl.rug.digitallab.themis.assignments.persistence.repositories.SubmissionMetaRepository
import nl.rug.digitallab.themis.assignments.util.SubmissionContextGenerator
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows

@QuarkusTest
class PersistSubmissionHandlerTest {
    @Inject
    lateinit var persistSubmissionHandler: PersistSubmissionHandler

    @Inject
    lateinit var submissionMetaRepository: SubmissionMetaRepository

    @Inject
    lateinit var contextGenerator: SubmissionContextGenerator

    @Test
    @TestTransaction
    fun `The PersistSubmissionHandler should persist the submission`() {
        val context = contextGenerator.generateContextWithAssignment()

        context.use {
            persistSubmissionHandler.handle(context)

            assertNotNull(it.submission)
            assertEquals(it.submission.status, Submission.SubmissionStatus.PROCESSING)

            assertDoesNotThrow { submissionMetaRepository.findById(it.submission.id) }
        }
    }

    @Test
    fun `The PersistSubmissionHandler should fail when the assignment is not present in the context`() {
        val context = contextGenerator.generateContext()

        context.use {
            assertThrows<UninitializedPropertyAccessException> { persistSubmissionHandler.handle(context) }
        }
    }
}
