package nl.rug.digitallab.themis.assignments.processing.handlers

import com.google.common.net.MediaType
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.withEphemeralDirectory
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.common.quarkus.archiver.TarArchiver
import nl.rug.digitallab.common.quarkus.archiver.ZipArchiver
import nl.rug.digitallab.common.quarkus.test.rest.MockFileUpload
import nl.rug.digitallab.common.quarkus.test.rest.MockFileUploadNullPath
import nl.rug.digitallab.themis.assignments.configs.SubmissionsConfig
import nl.rug.digitallab.themis.assignments.exceptions.file.*
import nl.rug.digitallab.themis.assignments.file.getMimeType
import nl.rug.digitallab.themis.assignments.util.*
import org.junit.jupiter.api.*
import java.io.File

@QuarkusTest
class ProcessSubmissionHandlerTest {
    @Inject
    lateinit var processSubmissionHandler: ProcessSubmissionHandler

    @Inject
    lateinit var contextGenerator: SubmissionContextGenerator

    @Inject
    lateinit var submissionsConfig: SubmissionsConfig

    @Inject
    lateinit var tarArchiver: TarArchiver

    @Inject
    lateinit var zipArchiver: ZipArchiver

    @Test
    fun `The ProcessSubmissionHandler should process the submission and produce a zip file`() {
        val context = contextGenerator.generateContextWithSubmission()

        context.use { ctx ->
            processSubmissionHandler.handle(context)

            // Unzip the zip file
            withEphemeralDirectory(prefix = "unzip-") { unzipDirectory ->
                zipArchiver.unarchive(ctx.zipFile.path, unzipDirectory)

                SubmissionsTestUtils.assertUploadedFileIsEqualToDownloadedFile(
                    tarArchiver,
                    context.addSubmissionRequest.fileUploads[0].uploadedFile().toFile(),
                    unzipDirectory
                )
            }
        }
    }

    @TestFactory
    fun `A valid file name should not result in an exception`(): List<DynamicTest> {
        return SubmissionsTestUtils.readValidFileNames().map {
            DynamicTest.dynamicTest("Valid file name '$it' should not result in an exception") {
                assertDoesNotThrow { processSubmissionHandler.checkValidFileName(it) }
            }
        }
    }

    @TestFactory
    fun `A file name with illegal characters should result in a FileNameInvalidException`(): List<DynamicTest> {
        return (SubmissionsTestUtils.readIllegalFileNames().toList() + "").map {
            DynamicTest.dynamicTest("Illegal file name '$it' should result in a FileNameInvalidException") {
                assertThrows<FileNameInvalidException> { processSubmissionHandler.checkValidFileName(it) }
            }
        }
    }

    @Test
    fun `A file name that is equal to the allowed file name length should not result in a FileNameInvalidException`() {
        val longFileName = "a".repeat(submissionsConfig.maxFileNameLength())
        assertDoesNotThrow { processSubmissionHandler.checkValidFileName(longFileName) }
    }

    @Test
    fun `A file name that is longer than the allowed file name length should result in a FileNameInvalidException`() {
        val longFileName = "a".repeat(submissionsConfig.maxFileNameLength() + 1)
        assertThrows<FileNameInvalidException> { processSubmissionHandler.checkValidFileName(longFileName) }
    }

    @Test
    fun `Getting the mime type of a file should return the correct mime type`() {
        val tarFile = getResource("file/example.tar").asPath()
        val mimeType = tarFile.getMimeType()
        Assertions.assertEquals(mimeType, MediaType.TAR)
    }

    @Test
    fun `Getting the mime type of a directory should result in a UnknownMimeTypeException`() {
        val directory = getResource("file/example/").asPath()
        assertThrows<UnknownMimeTypeException> { directory.getMimeType() }
    }

    @TestFactory
    fun `Processing file uploads should result in a zip containing the uploaded files`(): List<DynamicTest> {
        return SubmissionsTestUtils.allValidSubmissions().map {
            DynamicTest.dynamicTest("Processing file upload '${it.name}' should result in a zip containing the uploaded files") {
                val fileUploads = listOf(MockFileUpload(it))
                TestUtil.generateSubmissionContext(fileUploads).use { ctx ->
                    processSubmissionHandler.handle(ctx)

                    // Unzip the zip file
                    withEphemeralDirectory(prefix = "unzip-") { unzipDirectory ->
                        zipArchiver.unarchive(ctx.zipFile.path, unzipDirectory)

                        SubmissionsTestUtils.assertUploadedFileIsEqualToDownloadedFile(tarArchiver, it, unzipDirectory)
                    }
                }
            }
        }
    }

    @TestFactory
    fun `Processing file uploads which contain the same files should result in a DuplicateFileUploadedException`(): List<DynamicTest> {
        return SubmissionsTestUtils.allValidSubmissions().map {
            DynamicTest.dynamicTest("Processing file upload '${it.name}' which contain the same files should result in a DuplicateFileUploadedException") {
                val fileUploads = listOf(MockFileUpload(it), MockFileUpload(it))
                TestUtil.generateSubmissionContext(fileUploads).use { ctx ->
                    assertThrows<DuplicateFileUploadedException> { processSubmissionHandler.handle(ctx) }
                }
            }
        }
    }

    @TestFactory
    fun `Processing a file upload should result in the uploaded files being placed in the submission directory`(): List<DynamicTest> {
        // Write dynamic test
        return SubmissionsTestUtils.allValidSubmissions().map {
            DynamicTest.dynamicTest("Processing file upload '${it.name}' should result in the uploaded files being placed in the submission directory") {
                val fileUpload = MockFileUpload(it)
                withEphemeralDirectory(prefix = "submission-") { submissionDirectory ->
                    processSubmissionHandler.processFileUpload(fileUpload, submissionDirectory)

                    SubmissionsTestUtils.assertUploadedFileIsEqualToDownloadedFile(tarArchiver, it, submissionDirectory)
                }

            }
        }
    }

    @TestFactory
    fun `Processing a file upload that is invalid should always result in an Exception`(): List<DynamicTest> {
        return SubmissionsTestUtils.allInvalidSubmissions().map {
            DynamicTest.dynamicTest("Processing file upload '${it.name}' should result in an exception") {
                val fileUpload = MockFileUpload(it)
                withEphemeralDirectory(prefix = "submission") { directory ->
                    assertThrows<Exception> { processSubmissionHandler.processFileUpload(fileUpload, directory) }
                }
            }
        }
    }

    @Test
    fun `Processing a file upload with an empty name should always result in an Exception`() {
        val emptyName = File("")
        val fileUpload = MockFileUpload(emptyName)
        withEphemeralDirectory(prefix = "submission") { directory ->
            assertThrows<NoFilesUploadedException> { processSubmissionHandler.processFileUpload(fileUpload, directory) }
        }
    }

    @TestFactory
    fun `Processing a file upload with an empty archive should result in a NoFilesUploadedException`(): List<DynamicTest> {
        return SubmissionsTestUtils.invalidNoFilesSubmissions().map {
            DynamicTest.dynamicTest("Processing file upload '${it.name}' should result in a NoFilesUploadedException") {
                val fileUpload = MockFileUpload(it)
                withEphemeralDirectory(prefix = "submission") { directory ->
                    assertThrows<NoFilesUploadedException> {
                        processSubmissionHandler.processFileUpload(fileUpload, directory)
                    }
                }
            }
        }
    }

    @TestFactory
    fun `Processing a file upload with illegal file or directory names should result in an FileNameInvalidException`(): List<DynamicTest> {
        return SubmissionsTestUtils.invalidIllegalCharactersSubmissions().map {
            DynamicTest.dynamicTest("Processing file upload '${it.name}' should result in a FileNameInvalidException") {
                val fileUpload = MockFileUpload(it)
                withEphemeralDirectory(prefix = "submission") { directory ->
                    assertThrows<FileNameInvalidException> {
                        processSubmissionHandler.processFileUpload(fileUpload, directory)
                    }
                }
            }
        }
    }

    @TestFactory
    fun `Processing a file upload with file or directory names that are too long should result in an FileNameInvalidException`(): List<DynamicTest> {
        return SubmissionsTestUtils.invalidFileNameLengthSubmissions().map {
            DynamicTest.dynamicTest("Processing file upload '${it.name}' with file or directory names that are too long should result in an FileNameInvalidException") {
                val fileUpload = MockFileUpload(it)
                withEphemeralDirectory(prefix = "submission") { directory ->
                    assertThrows<FileNameInvalidException> {
                        processSubmissionHandler.processFileUpload(fileUpload, directory)
                    }
                }
            }
        }

    }

    @TestFactory
    fun `Preparing a file upload should result in that file upload being placed in the preparation directory`(): List<DynamicTest> {
        return SubmissionsTestUtils.allValidSubmissions().map {
            DynamicTest.dynamicTest("Preparing file upload '${it.name}' should result in that file upload being placed in the preparation directory") {
                val fileUpload = MockFileUpload(it)
                withEphemeralDirectory(prefix = "submission") { preparationDirectory ->
                    processSubmissionHandler.prepareFileUpload(fileUpload, preparationDirectory)
                    SubmissionsTestUtils.assertUploadedFileIsEqualToDownloadedFile(tarArchiver, it, preparationDirectory)
                }
            }
        }
    }

    @Test
    fun `Preparing a file upload that has no path should result in a FileUploadPathIsNullException`() {
        val fileUpload = MockFileUploadNullPath(File("somePath/someFile"))
        withEphemeralDirectory(prefix = "submission") { directory ->
            assertThrows<FileUploadPathIsNullException> {
                processSubmissionHandler.prepareFileUpload(fileUpload, directory)
            }
        }
    }
}
