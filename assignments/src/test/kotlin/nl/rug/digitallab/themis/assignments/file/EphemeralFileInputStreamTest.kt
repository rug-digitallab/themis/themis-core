package nl.rug.digitallab.themis.assignments.file

import nl.rug.digitallab.common.kotlin.helpers.ephemeral.EphemeralFile
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import kotlin.io.path.exists
import kotlin.io.path.notExists

class EphemeralFileInputStreamTest {
    @Test
    fun `EphemeralFileInputStream should close the EphemeralFile when the stream is closed`() {
        val ephemeralFile = EphemeralFile()
        val inputStream = EphemeralFileInputStream(ephemeralFile)

        assertTrue(ephemeralFile.path.exists()) // Before closing the stream, the file should exist
        inputStream.close()
        assertTrue(ephemeralFile.path.notExists()) // After closing the stream, the file should not exist
    }
}
