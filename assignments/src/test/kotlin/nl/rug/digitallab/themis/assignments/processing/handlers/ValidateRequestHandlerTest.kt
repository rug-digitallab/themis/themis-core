package nl.rug.digitallab.themis.assignments.processing.handlers

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.assignments.exceptions.assignment.AssignmentNotFoundException
import nl.rug.digitallab.themis.assignments.exceptions.assignment.ProfileNotFoundException
import nl.rug.digitallab.themis.assignments.exceptions.file.NoFilesUploadedException
import nl.rug.digitallab.themis.assignments.util.SubmissionContextGenerator
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows

@QuarkusTest
class ValidateRequestHandlerTest {
    @Inject
    lateinit var validateRequestHandler: ValidateRequestHandler

    @Inject
    lateinit var contextGenerator: SubmissionContextGenerator

    @Test
    fun `The ValidateRequestHandler should succeed on valid requests`() {
        val context = contextGenerator.generateContextWithAssignment()

        context.use {
            assertDoesNotThrow { validateRequestHandler.handle(it) }
        }
    }

    @Test
    fun `The ValidateRequestHandler should fail when the assignment is not found`() {
        val context = contextGenerator.generateContext()

        context.use {
            assertThrows<AssignmentNotFoundException> { validateRequestHandler.handle(it) }
        }
    }

    @Test
    fun `The ValidateRequestHandler should fail when the profile is not found`() {
        val context = contextGenerator.generateContextWithInvalidAssignmentProfile()

        context.use {
            assertThrows<ProfileNotFoundException> { validateRequestHandler.handle(it) }
        }
    }

    @Test
    fun `The ValidateRequestHandler should fail when no files are uploaded`() {
        val context = contextGenerator.generateContextWithNoFiles()

        context.use {
            assertThrows<NoFilesUploadedException> { validateRequestHandler.handle(it) }
        }
    }
}
