package nl.rug.digitallab.themis.assignments.managers

import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.themis.assignments.dtos.AddSubmissionRequest
import nl.rug.digitallab.themis.assignments.dtos.ListSubmissionsRequest
import nl.rug.digitallab.themis.assignments.dtos.SubmissionResponse
import nl.rug.digitallab.themis.assignments.exceptions.file.NoFilesUploadedException
import nl.rug.digitallab.themis.assignments.exceptions.submission.SubmissionNotFoundException
import nl.rug.digitallab.themis.assignments.persistence.entities.Submission
import nl.rug.digitallab.themis.assignments.persistence.namespaces.SubmissionNamespace
import nl.rug.digitallab.themis.assignments.persistence.repositories.SubmissionMetaRepository
import nl.rug.digitallab.themis.assignments.persistence.repositories.SubmissionObjectRepository
import nl.rug.digitallab.themis.assignments.processing.SubmissionContext
import nl.rug.digitallab.themis.assignments.processing.SubmissionProcessor
import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import nl.rug.digitallab.themis.common.typealiases.SubmissionId
import java.io.InputStream

/**
 * The [SubmissionsManager] is responsible for handling all business logic related to submissions.
 */
@ApplicationScoped
class SubmissionsManager {
    @Inject
    private lateinit var submissionObjectRepository: SubmissionObjectRepository

    @Inject
    private lateinit var submissionMetaRepository: SubmissionMetaRepository

    @Inject
    private lateinit var submissionProcessor: SubmissionProcessor

    /**
     * Add a new submission by triggering the submission processing pipeline, which produces a [Submission] entity.
     *
     * @param addSubmissionRequest The request DTO.
     *
     * @return The [SubmissionId] of the uploaded submission.
     *
     * @throws NoFilesUploadedException If no files were uploaded.
     */
    fun addSubmission(assignmentId: AssignmentId, addSubmissionRequest: AddSubmissionRequest): SubmissionResponse {
        val context = SubmissionContext(
            assignmentId = assignmentId,
            addSubmissionRequest = addSubmissionRequest
        )

        context.use {
            submissionProcessor.execute(context)
        }

        val submission = context.submission
        submissionMetaRepository.updateStatus(submission, Submission.SubmissionStatus.QUEUED)
        return submission.toDTO()
    }

    /**
     * Get the metadata for specific submission ID.
     *
     * @param submissionId The id of the submission.
     *
     * @return The [Submission] entity and its metadata.
     *
     * @throws SubmissionNotFoundException If the submission does not exist.
     */
    fun getSubmission(submissionId: SubmissionId): SubmissionResponse {
        return submissionMetaRepository
            .findByIdOrThrow(submissionId)
            .toDTO()
    }

    /**
     * Get all submissions and their metadata, optionally filtered by course, group, and/or assignment.
     *
     * @param listSubmissionsRequest The request DTO.
     *
     * @return A list of [Submission] entities and their metadata.
     */
    fun listSubmissions(listSubmissionsRequest: ListSubmissionsRequest): List<SubmissionResponse> {
        val courseInstanceId = listSubmissionsRequest.courseInstanceId
        val groupId = listSubmissionsRequest.groupId
        val assignmentId = listSubmissionsRequest.assignmentId

        return submissionMetaRepository
            .find(courseInstanceId, assignmentId, groupId)
            .map { it.toDTO() }
    }

    /**
     * Download a submission from the MinIO object store. The submission is streamed to the browser through an
     * [InputStream].
     *
     * @param submissionId The id of the submission.
     *
     * @return The submission as a zip file, streamed to the browser through an [InputStream].
     *
     * @throws SubmissionNotFoundException If the submission does not exist.
     */
    fun downloadSubmission(submissionId: SubmissionId): InputStream {
        val submissionNamespace = submissionMetaRepository
            .findByIdOrThrow(submissionId)
            .let { submission ->
                SubmissionNamespace(
                    submission.assignment.courseInstanceId,
                    submission.groupId,
                    submission.assignment.id,
                    submission.id,
                )
            }

        return submissionObjectRepository
            .downloadSubmission(submissionNamespace)
    }

    /**
     * Downloads a batch of submissions from the MinIO object store. The submissions are streamed to the browser through an
     * [InputStream]. The input is trusted and no checks for existence are performed, as this method
     * is intended to be used internally to reduce duplicate checks.
     *
     * @param submissionIds The ids of the submissions to download.
     *
     * @return The submissions as a zip file, streamed to the browser through an [InputStream].
     */
    fun downloadBatchSubmission(submissionIds: List<SubmissionId>): InputStream {
        val submissionNamespaces = submissionMetaRepository
            .find(submissionIds)
            .map { submission ->
                SubmissionNamespace(
                    submission.assignment.courseInstanceId,
                    submission.groupId,
                    submission.assignment.id,
                    submission.id,
                )
            }

        return submissionObjectRepository.downloadBatchSubmissions(submissionNamespaces)
    }
}
