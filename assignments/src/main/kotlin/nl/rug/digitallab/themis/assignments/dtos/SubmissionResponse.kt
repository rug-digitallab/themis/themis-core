package nl.rug.digitallab.themis.assignments.dtos

import nl.rug.digitallab.themis.assignments.persistence.entities.Submission.SubmissionStatus
import nl.rug.digitallab.themis.common.typealiases.*
import java.time.Instant

data class SubmissionResponse(
    val id: SubmissionId,
    val created: Instant,
    val updated: Instant,
    val courseInstanceId: CourseInstanceId,
    val assignmentId: AssignmentId,
    val groupId: GroupId,
    val configId: ConfigurationId,
    val status: SubmissionStatus,
)
