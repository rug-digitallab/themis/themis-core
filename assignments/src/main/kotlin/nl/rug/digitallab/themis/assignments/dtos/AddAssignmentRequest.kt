package nl.rug.digitallab.themis.assignments.dtos

import nl.rug.digitallab.themis.assignments.persistence.entities.VisibilitySpecification
import nl.rug.digitallab.themis.common.typealiases.CourseInstanceId
import java.time.Instant

/**
 * The [AddAssignmentRequest] is the request DTO for the AssignmentsResource.addAssignment endpoint.
 *
 * @property name The name of the assignment.
 * @property description A short description of the assignment - should not contain the entire assignment text.
 * @property softDeadline The last submission date for students as shown in the UI - any submissions made after this
 * moment are marked 'late', but submission is still possible. When unset, the soft deadline is equal to the hard deadline.
 * @property hardDeadline The last submission date for students as enforced by the system - any submissions made after this
 * moment are not accepted. When unspecified, the value from folder level will be inherited.
 * @property profileConfigs The profile configurations of the assignment.
 * @property visibilitySpecification The visibility specification of the assignment.
 */
data class AddAssignmentRequest(
    val courseInstanceId: CourseInstanceId,
    val name: String,
    val description: String,
    val softDeadline: Instant?,
    val hardDeadline: Instant?,
    val profileConfigs: List<AddProfileRequest>,
    val visibilitySpecification: VisibilitySpecification,
)
