package nl.rug.digitallab.themis.assignments.exceptions.minio

import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import org.jboss.resteasy.reactive.RestResponse.Status

class TagNotFoundException(missingTags: Set<String>):
    MappedException("The following tags were not found: $missingTags", Status.INTERNAL_SERVER_ERROR)
