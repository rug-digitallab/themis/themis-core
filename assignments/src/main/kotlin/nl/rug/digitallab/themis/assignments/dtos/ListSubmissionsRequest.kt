package nl.rug.digitallab.themis.assignments.dtos

import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import nl.rug.digitallab.themis.common.typealiases.CourseInstanceId
import nl.rug.digitallab.themis.common.typealiases.GroupId
import org.jboss.resteasy.reactive.RestQuery

/**
 * The [ListSubmissionsRequest] is the DTO for the SubmissionsResource.listSubmissions endpoint.
 *
 * @property assignmentId The ID of the assignment the submission is for.
 * @property courseInstanceId The ID of the course instance the submission is for.
 * @property groupId The ID of the group that submitted the submission.
 */
data class ListSubmissionsRequest (
    @field:RestQuery("courseInstanceId")
    var courseInstanceId: CourseInstanceId? = null,

    @field:RestQuery("assignmentId")
    var assignmentId: AssignmentId? = null,

    @field:RestQuery("groupId")
    var groupId: GroupId? = null,
)
