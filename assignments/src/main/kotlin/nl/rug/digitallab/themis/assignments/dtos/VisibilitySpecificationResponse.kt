package nl.rug.digitallab.themis.assignments.dtos

import java.time.Instant

data class VisibilitySpecificationResponse(
    val visibilityType: String,
    val start: Instant?,
    val end: Instant?,
)
