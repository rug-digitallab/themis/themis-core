package nl.rug.digitallab.themis.assignments.exceptions.file

import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import org.jboss.resteasy.reactive.RestResponse

class FileNameInvalidException(fileName: String, message: String):
    MappedException("File name '${fileName}' is invalid: $message", RestResponse.Status.BAD_REQUEST)
