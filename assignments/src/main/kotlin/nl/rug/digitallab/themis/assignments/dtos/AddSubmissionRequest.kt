package nl.rug.digitallab.themis.assignments.dtos

import nl.rug.digitallab.common.kotlin.helpers.noarg.NoArgConstructor
import nl.rug.digitallab.themis.common.typealiases.GroupId
import org.jboss.resteasy.reactive.RestForm
import org.jboss.resteasy.reactive.RestQuery
import org.jboss.resteasy.reactive.multipart.FileUpload

/**
 * The [AddSubmissionRequest] is the DTO for the SubmissionsResource.addSubmission endpoint.
 *
 * @property groupId The ID of the group that submitted the submission.
 * @property profileName The profile that should be used to execute the submission.
 * @property fileUploads The files that are part of the submission.
 */
@NoArgConstructor
data class AddSubmissionRequest(
    @field:RestQuery
    var groupId: GroupId,

    @field:RestQuery
    var profileName: String,

    @field:RestForm(FileUpload.ALL)
    var fileUploads: List<FileUpload>,
)
