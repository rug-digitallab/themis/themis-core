package nl.rug.digitallab.themis.assignments.exceptions.config

import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import nl.rug.digitallab.themis.common.typealiases.ConfigurationId
import org.jboss.resteasy.reactive.RestResponse

class ConfigurationNotFoundException(configId: ConfigurationId):
    MappedException("Assignment configuration '$configId' not found", RestResponse.Status.NOT_FOUND)
