package nl.rug.digitallab.themis.assignments.dtos

import nl.rug.digitallab.themis.assignments.dtos.config.ParsedConfiguration

/**
 * The [AddProfileRequest] is the request DTO for the profile configurations of an assignment.
 *
 * @property profileName The name of the profile.
 * @property config The configuration of the profile.
 */
data class AddProfileRequest(
    val profileName: String,
    val config: ParsedConfiguration,
)
