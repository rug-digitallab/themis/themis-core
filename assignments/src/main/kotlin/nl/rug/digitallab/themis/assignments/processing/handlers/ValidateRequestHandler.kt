package nl.rug.digitallab.themis.assignments.processing.handlers

import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.annotation.Priority
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.themis.assignments.exceptions.assignment.ProfileNotFoundException
import nl.rug.digitallab.themis.assignments.exceptions.file.NoFilesUploadedException
import nl.rug.digitallab.themis.assignments.persistence.repositories.AssignmentRepository
import nl.rug.digitallab.themis.assignments.processing.SubmissionContext
import nl.rug.digitallab.themis.assignments.processing.VALIDATE_REQUEST_HANDLER_PRIORITY

/**
 * The [ValidateRequestHandler] handler is responsible for validating the [AddSubmissionRequest] DTO.
 */
@ApplicationScoped
@Priority(VALIDATE_REQUEST_HANDLER_PRIORITY)
class ValidateRequestHandler : SubmissionHandler {
    @Inject
    private lateinit var assignmentRepository: AssignmentRepository

    @WithSpan("VALIDATING Submission Request")
    override fun handle(context: SubmissionContext) {
        // Check if the assignment exists.
        val assignment = assignmentRepository.findByIdOrThrow(context.assignmentId)

        // Check if the assignment has the requested profile.
        val profileName = context.addSubmissionRequest.profileName
        if (assignment.profiles.none { it.name == profileName })
            throw ProfileNotFoundException(profileName)

        // Check if any files were uploaded.
        if (context.addSubmissionRequest.fileUploads.isEmpty())
            throw NoFilesUploadedException()

        context.assignment = assignment
    }
}
