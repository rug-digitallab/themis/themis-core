package nl.rug.digitallab.themis.assignments.exceptions.file

import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import org.jboss.resteasy.reactive.RestResponse.Status

class NoFilesUploadedException:
    MappedException("No files were uploaded", Status.BAD_REQUEST)
