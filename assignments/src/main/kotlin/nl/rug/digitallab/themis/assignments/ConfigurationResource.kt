package nl.rug.digitallab.themis.assignments

import jakarta.inject.Inject
import jakarta.transaction.Transactional
import jakarta.ws.rs.GET
import jakarta.ws.rs.Path
import jakarta.ws.rs.Produces
import jakarta.ws.rs.core.MediaType
import nl.rug.digitallab.themis.assignments.dtos.config.ConfigurationResponse
import nl.rug.digitallab.themis.assignments.managers.ConfigurationManager
import nl.rug.digitallab.themis.common.typealiases.ConfigurationId
import org.jboss.resteasy.reactive.ResponseStatus
import org.jboss.resteasy.reactive.RestPath
import org.jboss.resteasy.reactive.RestResponse.StatusCode

/**
 * The [ConfigurationResource] is responsible for handling all REST requests related to assignment configurations.
 */
@Path("/api/v1/themis/configurations")
class ConfigurationResource {
    @Inject
    private lateinit var configurationManager: ConfigurationManager

    /**
     * Get a specific assignment configuration by its synthetic ID.
     *
     * @param configId The id of the assignment configuration.
     *
     * @return The full configuration together with its metadata.
     */
    @GET
    @Path("{configId}")
    @ResponseStatus(StatusCode.OK)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    fun getConfig(@RestPath configId: ConfigurationId): ConfigurationResponse {
        return configurationManager.getConfig(configId)
    }
}
