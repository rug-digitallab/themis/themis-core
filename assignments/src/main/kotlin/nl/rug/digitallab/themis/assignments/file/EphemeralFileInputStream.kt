package nl.rug.digitallab.themis.assignments.file

import nl.rug.digitallab.common.kotlin.helpers.ephemeral.EphemeralFile
import java.io.FileInputStream

/**
 * A [FileInputStream] that wraps an [EphemeralFile] and closes it when the input stream is closed.
 *
 * @property ephemeralFile The [EphemeralFile] to close when the stream is closed.
 */
class EphemeralFileInputStream(private val ephemeralFile: EphemeralFile): FileInputStream(ephemeralFile.path.toFile()) {
    override fun close() {
        super.close()
        ephemeralFile.close()
    }
}
