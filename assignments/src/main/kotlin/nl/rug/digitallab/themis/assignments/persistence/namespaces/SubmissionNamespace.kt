package nl.rug.digitallab.themis.assignments.persistence.namespaces

import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import nl.rug.digitallab.themis.common.typealiases.CourseInstanceId
import nl.rug.digitallab.themis.common.typealiases.GroupId
import nl.rug.digitallab.themis.common.typealiases.SubmissionId

/**
 * Represents the namespace of a submission: /{courseInstanceId}/{groupId}/{assignmentId}/{submissionId}/submission.zip
 *
 * @param courseInstanceId The id of the course instance the submission is for.
 * @param groupId The id of the group that the submission is for.
 * @param assignmentId The id of the assignment the submission is for.
 * @param submissionId The id of the submission.
 */
class SubmissionNamespace(
    courseInstanceId: CourseInstanceId,
    groupId: GroupId,
    assignmentId: AssignmentId,
    val submissionId: SubmissionId,
): AssignmentNamespace(courseInstanceId, groupId, assignmentId) {
    /**
     * The name of the submission directory.
     *
     * @return The name of the submission directory.
     */
    val directoryName = "$prefixName/$submissionId"

    /**
     * The name of the submission object as stored in the MinIO object store.
     * This will be the submission directory zipped.
     *
     * @return The name of the submission object.
     */
    val objectName = "$directoryName/submission.zip"

    override val fullName = "$bucketName/$objectName"
}
