package nl.rug.digitallab.themis.assignments.processing.handlers

import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.annotation.Priority
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.themis.assignments.managers.toEntity
import nl.rug.digitallab.themis.assignments.persistence.entities.Submission
import nl.rug.digitallab.themis.assignments.persistence.repositories.SubmissionMetaRepository
import nl.rug.digitallab.themis.assignments.processing.PERSIST_SUBMISSION_HANDLER_PRIORITY
import nl.rug.digitallab.themis.assignments.processing.SubmissionContext

/**
 * The [PersistSubmissionHandler] handler is responsible for creating a new submission entity in the database
 * and updating its status.
 */
@ApplicationScoped
@Priority(PERSIST_SUBMISSION_HANDLER_PRIORITY)
class PersistSubmissionHandler : SubmissionHandler {
    @Inject
    private lateinit var submissionMetaRepository: SubmissionMetaRepository

    @WithSpan("PERSISTING Submission")
    override fun handle(context: SubmissionContext) {
        val submission = context.addSubmissionRequest
            .toEntity(context.assignment)
            .also { it.status = Submission.SubmissionStatus.PROCESSING }

        submissionMetaRepository.create(submission)

        // Update the context with the created submission.
        context.submission = submission
    }
}
