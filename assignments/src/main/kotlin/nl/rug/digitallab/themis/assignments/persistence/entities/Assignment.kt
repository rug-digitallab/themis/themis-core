package nl.rug.digitallab.themis.assignments.persistence.entities

import jakarta.persistence.*
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import nl.rug.digitallab.themis.common.typealiases.CourseInstanceId
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.Instant

/**
 * Represents an assignment in a course instance.
 *
 * @property id The ID of the assignment.
 * @property created The time the assignment was created.
 * @property updated The time the assignment was last updated.
 * @property name The name of the assignment.
 * @property description The description of the assignment.
 * @property courseInstanceId The ID of the course instance the assignment is for.
 * @property softDeadline The soft deadline of the assignment. If null, there is no soft deadline.
 * @property hardDeadline The hard deadline of the assignment. If null, there is no hard deadline.
 * @property visibilitySpecification The visibility specification of the assignment.
 * @property profiles The profiles of the assignment.
 */
@Entity
class Assignment(
    @field:NotBlank(message = "Assignment name cannot be null or blank")
    var name: String,

    @field:NotNull(message = "Assignment description cannot be null")
    var description: String,

    @field:Column(updatable = false) // Assignments cannot be moved to another course instance
    @field:NotNull(message = "Assignment course instance cannot be null")
    var courseInstanceId: CourseInstanceId,

    var softDeadline: Instant? = null,

    var hardDeadline: Instant? = null,

    @field:Embedded
    @field:NotNull(message = "Assignment visibility cannot be null")
    var visibilitySpecification: VisibilitySpecification,

    // Eagerly fetched to ensure that the currentConfiguration is always available
    @field:OneToMany(mappedBy = "assignment", cascade = [CascadeType.ALL], orphanRemoval = true, fetch = FetchType.EAGER)
    var profiles: MutableSet<Profile> = mutableSetOf(),

    @field:OneToMany(mappedBy = "assignment", cascade = [CascadeType.ALL], orphanRemoval = true, fetch = FetchType.LAZY)
    var submissions: MutableSet<Submission> = mutableSetOf(),
) {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    lateinit var id: AssignmentId

    @CreationTimestamp
    lateinit var created: Instant

    @UpdateTimestamp
    lateinit var updated: Instant

    @PrePersist
    @PreUpdate
    private fun validateDeadline() {
        check(!(softDeadline != null && hardDeadline != null && softDeadline!!.isAfter(hardDeadline))) {
            "Soft deadline cannot be after hard deadline"
        }
    }
}
