package nl.rug.digitallab.themis.assignments.exceptions.submission

import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import nl.rug.digitallab.themis.assignments.processing.SubmissionContext
import org.jboss.resteasy.reactive.RestResponse.Status

/**
 * An exception that occurs during the submission processing pipeline.
 *
 * @param message The error message.
 * @param context The submission context at the time the exception occurred.
 * @param httpStatusCode The HTTP status code to map the exception to.
 * @param cause The cause of the exception. Defaults to null.
 */
class SubmissionProcessingException(
    message: String,
    val context: SubmissionContext,
    httpStatusCode: Status = Status.INTERNAL_SERVER_ERROR,
    cause: Throwable? = null,
) : MappedException(message, httpStatusCode, cause)
