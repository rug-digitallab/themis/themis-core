package nl.rug.digitallab.themis.assignments

import jakarta.inject.Inject
import jakarta.transaction.Transactional
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import nl.rug.digitallab.themis.assignments.dtos.AddAssignmentRequest
import nl.rug.digitallab.themis.assignments.dtos.AddSubmissionRequest
import nl.rug.digitallab.themis.assignments.dtos.AssignmentResponse
import nl.rug.digitallab.themis.assignments.dtos.config.ConfigurationResponse
import nl.rug.digitallab.themis.assignments.dtos.config.ParsedConfiguration
import nl.rug.digitallab.themis.assignments.managers.AssignmentsManager
import nl.rug.digitallab.themis.assignments.managers.SubmissionsManager
import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import nl.rug.digitallab.themis.common.typealiases.CourseInstanceId
import nl.rug.digitallab.themis.common.typealiases.SubmissionId
import org.jboss.resteasy.reactive.ResponseStatus
import org.jboss.resteasy.reactive.RestPath
import org.jboss.resteasy.reactive.RestQuery
import org.jboss.resteasy.reactive.RestResponse.StatusCode
import java.time.Instant

/**
 * The [AssignmentsResource] is responsible for handling all REST requests related to assignments.
 */
@Path("/api/v1/themis/assignments")
class AssignmentsResource {
    @Inject
    private lateinit var assignmentsManager: AssignmentsManager

    @Inject
    private lateinit var submissionsManager: SubmissionsManager

    /**
     * Add an assignment for a course instance.
     *
     * @param addAssignmentRequest The request DTO.
     *
     * @return The [AssignmentId] of the added assignment.
     */
    @POST
    @Path("")
    @ResponseStatus(StatusCode.CREATED)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    fun addAssignment(addAssignmentRequest: AddAssignmentRequest): AssignmentId {
        return assignmentsManager.addAssignment(addAssignmentRequest).id
    }

    /**
     * List all assignments for a specific course instance. Optionally filter by visibility at a specific time.
     *
     * @param courseInstanceId The id of the course instance to list assignments for.
     * @param visibleAtTime The time at which the retrieved assignments should be active. When null, no time filtering is applied.
     *
     * @return A list of [AssignmentResponse]s.
     */
    @GET
    @Path("")
    @ResponseStatus(StatusCode.OK)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    fun listAssignments(
        @RestQuery courseInstanceId: CourseInstanceId,
        @RestQuery visibleAtTime: Instant?,
    ): List<AssignmentResponse> {
        return assignmentsManager.listAssignments(courseInstanceId, visibleAtTime)
    }

    /**
     * Get a specific assignment by its ID.
     *
     * @param assignmentId The id of the assignment.
     *
     * @return The [AssignmentResponse] of the assignment.
     */
    @GET
    @Path("{assignmentId}")
    @ResponseStatus(StatusCode.OK)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    fun getAssignment(@RestPath assignmentId: AssignmentId): AssignmentResponse {
        return assignmentsManager.getAssignment(assignmentId)
    }

    /**
     * Get the configuration for a specific assignment and profile.
     *
     * @param assignmentId The id of the assignment.
     * @param profileName The name of the profile to get the configuration for.
     *
     * @return The [ConfigurationResponse] of the assignment.
     */
    @GET
    @Path("{assignmentId}/config")
    @ResponseStatus(StatusCode.OK)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    fun getConfigurations(
        @RestPath assignmentId: AssignmentId,
        @RestQuery profileName: String,
    ): List<ConfigurationResponse> {
        return assignmentsManager.getConfigurations(assignmentId, profileName)
    }

    @GET
    @Path("{assignmentId}/profile/{profileName}/config/{revision}")
    @ResponseStatus(StatusCode.OK)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    fun getConfig(
        @RestPath assignmentId: AssignmentId,
        @RestPath profileName: String,
        @RestPath revision: Int,
    ): ConfigurationResponse {
        return assignmentsManager.getConfig(assignmentId, profileName, revision)
    }

    /**
     * Update a specific assignment.
     *
     * @param assignmentId The id of the assignment to update.
     * @param addAssignmentRequest The request DTO, containing the updated assignment data.
     *
     * @return The [AssignmentResponse] of the updated assignment.
     */
    @PUT
    @Path("{assignmentId}")
    @ResponseStatus(StatusCode.OK)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    fun updateAssignment(
        @RestPath assignmentId: AssignmentId,
        addAssignmentRequest: AddAssignmentRequest,
    ): AssignmentResponse {
        return assignmentsManager.updateAssignment(assignmentId, addAssignmentRequest)
    }

    /**
     * Update the configuration for a specific assignment and profile.
     *
     * @param assignmentId The id of the assignment to update.
     * @param profileName The name of the profile to update the configuration for.
     * @param config The new configuration.
     *
     * @return The [ConfigurationResponse], containing the updated configuration.
     */
    @PUT
    @Path("{assignmentId}/config")
    @ResponseStatus(StatusCode.CREATED)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    fun updateConfiguration(
        @RestPath assignmentId: AssignmentId,
        @RestQuery profileName: String,
        config: ParsedConfiguration,
    ): ConfigurationResponse {
        return assignmentsManager.updateConfigurations(assignmentId, profileName, config)
    }

    /**
     * Delete a specific assignment.
     *
     * @param assignmentId The id of the assignment to delete.
     *
     * @return a [Unit] response.
     */
    @DELETE
    @Path("{assignmentId}")
    @ResponseStatus(StatusCode.NO_CONTENT)
    @Transactional
    fun deleteAssignment(@RestPath assignmentId: AssignmentId) {
        assignmentsManager.deleteAssignment(assignmentId)
    }


    /**
     * Add a new submission. Each uploaded file is checked, processed, and zipped before
     * uploading to the object store. The submission metadata is stored in the database.
     *
     * @param addSubmissionRequest The request DTO.
     *
     * @return The [SubmissionId] of the uploaded submission.
     */
    @POST
    @Path("{assignmentId}/submit")
    @ResponseStatus(StatusCode.ACCEPTED)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    fun addSubmission(@RestPath assignmentId: AssignmentId, @BeanParam addSubmissionRequest: AddSubmissionRequest): SubmissionId {
        return submissionsManager.addSubmission(assignmentId, addSubmissionRequest).id
    }
}
