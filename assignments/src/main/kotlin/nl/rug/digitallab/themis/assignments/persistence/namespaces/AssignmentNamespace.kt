package nl.rug.digitallab.themis.assignments.persistence.namespaces

import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import nl.rug.digitallab.themis.common.typealiases.CourseInstanceId
import nl.rug.digitallab.themis.common.typealiases.GroupId

/**
 * Represents the namespace of an assignment: /courseInstanceId/groupId/assignmentId
 *
 * @param courseInstanceId The id of the course instance the assignment is for.
 * @param groupId The id of the group that the assignment is for.
 * @param assignmentId The id of the assignment.
 */
open class AssignmentNamespace(
    val courseInstanceId: CourseInstanceId,
    val groupId: GroupId,
    val assignmentId: AssignmentId,
) {
    val bucketName = "$courseInstanceId"

    val prefixName = "$groupId/$assignmentId"

    open val fullName = "$bucketName/$prefixName"
}
