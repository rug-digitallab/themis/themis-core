package nl.rug.digitallab.themis.assignments.exceptions.assignment

import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import org.jboss.resteasy.reactive.RestResponse

class AssignmentNotFoundException(assignmentId: AssignmentId):
    MappedException("Assignment '$assignmentId' not found", RestResponse.Status.NOT_FOUND)
