package nl.rug.digitallab.themis.assignments.persistence.repositories

import io.minio.*
import io.minio.errors.ErrorResponseException
import io.minio.messages.*
import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.EphemeralFile
import nl.rug.digitallab.themis.assignments.exceptions.minio.NoResponseException
import nl.rug.digitallab.themis.assignments.exceptions.minio.TagNotFoundException
import nl.rug.digitallab.themis.assignments.exceptions.submission.SubmissionAlreadyExistsException
import nl.rug.digitallab.themis.assignments.exceptions.submission.SubmissionNotFoundException
import nl.rug.digitallab.themis.assignments.file.EphemeralFileInputStream
import nl.rug.digitallab.themis.assignments.persistence.namespaces.SubmissionNamespace
import nl.rug.digitallab.themis.common.typealiases.ProfileId
import org.jboss.logging.Logger
import java.io.InputStream
import java.nio.file.Path
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import java.util.zip.ZipOutputStream
import kotlin.io.path.absolute
import kotlin.io.path.outputStream

/**
 * Manages all interactions with MinIO in regard to submissions. Each submission is stored in a bucket using the following
 * naming scheme: /courseInstanceId/groupId/assignmentId/submissionId.zip. Where courseInstanceId is the bucket name,
 * and the rest is the object name.
 */
@ApplicationScoped
class SubmissionObjectRepository {
    @Inject
    private lateinit var log: Logger

    @Inject
    private lateinit var minioClient: MinioClient

    private val expectedTags = setOf("courseInstanceId", "assignmentId", "groupId", "submissionId", "configId")

    /**
     * Uploads a submission to MinIO.
     *
     * @param zipPath The path to the zip file to upload.
     * @param submissionNamespace The namespace of the submission.
     * @param configId The config that should be used to execute the submission.
     *
     * @throws SubmissionAlreadyExistsException When the submission already exists.
     *
     * @return The response from MinIO.
     */
    @WithSpan("UPLOAD Submission")
    fun uploadSubmission(
        zipPath: Path,
        submissionNamespace: SubmissionNamespace,
        configId: ProfileId,
    ): ObjectWriteResponse {
        val tags = mapOf(
            "submissionId" to submissionNamespace.submissionId,
            "courseInstanceId" to submissionNamespace.courseInstanceId,
            "assignmentId" to submissionNamespace.assignmentId,
            "groupId" to submissionNamespace.groupId,
            "configId" to configId
        ).mapValues { it.value.toString() }

        checkTags(tags)

        createBucket(submissionNamespace.bucketName)

        // Assuming we want a unique submission (no overwriting or versioning).
        // Unfortunately, versioning is mandatory when using object retention (WORM).
        // Therefore, we must check if the submission already exists before uploading.
        if (objectExists(submissionNamespace.bucketName, submissionNamespace.objectName)) {
            log.error("Submission already exists: ${submissionNamespace.fullName}}")
            throw SubmissionAlreadyExistsException(submissionNamespace.fullName)
        }

        val uploadArgs = UploadObjectArgs.Builder()
            .bucket(submissionNamespace.bucketName)
            .`object`(submissionNamespace.objectName)
            .filename(zipPath.absolute().toString())
            .tags(tags)
            .build()

        // Upload the submission to MinIO
        return minioClient.uploadObject(uploadArgs) ?: throw NoResponseException(uploadArgs)
    }

    /**
     * Downloads the submission from MinIO
     *
     * @param submissionNamespace The namespace of the submission.
     *
     * @return The [InputStream] representing the submission to be downloaded.
     */
    @WithSpan("DOWNLOAD Submission")
    fun downloadSubmission(submissionNamespace: SubmissionNamespace): InputStream {
        val getObjectArgs = GetObjectArgs.Builder()
            .bucket(submissionNamespace.bucketName)
            .`object`(submissionNamespace.objectName)
            .build()

        try {
            return minioClient.getObject(getObjectArgs)
        } catch (e: ErrorResponseException) {
            log.error("Submission does not exist: ${submissionNamespace.fullName}", e)
            throw SubmissionNotFoundException(e, submissionNamespace)
        }
    }

    /**
     * Downloads multiple submissions from MinIO
     *
     * @param submissionNamespaces The namespaces of the submissions.
     *
     * @return A map of [InputStream]s representing the submissions to be downloaded.
     */
    @WithSpan("DOWNLOAD Batch Submissions")
    fun downloadBatchSubmissions(submissionNamespaces: List<SubmissionNamespace>): InputStream {
        val ephemeralFile = EphemeralFile()

        // Write the zip output stream to the ephemeral file
        ZipOutputStream(ephemeralFile.path.outputStream()).use { output ->
            submissionNamespaces.forEach { submissionNamespace ->
                // Copy each submission to the zip output stream
                ZipInputStream(downloadSubmission(submissionNamespace)).use { input ->
                    generateSequence { input.nextEntry }.forEach { entry ->
                        output.putNextEntry(ZipEntry(submissionNamespace.directoryName + "/" + entry.name))
                        input.copyTo(output)
                        output.closeEntry()
                    }
                }
            }
        }

        // Delegate the responsibility of closing the EphemeralFile to RESTEasy via the EphemeralFileInputStream
        return EphemeralFileInputStream(ephemeralFile)
    }

    /**
     * Create a bucket for the submission if it does not exist already.
     *
     * @param bucketName The name of the bucket to create.
     */
    @WithSpan("CREATE Bucket")
    fun createBucket(bucketName: String) {
        val makeBucketArgs = MakeBucketArgs.Builder()
            .bucket(bucketName)
            .objectLock(true) // Enforces Write Once Read Many (WORM) on the bucket
            .build()

        // Configure duration-based object retention
        // https://min.io/docs/minio/linux/administration/object-management/object-retention.html#minio-object-locking-retention-modes
        val objectLockConfigurationArgs = SetObjectLockConfigurationArgs.Builder()
            .bucket(bucketName)
            .config(
                ObjectLockConfiguration(
                    RetentionMode.GOVERNANCE, // Prevents any operation that would mutate or modify the object or its locking settings...
                    RetentionDurationYears(2) // ... for 2 years, after which it can be deleted
                )
            )
            .build()

        // Configure automatic object expiration
        // https://min.io/docs/minio/linux/administration/object-management/create-lifecycle-management-expiration-rule.html
        val expiryDate = ZonedDateTime
            .now(ZoneId.of("GMT"))
            .plusYears(2)
            .plusDays(1)
            .withHour(0)
            .withMinute(0)
            .withSecond(0)
            .withNano(0) // 2 years from now, at midnight GMT
        val bucketLifeCycleArgs = SetBucketLifecycleArgs.Builder()
            .bucket(bucketName)
            .config(
                LifecycleConfiguration(
                    listOf(
                        LifecycleRule(
                            Status.ENABLED,
                            AbortIncompleteMultipartUpload(1), // Abort incomplete multipart uploads after 1 day
                            Expiration(expiryDate, null, null), // Expire upload after 2 years
                            RuleFilter(""),
                            "delete-after-2-years",
                            null,
                            null,
                            null
                        )
                    )
                )
            )
            .build()

        try {
            minioClient.makeBucket(makeBucketArgs)
            minioClient.setObjectLockConfiguration(objectLockConfigurationArgs)
            minioClient.setBucketLifecycle(bucketLifeCycleArgs)
        } catch (e: Exception) {
            when (e) { // Ignore if the bucket already exists
                is ErrorResponseException -> if (e.errorResponse().code() == "BucketAlreadyOwnedByYou") return
            }

            log.error("Failed to create and configure bucket ${bucketName}: ${e.message}", e)
            minioClient.removeBucket(RemoveBucketArgs.Builder().bucket(bucketName).build())
            throw e
        }
    }

    /**
     * Get a submission from the object store, retrieves the object data from MinIO.
     *
     * @param submissionNamespace The namespace of the submission.
     * @return The [GetObjectResponse] representing the submission.
     */
    fun getSubmission(submissionNamespace: SubmissionNamespace): GetObjectResponse {
        val getObjectArgs = GetObjectArgs.Builder()
            .bucket(submissionNamespace.bucketName)
            .`object`(submissionNamespace.objectName)
            .build()

        return try {
            minioClient.getObject(getObjectArgs)
        } catch (e: ErrorResponseException) {
            log.error("Submission does not exist: ${submissionNamespace.fullName}", e)
            throw SubmissionNotFoundException(e, submissionNamespace)
        }
    }

    /**
     * Check if an object exists in the object store.
     *
     * @param bucketName The name of the bucket.
     * @param objectName The name of the object.
     *
     * @return True if the object exists in the bucket, false if it does not.
     */
    private fun objectExists(bucketName: String, objectName: String): Boolean {
        val statObjectArgs = StatObjectArgs.Builder()
            .bucket(bucketName)
            .`object`(objectName)
            .build()

        return try {
            minioClient.statObject(statObjectArgs)
            true
        } catch (e: ErrorResponseException) {
            if(e.errorResponse().code() == "NoSuchKey") // Expected exception, object does not exist
                false
            else
                throw e // Unexpected exception, handle elsewhere
        }
    }

    /**
     * Check if expected tags are present, if not throw a [TagNotFoundException].
     *
     * @param tags The tags to check.
     *
     * @throws TagNotFoundException If any of the expected tags are not present.
     */
    private fun checkTags(tags: Map<String, String>) {
        val missingTags = expectedTags subtract tags.keys

        if(missingTags.isNotEmpty())
            throw TagNotFoundException(missingTags)
    }
}
