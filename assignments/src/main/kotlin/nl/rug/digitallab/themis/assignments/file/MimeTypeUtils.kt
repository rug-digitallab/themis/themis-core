package nl.rug.digitallab.themis.assignments.file

import com.google.common.net.MediaType
import nl.rug.digitallab.themis.assignments.exceptions.file.*
import org.jboss.resteasy.reactive.multipart.FileUpload
import java.net.URLConnection
import java.nio.file.Path
import kotlin.io.path.*

/**
 * Get the MIME type for a file. Extension function for [Path].
 *
 * @return The MIME type of the file. If the MIME type could not be determined, "application/octet-stream" is returned.
 *
 * @throws UnknownMimeTypeException If the MIME type of the file could not be determined. This can happen if the file is a directory.
 * @throws IllegalArgumentException If the MIME type has an invalid format.
 *
 */
fun Path.getMimeType(): MediaType {
    if(this.isDirectory())
        throw UnknownMimeTypeException(this)

    val mimeString = URLConnection.getFileNameMap().getContentTypeFor(fileName.toString()) ?: "application/octet-stream"

    return MediaType.parse(mimeString)
}

/**
 * Get the MIME type for a file upload. Extension function for [FileUpload].
 *
 * @return The MIME type of the file upload.
 */
fun FileUpload.getMimeType(): MediaType {
    return Path(fileName()).getMimeType()
}
