package nl.rug.digitallab.themis.assignments.processing

import nl.rug.digitallab.common.kotlin.helpers.ephemeral.EphemeralDirectory
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.EphemeralFile
import nl.rug.digitallab.themis.assignments.dtos.AddSubmissionRequest
import nl.rug.digitallab.themis.assignments.persistence.entities.Assignment
import nl.rug.digitallab.themis.assignments.persistence.entities.Submission
import nl.rug.digitallab.themis.assignments.persistence.namespaces.SubmissionNamespace
import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import kotlin.reflect.KProperty0
import kotlin.reflect.jvm.isAccessible

/**
 * The [SubmissionContext] class represents the context in which a submission is processed. Each submission has its own
 * context. The submission handlers in the submission processing pipeline use this context to pass information between
 * each other. The [SubmissionContext] is closable resource, and needs to be closed after it is no longer needed in
 * order to clean up the temporary resources.
 *
 * @param assignmentId The id of the assignment the submission is for.
 * @param addSubmissionRequest The request DTO to add the submission.
 *
 * @property zipFile The zip file containing the processed submission files.
 * @property processingDirectory The directory where the unprocessed submission files are placed and processed.
 * @property submission The submission entity.
 * @property assignment The assignment entity associated with the submission.
 * @property submissionNamespace The namespace of the submission in the object store.
 */
class SubmissionContext(
    val assignmentId: AssignmentId,
    val addSubmissionRequest: AddSubmissionRequest,
): AutoCloseable {
    lateinit var assignment: Assignment
    lateinit var submission: Submission
    lateinit var submissionNamespace: SubmissionNamespace

    val zipFile: EphemeralFile by lazy { EphemeralFile(suffix = ".zip") }
    val processingDirectory: EphemeralDirectory by lazy { EphemeralDirectory(prefix = "submission-") }

    /**
     * Checks if the submission is persisted. If the variable is not initialized, the submission is not persisted.
     *
     * @return True if the submission is persisted, false otherwise.
     */
    fun isSubmissionPersisted() = ::submission.isInitialized

    /**
     * Closes the context by closing the zip file and the processing directory, resulting in their deletion.
     */
    override fun close() {
        ::zipFile.orNull?.close()
        ::processingDirectory.orNull?.close()
    }

    /*
     * Checks if the lazy property is initialized.
     */
    private val KProperty0<*>.isLazyInitialized: Boolean
        get() {
            isAccessible = true
            return (getDelegate() as Lazy<*>).isInitialized()
        }

    /*
     * Gets the value of the lazy property if it is initialized, otherwise returns null.
     */
    private val <T> KProperty0<T>.orNull: T?
        get() = if (isLazyInitialized) get() else null
}

