package nl.rug.digitallab.themis.assignments.exceptions.submission

import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import org.jboss.resteasy.reactive.RestResponse.Status

class SubmissionAlreadyExistsException(objectName: String):
    MappedException("Submission '$objectName' already exists", Status.INTERNAL_SERVER_ERROR)
