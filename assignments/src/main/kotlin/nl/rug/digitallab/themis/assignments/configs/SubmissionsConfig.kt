package nl.rug.digitallab.themis.assignments.configs

import io.quarkus.runtime.annotations.StaticInitSafe
import io.smallrye.config.ConfigMapping

@StaticInitSafe
@ConfigMapping(prefix = "digital-lab.assignments.submissions")
interface SubmissionsConfig {
    /**
     * @return The maximum filename length of each file that is uploaded.
     */
    fun maxFileNameLength(): Int
}
