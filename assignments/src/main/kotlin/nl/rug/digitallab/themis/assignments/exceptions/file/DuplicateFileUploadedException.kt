package nl.rug.digitallab.themis.assignments.exceptions.file

import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import org.jboss.resteasy.reactive.RestResponse
import java.nio.file.Path

class DuplicateFileUploadedException(duplicateFile: Path, override val cause: Throwable?):
    MappedException("A duplicate file was uploaded: ${duplicateFile.fileName}", RestResponse.Status.BAD_REQUEST, cause)
