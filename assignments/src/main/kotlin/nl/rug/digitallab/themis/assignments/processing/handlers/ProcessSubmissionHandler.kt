package nl.rug.digitallab.themis.assignments.processing.handlers

import com.google.common.net.MediaType
import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.annotation.Priority
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.withEphemeralDirectory
import nl.rug.digitallab.common.quarkus.archiver.ArchiverManager
import nl.rug.digitallab.themis.assignments.configs.SubmissionsConfig
import nl.rug.digitallab.themis.assignments.exceptions.file.DuplicateFileUploadedException
import nl.rug.digitallab.themis.assignments.exceptions.file.FileNameInvalidException
import nl.rug.digitallab.themis.assignments.exceptions.file.FileUploadPathIsNullException
import nl.rug.digitallab.themis.assignments.exceptions.file.NoFilesUploadedException
import nl.rug.digitallab.themis.assignments.file.getMimeType
import nl.rug.digitallab.themis.assignments.processing.PROCESS_SUBMISSION_FILES_HANDLER_PRIORITY
import nl.rug.digitallab.themis.assignments.processing.SubmissionContext
import org.jboss.logging.Logger
import org.jboss.resteasy.reactive.multipart.FileUpload
import java.nio.file.FileAlreadyExistsException
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.Path
import kotlin.io.path.absolute
import kotlin.io.path.walk

/**
 * The [ProcessSubmissionHandler] handler is responsible for processing the file uploads in a submission request. It
 * copies the files to a processing directory, unarchives them if necessary, and archives the directory as a zip file.
 */
@ApplicationScoped
@Priority(PROCESS_SUBMISSION_FILES_HANDLER_PRIORITY)
class ProcessSubmissionHandler : SubmissionHandler {
    @Inject
    private lateinit var archiverManager: ArchiverManager

    @Inject
    private lateinit var submissionsConfig: SubmissionsConfig

    @Inject
    private lateinit var log: Logger

    // Explicit negative match (denylist) of characters that are not allowed in file names, enables reporting of the
    // specific illegal characters instead of just reporting that the file name is invalid.
    private val illegalCharactersRegex = Regex("[^a-zA-Z0-9_\\-. ]+")
    private val supportedArchiveMimeTypes = listOf(MediaType.TAR)

    @WithSpan("PROCESSING Submission")
    override fun handle(context: SubmissionContext) {
        // Process each file upload
        val fileUploads = context.addSubmissionRequest.fileUploads
        val processedFiles = fileUploads.map { processFileUpload(it, context.processingDirectory.path) }

        log.debug("Processed ${processedFiles.size} files in ${fileUploads.size} uploads")

        // Archive submission directory as a zip file
        archiverManager.archive(context.processingDirectory.path, context.zipFile.path)
    }

    /**
     * Process a file upload and copy all prepared files to the submission directory.
     *
     * @param fileUpload The file upload to process.
     * @param submissionDirectory The directory to copy the files to.
     *
     * @return A list of [Path]s to the files that were processed.
     *
     * @throws NoFilesUploadedException If no file was uploaded.
     * @throws DuplicateFileUploadedException If a file was uploaded twice.
     */
    fun processFileUpload(fileUpload: FileUpload, submissionDirectory: Path): List<Path> {
        // May happen when the form data specifies the key, but does not provide the value (= file)
        if(fileUpload.fileName().isEmpty())
            throw NoFilesUploadedException()

        return withEphemeralDirectory(prefix = "submission-preparation-") { preparationDirectory ->
            // Prepare file upload in the temporary preparation directory
            val preparedFiles = prepareFileUpload(fileUpload, preparationDirectory)
                .filter { path -> path.toString().isNotEmpty() }

            // Check if any files were uploaded
            if(preparedFiles.isEmpty())
                throw NoFilesUploadedException()

            // Check validity of file names and path segments
            preparedFiles.forEach { path ->
                path.forEach { segment -> checkValidFileName(segment.toString()) }
            }

            // Copy files to submission directory
            val processedFiles = preparedFiles.map { path ->
                val relativePath = preparationDirectory.relativize(path)
                val destinationPath = submissionDirectory.resolve(relativePath)
                Files.createDirectories(destinationPath.parent)

                // Move file to submission directory
                log.debug("Moving file ${path.absolute()} to ${destinationPath.absolute()}")
                try {
                    Files.move(path, destinationPath)
                } catch(e: FileAlreadyExistsException) {
                    throw DuplicateFileUploadedException(path, e)
                }

                return@map destinationPath
            }

            return@withEphemeralDirectory processedFiles
        }
    }

    /**
     * Prepare a [FileUpload] by copying it to a temporary directory and unarchiving it if necessary. Also checks if the
     * file upload has a path.
     *
     * @param fileUpload The file upload to prepare.
     * @param preparationDirectory The directory to copy the file upload to.
     *
     * @return A list of [Path]s to the files that were prepared.
     *
     * @throws FileUploadPathIsNullException If the file upload does not have a path.
     */
    @OptIn(ExperimentalPathApi::class)
    fun prepareFileUpload(fileUpload: FileUpload, preparationDirectory: Path): List<Path> {
        // Check if file upload has a path
        if(fileUpload.filePath() == null)
            throw FileUploadPathIsNullException(fileUpload)

        // Check if file upload is a supported archive, if so, unarchive it
        if (fileUpload.getMimeType()  in supportedArchiveMimeTypes) {
            log.debug("Processing archive ${fileUpload.fileName()}")
            archiverManager.unarchive(fileUpload.filePath(), preparationDirectory, fileUpload.getMimeType())
        } else {
            log.debug("Processing file ${fileUpload.fileName()}")
            Files.copy(fileUpload.filePath(), preparationDirectory.resolve(fileUpload.fileName()))
        }

        return preparationDirectory.walk().toList()
    }

    /**
     * Check if a file name is valid by performing a number of checks:
     * - The file name is not empty
     * - The file name is not too long
     * - The file name only contains POSIX portable file name characters
     *
     * @param fileName The file name to check.
     *
     * @throws FileNameInvalidException If the file name is invalid.
     */
    fun checkValidFileName(fileName: String) {
        // Check if file name is empty
        if(fileName.isEmpty())
            throw FileNameInvalidException(fileName, "File name is empty")

        val maxFileNameLength = submissionsConfig.maxFileNameLength()

        // Check if file name is too long
        if(fileName.length > maxFileNameLength)
            throw FileNameInvalidException(fileName, "File name is too long, maximum length is $maxFileNameLength")

        // Check if file name contains illegal characters, only allow POSIX portable file name character set
        val illegalCharacters = illegalCharactersRegex.findAll(fileName)
        if(illegalCharacters.count() > 0)
            throw FileNameInvalidException(fileName, "File name contains illegal characters: ${illegalCharacters.map { it.value }.joinToString(", ")}")
    }
}
