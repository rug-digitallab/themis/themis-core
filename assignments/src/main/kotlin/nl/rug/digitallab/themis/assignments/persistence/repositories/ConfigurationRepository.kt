package nl.rug.digitallab.themis.assignments.persistence.repositories

import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.persistence.EntityManager
import nl.rug.digitallab.common.quarkus.hibernate.orm.panache.repositories.BaseRepository
import nl.rug.digitallab.themis.assignments.exceptions.config.ConfigurationNotFoundException
import nl.rug.digitallab.themis.assignments.persistence.entities.Configuration
import nl.rug.digitallab.themis.assignments.persistence.entities.Profile
import nl.rug.digitallab.themis.common.typealiases.ConfigurationId
import org.hibernate.Session

/**
 * The [ConfigurationRepository] is responsible for handling all database operations related to [Configuration]s.
 */
@ApplicationScoped
class ConfigurationRepository : BaseRepository<Configuration, ConfigurationId>() {
    @Inject
    private lateinit var entityManager: EntityManager

    /**
     * Find an assignment configuration by its natural ID.
     *
     * @param profile The parent [Profile] of the [Configuration].
     * @param revision The numeric revision of the [Configuration].
     *
     * @return The [Configuration] entity.
     */
    fun findByNaturalId(profile: Profile, revision: Int): Configuration {
        return entityManager.unwrap(Session::class.java)
            .byNaturalId(Configuration::class.java)
            .using("profile", profile)
            .using("revision", revision)
            .load()
    }

    /**
     * Find an assignment configuration by ID, throwing an exception when the assignment configuration does not exist.
     *
     * @param configurationId The ID of the assignment configuration to find.
     *
     * @return The [Configuration] entity.
     *
     * @throws ConfigurationNotFoundException If the assignment configuration does not exist.
     */
    fun findByIdOrThrow(configurationId: ConfigurationId): Configuration {
        return findByIdOrThrow(configurationId) { ConfigurationNotFoundException(configurationId) }
    }
}
