package nl.rug.digitallab.themis.assignments.persistence.entities

import jakarta.persistence.*
import jakarta.validation.constraints.NotBlank
import nl.rug.digitallab.themis.common.typealiases.ConfigurationId
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.Generated
import org.hibernate.annotations.NaturalId
import java.time.Instant

@Entity
class Configuration(
    @field:NaturalId
    @field:ManyToOne(fetch = FetchType.EAGER)
    var profile: Profile,

    @field:NotBlank(message = "Configuration cannot be null or blank")
    @field:Column(updatable = false, columnDefinition = "JSON")
    var config: String,
) {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    lateinit var id: ConfigurationId

    @CreationTimestamp
    @Column(updatable = false)
    lateinit var created: Instant

    @Generated
    @Column(updatable = false)
    @NaturalId
    var revision: Int = -1
}
