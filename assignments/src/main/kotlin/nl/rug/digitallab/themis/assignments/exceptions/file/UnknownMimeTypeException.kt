package nl.rug.digitallab.themis.assignments.exceptions.file

import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import org.jboss.resteasy.reactive.RestResponse.Status
import java.nio.file.Path

class UnknownMimeTypeException(file: Path):
    MappedException("Unknown MIME type for file '$file'", Status.BAD_REQUEST)
