package nl.rug.digitallab.themis.assignments.dtos

import nl.rug.digitallab.themis.assignments.dtos.config.ParsedConfiguration
import nl.rug.digitallab.themis.common.typealiases.ConfigurationId
import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import nl.rug.digitallab.themis.common.typealiases.CourseInstanceId
import java.time.Instant

/**
 * Represents the response to a request for an assignment.
 *
 * @property id The id of the assignment.
 * @property created The time at which the assignment was created.
 * @property updated The time at which the assignment was last updated.
 * @property name The name of the assignment.
 * @property description The description of the assignment.
 * @property courseInstanceId The id of the course instance to which the assignment belongs.
 * @property softDeadline The soft deadline of the assignment.
 * @property hardDeadline The hard deadline of the assignment.
 * @property visibilitySpecification The visibility specification of the assignment.
 * @property activeProfileConfigs The currently active list of profile configurations.
 */
data class AssignmentResponse(
    val id: AssignmentId,
    val created: Instant,
    val updated: Instant,
    val name: String,
    val description: String,
    val courseInstanceId: CourseInstanceId,
    val softDeadline: Instant?,
    val hardDeadline: Instant?,
    val visibilitySpecification: VisibilitySpecificationResponse,
    val activeProfileConfigs: List<ActiveProfilesResponse>,
)

/**
 * Represents the visibility specification of an assignment.
 *
 * @property profileName The name of the profile that the config is a part
 * @property revision The revision of the config that is active within the profile.
 * @property config The configuration of the profile.
 * @property id The synthetic id of the configuration.
 */
data class ActiveProfilesResponse(
    val profileName: String,
    val revision: Int,
    val config: ParsedConfiguration,
    val id: ConfigurationId,
)
