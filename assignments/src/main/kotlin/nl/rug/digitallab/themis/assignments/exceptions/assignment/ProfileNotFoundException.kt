package nl.rug.digitallab.themis.assignments.exceptions.assignment

import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import org.jboss.resteasy.reactive.RestResponse

class ProfileNotFoundException(profileName: String):
    MappedException("Assignment profile '$profileName' not found", RestResponse.Status.NOT_FOUND)
