package nl.rug.digitallab.themis.assignments.processing.handlers

import io.opentelemetry.instrumentation.annotations.WithSpan
import io.smallrye.reactive.messaging.MutinyEmitter
import jakarta.annotation.Priority
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.themis.assignments.managers.toEvent
import nl.rug.digitallab.themis.assignments.processing.PUBLISH_SUBMISSION_HANDLER_PRIORITY
import nl.rug.digitallab.themis.assignments.processing.SubmissionContext
import nl.rug.digitallab.themis.common.constants.SUBMISSION_QUEUE
import nl.rug.digitallab.themis.common.events.SubmissionAddedEvent
import org.eclipse.microprofile.reactive.messaging.Channel

/**
 * The [PublishSubmissionHandler] handler is responsible for publishing a [SubmissionAddedEvent] to the submission queue.
 */
@ApplicationScoped
@Priority(PUBLISH_SUBMISSION_HANDLER_PRIORITY)
class PublishSubmissionHandler : SubmissionHandler {
    @Inject
    @Channel(SUBMISSION_QUEUE)
    private lateinit var submissionEmitter: MutinyEmitter<SubmissionAddedEvent>

    @WithSpan("PUBLISHING Submission")
    override fun handle(context: SubmissionContext) =
        submissionEmitter.sendAndAwait(context.submission.toEvent())
}
