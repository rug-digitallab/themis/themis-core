package nl.rug.digitallab.themis.assignments.persistence.repositories

import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.persistence.EntityManager
import nl.rug.digitallab.common.quarkus.hibernate.orm.panache.repositories.BaseRepository
import nl.rug.digitallab.themis.assignments.persistence.entities.Assignment
import nl.rug.digitallab.themis.assignments.persistence.entities.Profile
import nl.rug.digitallab.themis.common.typealiases.ProfileId
import org.hibernate.Session

/**
 * The [ProfileRepository] is responsible for handling all database operations related to [Profile]s.
 */
@ApplicationScoped
class ProfileRepository : BaseRepository<Profile, ProfileId>() {
    @Inject
    private lateinit var entityManager: EntityManager

    /**
     * Find a [Profile] by its natural ID.
     *
     * @param assignment The parent [Assignment] of the [Profile].
     * @param profileName The human-readable name of the [Profile].
     *
     * @return The [Profile] entity.
     */
    fun findByNaturalId(assignment: Assignment, profileName: String): Profile {
        return entityManager.unwrap(Session::class.java)
            .byNaturalId(Profile::class.java)
            .using("assignment", assignment)
            .using("name", profileName)
            .load()
    }
}
