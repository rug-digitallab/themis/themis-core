package nl.rug.digitallab.themis.assignments.managers

import com.fasterxml.jackson.databind.ObjectMapper
import nl.rug.digitallab.themis.assignments.dtos.*
import nl.rug.digitallab.themis.assignments.dtos.config.ConfigurationResponse
import nl.rug.digitallab.themis.assignments.dtos.config.ParsedConfiguration
import nl.rug.digitallab.themis.assignments.persistence.entities.Assignment
import nl.rug.digitallab.themis.assignments.persistence.entities.Configuration
import nl.rug.digitallab.themis.assignments.persistence.entities.Profile
import nl.rug.digitallab.themis.assignments.persistence.entities.Submission
import nl.rug.digitallab.themis.common.events.SubmissionAddedEvent


/**
 * Helper function to convert a [Assignment] entity to a [AssignmentResponse].
 *
 * @return The [AssignmentResponse].
 */
fun Assignment.toDTO() = AssignmentResponse(
    id = id,
    created = created,
    updated = updated,
    name = name,
    description = description,
    courseInstanceId = courseInstanceId,
    softDeadline = softDeadline,
    hardDeadline = hardDeadline,
    visibilitySpecification = VisibilitySpecificationResponse(
        visibilityType = visibilitySpecification.visibilityType.name,
        start = visibilitySpecification.start,
        end = visibilitySpecification.end,
    ),
    activeProfileConfigs = profiles.map { profile ->
        ActiveProfilesResponse(
            profileName = profile.name,
            revision = profile.currentConfiguration.revision,
            config = ObjectMapper().readValue(profile.currentConfiguration.config, ParsedConfiguration::class.java),
            id = profile.currentConfiguration.id,
        )
    },
)

/**
 * Helper function to convert a [Configuration] entity to a [ConfigurationResponse].
 *
 * @return The [ConfigurationResponse].
 */
fun Configuration.toDTO() = ConfigurationResponse(
    created = created,
    revision = revision,
    config = ObjectMapper().readValue(config, ParsedConfiguration::class.java),
    id = id,
)

/**
 * Helper function to convert a [Submission] entity to a [SubmissionResponse].
 *
 * @return The [SubmissionResponse].
 */
fun Submission.toDTO() = SubmissionResponse(
    id = id,
    created = created,
    updated = updated,
    courseInstanceId = assignment.courseInstanceId,
    assignmentId = assignment.id,
    groupId = groupId,
    configId = config.id,
    status = status,
)

/**
 * Helper function to convert a [Submission] entity to a [SubmissionAddedEvent].
 *
 * @return The [SubmissionAddedEvent].
 */
fun Submission.toEvent() = SubmissionAddedEvent(
    courseInstanceId = assignment.courseInstanceId,
    groupId = groupId,
    assignmentId = assignment.id,
    submissionId = id,
    configId = config.id,
)

/**
 * Helper function to convert a [AddAssignmentRequest] to a [Assignment] entity.
 *
 * @return The [Assignment] entity.
 */
fun AddAssignmentRequest.toEntity(): Assignment {
    val assignment = Assignment(
        name = name,
        description = description,
        courseInstanceId = courseInstanceId,
        softDeadline = softDeadline,
        hardDeadline = hardDeadline,
        visibilitySpecification = visibilitySpecification,
    )

    profileConfigs.forEach { (profileName, configString) ->
        val profile = Profile(
            name = profileName,
            assignment = assignment,
        )
        assignment.profiles.add(profile)

        val config = Configuration(
            config = ObjectMapper().writeValueAsString(configString),
            profile = profile,
        )
        profile.configurations.add(config)
    }

    return assignment
}

/**
 * Helper function to convert a [AddSubmissionRequest] to a [Submission] entity.
 *
 * @param assignment The [Assignment] to link the submission to.
 *
 * @return The [Submission] entity.
 */
fun AddSubmissionRequest.toEntity(assignment: Assignment) = Submission(
    groupId = groupId,
    assignment = assignment,
    config = assignment.profiles.first { profile -> profile.name == profileName }.currentConfiguration
)
