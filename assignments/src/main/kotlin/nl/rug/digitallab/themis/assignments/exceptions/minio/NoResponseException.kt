package nl.rug.digitallab.themis.assignments.exceptions.minio

import io.minio.UploadObjectArgs
import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import org.jboss.resteasy.reactive.RestResponse.Status

class NoResponseException(uploadArgs: UploadObjectArgs):
    MappedException("No response from minio for ${uploadArgs.bucket()}/${uploadArgs.`object`()}", Status.INTERNAL_SERVER_ERROR)
