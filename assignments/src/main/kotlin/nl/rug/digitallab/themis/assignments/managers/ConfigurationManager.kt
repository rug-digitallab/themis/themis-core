package nl.rug.digitallab.themis.assignments.managers

import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.themis.assignments.dtos.config.ConfigurationResponse
import nl.rug.digitallab.themis.assignments.persistence.repositories.ConfigurationRepository
import nl.rug.digitallab.themis.common.typealiases.ConfigurationId

/**
 * The [ConfigurationManager] is responsible for handling all business logic related to assignment configurations.
 */
@ApplicationScoped
class ConfigurationManager {
    @Inject
    private lateinit var configRepository: ConfigurationRepository

    /**
     * Get a specific assignment configuration by its synthetic ID.
     *
     * @param configId The id of the configuration.
     *
     * @return The full configuration together with its metadata.
     */
    fun getConfig(configId: ConfigurationId): ConfigurationResponse {
        return configRepository
            .findByIdOrThrow(configId)
            .toDTO()
    }
}
