package nl.rug.digitallab.themis.assignments

import jakarta.inject.Inject
import jakarta.transaction.Transactional
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import nl.rug.digitallab.themis.assignments.managers.SubmissionsManager
import nl.rug.digitallab.themis.assignments.dtos.ListSubmissionsRequest
import nl.rug.digitallab.themis.assignments.dtos.SubmissionResponse
import nl.rug.digitallab.themis.common.typealiases.SubmissionId
import org.jboss.resteasy.reactive.RestPath
import org.jboss.resteasy.reactive.RestQuery
import org.jboss.resteasy.reactive.RestResponse
import java.io.InputStream

/**
 * The [SubmissionsResource] is responsible for handling all REST requests related to submissions.
 */
@Path("/api/v1/themis/submissions")
class SubmissionsResource {
    @Inject
    private lateinit var submissionsManager: SubmissionsManager

    /**
     * Get all submissions and their metadata, optionally filtered by course, group, and/or assignment.
     *
     * @param listSubmissionsRequest The request DTO.
     *
     * @return A list of [SubmissionResponse] entities and their metadata.
     */
    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    fun listSubmissions(@BeanParam listSubmissionsRequest: ListSubmissionsRequest): List<SubmissionResponse> {
        return submissionsManager.listSubmissions(listSubmissionsRequest)
    }

    /**
     * Get the metadata for specific submission ID.
     *
     * @param submissionId The id of the submission.
     *
     * @return The [SubmissionResponse] entity and its metadata.
     */
    @GET
    @Path("{submissionId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    fun getSubmission(@RestPath submissionId: SubmissionId): SubmissionResponse {
        return submissionsManager.getSubmission(submissionId)
    }

    /**
     * Downloads all submissions files for the given submission ids.
     *
     * @param submissionIds The ids of the submissions to download.
     *
     * @return The submissions as a zip file, streamed to the browser through an [InputStream].
     */
    @GET
    @Path("download")
    @Transactional
    fun downloadBatchSubmissions(@RestQuery submissionIds: List<SubmissionId>): RestResponse<InputStream> {
        val submissions = submissionsManager.downloadBatchSubmission(submissionIds)

        return RestResponse.ResponseBuilder
            .ok(submissions)
            .header("Content-Type", "application/zip")
            .header("Content-Disposition", "attachment; filename=\"submissions.zip\"")
            .build()
    }

    /**
     * Download a specific submission from the MinIO object store.
     *
     * @param submissionId The id of the submission.
     *
     * @return The submission as a zip file, streamed to the browser through an [InputStream].
     */
    @GET
    @Path("{submissionId}/download")
    @Transactional
    fun downloadSubmission(@RestPath submissionId: SubmissionId): RestResponse<InputStream> {
        val submission = submissionsManager.downloadSubmission(submissionId)

        return RestResponse.ResponseBuilder
            .ok(submission)
            .header("Content-Type", "application/zip")
            .header("Content-Disposition", "attachment; filename=\"${submissionId}.zip\"")
            .build()
    }
}
