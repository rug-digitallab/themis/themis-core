package nl.rug.digitallab.themis.assignments.dtos.config

import nl.rug.digitallab.common.kotlin.helpers.noarg.NoArgConstructor
import nl.rug.digitallab.themis.common.data.RuntimeProfile

/**
 * Represents the full configuration of a profile of an assignment.
 *
 * @property pointCombinationExpression The expression that calculates the total points of a judgement based on the points of the jobs.
 * @property profile The profile of the assignment. This contains the per-stage and per-job configuration properties.
 */
@NoArgConstructor
data class ParsedConfiguration (
    val pointCombinationExpression: String,
    val profile: RuntimeProfile<JobConfig>,
)
