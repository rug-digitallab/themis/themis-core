package nl.rug.digitallab.themis.assignments.persistence.entities

import jakarta.persistence.*
import jakarta.validation.constraints.NotNull
import nl.rug.digitallab.themis.common.typealiases.GroupId
import nl.rug.digitallab.themis.common.typealiases.SubmissionId
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.Instant

/**
 * Represents a submission of a student for an assignment.
 *
 * @property id The ID of the submission.
 * @property created The time the submission was created.
 * @property updated The time the submission was last updated.
 * @property assignment The assignment the submission is for.
 * @property groupId The ID of the group that submitted the submission.
 * @property config The configuration of the assignment profile.
 * @property status The status of the submission. Defaults to [SubmissionStatus.PREPARING].
 */
@Entity
class Submission(
    @field:ManyToOne(fetch = FetchType.EAGER)
    @field:JoinColumn(name = "assignment_id")
    var assignment: Assignment,

    @field:NotNull(message = "Group ID cannot be null")
    var groupId: GroupId,

    @field:ManyToOne(fetch = FetchType.EAGER)
    @field:JoinColumn(name = "assignment_config_id")
    var config: Configuration,

    @field:NotNull
    var status: SubmissionStatus = SubmissionStatus.PREPARING,
) {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    lateinit var id: SubmissionId

    @CreationTimestamp
    lateinit var created: Instant

    @UpdateTimestamp
    lateinit var updated: Instant

    /**
     * Represents the status of a submission.
     *
     * @param code The code associated with the status.
     */
    enum class SubmissionStatus(val code: Int) {
        // Set by submission service
        FAILED(-10),
        PREPARING(10),
        PROCESSING(20),
        QUEUED(30),
    }

    /**
     * Converts a [SubmissionStatus] to and from an integer. This is necessary because JPA does not know how to serialize
     * and deserialize [SubmissionStatus] enum values with a custom field. By default, JPA uses the ordinal value of the
     * enum, which is not what we want.
     */
    @Converter(autoApply = true)
    class SubmissionStatusConverter: AttributeConverter<SubmissionStatus, Int> {
        override fun convertToDatabaseColumn(attribute: SubmissionStatus): Int = attribute.code
        override fun convertToEntityAttribute(dbData: Int): SubmissionStatus? = SubmissionStatus.entries.find { it.code == dbData }
    }
}
