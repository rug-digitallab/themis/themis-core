package nl.rug.digitallab.themis.assignments.persistence.entities

import jakarta.persistence.AttributeConverter
import jakarta.persistence.Converter
import jakarta.persistence.Embeddable
import jakarta.validation.constraints.NotNull
import java.time.Instant

/**
 * Represents the visibility of an assignment. The visibility type of an assignment can be [VisibilityType.NEVER],
 * [VisibilityType.TIMED], or [VisibilityType.ALWAYS]. an assignment is [VisibilityType.ALWAYS] when it is never visible
 * to users. an assignment is [VisibilityType.TIMED] when it is only visible during a specific period. an assignment is
 * [VisibilityType.ALWAYS] when it is always visible to users.
 *
 * @property visibilityType The visibility type of the assignment.
 * @property start From when this assignment should be visible to users. Cannot be null for [VisibilityType.TIMED].
 * @property end Until when this assignment should be visible to users. Cannot be null for [VisibilityType.TIMED].
 */
@Embeddable
class VisibilitySpecification {
    @NotNull(message = "Visibility cannot be null")
    lateinit var visibilityType: VisibilityType

    var start: Instant? = null

    var end: Instant? = null

    /**
     * Helper companion object to create [VisibilitySpecification]s.
     *
     * @property NEVER_VISIBLE A [VisibilitySpecification] with [VisibilityType.NEVER] visibility.
     * @property ALWAYS_VISIBLE A [VisibilitySpecification] with [VisibilityType.ALWAYS] visibility.
     */
    companion object {
        val NEVER_VISIBLE: VisibilitySpecification = create(VisibilityType.NEVER, null, null)
        val ALWAYS_VISIBLE: VisibilitySpecification = create(VisibilityType.ALWAYS, null, null)

        /**
         * Creates a [VisibilitySpecification] with [VisibilityType.TIMED] visibility.
         *
         * @param start From when this assignment should be visible to users.
         * @param end Until when this assignment should be visible to users.
         *
         * @return A [VisibilitySpecification] with [VisibilityType.TIMED] visibility.
         */
        fun timed(start: Instant, end: Instant): VisibilitySpecification {
            return create(VisibilityType.TIMED, start, end)
        }

        /**
         * Helper function to create a new visibility specification.
         *
         * @param visibilityType The visibility of the assignment.
         * @param start From when this assignment should be visible to users. Cannot be null for [VisibilityType.TIMED].
         * @param end Until when this assignment should be visible to users. Cannot be null for [VisibilityType.TIMED].
         *
         * @return A new [VisibilitySpecification].
         */
        private fun create(visibilityType: VisibilityType, start: Instant?, end: Instant?): VisibilitySpecification {
            val visibilitySpecification = VisibilitySpecification()
            visibilitySpecification.visibilityType = visibilityType
            visibilitySpecification.start = start
            visibilitySpecification.end = end
            return visibilitySpecification
        }
    }

    /**
     * Represents the visibility of an assignment.
     *
     * @property code The code associated with the visibility.
     */
    enum class VisibilityType(val code: Int) {
        /**
         * The assignment is never visible to users.
         */
        NEVER(10),

        /**
         * The assignment is only visible during the period between start and end.
         */
        TIMED(20),

        /**
         * The assignment is always visible to users.
         */
        ALWAYS(30),
    }

    /**
     * Converts a [VisibilityType] to and from its database representation.
     */
    @Converter(autoApply = true)
    class VisibilityConverter : AttributeConverter<VisibilityType, Int> {
        override fun convertToDatabaseColumn(attribute: VisibilityType): Int = attribute.code
        override fun convertToEntityAttribute(dbData: Int): VisibilityType? = VisibilityType.entries.find { it.code == dbData }
    }
}
