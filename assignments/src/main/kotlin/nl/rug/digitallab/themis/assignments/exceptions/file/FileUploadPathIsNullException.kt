package nl.rug.digitallab.themis.assignments.exceptions.file

import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import org.jboss.resteasy.reactive.RestResponse.Status
import org.jboss.resteasy.reactive.multipart.FileUpload

class FileUploadPathIsNullException(fileUpload: FileUpload):
    MappedException("File '${fileUpload.fileName()}' has no path", Status.BAD_REQUEST)
