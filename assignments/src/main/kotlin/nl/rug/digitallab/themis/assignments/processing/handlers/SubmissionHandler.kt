package nl.rug.digitallab.themis.assignments.processing.handlers

import nl.rug.digitallab.themis.assignments.processing.SubmissionContext

/**
 * The [SubmissionHandler] interface is implemented by each handler in the submission processing pipeline.
 */
interface SubmissionHandler {
    /**
     * Handles a step in the submission processing pipeline using the given [SubmissionContext], and returns the updated context.
     *
     * @param context The [SubmissionContext].
     *
     * @return The updated [SubmissionContext].
     */
    fun handle(context: SubmissionContext)
}
