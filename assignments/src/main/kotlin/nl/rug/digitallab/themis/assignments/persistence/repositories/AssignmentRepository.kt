package nl.rug.digitallab.themis.assignments.persistence.repositories

import jakarta.enterprise.context.ApplicationScoped
import nl.rug.digitallab.common.quarkus.hibernate.orm.panache.repositories.BaseRepository
import nl.rug.digitallab.themis.assignments.exceptions.assignment.AssignmentNotFoundException
import nl.rug.digitallab.themis.assignments.persistence.entities.Assignment
import nl.rug.digitallab.themis.assignments.persistence.entities.VisibilitySpecification.VisibilityType
import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import nl.rug.digitallab.themis.common.typealiases.CourseInstanceId
import java.time.Instant

/**
 * The [AssignmentRepository] is responsible for handling all database operations related to [Assignment]s.
 */
@ApplicationScoped
class AssignmentRepository : BaseRepository<Assignment, AssignmentId>() {
    /**
     * Find an assigment by ID, throwing an exception when the assignment does not exist.
     *
     * @param assignmentId The ID of the assignment to find.
     *
     * @return The [Assignment] entity.
     *
     * @throws AssignmentNotFoundException If the assignment does not exist.
     */
    fun findByIdOrThrow(assignmentId: AssignmentId): Assignment {
        return findByIdOrThrow(assignmentId) { AssignmentNotFoundException(assignmentId) }
    }

    /**
     * Find all assignments belonging to a course instance, optionally filtered by visibility at a given time.
     *
     * @param courseInstanceId The course instance to filter by.
     * @param visibleAtTime The time at which the retrieved assignment should be active. When null, no time filtering is applied.
     *
     * @return A list of filtered assignments.
     */
    fun find(courseInstanceId: CourseInstanceId, visibleAtTime: Instant? = null): List<Assignment> {
        return find(
            """
                courseInstanceId = ?1 AND
                (?2 IS NULL OR (
                    visibilitySpecification.visibilityType = ?3 OR (
                        visibilitySpecification.visibilityType = ?4 AND
                        visibilitySpecification.start   < ?2 AND
                        visibilitySpecification.end     > ?2
                        )
                    )
                )
            """, courseInstanceId, visibleAtTime, VisibilityType.ALWAYS, VisibilityType.TIMED
        ).list()
    }

    /**
     * Delete an assigment by ID, throwing an exception when the assignment does not exist.
     *
     * @param assignmentId The ID of the assignment to delete.
     *
     * @return The [Assignment] entity.
     *
     * @throws AssignmentNotFoundException If the assignment does not exist.
     */
    fun deleteByIdOrThrow(assignmentId: AssignmentId) {
        return deleteByIdOrThrow(assignmentId) { AssignmentNotFoundException(assignmentId) }
    }

    /**
     * Update an [Assignment] with exception.
     *
     * @param assignmentId The id of the entity
     * @param updateAssignment A method to take the old entity and should return [Assignment]
     *
     * @return The updated [Assignment]
     */
    fun updateWithOrThrow(
        assignmentId: AssignmentId,
        updateAssignment: (Assignment) -> Unit,
    ): Assignment {
        return updateWithOrThrow(assignmentId, { AssignmentNotFoundException(assignmentId) }, updateAssignment)
    }
}
