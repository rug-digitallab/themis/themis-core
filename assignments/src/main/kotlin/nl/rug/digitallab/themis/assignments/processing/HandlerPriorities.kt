package nl.rug.digitallab.themis.assignments.processing

/*
 * The priority of the handlers is used to determine the order in which they are executed by the [SubmissionProcessor].
 * The higher the priority, the earlier the handler is executed.
 */
const val VALIDATE_REQUEST_HANDLER_PRIORITY = 500
const val PERSIST_SUBMISSION_HANDLER_PRIORITY = 400
const val PROCESS_SUBMISSION_FILES_HANDLER_PRIORITY = 300
const val UPLOAD_SUBMISSION_HANDLER_PRIORITY = 200
const val PUBLISH_SUBMISSION_HANDLER_PRIORITY = 100
