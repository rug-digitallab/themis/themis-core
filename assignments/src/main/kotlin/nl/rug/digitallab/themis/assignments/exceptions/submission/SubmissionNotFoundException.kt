package nl.rug.digitallab.themis.assignments.exceptions.submission

import io.minio.errors.ErrorResponseException
import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import nl.rug.digitallab.themis.assignments.persistence.namespaces.SubmissionNamespace
import nl.rug.digitallab.themis.common.typealiases.SubmissionId
import org.jboss.resteasy.reactive.RestResponse.Status

class SubmissionNotFoundException(message: String): MappedException(message, Status.NOT_FOUND) {
    constructor(submissionId: SubmissionId): this("Submission '$submissionId' not found")

    constructor(submissionIds: Iterable<SubmissionId>): this("Submissions '${submissionIds.joinToString(", ")}' not found")

    constructor(errorResponseException: ErrorResponseException, submissionNamespace: SubmissionNamespace):
            this("Submission '${submissionNamespace.objectName}' not found: ${errorResponseException.message}")
}
