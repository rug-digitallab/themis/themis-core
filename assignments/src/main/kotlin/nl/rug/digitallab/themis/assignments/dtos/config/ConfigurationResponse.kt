package nl.rug.digitallab.themis.assignments.dtos.config

import nl.rug.digitallab.themis.common.typealiases.ConfigurationId
import java.time.Instant

/**
 * Represents the response to a request for an assignment configuration.
 *
 * @property created The time at which the configuration was created.
 * @property revision The revision of the configuration.
 * @property config The internal representation of the configuration of the assignment.
 * @property id The synthetic id of the configuration.
 */
data class ConfigurationResponse(
    val created: Instant,
    val revision: Int,
    val config: ParsedConfiguration,
    val id: ConfigurationId,
)
