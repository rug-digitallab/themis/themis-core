package nl.rug.digitallab.themis.assignments.persistence.entities

import jakarta.persistence.*
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotEmpty
import nl.rug.digitallab.themis.common.typealiases.ProfileId
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.JoinFormula
import org.hibernate.annotations.NaturalId
import org.hibernate.annotations.UpdateTimestamp
import java.time.Instant

/**
 * Represents a profile within an assignment.
 */
@Entity
@Table(
    // Ensure that a profile name is unique within an assignment
    uniqueConstraints = [UniqueConstraint(columnNames = ["name", "assignment_id"])]
)
class Profile(
    @field:NaturalId
    @field:NotBlank(message = "Profile name cannot be null or blank")
    var name: String,

    @field:NaturalId
    @field:ManyToOne(fetch = FetchType.LAZY)
    var assignment: Assignment,

    @field:NotEmpty(message = "Profile must have at least one configuration")
    @field:OneToMany(mappedBy = "profile", cascade = [CascadeType.ALL], orphanRemoval = true, fetch = FetchType.LAZY)
    var configurations: MutableSet<Configuration> = mutableSetOf(),
) {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    lateinit var id: ProfileId

    @CreationTimestamp
    lateinit var created: Instant

    @UpdateTimestamp
    lateinit var updated: Instant

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinFormula(value = """
        (
            SELECT config.id
            FROM configuration config
            WHERE config.profile_id = id
            ORDER BY config.revision DESC
            LIMIT 1
        )
    """)
    lateinit var currentConfiguration: Configuration

    /**
     * Updates the current configuration to the latest revision. @JoinFormula is not executed upon update / persist,
     * so we need to manually update the current assignment config.
     */
    @PostUpdate
    @PostPersist
    private fun updateConfiguration() {
        currentConfiguration = configurations.maxBy { it.revision }
    }
}
