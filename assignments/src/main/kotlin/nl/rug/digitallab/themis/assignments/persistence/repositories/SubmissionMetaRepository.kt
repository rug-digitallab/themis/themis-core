package nl.rug.digitallab.themis.assignments.persistence.repositories

import jakarta.enterprise.context.ApplicationScoped
import nl.rug.digitallab.common.quarkus.hibernate.orm.panache.repositories.BaseRepository
import nl.rug.digitallab.themis.assignments.exceptions.submission.SubmissionNotFoundException
import nl.rug.digitallab.themis.assignments.persistence.entities.Submission
import nl.rug.digitallab.themis.assignments.persistence.entities.Submission.SubmissionStatus
import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import nl.rug.digitallab.themis.common.typealiases.CourseInstanceId
import nl.rug.digitallab.themis.common.typealiases.GroupId
import nl.rug.digitallab.themis.common.typealiases.SubmissionId

/**
 * The [SubmissionMetaRepository] is responsible for handling all database operations related to submissions.
 */
@ApplicationScoped
class SubmissionMetaRepository: BaseRepository<Submission, SubmissionId>() {
    /**
     * Find a submission by its ID, throwing an exception when the submission does not exist.
     *
     * @param submissionId The ID of the submission to find.
     *
     * @return The [Submission] entity.
     *
     * @throws SubmissionNotFoundException If the submission does not exist.
     */
    fun findByIdOrThrow(submissionId: SubmissionId): Submission {
        return findByIdOrThrow(submissionId){ SubmissionNotFoundException(submissionId) }
    }

    /**
     * Find all submissions, optionally filtered by [CourseInstanceId], [GroupId], and/or [AssignmentId].
     *
     * @param courseInstanceId The id of the course to filter by.
     * @param groupId The id of the group to filter by.
     * @param assignmentId The id of the assignment to filter by.
     *
     * @return A list of (filtered) submissions.
     */
    fun find(courseInstanceId: CourseInstanceId? = null, assignmentId: AssignmentId? = null, groupId: GroupId? = null): List<Submission> {
        return find("""
                (cast(?1 AS uuid) IS null OR assignment.courseInstanceId = ?1) AND
                (cast(?2 AS uuid) IS null OR groupId                     = ?2) AND 
                (cast(?3 AS uuid) IS null OR assignment.id               = ?3)
            """, courseInstanceId, groupId, assignmentId)
            .list()
    }

    /**
     * Find multiple submissions by their [SubmissionId]. Any non-existent submissions are ignored.
     *
     * @param submissionIds The ids of the submissions to find.
     *
     * @return A list of submissions.
     *
     * @throws SubmissionNotFoundException If any of the submissions do not exist.
     */
    fun find(submissionIds: List<SubmissionId>): List<Submission> {
        val submissions = find("id IN ?1", submissionIds)
            .list()

        if (submissions.size != submissionIds.size) {
            val foundIds = submissions.map { it.id }
            val missingIds = submissionIds.filter { it !in foundIds }
            throw SubmissionNotFoundException(missingIds)
        }

        return submissions
    }

    /**
     * Update the status of a [Submission] to a [SubmissionStatus].
     *
     * @param submission The submission to update.
     * @param status The new status of the submission.
     */
    fun updateStatus(submission: Submission, status: SubmissionStatus) {
        submission.status = status
        persist(submission)
    }
}
