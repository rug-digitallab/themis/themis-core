package nl.rug.digitallab.themis.assignments.managers

import com.fasterxml.jackson.databind.ObjectMapper
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.themis.assignments.dtos.AddAssignmentRequest
import nl.rug.digitallab.themis.assignments.dtos.AssignmentResponse
import nl.rug.digitallab.themis.assignments.dtos.config.ConfigurationResponse
import nl.rug.digitallab.themis.assignments.dtos.config.ParsedConfiguration
import nl.rug.digitallab.themis.assignments.exceptions.assignment.AssignmentNotFoundException
import nl.rug.digitallab.themis.assignments.exceptions.assignment.ProfileNotFoundException
import nl.rug.digitallab.themis.assignments.persistence.entities.Configuration
import nl.rug.digitallab.themis.assignments.persistence.entities.Profile
import nl.rug.digitallab.themis.assignments.persistence.repositories.ConfigurationRepository
import nl.rug.digitallab.themis.assignments.persistence.repositories.ProfileRepository
import nl.rug.digitallab.themis.assignments.persistence.repositories.AssignmentRepository
import nl.rug.digitallab.themis.common.typealiases.AssignmentId
import nl.rug.digitallab.themis.common.typealiases.CourseInstanceId
import java.time.Instant

/**
 * The [AssignmentsManager] is responsible for handling all business logic related to assignments.
 */
@ApplicationScoped
class AssignmentsManager {
    @Inject
    private lateinit var assignmentRepository: AssignmentRepository

    @Inject
    private lateinit var profileRepository: ProfileRepository

    @Inject
    private lateinit var configRepository: ConfigurationRepository

    @Inject
    private lateinit var objectMapper: ObjectMapper

    /**
     * Add a new assignment to the database.
     *
     * @param addAssignmentRequest The request DTO.
     *
     * @return The [AssignmentResponse] of the added assignment.
     */
    fun addAssignment(addAssignmentRequest: AddAssignmentRequest): AssignmentResponse {
        val assignment = addAssignmentRequest.toEntity()

        assignmentRepository.create(assignment)

        return assignment.toDTO()
    }

    /**
     * List all assignments for a specific course instance. Optionally filter by visibility at a specific time.
     *
     * @param courseInstanceId The id of the course instance to list assignments for.
     * @param visibleAtTime The time at which the retrieved assignments should be active. When null, no time filtering is applied.
     *
     * @return A list of [AssignmentResponse]s.
     */
    fun listAssignments(courseInstanceId: CourseInstanceId, visibleAtTime: Instant? = null): List<AssignmentResponse> {
        return assignmentRepository
            .find(courseInstanceId = courseInstanceId, visibleAtTime = visibleAtTime)
            .map { it.toDTO() }
    }

    /**
     * Get a specific assignment by its id.
     *
     * @param assignmentId The id of the assignment.
     *
     * @return The [AssignmentResponse] of the assignment.
     *
     * @throws AssignmentNotFoundException If the assignment does not exist.
     */
    fun getAssignment(assignmentId: AssignmentId): AssignmentResponse {
        return assignmentRepository
            .findByIdOrThrow(assignmentId)
            .toDTO()
    }

    /**
     * Get the configurations for a specific assignment profile.
     *
     * @param assignmentId The id of the assignment.
     * @param profileName The name of the profile to get the configurations for.
     *
     * @return A list of [ConfigurationResponse]s.
     *
     * @throws ProfileNotFoundException If the profile does not exist.
     */
    fun getConfigurations(assignmentId: AssignmentId, profileName: String): List<ConfigurationResponse> {
        val assignment = assignmentRepository.findByIdOrThrow(assignmentId)

        val profile = assignment.profiles.find { it.name == profileName }
            ?: throw ProfileNotFoundException(profileName)

        return profile.configurations.map { it.toDTO() }
    }

    /**
     * Update a specific assignment. Does not update the profile and configs, use [updateConfigurations] for that.
     *
     * @param assignmentId The id of the assignment to update.
     * @param addAssignmentRequest The request DTO containing the updated assignment data.
     *
     * @return The [AssignmentResponse] of the updated assignment.
     *
     * @throws AssignmentNotFoundException If the assignment does not exist.
     */
    fun updateAssignment(
        assignmentId: AssignmentId,
        addAssignmentRequest: AddAssignmentRequest,
    ): AssignmentResponse {
        // Check if the assignment exists, and if so, update it, otherwise throw an exception
        return assignmentRepository
            .updateWithOrThrow(
                assignmentId = assignmentId,
                updateAssignment = { assignment ->
                    assignment.name = addAssignmentRequest.name
                    assignment.description = addAssignmentRequest.description
                    assignment.courseInstanceId = addAssignmentRequest.courseInstanceId
                    assignment.softDeadline = addAssignmentRequest.softDeadline
                    assignment.hardDeadline = addAssignmentRequest.hardDeadline
                    assignment.visibilitySpecification = addAssignmentRequest.visibilitySpecification
                }
            )
            .toDTO()
    }

    /**
     * Update the configuration for a specific assignment and profile. If the profile does not exist, it will be created.
     *
     * @param assignmentId The id of the assignment to update.
     * @param profileName The name of the profile to update the configuration for.
     * @param config The new configuration.
     *
     * @return The [ConfigurationResponse] of the updated assignment.
     */
    fun updateConfigurations(assignmentId: AssignmentId, profileName: String, config: ParsedConfiguration): ConfigurationResponse {
        val assignment = assignmentRepository.findByIdOrThrow(assignmentId)

        val retrievedProfile = assignment.profiles.find { it.name == profileName }
            ?: Profile(
                name = profileName,
                assignment = assignment,
            ).also { assignment.profiles.add(it) }

        val configuration = Configuration(
            config = objectMapper.writeValueAsString(config),
            profile = retrievedProfile,
        )

        retrievedProfile.configurations.add(configuration)
        assignmentRepository.create(assignment)

        return configuration.toDTO()
    }

    /**
     * Delete a specific assignment.
     *
     * @param assignmentId The id of the assignment to delete.
     *
     * @return A [Uni] that completes when the course series is deleted.
     *
     * @throws AssignmentNotFoundException If the assignment does not exist.
     */
    fun deleteAssignment(assignmentId: AssignmentId) {
        assignmentRepository.deleteByIdOrThrow(assignmentId)
    }

    /**
     * Get a specific revision of the assignment configuration for a specific profile.
     *
     * @param assignmentId The id of the assignment the profile belongs to.
     * @param profileName The name of the profile to get the configuration for.
     * @param revision The revision of the configuration.
     *
     * @return The [ConfigurationResponse] of the configuration.
     */
    fun getConfig(assignmentId: AssignmentId, profileName: String, revision: Int): ConfigurationResponse {
        val assignment = assignmentRepository.findByIdOrThrow(assignmentId)

        val profile = profileRepository.findByNaturalId(assignment, profileName)

        val config = configRepository.findByNaturalId(profile, revision)

        return config.toDTO()
    }
}
