package nl.rug.digitallab.themis.assignments.dtos.config

import nl.rug.digitallab.common.kotlin.helpers.noarg.NoArgConstructor
import nl.rug.digitallab.themis.common.data.RuntimeProfile

/**
 * Represents the configuration of a job, within a stage, within a profile of an assignment.
 *
 * @property name The name of the job.
 * @property labelAssertions A list of assertions together with a label. If the assertion is true, the label is applied to the job.
 * @property labelPoints A list of labels together with a number of points. If the job has the label, the points are added to the job.
 */
@NoArgConstructor
data class JobConfig(
    override val name: String,
    val labelAssertions: Map<String, String>,
    val labelPoints: Map<String, Int>,
) : RuntimeProfile.Job
