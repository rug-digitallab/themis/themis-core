package nl.rug.digitallab.themis.assignments.processing.handlers

import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.annotation.Priority
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.themis.assignments.persistence.namespaces.SubmissionNamespace
import nl.rug.digitallab.themis.assignments.persistence.repositories.SubmissionObjectRepository
import nl.rug.digitallab.themis.assignments.processing.SubmissionContext
import nl.rug.digitallab.themis.assignments.processing.UPLOAD_SUBMISSION_HANDLER_PRIORITY

/**
 * The [UploadSubmissionHandler] handler is responsible for uploading the submission files to the object repository.
 */
@ApplicationScoped
@Priority(UPLOAD_SUBMISSION_HANDLER_PRIORITY)
class UploadSubmissionHandler : SubmissionHandler {
    @Inject
    private lateinit var submissionObjectRepository: SubmissionObjectRepository

    @WithSpan("UPLOADING Submission")
    override fun handle(context: SubmissionContext) {
        val submission = context.submission
        val submissionNamespace = SubmissionNamespace(
            submission.assignment.courseInstanceId,
            submission.groupId,
            submission.assignment.id,
            submission.id,
        )

        submissionObjectRepository.uploadSubmission(
            context.zipFile.path,
            submissionNamespace,
            submission.config.id,
        )

        context.submissionNamespace = submissionNamespace
    }
}
