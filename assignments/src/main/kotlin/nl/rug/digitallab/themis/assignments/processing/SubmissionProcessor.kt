package nl.rug.digitallab.themis.assignments.processing

import io.quarkus.arc.All
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import nl.rug.digitallab.themis.assignments.exceptions.submission.SubmissionProcessingException
import nl.rug.digitallab.themis.assignments.persistence.entities.Submission
import nl.rug.digitallab.themis.assignments.persistence.repositories.SubmissionMetaRepository
import nl.rug.digitallab.themis.assignments.processing.handlers.SubmissionHandler
import org.jboss.logging.Logger
import org.jboss.resteasy.reactive.RestResponse

/**
 * The submission processor is responsible for executing the submission processing pipeline. The pipeline consists of a
 * series of handlers that are executed in order. Each handler is responsible for a specific part of the submission
 * processing pipeline.
 */
@ApplicationScoped
class SubmissionProcessor {
    @All
    private lateinit var handlers: MutableList<SubmissionHandler>

    @Inject
    private lateinit var submissionMetaRepository: SubmissionMetaRepository

    @Inject
    private lateinit var log: Logger

    /**
     * Execute the submission processing pipeline. A starting context is provided, which is then passed through each
     * handler in the pipeline. The context is updated by each handler, and the updated context is passed to the next
     * handler. The final context is returned after all handlers have finished processing.
     *
     * @param context The submission context.
     *
     * @return The updated submission context.
     */
    fun execute(context: SubmissionContext) {
        val request = context.addSubmissionRequest

        log.info("Processing submission for assignment ${context.assignmentId} with profile ${request.profileName} " +
            "for group ${request.groupId} with ${request.fileUploads.size} file(s)")

        return handlers.forEach { handler ->
            try {
                handler.handle(context)
            } catch (e: Throwable) {
                throw handleException(context, e)
            }
        }
    }

    /**
     * Handle an exception that occurred during the submission processing pipeline. This method logs the exception and
     * performs additional steps depending on the context at the time the exception occurred.
     *
     * @param context The submission context at the time the exception occurred.
     * @param exception The exception that occurred.
     *
     * @return The exception that should be thrown.
     */
    private fun handleException(context: SubmissionContext, exception: Throwable): MappedException {
        log.error("Failed to process submission: ${exception.message}, context: $context", exception)

        // Check if a submission was created, if so, we need to update the state of the submission.
        if(context.isSubmissionPersisted()) {
            submissionMetaRepository.updateStatus(context.submission, Submission.SubmissionStatus.FAILED)
        }

        // Use the HTTP status code from the exception if it is a MappedException, otherwise use 500.
        val httpStatus = when(exception) {
            is MappedException -> exception.httpStatusCode
            else -> RestResponse.Status.INTERNAL_SERVER_ERROR
        }

        return SubmissionProcessingException(
            "Failed to process submission: ${exception.message}",
            context,
            httpStatus,
            exception,
        )
    }
}
