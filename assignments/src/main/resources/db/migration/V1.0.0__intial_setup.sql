
create table assignment (
                            id binary(16) not null,
                            course_instance_id binary(16) not null,
                            created datetime(6),
                            description varchar(255) not null,
                            hard_deadline datetime(6),
                            name varchar(255) not null,
                            soft_deadline datetime(6),
                            updated datetime(6),
                            end datetime(6),
                            start datetime(6),
                            visibility_type integer,
                            primary key (id)
) engine=InnoDB;

create table configuration (
                               id binary(16) not null,
                               config JSON not null,
                               created datetime(6),
                               revision integer,
                               profile_id binary(16),
                               primary key (id)
) engine=InnoDB;

create table profile (
                         id binary(16) not null,
                         created datetime(6),
                         name varchar(255) not null,
                         updated datetime(6),
                         assignment_id binary(16),
                         primary key (id)
) engine=InnoDB;

create table submission (
                            id binary(16) not null,
                            created datetime(6),
                            group_id binary(16) not null,
                            status integer not null,
                            updated datetime(6),
                            assignment_id binary(16),
                            assignment_config_id binary(16),
                            primary key (id)
) engine=InnoDB;

alter table if exists configuration
    add constraint UKqwx0tn3remsu78dirc5i36of4 unique (profile_id, revision);

alter table if exists profile
    add constraint UKeyqoa0nxdbjtben1tsrf8uk2w unique (name, assignment_id);

alter table if exists configuration
    add constraint FK1ne65b97g37p454k4oie145w4
    foreign key (profile_id)
    references profile (id);

alter table if exists profile
    add constraint FK55whgrw7bi5q36jyj1jamk0ay
    foreign key (assignment_id)
    references assignment (id);

alter table if exists submission
    add constraint FK3q8643roa73llngo64dvpvtxt
    foreign key (assignment_id)
    references assignment (id);

alter table if exists submission
    add constraint FK75f03yho5dpo9c4q4ili0apus
    foreign key (assignment_config_id)
    references configuration (id);
