-- This table stores the last revision of every configuration
-- Without this table, the database performance would drop dramatically as the number of configs increases
CREATE TABLE configuration_latest_revision (
   profile_id binary(16) NOT NULL,
   latest_revision INTEGER,
   PRIMARY KEY (profile_id)
) engine=InnoDB;

-- Automatically calculates the revision property so that the application does not have to worry about it.
CREATE TRIGGER configuration_calculate_revision
    BEFORE INSERT ON configuration
    FOR EACH ROW
BEGIN
    DECLARE latest_rev INT;

    -- First try to select and lock the existing row
    SELECT latest_revision INTO latest_rev
    FROM configuration_latest_revision
    WHERE profile_id = NEW.profile_id
        FOR UPDATE; -- Lock the row during the transaction. This will prevent any race conditions when generating the revision.

    IF latest_rev IS NULL THEN
        -- No row exists yet, insert with initial value
        INSERT INTO configuration_latest_revision (profile_id, latest_revision)
        VALUES (NEW.profile_id, 1);
        SET latest_rev = 1;
    ELSE
        -- Row exists and is locked, update it
        UPDATE configuration_latest_revision
        SET latest_revision = latest_revision + 1
        WHERE profile_id = NEW.profile_id;
        SET latest_rev = latest_rev + 1;
    END IF;

    -- Assign the value to NEW.revision
    SET NEW.revision = latest_rev;
END;
