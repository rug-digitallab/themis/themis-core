rootProject.name = "themis-core"

pluginManagement {
    plugins {
        val digitalLabGradlePluginVersion: String by settings

        id("nl.rug.digitallab.gradle.plugin.quarkus.project") version digitalLabGradlePluginVersion
        id("nl.rug.digitallab.gradle.plugin.quarkus.library") version digitalLabGradlePluginVersion
    }

    repositories {
        maven("https://gitlab.com/api/v4/groups/65954571/-/packages/maven") // Digital Lab Group Repository
        gradlePluginPortal()
    }
}

include("assignments")
include("judgements")
include("provisioning")
include("runtime-orchestrator:orchestrator")
include("runtime-orchestrator:service")
include("libraries:common-themis")
include("libraries:expression-evaluator")
