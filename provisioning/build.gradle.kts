import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.project")
}

dependencies {
    val commonKotlinVersion: String by project
    val commonQuarkusVersion: String by project
    val kotlinMockitoVersion: String by rootProject
    val mockitoInlineVersion: String by rootProject
    val otelExtensionKotlinVersion: String by project
    val quarkusMinioVersion: String by project
    val sprinklerUtilsVersion: String by project
    val testContainersVersion: String by project

    // Helper dependencies
    implementation("nl.rug.digitallab.common.quarkus:archiver:$commonQuarkusVersion")
    implementation("nl.rug.digitallab.common.quarkus:exception-mapper-rest:$commonQuarkusVersion")

    // Closeable scope
    implementation("com.black-kamelia.sprinkler:utils:$sprinklerUtilsVersion")

    // MinIO Object Store dependencies
    implementation("io.quarkiverse.minio:quarkus-minio:$quarkusMinioVersion")

    // REST dependencies
    implementation("io.quarkus:quarkus-rest-jackson")
    implementation("io.quarkus:quarkus-smallrye-openapi")
    implementation("io.quarkus:quarkus-hibernate-validator")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    // Observability
    implementation("io.quarkus:quarkus-observability-devservices-lgtm")
    implementation("nl.rug.digitallab.common.quarkus:opentelemetry:$commonQuarkusVersion")
    implementation("io.opentelemetry:opentelemetry-extension-kotlin:$otelExtensionKotlinVersion")

    // Testing dependencies
    testImplementation("io.quarkus:quarkus-junit5-component")
    testImplementation("io.quarkus:quarkus-junit5-mockito") // Mocking support for Quarkus
    testImplementation("org.mockito.kotlin:mockito-kotlin:$kotlinMockitoVersion") // Mocking support for Kotlin
    testImplementation("org.mockito:mockito-inline:$mockitoInlineVersion") // Mocking final classes, such as URL
    testImplementation("org.testcontainers:testcontainers:$testContainersVersion") // Starting services to test against
    testImplementation("nl.rug.digitallab.common.quarkus:test-rest:$commonQuarkusVersion")
    testImplementation("nl.rug.digitallab.common.kotlin:approval-tests:$commonKotlinVersion")

    // RESTAssured for testing the REST API
    testImplementation("io.rest-assured:kotlin-extensions")
    testImplementation("io.rest-assured:rest-assured")
}

/**
 * emit-jvm-type-annotations is necessary for jakarta validation of container elements (via annotation of type arguments)
 */
tasks.withType<KotlinCompile> {
    compilerOptions {
        freeCompilerArgs.add("-Xemit-jvm-type-annotations")
    }
}

