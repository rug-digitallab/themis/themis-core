# Provisioning Service

This subproject holds a container image provisioning service that builds container images, given a `Dockerfile` and
build context, via a REST API. 

You can supply the build context (i.e: the files that the `Dockerfile` uses):
- Via the REST API, in the form of a zip
- Via a URL, GETable via an HTTP request
- Via a reference to the object store that this service is deployed alongside

The path of downloaded files can be specified anywhere into the build context.

**Note**: If a Dockerfile is placed into the root of the build context via the above methods and is named `Dockerfile`, 
it **will** be overwritten by the Dockerfile explicitly provided through the API.

After building, this image is then pushed onto our container registry.

## Requirements

Alongside the usual requirements to deploy a Quarkus REST service, the `buildah` CLI utility must be present. This can
be obtained via any package management tool on the operating system, without explicitly adding a repository. For example,
using `apt`:
```bash
sudo apt install buildah
```
See [here](https://github.com/containers/buildah/blob/main/install.md) for more details.

## Usage

To build an image that will be pushed to the container registry, use the following `curl` command:

```shell
curl -X 'POST' \
  '$SERVICE_URL/api/v1/provisioning/build?imageName=$IMAGE_NAME&imageTag=$IMAGE_TAG' \
  -H 'accept: application/json' \
  -H 'Content-Type: multipart/form-data' \
  -F 'dockerfile=$PATH_TO_DOCKERFILE' \
  -F 'contextFromArchive=$PATH_TO_ARCHIVE \ # For no archive, use an empty string
  # The keys in these maps are the relative paths the files are downloaded to. The folders are created automatically
  -F 'contextFromURL = { # For no URLS, send an empty map, i.e: {}
    "fileInRoot": "http://example.com", # You can access resources via HTTP that are GETable
    "folder/fileNotInRoot": "https://example.com", # Or HTTPS
  };type=application/json' \
  -F 'contextFromObjectStore = { # For no object store downloads, send an empty map, i.e: {}
    "anotherFileInRoot": {
      "bucket": "bucket1",
      "path": "fileInRoot"
    },
    "anotherFolder/anotherFileNotInRoot": {
      "bucket": "bucket2",
      "path": "folder/fileInFolder"
    },
  };type=application/json'
```
where:
* `SERVICE_URL`: Is the URL that the service is hosted on
* `IMAGE_NAME` and `IMAGE_TAG`: Is the name and tag for the image you wish to be built
* `PATH_TO_DOCKERFILE`: Is the path to the `Dockerfile` that will be used to build the image
* `PATH_TO_ARCHIVE`: Is the path to the archive that will be used to populate the build context

Note: Remember that paths to files on the host should be prefixed with `@`.
