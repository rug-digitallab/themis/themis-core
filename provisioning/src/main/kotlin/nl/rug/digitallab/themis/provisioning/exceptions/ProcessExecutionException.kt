package nl.rug.digitallab.themis.provisioning.exceptions

import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import org.jboss.resteasy.reactive.RestResponse.Status

/**
 * The exception class to be thrown when an error occurs during a [Process]'s execution.
 *
 * @param name The name of the process that had an error.
 * @param cause Information from the underlying exception that caused the error.
 */
class ProcessExecutionException(name: String = "a process", cause: Throwable? = null) :
    MappedException("There was an error executing $name", Status.INTERNAL_SERVER_ERROR, cause)
