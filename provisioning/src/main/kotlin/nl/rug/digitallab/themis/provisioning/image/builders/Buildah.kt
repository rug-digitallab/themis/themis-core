package nl.rug.digitallab.themis.provisioning.image.builders

import io.opentelemetry.api.trace.Tracer
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import kotlinx.coroutines.*
import nl.rug.digitallab.common.quarkus.opentelemetry.withCoroutineSpan
import nl.rug.digitallab.themis.provisioning.exceptions.BuildahException
import nl.rug.digitallab.themis.provisioning.exceptions.ProcessExecutionException
import nl.rug.digitallab.themis.provisioning.image.ImageRegistry
import nl.rug.digitallab.themis.provisioning.image.builders.ImageBuilder.Companion.imageName
import nl.rug.digitallab.themis.provisioning.image.builders.commands.buildah.*
import org.eclipse.microprofile.context.ManagedExecutor
import org.jboss.logging.Logger
import java.io.InputStream
import java.nio.file.Path
import kotlin.time.Duration

/**
 * An implementation of [ImageBuilder] using the buildah CLI utility.
 */
@ApplicationScoped
class Buildah : ImageBuilder {
    @Inject
    private lateinit var tracer: Tracer

    @Inject
    private lateinit var managedExecutor: ManagedExecutor

    @Inject
    private lateinit var logger: Logger

    override suspend fun build(
        dockerfile: Path,
        name: String,
        tag: String,
        contextLocation: Path,
        imageRegistry: ImageRegistry,
        timeout: Duration,
    ): ImageBuilder.CommandResult = withCoroutineSpan("BUILD image") {
        val buildCommand = BuildCommand(
            dockerfile = dockerfile,
            name = name,
            tag = tag,
            contextLocation = contextLocation,
        ).withRegistry(imageRegistry)

        return@withCoroutineSpan try {
            executeBuildah(buildCommand, timeout)
        } catch (e: Exception) {
            throw BuildahException("Could not build image ${imageName(name,tag)}", e)
        }
    }

    override suspend fun push(
        name: String,
        tag: String,
        imageRegistry: ImageRegistry,
        timeout: Duration,
    ): ImageBuilder.CommandResult = withCoroutineSpan("PUSH image") {
        val pushCommand = PushCommand(
            name = name,
            tag = tag,
        ).withRegistry(imageRegistry)

        return@withCoroutineSpan try {
            executeBuildah(pushCommand, timeout)
        } catch (e: Exception) {
            throw BuildahException("Could not push image ${imageName(name,tag)}", e)
        }
    }

    override suspend fun listImages(
        timeout: Duration,
    ): ImageBuilder.ListImageResult = withCoroutineSpan("LIST images") {
        val listImagesCommand = ListImagesCommand()

        try {
            return@withCoroutineSpan executeBuildah(listImagesCommand, timeout)
        } catch (e: Exception) {
            throw BuildahException("Could not list images", e)
        }
    }

    override suspend fun removeImage(
        name: String,
        tag: String,
        timeout: Duration,
    ): ImageBuilder.CommandResult = withCoroutineSpan("REMOVE image") {
        val removeImageCommand = RemoveImageCommand(
            name = name,
            tag = tag,
        )

        try {
            return@withCoroutineSpan executeBuildah(removeImageCommand, timeout)
        } catch (e: Exception) {
            throw BuildahException("Could not remove image ${imageName(name,tag)}", e)
        }
    }

    /**
     * Executes buildah with the supplied [BuildahCommand].
     *
     * @param buildahCommand The buildah command to execute.
     *
     * @throws ProcessExecutionException If buildah exits with an error code that is not 0
     */
    suspend fun <T> executeBuildah(
        buildahCommand: BuildahCommand<T>,
        timeout: Duration,
    ): T = withContext(managedExecutor.asCoroutineDispatcher()) {
        val commandList = buildahCommand.commandList()
        val processBuilder = ProcessBuilder(commandList)

        logger.info("Executing command: ${commandList.joinToString(separator = " ")}")
        val process = processBuilder.start()

        // Read and collect the output and error streams concurrently
        val stdOutJob = async { process.inputStream.collectAndLogLines(logger::info, "stdOut") }
        val stdErrJob = async { process.errorStream.collectAndLogLines(logger::info, "stdErr") }

        try {
            withTimeout(timeout) {
                val exitCode = runInterruptible {
                    process.waitFor()
                }

                if (exitCode != 0) {
                    throw ProcessExecutionException("Error while running `${commandList.joinToString(separator = " ")}`, exit code: $exitCode")
                }

                val stdOut = stdOutJob.await()
                val stdError = stdErrJob.await()

                buildahCommand.parseResult(stdOut, stdError, exitCode)
            }
        } catch (e: TimeoutCancellationException) {
            process.destroyForcibly().waitFor()
            throw e
        }
    }

    /**
     * Collects the lines from the input stream and logs them using the provided log function. The lines are also
     * collected into a single string and returned.
     *
     * @param logFunction The function to use for logging the lines.
     * @param prefix The prefix to use for each log line.
     *
     * @return The lines collected from the input stream.
     */
    private fun InputStream.collectAndLogLines(logFunction: (String) -> Unit, prefix: String): String = bufferedReader()
        .useLines { lines ->
            buildString {
                for (line in lines) {
                    logFunction("$prefix > $line")
                    appendLine(line)
                }
            }
        }
}
