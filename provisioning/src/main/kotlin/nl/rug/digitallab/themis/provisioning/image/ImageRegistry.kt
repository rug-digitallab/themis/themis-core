package nl.rug.digitallab.themis.provisioning.image

/**
 * A [ImageRegistry] represents the registry that images will be pushed to.
 *
 * @property address The address to access the registry by.
 * @property isSecure Whether the registry should be accessed via HTTP (!isSecure) or HTTPS (isSecure).
 * @property user The username used to push to this registry.
 * @property password The password used to push to this registry.
 */
data class ImageRegistry(
    val address: String,
    val isSecure: Boolean = false,
    val user: String? = null,
    val password: String? = null,
)