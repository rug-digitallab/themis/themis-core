package nl.rug.digitallab.themis.provisioning.image

import com.google.common.net.MediaType
import com.kamelia.sprinkler.util.closeableScope
import io.opentelemetry.instrumentation.annotations.WithSpan
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeout
import nl.rug.digitallab.common.quarkus.archiver.ArchiverManager
import nl.rug.digitallab.common.quarkus.archiver.MimeTypeUtils.getMimeType
import nl.rug.digitallab.themis.provisioning.exceptions.InvalidContextException
import nl.rug.digitallab.themis.provisioning.namespaces.ObjectNamespace
import nl.rug.digitallab.themis.provisioning.persistence.repositories.ObjectRepository
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.net.URL
import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.copyTo
import kotlin.time.Duration

/**
 * The [BuildContext] manages files within a specific build context, for image building.
 *
 * @property archiveManager The archive manager that will be used to unarchive archives.
 * @property objectStore The object store that will be used to download objects from.
 * @property downloadTimeout The maximum time to download a file from the object store or the internet.
 * @property buildDir The path to the directory that will house the build context. It must already be created, before
 * being supplied to [BuildContext]. Defaults to a temporary file.
 */
class BuildContext(
    private val archiveManager: ArchiverManager,
    private val objectStore: ObjectRepository,
    private val downloadTimeout: Duration,
    val buildDir: Path,
) {
    /**
     * Adds [sourceFile] to the build context, based on the [destinationFile] supplied.
     *
     * @param sourceFile The file to be added to the build context.
     * @param destinationFile The path (relative to the build context root) the file should be located at.
     *
     * @throws InvalidContextException When there is an error setting up the build context.
     */
    fun addFromFile(
        sourceFile: Path,
        destinationFile: Path,
    ) {
        try {
            sourceFile.copyTo(buildDir.resolve(destinationFile), overwrite = true)
        } catch (e: IOException) {
            throw InvalidContextException("Copying the supplied Dockerfile failed: ${e.message}", e)
        }
    }

    /**
     * Places all files (preserving the directory structure) inside [sourceArchive] into the build context.
     *
     * @param sourceArchive The archive that will be used to populate the build context.
     * @param destinationDirectory The prefix that will be attached to every file / directory in the archive. Defaults to "/".
     * @param mediaType The mime type of the archive.
     *
     * @throws InvalidContextException When there is an error setting up the build context.
     */
    @WithSpan("UNZIPPING Archive Into Build Context")
    fun addFromArchive(
        sourceArchive: File,
        destinationDirectory: Path = Path("."),
        mediaType: MediaType = sourceArchive.toPath().getMimeType(),
    ) {
        try {
            archiveManager.unarchive(sourceArchive.toPath(), buildDir.resolve(destinationDirectory), mediaType)
        } catch (e: Exception) {
            throw InvalidContextException("Unzipping the supplied archive failed: ${e.message}", e)
        }
    }

    /**
     * Downloads the corresponding file at the URL and places it into the build context.
     *
     * @param sourceUrl The URI for the file to be retrieved.
     * @param destinationFile The path (relative to the build context root) the file should be located at.
     *
     * @throws InvalidContextException When there is an error setting up the build context.
     */
    @WithSpan("DOWNLOADING URL Into Build Context")
    suspend fun addFromURL(
        sourceUrl: URL,
        destinationFile: Path,
    ) {
        try {
            withTimeout(downloadTimeout) {
                launch {
                    sourceUrl.openStream().use {
                        addFromStream(it, destinationFile)
                    }
                }
            }
        } catch (e: Exception) {
            throw InvalidContextException("Downloading $sourceUrl failed: ${e.message}", e)
        }
    }


    /**
     * Downloads the corresponding file in the object store and places it into the build context.
     *
     * @param sourceNamespace The namespace corresponding to the object in the object store.
     * @param destinationFile The path (relative to the build context root) the file should be located at.
     *
     * @throws InvalidContextException When there is an error setting up the build context.
     */
    @WithSpan("DOWNLOADING Object From Object Store")
    suspend fun addFromObjectStore(
        sourceNamespace: ObjectNamespace,
        destinationFile: Path,
    ) {
        try {
            withTimeout(downloadTimeout) {
                launch {
                    addFromStream(objectStore.downloadObject(sourceNamespace), destinationFile)
                }
            }
        } catch (e: Exception) {
            throw InvalidContextException(
                "Downloading ${sourceNamespace.bucket}/${sourceNamespace.path} from the object store failed: ${e.message}", e
            )
        }

    }

    /**
     * Copies the data at [sourceInputStream] to the file located at [destinationFile] (relative to the build folder).
     *
     * @param sourceInputStream The source of data to copy.
     * @param destinationFile The path (relative to the build context root) the file should be located at.
     */
    private fun addFromStream(
        sourceInputStream: InputStream,
        destinationFile: Path,
    ) = closeableScope {
        val input = using(sourceInputStream)

        // Create output file if needed
        val outputFile = buildDir.resolve(destinationFile).toFile()
        outputFile.parentFile.mkdirs()
        outputFile.createNewFile()

        val output = using(outputFile.outputStream())

        input.copyTo(output)
    }
}
