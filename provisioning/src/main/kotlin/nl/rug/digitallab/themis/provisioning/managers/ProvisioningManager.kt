package nl.rug.digitallab.themis.provisioning.managers

import com.google.common.net.MediaType
import com.kamelia.sprinkler.util.closeableScope
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.withEphemeralDirectory
import nl.rug.digitallab.common.quarkus.archiver.ArchiverManager
import nl.rug.digitallab.common.quarkus.archiver.MimeTypeUtils.getMimeType
import nl.rug.digitallab.themis.provisioning.configs.ProvisioningConfig
import nl.rug.digitallab.themis.provisioning.dtos.BuildRequest
import nl.rug.digitallab.themis.provisioning.exceptions.BuildahException
import nl.rug.digitallab.themis.provisioning.exceptions.InvalidContextException
import nl.rug.digitallab.themis.provisioning.image.BuildContext
import nl.rug.digitallab.themis.provisioning.image.builders.Buildah
import nl.rug.digitallab.themis.provisioning.image.ImageRegistry
import nl.rug.digitallab.themis.provisioning.persistence.repositories.ObjectRepository
import org.jboss.logging.Logger
import org.jboss.resteasy.reactive.multipart.FileUpload
import kotlin.io.path.Path
import kotlin.jvm.optionals.getOrNull

/**
 * The [ProvisioningManager] is responsible for encapsulating all logic related to building container images.
 */
@ApplicationScoped
class ProvisioningManager {
    @Inject
    private lateinit var config: ProvisioningConfig

    @Inject
    private lateinit var objectStore: ObjectRepository

    @Inject
    private lateinit var archiveManager: ArchiverManager

    @Inject
    private lateinit var imageBuilder: Buildah

    @Inject
    private lateinit var logger: Logger

    /**
     * Build an image with a specified name, tag, Dockerfile and build context files.
     *
     * @param buildRequest Information used for building the image (i.e: the Dockerfile and the build context files).
     *
     * @throws InvalidContextException When there is an error setting up the build context.
     * @throws BuildahException When there is an error building the image.
     */
    suspend fun buildImage(buildRequest: BuildRequest) = withEphemeralDirectory { directory ->
        /*
         * We create a build context, and we:
         * 1. Populate it
         * 2. Build the image inside the context
         * 3. Push the resulting image
         */
        logger.info("Creating image for $buildRequest")

        with(BuildContext(archiveManager, objectStore, config.externalDownloadTimeout, directory)) {
            populateBuildContext(buildRequest)
            buildImage(buildRequest.imageName, buildRequest.imageTag)
            pushImage(buildRequest.imageName, buildRequest.imageTag)
        }
    }

    /**
     * Populates the build context managed by [BuildContext].
     *
     * @param buildRequest The DTO containing the information used to populate the build context.
     *
     * @throws InvalidContextException When there is an error setting up the build context.
     */
    context(BuildContext)
    private suspend fun populateBuildContext(buildRequest: BuildRequest) = closeableScope {
        logger.info("Populating build context")

        // Populate build context
        buildRequest.contextFromArchive?.let { archive ->
            if (archive.fileName() != null || archive.fileName().isNotEmpty()) {
                addFromArchive(archive.uploadedFile().toFile(), mediaType = archive.getMimeType())
            }
        }

        buildRequest.contextFromURL.forEach { (path, uri) ->
            addFromURL(uri, path)
        }

        buildRequest.contextFromObjectStore.forEach { (path, namespace) ->
            addFromObjectStore(namespace, path)
        }

        /*
         * Place the dockerfile in the build context. This must be the last step, to ensure that if a Dockerfile was
         * downloaded / unzipped from the archive, it will be overwritten.
         */
        val dockerfile = buildRequest.dockerfile
        addFromFile(dockerfile.toPath(), Path("Dockerfile"))
    }

    /**
     * Builds the image inside the build context managed by [BuildContext].
     *
     * @param imageName The name of the image.
     * @param imageTag The tag associated with this image build.
     *
     * @throws BuildahException When there is an error building the image.
     */
    context(BuildContext)
    private suspend fun buildImage(imageName: String, imageTag: String) {
        logger.info("Building image $imageName:$imageTag")

        imageBuilder.build(
            dockerfile = buildDir.resolve(Path("Dockerfile")),
            name = imageName,
            tag = imageTag,
            contextLocation = buildDir,
            imageRegistry = ImageRegistry(
                address = config.registryAddress,
                isSecure = config.registryIsSecure,
                user = config.registryUsername.getOrNull(),
                password = config.registryPassword.getOrNull(),
            ),
        )
    }

    /**
     * Push [imageName]:[imageTag] to the container image registry.
     *
     * @param imageName The name of the image.
     * @param imageTag The tag associated with this image build.
     *
     * @throws BuildahException When there is an error pushing the image.
     */
    private suspend fun pushImage(imageName: String, imageTag: String) {
        logger.info("Pushing image $imageName:$imageTag")

        imageBuilder.push(
            name = imageName,
            tag = imageTag,
            imageRegistry = ImageRegistry(
                address = config.registryAddress,
                isSecure = config.registryIsSecure,
                user = config.registryUsername.getOrNull(),
                password = config.registryPassword.getOrNull(),
            ),
        )
    }

    /**
     * Get the MIME type for a file upload. Extension function for [FileUpload].
     *
     * @return The MIME type of the file upload.
     */
    fun FileUpload.getMimeType(): MediaType {
        return Path(fileName()).getMimeType()
    }
}
