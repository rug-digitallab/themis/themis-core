package nl.rug.digitallab.themis.provisioning.image.builders.commands.buildah

import nl.rug.digitallab.themis.provisioning.image.builders.ImageBuilder

/**
 * Pushes a container image to a registry using Buildah.
 *
 * @param name The name of the image.
 * @param tag The tag of the image.
 */
data class PushCommand(
    val name: String,
    val tag: String,
): BuildahCommand<ImageBuilder.CommandResult>() {
    override fun commandList(): List<String> =
        buildList {
            add(EXECUTABLE)
            add("push")
            addAll(registryArguments())
            add(ImageBuilder.imageName(name, tag))
            add("docker://${imageRegistry!!.address}/${ImageBuilder.imageName(name, tag)}")
        }

    override fun parseResult(
        stdOut: String,
        stdError: String,
        exitCode: Int,
    ): ImageBuilder.CommandResult = ImageBuilder.CommandResult(stdOut, stdError, exitCode)
}
