package nl.rug.digitallab.themis.provisioning.dtos

import jakarta.validation.constraints.NotNull
import jakarta.ws.rs.DefaultValue
import jakarta.ws.rs.core.MediaType
import nl.rug.digitallab.common.kotlin.helpers.noarg.NoArgConstructor
import nl.rug.digitallab.themis.provisioning.namespaces.ObjectNamespace
import nl.rug.digitallab.themis.provisioning.validation.constraints.ValidURLContext
import nl.rug.digitallab.themis.provisioning.validation.constraints.OCINamePattern
import nl.rug.digitallab.themis.provisioning.validation.constraints.OCITagPattern
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType
import org.eclipse.microprofile.openapi.annotations.media.Schema
import org.jboss.resteasy.reactive.PartType
import org.jboss.resteasy.reactive.RestForm
import org.jboss.resteasy.reactive.RestQuery
import org.jboss.resteasy.reactive.multipart.FileUpload
import java.io.File
import java.net.URL
import java.nio.file.Path

/**
 * The [BuildRequest] is the DTO for the ProvisioningServiceResource.buildImage endpoint.
 *
 * @property imageName The name of the image that will be created. Must correspond to OCI conventions.
 * @property imageTag The tag for the image that will be created. Must correspond to OCI conventions.
 * @property dockerfile The dockerfile that specifies how to build this image.
 * @property contextFromArchive Files to be placed in the build context, inside a zip. The zip can have an arbitrary.
 * folder structure.
 * @property contextFromURL Files to be placed in the build context, specified via URIs of URLs.
 * @property contextFromObjectStore Files to be placed in the build context, specified via object store references.
 */
@NoArgConstructor
class BuildRequest(
    @field:RestQuery
    @field:OCINamePattern
    @field:NotNull(message = "The imageName query parameter is missing")
    var imageName: String,

    @field:RestQuery
    @field:DefaultValue("latest")
    @field:OCITagPattern
    var imageTag: String,

    @field:RestForm
    @field:NotNull(message = "The dockerfile form parameter is missing")
    var dockerfile: File,

    @field:RestForm
    @field:Schema(implementation = BinaryStringSchema::class) // Smallrye OpenAPI supports `FileUpload` but not `FileUpload?`
    var contextFromArchive: FileUpload?,

    @field:RestForm
    @field:ValidURLContext
    @field:PartType(MediaType.APPLICATION_JSON)
    @field:Schema(example =
        """{
            "root":     "http://path.to/file",
            "non/root": "https://path.to/another#file"
        }""")
    var contextFromURL: Map<Path, URL>,

    @field:RestForm
    @field:PartType(MediaType.APPLICATION_JSON)
    @field:Schema(example =
        """{
            "root": {
                "bucket":   "bucketName",
                "path":     "path/in/bucket"
            },
            "non/root": {
                "bucket":   "bucketName",
                "path":     "path/in/bucket"
            }
        }"""
    )
    var contextFromObjectStore: Map<Path, ObjectNamespace>,
)

@Schema(type = SchemaType.STRING, format = "binary")
interface BinaryStringSchema
