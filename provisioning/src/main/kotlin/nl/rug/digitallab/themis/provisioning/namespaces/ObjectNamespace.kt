package nl.rug.digitallab.themis.provisioning.namespaces

/**
 * Represents the namespace of any object in an object store.
 *
 * @param bucket The bucket in which the object resides in.
 * @param path The path to the object, within the bucket.
 */
data class ObjectNamespace(val bucket: String, val path: String)
