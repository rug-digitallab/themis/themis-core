package nl.rug.digitallab.themis.provisioning

import io.opentelemetry.context.Context
import io.opentelemetry.extension.kotlin.asContextElement
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.validation.Valid
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import kotlinx.coroutines.*
import nl.rug.digitallab.themis.provisioning.dtos.BuildRequest
import nl.rug.digitallab.themis.provisioning.managers.ProvisioningManager
import nl.rug.digitallab.themis.provisioning.workers.CoroutineBackgroundWorker
import org.jboss.logging.Logger
import org.jboss.resteasy.reactive.ResponseStatus
import org.jboss.resteasy.reactive.RestResponse.StatusCode

/**
 * The [ProvisioningResource] is responsible for handling all REST requests related to building container images.
 */
@ApplicationScoped
@Path("/api/v1/provisioning")
class ProvisioningResource {
    @Inject
    private lateinit var logger: Logger

    @Inject
    private lateinit var worker: CoroutineBackgroundWorker

    @Inject
    private lateinit var manager: ProvisioningManager

    /**
     * Build an image via a Dockerfile and a build context. This image is automatically pushed to the container image
     * registry with the supplied name and tag.
     *
     * The endpoint is asynchronous, meaning that if the request is correctly formatted, ACCEPTED is returned. Then
     * the image is built in the background.
     *
     * If there are errors during the building of the image, then that status is held locally.
     *
     * @param buildRequest Contains all the information required to build the image.
     */
    @POST
    @Path("/build")
    @ResponseStatus(StatusCode.ACCEPTED)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    fun buildImage(
        @BeanParam @Valid buildRequest: BuildRequest,
    ) {
        logger.info("Building image with request: $buildRequest")

        // Since the coroutine is started in a different thread, with a different context, we need to capture the
        // current context and use it in the coroutine. Not doing so would result in the OTEL trace context not being
        // propagated, disconnecting the trace.
        val capturedContext = Context.current()

        worker.enqueueTask {
            // The coroutine is started in the context of the worker, so we need to capture the context and activate it.
            withContext(capturedContext.asContextElement()) {
                manager.buildImage(buildRequest)
            }
        }
    }
}
