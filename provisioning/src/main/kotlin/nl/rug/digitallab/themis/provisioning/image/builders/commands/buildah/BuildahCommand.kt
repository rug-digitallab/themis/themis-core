package nl.rug.digitallab.themis.provisioning.image.builders.commands.buildah

import nl.rug.digitallab.themis.provisioning.image.ImageRegistry
import nl.rug.digitallab.themis.provisioning.image.builders.commands.ImageBuilderCommand

/**
 * A generic Buildah command.
 */
abstract class BuildahCommand<T> : ImageBuilderCommand<T> {
    protected var imageRegistry: ImageRegistry? = null

    /**
     * Sets the image registry to use for this command.
     */
    fun withRegistry(imageRegistry: ImageRegistry) = apply {
        this.imageRegistry = imageRegistry
    }

    /**
     * Adds the remote cache arguments to the command list.
     *
     * @param name The name of the image.
     *
     * @return The remote cache arguments.
     */
    protected fun remoteCacheArguments(name: String): List<String> {
        val registry = checkNotNull(imageRegistry) { "Image registry must be set" }

        return buildList {
            add("--cache-to")
            add("${registry.address}/$name/cache")
            add("--cache-from")
            add("${registry.address}/$name/cache")
        }
    }

    /**
     * Adds the registry arguments to the command list.
     *
     * @return The registry arguments.
     */
    protected fun registryArguments(): List<String> {
        val registry = checkNotNull(imageRegistry) { "Image registry must be set" }

        return buildList {
            if (!registry.isSecure) {
                add("--tls-verify=false")
            }

            if (registry.user != null) {
                addAll(listOf("--creds", "${registry.user}:${registry.password}"))
            }
        }
    }

    companion object {
        const val EXECUTABLE: String = "buildah"
    }
}
