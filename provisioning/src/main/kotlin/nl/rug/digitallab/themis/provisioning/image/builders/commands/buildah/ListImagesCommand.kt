package nl.rug.digitallab.themis.provisioning.image.builders.commands.buildah

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import nl.rug.digitallab.common.kotlin.helpers.noarg.NoArgConstructor
import nl.rug.digitallab.themis.provisioning.image.builders.ImageBuilder

/**
 * Lists all images using Buildah.
 */
class ListImagesCommand: BuildahCommand<ImageBuilder.ListImageResult>() {
    override fun commandList(): List<String> =
        buildList {
            add(EXECUTABLE)
            add("images")
            add("--json")
        }

    override fun parseResult(
        stdOut: String,
        stdError: String,
        exitCode: Int,
    ): ImageBuilder.ListImageResult {
        val objectMapper = ObjectMapper()
        val images = objectMapper.readValue<List<Image>>(stdOut)
        return ImageBuilder.ListImageResult(images.mapNotNull { it.names }.flatten())
    }

    /**
     * Helper data class to parse the JSON output of the list images command.
     *
     * @param names The names of the images.
     */
    @NoArgConstructor
    @JsonIgnoreProperties(ignoreUnknown = true) // Ignore unknown properties in the JSON, we only care about "names".
    private data class Image(val names: List<String>?)
}
