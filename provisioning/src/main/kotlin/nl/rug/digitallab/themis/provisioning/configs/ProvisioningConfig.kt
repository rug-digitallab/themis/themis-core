package nl.rug.digitallab.themis.provisioning.configs

import io.smallrye.config.*
import nl.rug.digitallab.common.kotlin.quantities.integrations.smallrye.config.DurationLongConverter
import java.util.Optional
import kotlin.time.Duration

/**
 * The [ProvisioningConfig] holds configuration data about the provisioning service.
 */
@ConfigMapping(prefix = "digital-lab.provisioning")
interface ProvisioningConfig {
    /**
     * The registry address (as a URL) of the container image registry.
     */
    @get:WithDefault("registry/address")
    val registryAddress: String

    /**
     * Whether the registry is accessed via HTTPS or via HTTP.
     */
    @get:WithDefault("true")
    val registryIsSecure: Boolean

    /**
     * The username used when pushing to the registry.
     */
    val registryUsername: Optional<String>

    /**
     * The password used when pushing to the registry.
     * Can also be a personal access token.
     */
    val registryPassword: Optional<String>


    /**
     * The maximum time the service should wait before timing out, when building a container image.
     */
    @get:WithName("image-build-timeout")
    @get:WithConverter(DurationLongConverter::class)
    val imageBuildTimeout: Duration

    /**
     * The maximum time the service should wait before timing out, when pushing a container image.
     */
    @get:WithName("image-push-timeout")
    @get:WithConverter(DurationLongConverter::class)
    val imagePushTimeout: Duration

    /**
     * The maximum time the service should wait before timing out, when downloading a file from the object store
     * or URL.
     */
    @get:WithName("external-download-timeout")
    @get:WithConverter(DurationLongConverter::class)
    val externalDownloadTimeout: Duration
}
