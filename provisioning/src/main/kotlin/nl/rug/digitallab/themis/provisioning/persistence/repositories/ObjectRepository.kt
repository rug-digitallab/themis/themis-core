package nl.rug.digitallab.themis.provisioning.persistence.repositories

import io.minio.GetObjectArgs
import io.minio.MinioClient
import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.themis.provisioning.namespaces.ObjectNamespace
import java.io.InputStream

/**
 * The [ObjectRepository] manages all interactions with MinIO.
 */
@ApplicationScoped
class ObjectRepository {

    @Inject
    private lateinit var minioClient: MinioClient

    /**
     * Downloads an object from MinIO.
     *
     * @param namespace The namespace of the object.
     *
     * @return The [InputStream] representing the submission to be downloaded.
     */
    @WithSpan("DOWNLOAD object")
    fun downloadObject(namespace: ObjectNamespace): InputStream {
        val getObjectArgs = GetObjectArgs.Builder()
            .bucket(namespace.bucket)
            .`object`(namespace.path)
            .build()

        return minioClient.getObject(getObjectArgs)
    }
}
