package nl.rug.digitallab.themis.provisioning.image.builders.commands.buildah

import nl.rug.digitallab.themis.provisioning.image.builders.ImageBuilder

/**
 * Removes a container image using Buildah.
 *
 * @param name The name of the image.
 * @param tag The tag of the image.
 */
data class RemoveImageCommand(
    val name: String,
    val tag: String,
) : BuildahCommand<ImageBuilder.CommandResult>() {
    override fun commandList(): List<String> =
        buildList {
            add(EXECUTABLE)
            add("rmi")
            add("-f")
            add(ImageBuilder.imageName(name, tag))
        }

    override fun parseResult(
        stdOut: String,
        stdError: String,
        exitCode: Int,
    ): ImageBuilder.CommandResult = ImageBuilder.CommandResult(stdOut, stdError, exitCode)
}
