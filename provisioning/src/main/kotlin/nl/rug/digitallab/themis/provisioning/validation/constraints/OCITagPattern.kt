package nl.rug.digitallab.themis.provisioning.validation.constraints

import jakarta.validation.Constraint
import jakarta.validation.Payload
import jakarta.validation.constraints.Pattern
import kotlin.annotation.AnnotationTarget.*
import kotlin.reflect.KClass


/**
 * A constraint annotation that validates a string to be a valid tag for an OCI image.
 * https://github.com/opencontainers/distribution-spec/blob/main/spec.md#pull. The regex
 * associated with this pattern can be interpreted as follows:
 *
 * A tag is at mos 128 characters long. It consists of letters, numbers, dots, dashes or
 * underscores, but it cannot start with a dot nor a dash.
 *
 * @property message The message to return when the constraint is violated.
 * @property groups The groups the constraint belongs to.
 * @property payload The payload associated with this constraint.
 */
@Pattern(
    regexp = "[a-zA-Z0-9_][a-zA-Z0-9._-]{0,127}",
    message = "May only contain upto 128 letters, numbers, dots, underscores and dashes and at least one. Cannot start with a dot or a dash",
)
@Target(FIELD, PROPERTY, VALUE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@Constraint(validatedBy = [])
annotation class OCITagPattern(
    val message: String = "May only contain upto 128 letters, numbers, dots, underscores and dashes and at least one. Cannot start with a dot or a dash",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = [],
)
