package nl.rug.digitallab.themis.provisioning.image.builders

import nl.rug.digitallab.themis.provisioning.exceptions.BuildahException
import nl.rug.digitallab.themis.provisioning.image.ImageRegistry
import org.eclipse.microprofile.config.ConfigProvider
import java.nio.file.Path
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

/**
 * The [ImageBuilder] class defines the functionalities that a container image builder should possess.
 */
interface ImageBuilder {

    companion object {
        /**
         * The default timeout for image builder operations.
         */
        val defaultTimeout = 10.seconds

        /**
         * Get a specific timeout from the configuration.
         *
         * @param timeoutName The name of the timeout in the configuration, is appended to the path "digital-lab.provisioning".
         *
         * @return The timeout.
         */
        fun getTimeout(timeoutName: String): Duration =
            ConfigProvider.getConfig().getValue("digital-lab.provisioning.$timeoutName", Duration::class.java)

        /**
         * The name of an image, given a repository and a tag.
         *
         * @param name The name of the image.
         * @param tag The tag of the image.
         *
         * @return The name of the image.
         */
        fun imageName(name: String, tag: String) = "$name:$tag"
    }

    /**
     * Builds a container image specified by the arguments.
     *
     * @param dockerfile The dockerfile that specifies the build instructions.
     * @param name The name of the image.
     * @param tag The tag of the image.
     * @param contextLocation The location of the build context.
     * @param imageRegistry The information (address, credentials, etc.) associated with this registry.
     * @param timeout The time the build operation can run before a timeout.
     *
     * @return The result of the build operation.
     *
     * @throws BuildahException When there is an error with the build.
     */
    suspend fun build(
        dockerfile: Path,
        name: String,
        tag: String,
        contextLocation: Path,
        imageRegistry: ImageRegistry,
        timeout: Duration = getTimeout("image-build-timeout"),
    ): CommandResult

    /**
     * Pushes a container image specified by the arguments.
     *
     * @param name The name of the image.
     * @param tag The tag of the image.
     * @param imageRegistry The information (address, credentials, etc.) associated with this registry.
     * @param timeout The time the push operation can run before a timeout.
     *
     * @return The result of the push operation.
     *
     * @throws BuildahException When there is an error with the push.
     */
    suspend fun push(
        name: String,
        tag: String,
        imageRegistry: ImageRegistry,
        timeout: Duration = getTimeout("image-push-timeout"),
    ): CommandResult

    /**
     * Lists the images available on the system.
     *
     * @param timeout The time the list operation can run before a timeout.
     *
     * @return The result of the list operation.
     */
    suspend fun listImages(
        timeout: Duration = defaultTimeout,
    ): ListImageResult

    /**
     * Removes an image from the system.
     *
     * @param name The name of the image.
     * @param tag The tag of the image.
     * @param timeout The time the remove operation can run before a timeout.
     *
     * @return The result of the remove operation.
     */
    suspend fun removeImage(
        name: String,
        tag: String,
        timeout: Duration = defaultTimeout,
    ): CommandResult

    /**
     * The result of an image builder operation.
     *
     * @param stdOut The standard output of the operation.
     * @param stdError The standard error of the operation.
     * @param exitCode The exit code of the operation.
     */
    data class CommandResult(
        val stdOut: String,
        val stdError: String,
        val exitCode: Int,
    )

    /**
     * The result of listing images.
     *
     * @param images The list of images.
     */
    data class ListImageResult(val images: List<String>)
}
