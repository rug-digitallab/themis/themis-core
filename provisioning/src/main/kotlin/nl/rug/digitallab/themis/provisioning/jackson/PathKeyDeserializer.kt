package nl.rug.digitallab.themis.provisioning.jackson

import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.module.SimpleModule
import io.quarkus.jackson.ObjectMapperCustomizer
import jakarta.inject.Singleton
import java.nio.file.Path
import java.nio.file.Paths

/**
 * A key deserializer for [Path].
 */
class PathKeyDeserializer : KeyDeserializer() {
    override fun deserializeKey(key: String, ctxt: DeserializationContext): Path {
        return Paths.get(key)
    }
}

/**
 * Registers the key deserializer for [Path]: [PathKeyDeserializer].
 */
@Singleton
class RegisterPathKeyDeserializer : ObjectMapperCustomizer {
    override fun customize(objectMapper: ObjectMapper) {
        objectMapper.registerModules(SimpleModule().addKeyDeserializer(Path::class.java,PathKeyDeserializer()))
    }
}
