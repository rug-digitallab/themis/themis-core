package nl.rug.digitallab.themis.provisioning.workers

import jakarta.annotation.PostConstruct
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import org.jboss.logging.Logger

/**
 * The [CoroutineBackgroundWorker] is responsible for executing tasks in the background using coroutines. It executes
 * tasks in a FIFO manner and will not start a new task until the previous one has finished.
 */
@ApplicationScoped
class CoroutineBackgroundWorker {
    @Inject
    private lateinit var logger: Logger

    private val jobChannel = Channel<suspend () -> Unit>(capacity = Channel.UNLIMITED)

    @PostConstruct
    fun init() {
        val scope = CoroutineScope(SupervisorJob() + Dispatchers.IO)

        scope.launch {
            for (job in jobChannel) {
                try {
                    job()
                } catch (e: Exception) {
                    logger.error("Error executing background task", e)
                }
            }
        }
    }

    /**
     * Enqueues a task to be executed in the background.
     *
     * @param task The task to be executed.
     *
     * @throws IllegalStateException When the task cannot be enqueued.
     */
    fun enqueueTask(task: suspend () -> Unit) {
        jobChannel.trySend(task).getOrThrow()
    }

}
