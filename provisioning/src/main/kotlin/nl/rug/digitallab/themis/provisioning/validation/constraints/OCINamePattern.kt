package nl.rug.digitallab.themis.provisioning.validation.constraints

import jakarta.validation.Constraint
import jakarta.validation.Payload
import jakarta.validation.constraints.Pattern
import kotlin.annotation.AnnotationTarget.*
import kotlin.reflect.KClass

/**
 * A constraint annotation that validates a string to be a valid name for an OCI image.
 * https://github.com/opencontainers/distribution-spec/blob/main/spec.md#pull. The regexp
 * associated with this pattern can be interpreted as follows:
 *
 * <valid-string>    ::= [a-z0-9]+
 * <separator>      ::= (\.|_|__|-+)
 * <name-part>      ::= <valid-string>(<separator><valid-string>)*
 * <regexp>         ::= <name-part>(/<name-part>)*
 *
 * That is, a name-part, optionally followed by an unbounded number of name-parts. A name-part
 * consist of a string using valid characters, which can be separated up. Valid separators are
 * a dot, one or two underscores, or one or more dashes.
 *
 * @property message The message to return when the constraint is violated.
 * @property groups The groups the constraint belongs to.
 * @property payload The payload associated with this constraint.
 *
 */
@Pattern(
    regexp = "[a-z0-9]+((\\.|_|__|-+)[a-z0-9]+)*(/[a-z0-9]+((\\.|_|__|-+)[a-z0-9]+)*)*",
    message = "May only contain lower case letters or digits, separated by /,.,_,__, or any number of dashes",
)
@Target(FIELD, PROPERTY, VALUE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@Constraint(validatedBy = [])
annotation class OCINamePattern(
    val message: String = "May only contain lower case letters or digits, separated by /,.,_,__, or any number of dashes",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = [],
)
