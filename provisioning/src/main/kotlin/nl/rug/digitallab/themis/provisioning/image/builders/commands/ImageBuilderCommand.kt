package nl.rug.digitallab.themis.provisioning.image.builders.commands

/**
 * A generic command that can be translated into a command list that consists of the command and its arguments.
 *
 * @param T The type of the result of running the command.
 */
interface ImageBuilderCommand<T> {
    /**
     * Create the command list for this command.
     *
     * @return The command list consisting of the command and its arguments.
     */
    fun commandList(): List<String>

    /**
     * Parse the result of running the command.
     *
     * @param stdOut The standard output of the command.
     * @param stdError The standard error of the command.
     * @param exitCode The exit code of the command.
     *
     * @return The parsed result.
     */
    fun parseResult(stdOut: String, stdError: String, exitCode: Int): T
}
