package nl.rug.digitallab.themis.provisioning.image.builders.commands.buildah

import nl.rug.digitallab.themis.provisioning.image.builders.ImageBuilder
import java.nio.file.Path
import kotlin.io.path.absolute

/**
 * Builds a container image using Buildah.
 *
 * @param dockerfile The path to the dockerfile that specifies the build instructions.
 * @param name The name of the image.
 * @param tag The tag of the image.
 * @param contextLocation The location of the build context.
 * @param cacheLayers Whether to cache intermediate layers or not.
 * @param cacheRemote Whether to use a remote cache or not.
 */
data class BuildCommand(
    val dockerfile: Path,
    val name: String,
    val tag: String,
    val contextLocation: Path,
    val cacheRemote: Boolean = true,
    val cacheLayers: Boolean = true,
): BuildahCommand<ImageBuilder.CommandResult>() {
    override fun commandList(): List<String> =
        buildList {
            add(EXECUTABLE)
            add("bud")
            add("-f")
            add(dockerfile.absolute().toString())
            if(cacheLayers) add("--layers")
            if(cacheRemote) addAll(remoteCacheArguments(name))
            if(imageRegistry != null) addAll(registryArguments())
            add("-t")
            add(ImageBuilder.imageName(name, tag))
            add(contextLocation.absolute().toString())
        }

    override fun parseResult(
        stdOut: String,
        stdError: String,
        exitCode: Int,
    ): ImageBuilder.CommandResult = ImageBuilder.CommandResult(stdOut, stdError, exitCode)
}
