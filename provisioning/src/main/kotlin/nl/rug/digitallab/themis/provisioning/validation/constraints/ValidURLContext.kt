package nl.rug.digitallab.themis.provisioning.validation.constraints

import jakarta.validation.*
import java.net.URL
import kotlin.annotation.AnnotationTarget.*
import kotlin.reflect.KClass

/**
 * URL Contexts (Map<Path, URL>) should only contain URLs with the http or https protocol.
 *
 * @property message The message to return when the constraint is violated.
 * @property groups The groups the constraint belongs to.
 * @property payload The payload associated with this constraint.
 */
@Target(FIELD, PROPERTY, VALUE_PARAMETER, TYPE)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@Constraint(validatedBy = [URLContextValidator::class])
annotation class ValidURLContext(
    val message: String = "The URL needs to use the http or https protocol",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = [],
)

class URLContextValidator : ConstraintValidator<ValidURLContext, Map<java.nio.file.Path, URL>> {
    override fun isValid(value: Map<java.nio.file.Path, URL>?, constraintValidator: ConstraintValidatorContext?): Boolean {
        if (value == null) {
            return true
        }

        return value.all { (_, url) ->
            url.protocol in setOf("http", "https")
        }
    }
}
