package nl.rug.digitallab.themis.provisioning.exceptions

import nl.rug.digitallab.common.quarkus.exception.mapper.rest.MappedException
import org.jboss.resteasy.reactive.RestResponse.Status

/**
 * The exception class to be thrown when an error occurs as the build context for an image is being populated.
 *
 * @param message Informational message that describes the exception.
 * @param cause Information from the underlying exception that caused the error.
 */
class InvalidContextException(message: String, cause: Throwable? = null) :
    MappedException(message, Status.INTERNAL_SERVER_ERROR, cause)
