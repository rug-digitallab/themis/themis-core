package nl.rug.digitallab.themis.provisioning.image.builders

import io.quarkus.test.common.WithTestResource
import io.quarkus.test.junit.QuarkusTest
import io.smallrye.config.ConfigMapping
import io.smallrye.config.WithConverter
import io.smallrye.config.WithName
import jakarta.inject.Inject
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.common.kotlin.quantities.integrations.smallrye.config.DurationLongConverter
import nl.rug.digitallab.themis.provisioning.configs.ProvisioningConfig
import nl.rug.digitallab.themis.provisioning.exceptions.BuildahException
import nl.rug.digitallab.themis.provisioning.exceptions.ProcessExecutionException
import nl.rug.digitallab.themis.provisioning.image.ImageRegistry
import nl.rug.digitallab.themis.provisioning.image.builders.commands.buildah.BuildCommand
import nl.rug.digitallab.themis.provisioning.util.BuildahUtils
import nl.rug.digitallab.themis.provisioning.util.ImageRegistryTestResource
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.condition.DisabledOnOs
import org.junit.jupiter.api.condition.OS
import kotlin.jvm.optionals.getOrNull
import kotlin.time.Duration
import kotlin.time.Duration.Companion.nanoseconds
import kotlin.time.Duration.Companion.seconds

@QuarkusTest
@DisabledOnOs(OS.WINDOWS) // Buildah does not work on Windows
@WithTestResource(ImageRegistryTestResource::class)
class BuildahTest {
    @Inject
    private lateinit var config: ProvisioningConfig

    @Inject
    private lateinit var buildah: Buildah

    @Inject
    private lateinit var buildahUtils: BuildahUtils

    @Inject
    private lateinit var testConfig: ProvisioningTestConfig

    private val sleepingDockerfile = getResource("dockerfiles/Sleep15").asPath()

    private val dockerfile = getResource("contexts/Dockerfile").asPath()
    private val validContextLocation = getResource("contexts/valid").asPath()
    private val invalidContextLocation = getResource("contexts/invalid").asPath()

    @AfterEach
    fun cleanUpImages() = buildahUtils.removeImages()

    @Test
    fun `A valid build context should create a local image`() = runBlocking {
        val imageName = "buildah-test"
        val imageTag = "valid"

        buildah.build(
            dockerfile,
            imageName,
            imageTag,
            validContextLocation,
            ImageRegistry(
                config.registryAddress,
                config.registryIsSecure,
                config.registryUsername.getOrNull(),
                config.registryPassword.getOrNull(),
            ),
            testConfig.defaultTimeout,
        )
        buildahUtils.registerToRemove(imageName, imageTag)
    }

    @Test
    fun `Removing an image should succeed`() = runBlocking {
        val imageName = "buildah-test"
        val imageTag = "remove-test"

        buildah.build(
            dockerfile,
            imageName,
            imageTag,
            validContextLocation,
            ImageRegistry(
                config.registryAddress,
                config.registryIsSecure,
                config.registryUsername.getOrNull(),
                config.registryPassword.getOrNull(),
            ),
            testConfig.defaultTimeout,
        )

        assertTrue(buildahUtils.imageExists(imageName, imageTag))
        buildah.removeImage(imageName, imageTag, testConfig.defaultTimeout)
        assertFalse(buildahUtils.imageExists(imageName, imageTag))
    }

    @Test
    fun `Removing a non-existent image should fail`() {
        val imageName = "buildah-test"
        val imageTag = "remove-non-existent"

        val thrown = assertThrows(BuildahException::class.java) {
            runBlocking { buildah.removeImage(imageName, imageTag, testConfig.defaultTimeout) }
        }

        assertInstanceOf(ProcessExecutionException::class.java, thrown.cause)
    }

    @Test
    fun `An invalid build context should result in an InvalidBuildException`() {
        val imageName = "buildah-test"
        val imageTag = "invalid"

        val thrown = assertThrows(BuildahException::class.java) {
            runBlocking {
                buildah.build(
                    dockerfile,
                    imageName,
                    imageTag,
                    invalidContextLocation,
                    ImageRegistry(
                        config.registryAddress,
                        config.registryIsSecure,
                        config.registryUsername.getOrNull(),
                        config.registryPassword.getOrNull(),
                    ),
                    testConfig.defaultTimeout,
                )
            }
        }
        assertFalse(buildahUtils.imageExists(imageName, imageTag))
        assertInstanceOf(ProcessExecutionException::class.java, thrown.cause)
    }

    @Test
    fun `Pushing an existing image to an existing registry should work`(): Unit = runBlocking {
        val imageName = "buildah-test"
        val imageTag = "push-test"

        buildah.build(
            dockerfile,
            imageName,
            imageTag,
            validContextLocation,
            ImageRegistry(
                config.registryAddress,
                config.registryIsSecure,
                config.registryUsername.getOrNull(),
                config.registryPassword.getOrNull(),
            ),
            testConfig.defaultTimeout,
        )
        buildahUtils.registerToRemove(imageName, imageTag)

        buildah.push(
            imageName,
            imageTag,
            ImageRegistry(
                config.registryAddress,
                config.registryIsSecure,
                config.registryUsername.getOrNull(),
                config.registryPassword.getOrNull(),
            ),
            testConfig.defaultTimeout,
        )
    }

    @Test
    fun `Pushing an image that does not exist should result in an InvalidPushException`() {
        val imageName = "buildah-test"
        val imageTag = "bad-push"

        val thrown = assertThrows(BuildahException::class.java) {
            runBlocking {
                buildah.push(
                    imageName,
                    imageTag,
                    ImageRegistry(
                        config.registryAddress,
                        config.registryIsSecure,
                        config.registryUsername.getOrNull(),
                        config.registryPassword.getOrNull(),
                    ),
                    testConfig.defaultTimeout,
                )
            }
        }

        assertFalse(buildahUtils.imageExists(imageName, imageTag))
        assertInstanceOf(ProcessExecutionException::class.java, thrown.cause)
    }

    @Test
    fun `A build that takes too long should throw an InvalidBuildException`() {
        val imageName = "buildah-test"
        val imageTag = "build-too-long"

        buildahUtils.removeImages()

        assertFalse(buildahUtils.imageExists(imageName, imageTag))
        val thrown = assertThrows(BuildahException::class.java) {
            runBlocking {
                buildah.build(
                    sleepingDockerfile,
                    imageName,
                    imageTag,
                    validContextLocation,
                    ImageRegistry(
                        config.registryAddress,
                        config.registryIsSecure,
                        config.registryUsername.getOrNull(),
                        config.registryPassword.getOrNull(),
                    ),
                    1.nanoseconds,
                )
            }
        }

        assertFalse(buildahUtils.imageExists(imageName, imageTag))
        assertInstanceOf(TimeoutCancellationException::class.java, thrown.cause)
    }

    @Test
    fun `A push that takes too long should throw an InvalidPushException`() {
        val imageName = "buildah-test"
        val imageTag = "push-too-long"

        val thrown = assertThrows(BuildahException::class.java) {
            runBlocking {
                buildah.build(
                    dockerfile,
                    imageName,
                    imageTag,
                    /**
                     * Note that any context location could be used, as the sleeping dockerfile does not need anything
                     * from its context
                     */
                    validContextLocation,
                    ImageRegistry(
                        config.registryAddress,
                        config.registryIsSecure,
                        config.registryUsername.getOrNull(),
                        config.registryPassword.getOrNull(),
                    ),
                    testConfig.defaultTimeout,
                )
                buildahUtils.registerToRemove(imageName, imageTag)

                buildah.push(
                    imageName,
                    imageTag,
                    ImageRegistry(
                        config.registryAddress,
                        config.registryIsSecure,
                        config.registryUsername.getOrNull(),
                        config.registryPassword.getOrNull(),
                    ),
                    1.nanoseconds,
                )
            }
        }

        assertInstanceOf(TimeoutCancellationException::class.java, thrown.cause)
    }

    @Test
    fun `Buildah caches image layers during build`() {
        val imageName = "buildah-test"
        val imageTag = "cache"

        runBlocking {
            buildah.build(
                sleepingDockerfile,
                imageName,
                imageTag,
                validContextLocation,
                ImageRegistry(
                    config.registryAddress,
                    config.registryIsSecure,
                    config.registryUsername.getOrNull(),
                    config.registryPassword.getOrNull(),
                ),
                testConfig.defaultTimeout,
            )
        }

        runBlocking {
            buildah.build(
                sleepingDockerfile,
                imageName,
                imageTag,
                validContextLocation,
                ImageRegistry(
                    config.registryAddress,
                    config.registryIsSecure,
                    config.registryUsername.getOrNull(),
                    config.registryPassword.getOrNull(),
                ),
                // Shorter than RUN sleep 15 inside sleepingDockerfile, but long enough to still build image
                5.seconds,
            )
        }

        buildahUtils.registerToRemove(imageName, imageTag)
    }

    @Test
    fun `Building a complex image should succeed`() {
        val imageName = "buildah-test"
        val imageTag = "complex"

        val buildWithoutCache = BuildCommand(
            dockerfile = getResource("dockerfiles/Complex").asPath(),
            name = imageName,
            tag = imageTag,
            contextLocation = validContextLocation,
            cacheLayers = false,
            cacheRemote = false,
        )

        val result = runBlocking {
            buildah.executeBuildah(
                buildWithoutCache,
                testConfig.defaultTimeout,
            )
        }

        buildahUtils.registerToRemove(imageName, imageTag)

        val lineCounts = setOf(
            "Connecting to RNS server at localhost:13370" to 2,
            "Connected to RNS server at localhost:13370" to 2,
            "Found rug at 129.125.2.51" to 1,
            "Found foo at 1.1.1.1" to 1,
        )

        val combinedLog = buildString {
            append(result.stdOut)
            appendLine()
            append(result.stdError)
        }

        lineCounts.forEach { (line, count) ->
            assertEquals(count, combinedLog.lines().count { it.contains(line) }) {
                "Expected $count occurrences of '$line'"
            }
        }

    }
}

/**
 * The [ProvisioningConfig] holds configuration data about the provisioning service.
 */
@ConfigMapping(prefix = "digital-lab.provisioning.test")
interface ProvisioningTestConfig {
    /**
     * The maximum time the service should wait before timing out, when downloading a file from the object store
     * or URL.
     */
    @get:WithName("default-timeout")
    @get:WithConverter(DurationLongConverter::class)
    val defaultTimeout: Duration
}