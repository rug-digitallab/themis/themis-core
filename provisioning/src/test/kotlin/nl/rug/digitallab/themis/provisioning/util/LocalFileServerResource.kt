package nl.rug.digitallab.themis.provisioning.util

import jakarta.ws.rs.GET
import jakarta.ws.rs.Path
import jakarta.ws.rs.Produces
import jakarta.ws.rs.core.MediaType
import nl.rug.digitallab.common.kotlin.helpers.resource.asFile
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import org.jboss.resteasy.reactive.RestPath
import java.io.File
import kotlin.io.path.Path

/**
 * A test resource to serve local files
 */
@Path("test-resource")
class LocalFileServerResource {
    @GET
    @Path("plaintexts/{fileName}")
    @Produces(MediaType.TEXT_PLAIN)
    fun getPlaintextFile(@RestPath fileName: String): File {
        return getResource(Path("plaintexts", fileName)).asFile()
    }
}
