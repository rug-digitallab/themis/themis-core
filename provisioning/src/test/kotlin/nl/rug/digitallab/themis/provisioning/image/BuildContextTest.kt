package nl.rug.digitallab.themis.provisioning.image

import io.minio.errors.ErrorResponseException
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.mockito.InjectSpy
import jakarta.inject.Inject
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.withEphemeralDirectory
import nl.rug.digitallab.common.kotlin.helpers.resource.asFile
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.common.quarkus.archiver.ArchiverManager
import nl.rug.digitallab.themis.provisioning.exceptions.InvalidContextException
import nl.rug.digitallab.themis.provisioning.namespaces.ObjectNamespace
import nl.rug.digitallab.themis.provisioning.persistence.repositories.ObjectRepository
import nl.rug.digitallab.themis.provisioning.util.MinioUtils
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.internal.stubbing.answers.AnswersWithDelay
import org.mockito.internal.stubbing.answers.CallsRealMethods
import org.mockito.kotlin.any
import org.mockito.kotlin.spy
import org.mockito.kotlin.whenever
import java.net.SocketException
import java.net.URI
import java.nio.file.NoSuchFileException
import java.nio.file.Path
import java.util.zip.ZipException
import kotlin.io.path.Path
import kotlin.io.path.readBytes
import kotlin.time.Duration
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

@QuarkusTest
class BuildContextTest {
    @InjectSpy // Injected as spy so we can add a delay to the download method to test timeouts
    private lateinit var objectStore: ObjectRepository

    @Inject
    private lateinit var archiveManager: ArchiverManager

    @Inject
    private lateinit var minioUtils: MinioUtils

    private val validZip = getResource("zips/valid.zip").asFile()
    private val invalidZip = getResource("zips/invalid.zip").asFile()
    private val plaintext1 = getResource("plaintexts/file1").asFile()

    @Test
    fun `An invalid zip should raise an InvalidContextException with cause ZipException`() =
        testFailAddingFile(ZipException::class.java) {
            addFromArchive(invalidZip)
        }

    @Test
    fun `An invalid path to URL mapping should raise an InvalidContextException with cause SocketException`() =
        testFailAddingFile(SocketException::class.java) {
            addFromURL(URI("http://localhost:0").toURL(), Path("wontexist"))
        }

    @Test
    fun `An invalid path to object store reference mapping should raise an InvalidContextException with cause ErrorResponseException`() =
        testFailAddingFile(ErrorResponseException::class.java) {
            addFromObjectStore(ObjectNamespace("doest", "exist"), Path("wontexist"))
        }

    @Test
    fun `An invalid path to a file should raise an InvalidContextException with cause NoSuchFileException`() =
        testFailAddingFile(NoSuchFileException::class.java) {
            addFromFile(Path("doesnt/exist"), Path("wontexist"))
        }

    @Test
    fun `A valid zip should be unarchived into the build context without being flattened`() =
        injectedBuildContextScope {
            addFromArchive(validZip)

            assertFileContentsIsSame(Path("file1.txt"), "This file is in the root.")
            assertFileContentsIsSame(Path("nonRoot/file2.txt"), "This is a file inside a nonroot folder.")
        }

    @Test
    fun `A valid zip can be unzipped to within a directory`() = injectedBuildContextScope {
        addFromArchive(validZip, Path("nested"))

        assertFileContentsIsSame(Path("nested/file1.txt"), "This file is in the root.")
        assertFileContentsIsSame(Path("nested/nonRoot/file2.txt"), "This is a file inside a nonroot folder.")
    }

    @Test
    fun `A valid URL should be downloaded into the build context`() = runBlocking {
        injectedBuildContextScope {
            val downloadTo = Path("downloaded")
            addFromURL(URI("http://localhost:8272/test-resource/plaintexts/file1").toURL(), downloadTo)

            assertFileContentsIsSame(downloadTo, plaintext1.toPath())
        }
    }

    @Test
    fun `A valid URL that takes too long to download should raise an InvalidContextException with cause TimeoutCancellationException`() =
        testFailAddingFile(TimeoutCancellationException::class.java, 1.seconds) {
            val downloadTo = Path("downloaded")
            val url = spy(URI("http://localhost:8272/test-resource/plaintexts/file1").toURL())

            // Delay the openStream method by 2 seconds
            Mockito
                .doAnswer(AnswersWithDelay(2.seconds.inWholeMilliseconds, CallsRealMethods()))
                .whenever(url).openStream()

            addFromURL(url, downloadTo)
        }

    @Test
    fun `A valid path to the object store should be downloaded into the build context`() = runBlocking {
        val uploadedNamespace = ObjectNamespace("bucket", "object")
        minioUtils.uploadObject(uploadedNamespace, plaintext1.toPath())

        injectedBuildContextScope {
            val downloadTo = Path("downloaded")
            addFromObjectStore(uploadedNamespace, downloadTo)

            assertFileContentsIsSame(downloadTo, plaintext1.toPath())
        }
    }

    @Test
    fun `A valid path to the object store that takes too long to download should raise an InvalidContextException with cause TimeoutCancellationException`() {
        val uploadedNamespace = ObjectNamespace("bucket", "object")
        minioUtils.uploadObject(uploadedNamespace, plaintext1.toPath())

        // Delay the downloadObject method by 2 seconds
        Mockito
            .doAnswer(AnswersWithDelay(2.seconds.inWholeMilliseconds, CallsRealMethods()))
            .whenever(objectStore).downloadObject(any())

        testFailAddingFile(TimeoutCancellationException::class.java, 1.seconds) {
            val downloadTo = Path("downloaded")
            addFromObjectStore(uploadedNamespace, downloadTo)
        }
    }

    @Test
    fun `A valid path to a file should copy it into the build context`() = injectedBuildContextScope {
        val copiedTo = Path("copied")
        addFromFile(plaintext1.toPath(), copiedTo)

        assertFileContentsIsSame(copiedTo, plaintext1.toPath())
    }

    /**
     * Helper function to run [block] within a [BuildContext] that uses an injected [ArchiverManager] and [ObjectRepository].
     *
     * @param timeout The maximum time a download can take
     * @param block The block of code to run
     */
    private inline fun injectedBuildContextScope(
        timeout: Duration = 60.seconds,
        block: BuildContext.() -> Unit,
    ) =
        withEphemeralDirectory { directory ->
            with(BuildContext(archiveManager, objectStore, timeout, directory)) {
                block()
            }
        }

    /**
     * Helper function for testing adding file failures.
     *
     * @param expectedCause The expected cause for adding to fail.
     * @param addFile The block of code to run that tries to add a file.
     */
    private fun <S : Throwable> testFailAddingFile(
        expectedCause: Class<S>,
        timeout: Duration = 5.minutes,
        addFile: suspend BuildContext.() -> Unit,
    ) {
        val thrown = assertThrows(InvalidContextException::class.java) {
            runBlocking {
                injectedBuildContextScope(timeout) {
                    addFile()
                }
            }
        }

        assertInstanceOf(expectedCause, thrown.cause)
    }

    /**
     * Helper function for testing contents of a file.
     *
     * @param file The [Path] to the file we are checking the content of. Relative to the build context folder
     * @param expected The [Path] to a file that contains the expected content
     */
    private fun BuildContext.assertFileContentsIsSame(file: Path, expected: Path) {
        val fileContent = buildDir.resolve(file).readBytes()
        val expectedContent = expected.toFile().readBytes()
        assertArrayEquals(expectedContent, fileContent)
    }

    /**
     * Helper function for testing contents of a file that contains plaintext.
     *
     * @param file The [Path] to the file we are checking the content of. Relative to the build context folder.
     * @param expectedContent The expected content of the file.
     */
    private fun BuildContext.assertFileContentsIsSame(file: Path, expectedContent: String) {
        val fileContent = buildDir.resolve(file).toFile().readText()
        assertEquals(expectedContent, fileContent)
    }
}
