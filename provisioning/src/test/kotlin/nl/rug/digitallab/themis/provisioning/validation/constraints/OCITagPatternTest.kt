package nl.rug.digitallab.themis.provisioning.validation.constraints

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import jakarta.validation.Validation
import jakarta.validation.Validator
import nl.rug.digitallab.themis.provisioning.util.assertHasViolation
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

@QuarkusTest
class OCITagPatternTest {
    @Inject
    private lateinit var validator: Validator

    @TestFactory
    fun `An @OCITagPattern annotated field should be invalid if it contains invalid characters`(): List<DynamicTest> {
        return invalidCharacters.map {
            DynamicTest.dynamicTest("An @OCITagPattern annotated field containing '$it' should be invalid") {
                val invalidString = "inv${it}lid"
                val invalidDTO = OCITagPatternTestClass(invalidString)

                validator.assertHasViolation(
                    invalidDTO,
                    invalidString,
                    "someString",
                    "May only contain upto 128 letters, numbers, dots, underscores and dashes and at least one. Cannot start with a dot or a dash",
                )
            }
        }
    }

    @TestFactory
    fun `An @OCITagPattern annotated field should be invalid if it starts incorrectly`(): List<DynamicTest> {
        return invalidStartingCharacters.map {
            DynamicTest.dynamicTest("An @OCITagPattern annotated field starting with '$it' should be invalid") {
                val invalidString = "${it}invalid"
                val invalidDTO = OCITagPatternTestClass(invalidString)

                validator.assertHasViolation(
                    invalidDTO,
                    invalidString,
                    "someString",
                    "May only contain upto 128 letters, numbers, dots, underscores and dashes and at least one. Cannot start with a dot or a dash",
                )
            }
        }
    }

    @TestFactory
    fun `An @OCITagPattern annotated field should be invalid if it's length is disallowed`(): List<DynamicTest> {
       return invalidLengths.map {
           DynamicTest.dynamicTest("An @OCITagPattern annotated field of lenght $it should be invalid") {
               val invalidString = "a".repeat(it)
               val invalidDTO = OCITagPatternTestClass(invalidString)

               validator.assertHasViolation(
                   invalidDTO,
                   invalidString,
                   "someString",
                   "May only contain upto 128 letters, numbers, dots, underscores and dashes and at least one. Cannot start with a dot or a dash",
               )
           }
        }
    }

    @TestFactory
    fun `An @OCITagPattern annotated field should be valid when using valid names`(): List<DynamicTest> {
        val validator = Validation.buildDefaultValidatorFactory().validator
        return validTags.map {
            DynamicTest.dynamicTest("An @OCITagPattern annotated field containing '$it' should be valid") {
                val testDTO = OCITagPatternTestClass(it)

                val violations = validator.validate(testDTO)
                assertEquals(0, violations.size)
            }
        }
    }

    private val invalidCharacters = listOf(
        '~', '`', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '+', '=', '{', '[', '}', ']', '|', '\\',
        ':', ';', '\'', '"', '<', ',', '>', '?', '/'
    )

    private val invalidStartingCharacters = listOf('.', '-')

    private val invalidLengths = listOf(0, 129, 400)

    private val validTags = listOf(
        "a0123asd",
        "0df21",
        "_asdasd",
        "asdas-asda.959_xcv0123"
    )

    data class OCITagPatternTestClass(
        @field:OCITagPattern
        var someString: String,
    )
}
