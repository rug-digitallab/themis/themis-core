package nl.rug.digitallab.themis.provisioning.util

import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.inject.Singleton
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.themis.provisioning.image.builders.Buildah
import nl.rug.digitallab.themis.provisioning.image.builders.ImageBuilder
import kotlin.time.Duration.Companion.seconds

@Singleton
class BuildahUtils: ImageBuilderUtils {
    @Inject
    private lateinit var buildah: Buildah

    /**
     * The set of images to remove, with each image encoded as a pair of its name and tag.
     */
    private val imagesToRemove: MutableSet<Pair<String, String>> = mutableSetOf()

    override fun registerToRemove(imageName: String, imageTag: String) {
        imagesToRemove.add(imageName to imageTag)
    }

    override fun removeImages(ignoreExceptions: Boolean) {
        imagesToRemove.forEach { (name, tag) ->
            try {
                runBlocking { buildah.removeImage(name, tag, 10.seconds) }
            } catch (e: Exception) {
                if (!ignoreExceptions) throw e
            }
        }
        imagesToRemove.clear()
    }

    override fun numberOfImages() = getAllImages().size

    override fun imageExists(imageName: String, imageTag: String): Boolean =
        getAllImages().contains("localhost/${ImageBuilder.imageName(imageName, imageTag)}")

    private fun getAllImages(): List<String> = runBlocking {
        buildah.listImages(10.seconds).images
    }
}
