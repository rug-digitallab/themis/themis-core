package nl.rug.digitallab.themis.provisioning.persistence.repositories

import com.kamelia.sprinkler.util.closeableScope
import io.minio.errors.ErrorResponseException
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.withEphemeralFile
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.themis.provisioning.namespaces.ObjectNamespace
import nl.rug.digitallab.themis.provisioning.util.MinioUtils
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import kotlin.io.path.outputStream
import kotlin.io.path.readBytes

@QuarkusTest
class ObjectRepositoryTest {
    @Inject
    private lateinit var objectRepository: ObjectRepository

    @Inject
    private lateinit var minioUtils: MinioUtils

    @Test
    fun `Downloading an existing object should return the correct file`() = closeableScope {
        val objectNamespace = ObjectNamespace("bucket", "path/in/bucket")
        val uploadedFile = getResource("dockerfiles/Base").asPath()

        // Upload file
        minioUtils.uploadObject(objectNamespace, uploadedFile)

        // Get file in repository
        val downloadInputStream = using(objectRepository.downloadObject(objectNamespace))
        withEphemeralFile { downloadedFile ->
            val downloadedFileStream = using(downloadedFile.outputStream())
            downloadInputStream.copyTo(downloadedFileStream)

            // Compare downloaded data vs. uploaded data
            val downloadedFileData = downloadedFile.readBytes()

            val uploadedFileData = uploadedFile.readBytes()
            assertEquals(uploadedFileData.size, downloadedFileData.size)
            assertArrayEquals(uploadedFileData, downloadedFileData)
        }
    }

    @Test
    fun `Downloading a non-existing object should result in an ErrorResponseException`() {
        assertThrows(ErrorResponseException::class.java) {
            objectRepository.downloadObject(ObjectNamespace("doesnt", "exist"))
        }
    }
}
