package nl.rug.digitallab.themis.provisioning.managers

import com.kamelia.sprinkler.util.closeableScope
import io.minio.errors.ErrorResponseException
import io.quarkus.test.common.WithTestResource
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.QuarkusTestProfile
import io.quarkus.test.junit.TestProfile
import jakarta.inject.Inject
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import nl.rug.digitallab.common.kotlin.helpers.resource.asFile
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.common.quarkus.test.rest.MockFileUpload
import nl.rug.digitallab.themis.provisioning.dtos.BuildRequest
import nl.rug.digitallab.themis.provisioning.exceptions.BuildahException
import nl.rug.digitallab.themis.provisioning.exceptions.InvalidContextException
import nl.rug.digitallab.themis.provisioning.namespaces.ObjectNamespace
import nl.rug.digitallab.themis.provisioning.util.*
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertInstanceOf
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.condition.DisabledOnOs
import org.junit.jupiter.api.condition.OS
import java.io.File
import java.net.ConnectException
import java.net.URI
import java.net.URL
import java.nio.file.Path
import java.util.zip.ZipException
import kotlin.io.path.Path
import kotlin.time.Duration.Companion.seconds

@QuarkusTest
@DisabledOnOs(OS.WINDOWS) // Buildah does not work on Windows
@WithTestResource(ImageRegistryTestResource::class)
class ProvisioningManagerTest {
    @Inject
    private lateinit var manager: ProvisioningManager

    @Inject
    private lateinit var minioUtils: MinioUtils

    @Inject
    private lateinit var buildahUtils: BuildahUtils

    private val baseDockerfile = getResource("dockerfiles/Base").asFile()
    // Dockerfile that sleeps and will timeout
    private val sleepingFor15Dockerfile = getResource("dockerfiles/Sleep15").asFile()
    // Dockerfile that sleeps but does not timeout
    private val sleepingFor5Dockerfile = getResource("dockerfiles/Sleep5").asFile()
    private val invalidZip = getResource("zips/invalid.zip").asFile()
    private val dockerfileForValidZip = getResource("dockerfiles/ForValidZip").asFile()
    private val validZip = getResource("zips/valid.zip").asFile()
    private val badDockerfileZip = getResource("zips/badDockerfile.zip").asFile()

    private val plaintext1 = getResource("plaintexts/file1").asFile()
    private val plaintext2 = getResource("plaintexts/file2").asFile()

    /**
     * Images that do not exist will result in the buildah process removing the image exiting with an error, but
     * this does not cause any error for the test themselves / corrupt the registry.
     */
    @AfterEach
    fun cleanUpImages() = buildahUtils.removeImages()

    @Test
    fun `A simple image should be built correctly`() = runBlocking {
        closeableScope {
            val imageName = "simple"
            val imageTag = "latest"

            manager.createImageWithDefaults(imageName, imageTag, dockerfile = baseDockerfile)

            assertTrue(buildahUtils.imageExists(imageName, imageTag))
            buildahUtils.registerToRemove(imageName, imageTag)
        }
    }


    @Test
    fun `A simple image with just a Dockerfile should be built correctly`() = runBlocking {
        closeableScope {
            val imageName = "simple"
            val imageTag = "default-tag"

            manager.createImageWithDefaults(imageName, imageTag, baseDockerfile)

            assertTrue(buildahUtils.imageExists(imageName, imageTag))
            buildahUtils.registerToRemove(imageName, imageTag)
        }
    }

    @Test
    fun `A simple image with a Dockerfile and some build context specification via an archive should be built correctly`() =
        runBlocking {
            closeableScope {
                val imageName = "with-context"
                val imageTag = "archive"

                manager.createImageWithDefaults(imageName, imageTag, dockerfileForValidZip, contextFromArchive = validZip)

                assertTrue(buildahUtils.imageExists(imageName, imageTag))
                buildahUtils.registerToRemove(imageName, imageTag)
            }
        }

    @Test
    fun `A simple image with a Dockerfile and some build context specification via external files should be built correctly`() =
        runBlocking {
            closeableScope {
                val imageName = "with-context"
                val imageTag = "urls"

                manager.createImageWithDefaults(
                    imageName,
                    imageTag,
                    baseDockerfile,
                    contextFromURL = mapOf(
                        Path("file1") to URI("http://localhost:8272/test-resource/plaintexts/file1").toURL(),
                        Path("nested/file2") to URI("http://localhost:8272/test-resource/plaintexts/file1").toURL(),
                    )
                )

                assertTrue(buildahUtils.imageExists(imageName, imageTag))
                buildahUtils.registerToRemove(imageName, imageTag)
            }
        }

    @Test
    fun `A simple image with a Dockerfile and some build context specification via the object store should be built correctly`() =
        runBlocking {
            closeableScope {
                val imageName = "with-context"
                val imageTag = "object-store"

                val file1Namespace = ObjectNamespace("bucket", "file1")
                val file2Namespace = ObjectNamespace("bucket", "nonRoot/file2")
                minioUtils.uploadObject(file1Namespace, plaintext1.toPath())
                minioUtils.uploadObject(file2Namespace, plaintext2.toPath())

                manager.createImageWithDefaults(
                    imageName,
                    imageTag,
                    baseDockerfile,
                    contextFromObjectStore = mapOf(
                        Path("file1") to file1Namespace,
                        Path("nested/file2") to file2Namespace,
                    )
                )

                assertTrue(buildahUtils.imageExists(imageName, imageTag))
                buildahUtils.registerToRemove(imageName, imageTag)
            }
        }

    @Test
    fun `A simple image with a Dockerfile and a build context that places a Dockerfile into the root of the build context uses the explicitly supplied Dockerfile`() =
        runBlocking {
            closeableScope {
                val imageName = "with-context"
                val imageTag = "dockerfile-with-zip"

                manager.createImageWithDefaults(
                    imageName,
                    imageTag,
                    baseDockerfile,
                    contextFromArchive = badDockerfileZip,
                )

                assertTrue(buildahUtils.imageExists(imageName, imageTag))
                buildahUtils.registerToRemove(imageName, imageTag)
            }
        }

    @Test
    fun `Requesting rebuilding an image with the same name should take advantage of the image cache`() = runBlocking {
        closeableScope {
            val imageName = "caching"
            val firstImageTag = "tag-1"
            val secondImageTag = "tag-2"

            manager.createImageWithDefaults(
                imageName,
                firstImageTag,
                sleepingFor5Dockerfile,
            )

            /**
             * sleepingDockerfile should take 5 seconds to build, if cache is not present. So if caching works, then
             * we should not get a [TimeoutCancellationException].
             */
            withTimeout(4.seconds) {
                manager.createImageWithDefaults(
                    imageName,
                    secondImageTag,
                    sleepingFor5Dockerfile,
                )
            }

            assertTrue(buildahUtils.imageExists(imageName, firstImageTag))
            assertTrue(buildahUtils.imageExists(imageName, secondImageTag))
            buildahUtils.registerToRemove(imageName, firstImageTag)
            buildahUtils.registerToRemove(imageName, secondImageTag)
        }
    }

    @Test
    fun `Requesting a build that takes too long should throw an InvalidBuildException`(): Unit = runBlocking {
        closeableScope {
            val imageName = "timeout"
            val imageTag = "build"

            val thrown = assertThrows<BuildahException> {
                manager.createImageWithDefaults(
                    imageName,
                    imageTag,
                    sleepingFor15Dockerfile,
                )
            }

            assertFalse(buildahUtils.imageExists(imageName, imageTag))
            assertInstanceOf(TimeoutCancellationException::class.java, thrown.cause)
        }
    }

    @Test
    fun `A simple image with an invalid path to object store should thrown an InvalidContextException`(): Unit =
        runBlocking {
            closeableScope {
                val imageName = "bad-context"
                val imageTag = "object-store"

                val thrown = assertThrows<InvalidContextException> {
                    manager.createImageWithDefaults(
                        imageName,
                        imageTag,
                        baseDockerfile,
                        contextFromObjectStore = mapOf(
                            Path("broken") to ObjectNamespace("doesnt", "exist/here")
                        )
                    )
                }

                assertFalse(buildahUtils.imageExists(imageName, imageTag))
                assertInstanceOf(ErrorResponseException::class.java, thrown.cause)
            }
        }

    @Test
    fun `A simple image with an invalid archive should throw an InvalidContextException`(): Unit = runBlocking {
        closeableScope {
            val imageName = "bad-context"
            val imageTag = "archive"

            val thrown = assertThrows<InvalidContextException> {
                manager.createImageWithDefaults(
                    imageName,
                    imageTag,
                    baseDockerfile,
                    contextFromArchive = invalidZip
                )
            }

            assertFalse(buildahUtils.imageExists(imageName, imageTag))
            assertInstanceOf(ZipException::class.java, thrown.cause)
        }
    }

    @Test
    fun `A simple image with an unreachable URL to download should throw an InvalidContextException`(): Unit =
        runBlocking {
            closeableScope {
                val imageName = "bad-context"
                val imageTag = "urls"

                val thrown = assertThrows<InvalidContextException> {
                    manager.createImageWithDefaults(
                        imageName,
                        imageTag,
                        baseDockerfile,
                        contextFromURL = mapOf(
                            Path("broken") to URI("http://localhost:0").toURL()
                        )
                    )
                }

                assertFalse(buildahUtils.imageExists(imageName, imageTag))
                assertInstanceOf(ConnectException::class.java, thrown.cause)
            }
        }
}

@QuarkusTest
@DisabledOnOs(OS.WINDOWS) // Buildah does not work on Windows
@TestProfile(ProvisioningResourcePushTimeoutTest::class)
@WithTestResource(ImageRegistryTestResource::class)
class ProvisioningResourcePushTimeoutTest : QuarkusTestProfile {
    @Inject
    private lateinit var manager: ProvisioningManager

    @Inject
    private lateinit var buildahUtils: BuildahUtils

    private val baseDockerfile = getResource("dockerfiles/Base").asFile()

    /**
     * Images that do not exist will result in the buildah process removing the image exiting with an error, therefore
     * we ignore exceptions when removing images.
     */
    @AfterEach
    fun cleanUpImages() = buildahUtils.removeImages(ignoreExceptions = true)

    override fun getConfigOverrides(): MutableMap<String, String> {
        return mutableMapOf(
            "digital-lab.provisioning.image-push-timeout" to "0ns",
        )
    }

    @Test
    fun `Requesting a build that takes too long to push should throw an InvalidPushException`(): Unit = runBlocking {
        closeableScope {
            val imageName = "timeout"
            val imageTag = "push"

            val thrown = assertThrows<InvalidContextException> {
                manager.createImageWithDefaults(
                    imageName,
                    imageTag,
                    baseDockerfile,
                    contextFromURL = mapOf(
                        Path("broken") to URI("http://localhost:0").toURL()
                    )
                )
            }

            assertInstanceOf(ConnectException::class.java, thrown.cause)
            buildahUtils.registerToRemove(imageName, imageTag)
        }
    }
}

@QuarkusTest
@DisabledOnOs(OS.WINDOWS) // Buildah does not work on Windows
@TestProfile(ProvisioningDownloadTest::class)
class ProvisioningDownloadTest : QuarkusTestProfile {
    @Inject
    private lateinit var minioUtils: MinioUtils

    @Inject
    private lateinit var buildahUtils: BuildahUtils

    @Inject
    private lateinit var manager: ProvisioningManager

    private val baseDockerfile = getResource("dockerfiles/Base").asFile()

    private val plaintext1 = getResource("plaintexts/file1").asFile()

    override fun getConfigOverrides(): MutableMap<String, String> {
        return mutableMapOf(
            "digital-lab.provisioning.external-download-timeout" to "0ns",
        )
    }

    @Test
    fun `Requesting a build with a url that takes too long to download should thrown an InvalidContextException`() {
        runBlocking {
            closeableScope {
                val imageName = "timeout"
                val imageTag = "download-url"

                val downloadPath = URI("http://localhost:8272/test-resource/plaintexts/file1").toURL()

                val thrown = assertThrows<InvalidContextException> {
                    manager.createImageWithDefaults(
                        imageName,
                        imageTag,
                        baseDockerfile,
                        contextFromURL = mapOf(
                            Path("file1") to downloadPath,
                        )
                    )
                }

                assertFalse(buildahUtils.imageExists(imageName, imageTag))
                assertInstanceOf(TimeoutCancellationException::class.java, thrown.cause)
            }
        }
    }

    @Test
    fun `Requesting a build with a object store item that takes too long to download should throw an InvalidContextException`() {
        runBlocking {
            closeableScope {
                val imageName = "timeout"
                val imageTag = "download-object-store"

                val fileNamespace = ObjectNamespace("bucket", "file1")
                minioUtils.uploadObject(fileNamespace, plaintext1.toPath())

                val thrown = assertThrows<InvalidContextException> {
                    manager.createImageWithDefaults(
                        imageName,
                        imageTag,
                        baseDockerfile,
                        contextFromObjectStore = mapOf(
                            Path("file1") to fileNamespace,
                        )
                    )
                }

                assertFalse(buildahUtils.imageExists(imageName, imageTag))
                assertInstanceOf(TimeoutCancellationException::class.java, thrown.cause)
            }
        }
    }
}

/**
 * A convenience function to execute [ProvisioningManager.buildImage] with reasonable defaults.
 *
 * @param imageName The name of the image, in the request
 * @param imageTag The tag of the image, in the request
 * @param dockerfile The dockerfile for the image, in the request.
 * @param contextFromArchive Context to be used, via a zip file, in the request
 * @param contextFromURL Context to be used, via URLs, in the request. It is a String, to allow testing URIs that
 * are not syntactically valid.
 * @param contextFromObjectStore Context to be used, via object store namespaces, in the request.
 */
private suspend fun ProvisioningManager.createImageWithDefaults(
    imageName: String = "image-name",
    imageTag: String = "image-tag",
    dockerfile: File,
    contextFromArchive: File? = null,
    contextFromURL: Map<Path, URL> = emptyMap(),
    contextFromObjectStore: Map<Path, ObjectNamespace> = emptyMap(),
) = buildImage(
    BuildRequest(
        imageName,
        imageTag,
        dockerfile,
        if (contextFromArchive != null) MockFileUpload(contextFromArchive) else null,
        contextFromURL,
        contextFromObjectStore,
    )
)
