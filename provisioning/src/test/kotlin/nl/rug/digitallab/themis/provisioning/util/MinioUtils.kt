package nl.rug.digitallab.themis.provisioning.util

import io.minio.*
import io.minio.errors.ErrorResponseException
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.themis.provisioning.namespaces.ObjectNamespace
import java.nio.file.Path
import kotlin.io.path.absolute

/**
 * Helper class for interacting with the MinIO object store, primarily to avoid creating explicit *Args objects
 */
@ApplicationScoped
class MinioUtils {
    @Inject
    private lateinit var minioClient: MinioClient

    /**
     * Creates a bucket, without checking if the bucket exists
     *
     * @param bucket The name of the bucket
     */
    fun makeBucket(bucket: String) {
        val makeBucketArgs = MakeBucketArgs.Builder()
            .bucket(bucket)
            .build()

        minioClient.makeBucket(makeBucketArgs)
    }

    /**
     * Uploads the object at [objectPath] to the object store, at [objectNamespace]
     *
     * @param objectNamespace The namespace corresponding to the final destination the object, in the object store
     * @param objectPath The path to the object to upload
     */
    fun uploadObject(objectNamespace: ObjectNamespace, objectPath: Path) {
        val bucket = objectNamespace.bucket
        try {
            makeBucket(bucket)
        } catch (e: Exception) {
            when (e) {
                is ErrorResponseException -> if (e.errorResponse().code() != "BucketAlreadyOwnedByYou") throw e
            }
        }

        val uploadArgs = UploadObjectArgs.Builder()
            .bucket(bucket)
            .`object`(objectNamespace.path)
            .filename(objectPath.absolute().toString())
            .build()

        minioClient.uploadObject(uploadArgs)
    }
}
