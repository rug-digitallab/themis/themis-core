package nl.rug.digitallab.themis.provisioning.image.builders

import com.google.common.io.Resources.getResource
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.common.kotlin.approvaltests.ApprovalTestSpec
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.withEphemeralDirectory
import nl.rug.digitallab.common.kotlin.helpers.resource.asPath
import nl.rug.digitallab.themis.provisioning.image.builders.commands.buildah.BuildahCommand
import org.apache.commons.codec.digest.DigestUtils
import org.approvaltests.Approvals
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.condition.DisabledOnOs
import org.junit.jupiter.api.condition.OS
import org.junit.platform.commons.annotation.Testable
import java.nio.file.Path
import kotlin.io.path.*
import kotlin.time.Duration.Companion.seconds

@QuarkusTest
@DisabledOnOs(OS.WINDOWS) // Buildah does not work on Windows
class BuildahReproducibilityTest {
    @Inject
    private lateinit var buildah: Buildah

    @TestFactory
    fun `Building images should be reproducible`(): List<DynamicTest> =
        listOf(
            TestCase("base") { buildWithChecksums(getResource("dockerfiles/BaseReproducible").asPath()) },
            TestCase("base-with-context") { buildWithChecksums(getResource("dockerfiles/BaseReproducibleWithContext").asPath()) },
        ).map { testCase ->
            DynamicTest.dynamicTest("Building images should be reproducible for ${testCase.name}") {
                testChecksums(testCase)
            }
        }

    @Testable
    private fun testChecksums(testCase: TestCase) {
        val approvalSpec = ApprovalTestSpec(
            outputDirectory = "approval-tests/reproducible/checksums",
            name = testCase.name,
            attributes = testCase.attributes,
        )

        val checksums = testCase.execute()

        Approvals.verify(
            checksums.entries.sortedBy { it.key }.joinToString("\n") { (path, checksum) ->
                "${path.relativeTo(path.fileSystem.getPath("."))}" + if(checksum != null) " - $checksum" else "/"
            },
            approvalSpec.toOptions(),
        )
    }

    /**
     * Builds the image and returns the checksums of the files in the image.
     *
     * @param dockerfile The dockerfile to build.
     * @param contextLocation The location of the build context.
     *
     * @return The checksums of the files in the image.
     */
    private fun buildWithChecksums(
        dockerfile: Path,
        contextLocation: Path = getResource("contexts/valid").asPath(),
    ): Map<Path, String?> = withEphemeralDirectory { destination ->
        runBlocking {
            buildah.executeBuildah(
                BuildToDirectoryCommand(
                    dockerfile = dockerfile,
                    contextLocation = contextLocation,
                    destination = destination,
                ),
                timeout = 60.seconds,
            )
        }

        return destination.fileChecksums()
    }
}

/**
 * A test case for the reproducibility test.
 *
 * @param name The name of the test case, will become part of the approval test filename.
 * @param attributes The attributes of the test case, will become part of the approval test filename.
 * @param execute The function to execute the test case.
 */
private data class TestCase(
    val name: String,
    val attributes: List<String> = listOf("checksums"),
    val execute: () -> Map<Path, String?>,
)

/**
 * A command to build an image to a directory.
 *
 * @param dockerfile The dockerfile that specifies the build instructions.
 * @param contextLocation The location of the build context.
 * @param destination The location to store the built image.
 *
 * @return The result of the build operation.
 */
private class BuildToDirectoryCommand(
    val dockerfile: Path,
    val contextLocation: Path,
    val destination: Path,
): BuildahCommand<ImageBuilder.CommandResult>() {
    override fun commandList(): List<String> =
        buildList { // buildah build --output type=local,dest=buildA .
            add(EXECUTABLE)
            add("build")
            add("-f")
            add(dockerfile.absolute().toString())
            add("--output")
            add("type=local,dest=${destination.absolute()}")
            add(contextLocation.absolute().toString())
        }

    override fun parseResult(stdOut: String, stdError: String, exitCode: Int): ImageBuilder.CommandResult =
        ImageBuilder.CommandResult(stdOut, stdError, exitCode)
}

/**
 * Returns a map of files and directories, and the checksums of the files. The checksum of directories is `null`.
 *
 * @return The map of files and directories, and the checksums of the files.
 */
@OptIn(ExperimentalPathApi::class)
private fun Path.fileChecksums(): Map<Path, String?> =
    walk(PathWalkOption.INCLUDE_DIRECTORIES)
        .filter { !it.isSymbolicLink() }
        .map { file ->
            file.relativeTo(this) to
                if(file.isRegularFile()) DigestUtils.sha256Hex(file.readBytes()) else null
        }
        .toMap()
