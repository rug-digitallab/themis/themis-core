package nl.rug.digitallab.themis.provisioning.validation.constraints

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import jakarta.validation.Validation
import jakarta.validation.Validator
import nl.rug.digitallab.themis.provisioning.util.assertHasViolation
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory

@QuarkusTest
class OCINamePatternTest {
    @Inject
    private lateinit var validator: Validator

    @TestFactory
    fun `An @OCINamePattern annotated field should be invalid if it uses invalid characters for names`(): List<DynamicTest> {
        return invalidCharacters.map {
            DynamicTest.dynamicTest("An @OCINamePattern annotated field containing '$it' should be invalid") {
                val failingInOnePlace = "inv${it}lid"
                val failingAcrossSeparators = "inv${it}/a/l${it}d"

                val failingInOnePlaceDTO = OCINamePatternTestClass(failingInOnePlace)
                val failingAcrossSeparatorsDTO = OCINamePatternTestClass(failingAcrossSeparators)

                validator.assertHasViolation(
                    failingInOnePlaceDTO,
                    failingInOnePlace,
                    "someString",
                    "May only contain lower case letters or digits, separated by /,.,_,__, or any number of dashes",
                )

                validator.assertHasViolation(
                    failingAcrossSeparatorsDTO,
                    failingAcrossSeparators,
                    "someString",
                    "May only contain lower case letters or digits, separated by /,.,_,__, or any number of dashes",
                )
            }
        }
    }

    @Test
    fun `An @OCINamePattern annotated field should be invalid if it uses capital letters`() {
        val capitalLetters = "CAPITAL_LETTERS"
        val capitalLettersAcrossSeperators = "CAP/it/AL"

        val capitalLettersDTO = OCINamePatternTestClass(capitalLetters)
        val capitalLettersAcrossSeparatorsDTO = OCINamePatternTestClass(capitalLettersAcrossSeperators)

        validator.assertHasViolation(
            capitalLettersDTO,
            capitalLetters,
            "someString",
            "May only contain lower case letters or digits, separated by /,.,_,__, or any number of dashes",
        )

        validator.assertHasViolation(
            capitalLettersAcrossSeparatorsDTO,
            capitalLettersAcrossSeperators,
            "someString",
            "May only contain lower case letters or digits, separated by /,.,_,__, or any number of dashes",
        )
    }

    @TestFactory
    fun `An @OCINamePattern annotated field should be invalid if uses invalid characters for separators`(): List<DynamicTest> {
        return invalidSeparations.map {
            DynamicTest.dynamicTest("An @OCINamePattern annotated field containing '$it' should be invalid") {
                val testDTO = OCINamePatternTestClass(it)

                validator.assertHasViolation(
                    testDTO,
                    it,
                    "someString",
                    "May only contain lower case letters or digits, separated by /,.,_,__, or any number of dashes",
                )
            }
        }
    }

    @TestFactory
    fun `An @OCINamePattern annotated field should be valid when using valid names`(): List<DynamicTest> {
        val validator = Validation.buildDefaultValidatorFactory().validator
        return validNames.map {
            DynamicTest.dynamicTest("An @OCINamePattern annotated field containing '$it' should be valid") {
                val testDTO = OCINamePatternTestClass(it)

                val violations = validator.validate(testDTO)
                assertEquals(0, violations.size)
            }
        }
    }

    val invalidCharacters = listOf(
        '~', '`', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '+', '=', '{', '[', '}', ']', '|', '\\',
        ':', ';', '\'', '"', '<', ',', '>', '?',
    )

    val invalidSeparations = listOf(
        "this is wrong",
        "this;is;wrong",
        "this:is:wrong",
        "in..valid",
        "in___valid",
        "nothing//in/between",
    )

    private val validNames = listOf(
        "flat",
        "nested/path",
        "multi-name-part",
        "long------------sep",
        "mix.sep_er__a-t---------------or",
        "mix.sep_er__a-t---------------or/mix.sep_er__a-t---------------or",
        "many/many/many/many/many/many/many/many/many/many/many/many/many/many/many/path/components",
    )

    data class OCINamePatternTestClass(
        @field:OCINamePattern
        var someString: String,
    )
}
