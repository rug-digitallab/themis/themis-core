package nl.rug.digitallab.themis.provisioning.validation.constraints

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import jakarta.validation.Validator
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.withEphemeralFile
import nl.rug.digitallab.themis.provisioning.util.assertHasViolation
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import java.net.URI
import java.net.URL
import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.absolute

@QuarkusTest
class ValidURLContextTest {
    @Inject
    private lateinit var validator: Validator

    @Test
    fun `A @ValidURLContext annotated field should be invalid if the URL doesn't use http or https`() =
        withEphemeralFile { file ->
            val fileURL = file.absolute().toUri().toURL()
            val fileMap = ValidURLContextTestClass(mapOf(
                Path("") to fileURL
            ))

            validator.assertHasViolation(
                fileMap,
                fileMap.someMap,
                "someMap",
                "The URL needs to use the http or https protocol",
            )

        }

    @TestFactory
    fun `A @ValidURLContext annotated field should be valid if the URL uses http or https`(): List<DynamicTest> {
        return validProtocols.map {
            DynamicTest.dynamicTest("A @ValidURLContext annotated field should be valid if the URL uses $it") {
                val testDTO = ValidURLContextTestClass(mapOf(
                    Path("") to URI("$it://localhost:8080").toURL()
                ))

                val violations = validator.validate(testDTO)
                assertEquals(0, violations.size)
            }
         }
    }

    private val validProtocols = listOf(
        "http",
        "https"
    )

    data class ValidURLContextTestClass(
        @field:ValidURLContext
        val someMap: Map<Path, URL>,
    )
}
