package nl.rug.digitallab.themis.provisioning.util

import jakarta.validation.Validator
import org.junit.jupiter.api.Assertions.assertEquals

/**
 * Helper function to run a validator upon an object that has some violation upon it.
 *
 * @param toValidate The object that will be validated, that contains violations
 * @param failingValue The value that caused the violation
 * @param expectedErrorMessage The expected error message upon triggering the violation.
 */
fun <S, T> Validator.assertHasViolation(
    toValidate: S,
    failingValue: T,
    failingProperty: String,
    expectedErrorMessage: String,
) {
    val violations = this.validate(toValidate)

    assertEquals(1, violations.size)
    violations.iterator().next().apply {
        assertEquals(failingProperty, propertyPath.toString())
        assertEquals(failingValue, invalidValue)
        assertEquals(
            expectedErrorMessage,
            message
        )
    }
}
