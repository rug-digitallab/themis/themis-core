package nl.rug.digitallab.themis.provisioning.util

interface ImageBuilderUtils {
    /**
     * Registers an image to remove from the local registry.
     *
     * @param imageName The name of the image to remove.
     * @param imageTag The tag of the image to remove.
     */
    fun registerToRemove(imageName: String, imageTag: String)

    /**
     * Removes all images that have been registered to remove.
     *
     * @param ignoreExceptions Whether to ignore exceptions that occur during the removal process.
     */
    fun removeImages(ignoreExceptions: Boolean = false)

    /**
     * Get the amount of images in the local repository.
     *
     * @return Rhe number of images that are in the local repository.
     */
    fun numberOfImages(): Int

    /**
     * Check if an image exists
     *
     * @param imageName The name of the image to search for.
     * @param imageTag The tag of the image to search for.
     *
     * @return true if the image specified by the name / tag exists in the local repository.
     */
    fun imageExists(imageName: String, imageTag: String): Boolean
}
