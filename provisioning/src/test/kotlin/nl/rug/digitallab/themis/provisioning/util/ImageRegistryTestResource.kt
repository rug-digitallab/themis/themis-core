package nl.rug.digitallab.themis.provisioning.util

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager
import org.testcontainers.containers.GenericContainer
import org.testcontainers.utility.DockerImageName

/**
 * Lifecycle manager in charge of starting an image registry service.
 */
class ImageRegistryTestResource : QuarkusTestResourceLifecycleManager {
    /**
     * We use the registry image supplied by docker.
     */
    private val registry = GenericContainer(DockerImageName.parse("registry:latest"))

    init {
        // We expose the port that the registry works on
        registry.addExposedPort(5000)
    }

    override fun start(): MutableMap<String, String> {
        registry.start()

        // We update the configuration properties to reflect the nature of the registry
        return mutableMapOf(
            "digital-lab.provisioning.registry-address" to "${registry.host}:${registry.firstMappedPort}",
            "digital-lab.provisioning.registry-is-secure" to "false",
        )
    }

    override fun stop() {
        registry.stop()
    }
}
