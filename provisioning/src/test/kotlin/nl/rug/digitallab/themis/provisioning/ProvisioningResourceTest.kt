package nl.rug.digitallab.themis.provisioning

import io.quarkus.test.InjectMock
import io.quarkus.test.common.WithTestResource
import io.quarkus.test.junit.QuarkusTest
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import io.restassured.response.ValidatableResponse
import jakarta.inject.Inject
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.withEphemeralFile
import nl.rug.digitallab.common.kotlin.helpers.resource.asFile
import nl.rug.digitallab.common.kotlin.helpers.resource.getResource
import nl.rug.digitallab.themis.provisioning.managers.ProvisioningManager
import nl.rug.digitallab.themis.provisioning.namespaces.ObjectNamespace
import nl.rug.digitallab.themis.provisioning.util.ImageRegistryTestResource
import nl.rug.digitallab.themis.provisioning.util.MinioUtils
import org.jboss.resteasy.reactive.RestResponse.StatusCode
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.kotlin.*
import org.mockito.verification.VerificationMode
import java.io.File
import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.absolute
import kotlin.time.Duration.Companion.seconds
import org.hamcrest.Matchers.`is` as Is

@QuarkusTest
@WithTestResource(ImageRegistryTestResource::class)
class ProvisioningResourceTest {
    @Inject
    private lateinit var minioUtils: MinioUtils

    /**
     * We mock the manager so that it will do nothing
     */
    @InjectMock
    private lateinit var manager: ProvisioningManager

    private val baseDockerfile = getResource("dockerfiles/Base").asFile()
    private val sleepingDockerfile = getResource("dockerfiles/Sleep15").asFile()

    private val dockerfileForValidZip = getResource("dockerfiles/ForValidZip").asFile()
    private val validZip = getResource("zips/valid.zip").asFile()
    private val invalidZip = getResource("zips/invalid.zip").asFile()

    private val plaintext1 = getResource("plaintexts/file1").asFile()
    private val plaintext2 = getResource("plaintexts/file2").asFile()

    /**
     * Do nothing when createImage is called.
     */
    @BeforeEach
    fun setupMock(): Unit = runBlocking {
        whenever(manager.buildImage(any())).thenReturn(Unit)
    }

    @AfterEach
    fun resetMocks() {
        reset(manager)
    }

    @Test
    fun `Requesting a build without an image tag should be accepted`() {
        // The default tag is latest
        val imageName = "simple"

        testBuildRequest(
            imageName,
            dockerfile = baseDockerfile,
        )

        runBlocking {
            verifyWithTimeout(manager).buildImage(any())
        }
    }

    @Test
    fun `Requesting a build with just a Dockerfile should be accepted`() {
        val imageName = "simple"
        val imageTag = "default-tag"

        testBuildRequest(
            imageName,
            imageTag,
            dockerfile = baseDockerfile,
        )

        runBlocking {
            verifyWithTimeout(manager).buildImage(any())
        }
    }

    @Test
    fun `Requesting a build with a Dockerfile and some build context specification via an archive should be accepted`() {
        val imageName = "with-context"
        val imageTag = "archive"

        testBuildRequest(
            imageName,
            imageTag,
            dockerfileForValidZip,
            contextFromArchive = validZip,
        )

        runBlocking {
            verifyWithTimeout(manager).buildImage(any())
        }
    }

    @Test
    fun `Requesting a build with a Dockerfile and some build context specification via external files should be accepted`() {
        val imageName = "with-context"
        val imageTag = "urls"

        testBuildRequest(
            imageName,
            imageTag,
            baseDockerfile,
            contextFromURL = mapOf(
                Path("file1") to "http://localhost:8272/test-resource/plaintexts/file1",
                Path("nested/file2") to "http://localhost:8272/test-resource/plaintexts/file1",
            ),
        )
        runBlocking {
            verifyWithTimeout(manager).buildImage(any())
        }
    }

    @Test
    fun `Requesting a build with a Dockerfile and some build context specification via the object store should be accepted`() {
        val imageName = "with-context"
        val imageTag = "object-store"

        val file1Namespace = ObjectNamespace("bucket", "file1")
        val file2Namespace = ObjectNamespace("bucket", "nonRoot/file2")
        minioUtils.uploadObject(file1Namespace, plaintext1.toPath())
        minioUtils.uploadObject(file2Namespace, plaintext2.toPath())

        testBuildRequest(
            imageName,
            imageTag,
            baseDockerfile,
            contextFromObjectStore = mapOf(
                Path("file1") to file1Namespace,
                Path("nested/file2") to file2Namespace,
            ),
        )

        runBlocking {
            verifyWithTimeout(manager).buildImage(any())
        }
    }

    @Test
    fun `Multiple requests can be handled simultaneously`() = runBlocking {
        /**
         * The dockerfile is for a image that will take >5 seconds to build. Each request should be resolved very
         * quickly, so sending 5 of these requests should be resolved within the timeout
         */
        withTimeout(5.seconds) {
            testBuildRequest("image1", "latest", dockerfile = sleepingDockerfile)
            testBuildRequest("image2", "latest", dockerfile = sleepingDockerfile)
            testBuildRequest("image3", "latest", dockerfile = sleepingDockerfile)
            testBuildRequest("image4", "latest", dockerfile = sleepingDockerfile)
            testBuildRequest("image5", "latest", dockerfile = sleepingDockerfile)
        }

        runBlocking {
            verifyWithTimeout(manager, timeout(2000).times(5)).buildImage(any())
        }
    }

    @Test
    fun `Requesting a build that takes too long should be accepted`() {
        val imageName = "timeout"
        val imageTag = "build"

        testBuildRequest(
            imageName,
            imageTag,
            sleepingDockerfile,
        )

        runBlocking {
            verifyWithTimeout(manager).buildImage(any())
        }
    }

    @Test
    fun `Requesting a build with an invalid path to object store should be accepted`() {
        val imageName = "bad-context"
        val imageTag = "object-store"

        testBuildRequest(
            imageName,
            imageTag,
            baseDockerfile,
            contextFromObjectStore = mapOf(
                Path("broken") to ObjectNamespace("doesnt", "exist/here")
            ),
        )

        runBlocking {
            verifyWithTimeout(manager).buildImage(any())
        }
    }

    @Test
    fun `Requesting a build with an invalid archive should be accepted`() {
        val imageName = "bad-context"
        val imageTag = "archive"

        testBuildRequest(
            imageName,
            imageTag,
            baseDockerfile,
            contextFromArchive = invalidZip,
        )

        runBlocking {
            verifyWithTimeout(manager).buildImage(any())
        }
    }

    @Test
    fun `Requesting a build with an unreachable URL to download should be accepted`() {
        val imageName = "bad-context"
        val imageTag = "urls"
        testBuildRequest(
            imageName,
            imageTag,
            baseDockerfile,
            contextFromURL = mapOf(
                Path("broken") to "http://localhost:0"
            ),
        )

        runBlocking {
            verifyWithTimeout(manager).buildImage(any())
        }
    }

    @Test
    fun `Requesting a build without a Dockerfile should receive error code 400`() {
        val imageName = "invalid"
        val imageTag = "no-dockerfile"

        testBuildRequest(
            imageName,
            imageTag,
            expectedStatus = StatusCode.BAD_REQUEST,
        ) {
            body(
                "errorMessage",
                Is("buildImage.buildRequest.dockerfile: The dockerfile form parameter is missing")
            )
        }
    }

    @Test
    fun `Requesting a build without an image name should receive error code 400`() {
        val imageTag = "no-name"

        testBuildRequest(
            imageTag = imageTag,
            dockerfile = baseDockerfile,
            expectedStatus = StatusCode.BAD_REQUEST,
        ) {
            body(
                "errorMessage",
                Is("buildImage.buildRequest.imageName: The imageName query parameter is missing")
            )
        }
    }

    @Test
    fun `Requesting a build with a invalid URL to download should receive error code 400`() {
        val imageName = "bad-context"
        val imageTag = "bad-uri"

        testBuildRequest(
            imageName,
            imageTag,
            baseDockerfile,
            contextFromURL = mapOf(
                Path("broken") to "this|isnt|an|uri"
            ),
            expectedStatus = StatusCode.BAD_REQUEST,
        )
    }

    @Test
    fun `Requesting a build with a URL that is not using the http or https scheme should receive error code 400`() {
        val imageName = "bad-context"
        val imageTag = "uri-schema"

        withEphemeralFile { fileOnService ->
            testBuildRequest(
                imageName,
                imageTag,
                baseDockerfile,
                contextFromURL = mapOf(
                    Path("broken") to "file://${fileOnService.absolute()}"
                ),
                expectedStatus = StatusCode.BAD_REQUEST,
            ) {
                body(
                    "errorMessage",
                    Is("buildImage.buildRequest.contextFromURL: The URL needs to use the http or https protocol")
                )
            }
        }
    }

    @Test
    fun `Requesting a build with an invalid image name should receive error code 400`() {
        val imageName = "NotAValidName"
        val imageTag = "doesntmatter"

        testBuildRequest(
            imageName,
            imageTag,
            baseDockerfile,
            expectedStatus = StatusCode.BAD_REQUEST,
        ) {
            body(
                "errorMessage",
                Is("buildImage.buildRequest.imageName: May only contain lower case letters or digits, separated by /,.,_,__, or any number of dashes")
            )
        }
    }

    @Test
    fun `Requesting a build with an invalid image tag should receive error code 400`() {
        val imageName = "valid-name"
        val imageTag = ".not.a.valid.tag"

        testBuildRequest(
            imageName,
            imageTag,
            baseDockerfile,
            expectedStatus = StatusCode.BAD_REQUEST,
        ) {
            body(
                "errorMessage",
                Is("buildImage.buildRequest.imageTag: May only contain upto 128 letters, numbers, dots, underscores and dashes and at least one. Cannot start with a dot or a dash"),
            )
        }
    }

    /**
     * A convenience function to use rest-assured to post a build request and then check if the expected status is
     * received, as well as any other checks on the message.
     *
     * @param imageName The name of the image, in the request
     * @param imageTag The tag of the image, in the request
     * @param dockerfile The dockerfile for the image, in the request.
     * @param contextFromArchive Context to be used, via a zip file, in the request
     * @param contextFromURL Context to be used, via URLs, in the request. It is a String, to allow testing URIs that
     * are not syntactically valid.
     * @param contextFromObjectStore Context to be used, via object store namespaces, in the request.
     * @param expectedStatus The expected status of the request
     * @param additionalAssertions Any additional assertions about the request to be made.
     */
    private fun testBuildRequest(
        imageName: String? = null,
        imageTag: String? = null,
        dockerfile: File? = null,
        contextFromArchive: File? = null,
        contextFromURL: Map<Path, String> = emptyMap(),
        contextFromObjectStore: Map<Path, ObjectNamespace> = emptyMap(),
        expectedStatus: Int = StatusCode.ACCEPTED,
        additionalAssertions: ValidatableResponse.() -> Unit = {},
    ) {
        Given {
            imageName?.let { queryParam("imageName", imageName) }
            imageTag?.let { queryParam("imageTag", imageTag) }
            dockerfile?.let { multiPart("dockerfile", dockerfile) }
            contextFromArchive?.let { multiPart("contextFromArchive", contextFromArchive) }
            multiPart("contextFromURL", contextFromURL)
            multiPart("contextFromObjectStore", contextFromObjectStore)
        } When {
            post("/api/v1/provisioning/build")
        } Then {
            statusCode(expectedStatus)
            additionalAssertions()
        }
    }

    /**
     * A helper function to verify a mock with a timeout. This is needed for the manager as the request is sent on the event
     * bus, and the manager is called in a different thread. Therefore, we can have a race condition where the manager is
     * called after the test has finished.
     *
     * @param mock The mock to verify
     * @param timeout The timeout to use
     * @return The mock object
     */
    fun <T> verifyWithTimeout(mock: T, timeout: VerificationMode = timeout(2000)): T =
        Mockito.verify(mock, timeout)
}
