package nl.rug.digitallab.themis.provisioning.workers

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

@QuarkusTest
class CoroutineBackgroundWorkerTest {
    @Inject
    private lateinit var worker: CoroutineBackgroundWorker

    @Test
    fun `Enqueueing a task should execute it in the background`() {
        val latch = CountDownLatch(1)

        worker.enqueueTask {
            latch.countDown()
        }

        val completed = latch.await(5, TimeUnit.SECONDS)

        assertTrue(completed, "The task was not executed in the background")
    }

    @Test
    fun `Enqueueing a task that throws an exception should not prevent the next task from executing`() {
        val latch = CountDownLatch(1)

        worker.enqueueTask {
            throw IllegalStateException("This task failed")
        }

        worker.enqueueTask {
            latch.countDown()
        }

        val completed = latch.await(5, TimeUnit.SECONDS)

        assertTrue(completed, "The second task was not executed in the background")
    }
}
